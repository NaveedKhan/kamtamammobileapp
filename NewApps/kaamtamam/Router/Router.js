import React from 'react';
import {Scene, Router, Stack } from 'react-native-router-flux';
import SplashScreen from '../SplashScreen';

const RouterComponent =() => 
{
    return (
        <Router >
            <Stack  key="root">
                <Scene key="SplashScreen" component={SplashScreen} title="Login Section" initial hideNavBar />
           </Stack >
        </Router>
    );
}

export default RouterComponent;