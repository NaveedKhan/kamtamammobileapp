export const ADD_PLACE = 'ADD_PLACE';
export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESSFUL = 'login_successful';
export const LOGIN_USER_FAIL = 'login_fail';
export const LOGIN_USER = 'login_user';