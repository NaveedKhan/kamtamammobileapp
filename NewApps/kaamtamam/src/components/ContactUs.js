import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Card,
  Left,
  Right,
  Body,
  Title,
  Button,
  CardItem,
  Text,
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import {CustomHeaderWithText} from './Grocessry/Common/Header';
import { Alert,TouchableOpacity, Linking } from 'react-native';

export default class ContactUs extends Component {
  openFacebookPage(url)
  {
    if(url != null && url != '')
    {
        Linking.openURL(url)
    }
    
  }

  callUs(phone) {
    debugger;
    //let phone = '033323233232';
    console.log('callNumber ----> ', '033323233232');
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  }

  render() {
    return (
      <Container style={{flex: 1, backgroundColor: 'transparent'}}>
        <CustomHeaderWithText text="Contact Us" />
        <Content style={{backgroundColor: 'transparent',margin:5}}>
          <Card style={{backgroundColor: 'transparent',margin:5}}>
           
            <CardItem style={{backgroundColor: 'transparent'}} >
              <TouchableOpacity style={{flexDirection:'row',margin:5}}
              onPress={()=> this.openFacebookPage('http://www.facebook.com/kaamtamampk')}
              >
              <Icon type="font-awesome"  name="facebook" color="orange"  style={{alignSelf:'center',width:40}}/>
              <Text style={{fontSize: 13, marginLeft: 15, color: '#000'}}>
                www.facebook.com/kaamtamampk
              </Text>
              </TouchableOpacity>
            </CardItem>

            <CardItem style={{backgroundColor: 'transparent'}}>
            <TouchableOpacity style={{flexDirection:'row',margin:5}}>
              <Icon type="AntDesign"  name="mail" color="orange" style={{alignSelf:'center',width:40}} />
              <Text style={{fontSize: 13, marginLeft: 15, color: '#000'}}>              
                info@kaamtamam.com.pk
              </Text> 
              </TouchableOpacity>          
            </CardItem>

            <CardItem style={{backgroundColor: 'transparent'}}>
            <TouchableOpacity style={{flexDirection:'row',margin:5}}
            onPress={()=>this.callUs('0915701684')}
            >
              <Icon type="AntDesign"  name="phone" color="orange" style={{alignSelf:'center',width:40}}/>
              <Text style={{fontSize: 13, marginLeft: 15, color: '#000'}}>
                091 5701684
              </Text>
              </TouchableOpacity>
            </CardItem>

            <CardItem style={{backgroundColor: 'transparent'}}>
            <TouchableOpacity style={{flexDirection:'row',margin:5}}
             onPress={()=>this.callUs('03110605352')}
            >
              <Icon type="font-awesome"  name="mobile-phone" color="orange" style={{alignSelf:'center',width:40}}/>
              <Text style={{fontSize: 13, marginLeft: 15, color: '#000'}}>
                0311 0605352
              </Text>
              </TouchableOpacity>
            </CardItem>

            <CardItem style={{backgroundColor: 'transparent'}}>
            <TouchableOpacity style={{flexDirection:'row',margin:5}}
            onPress={()=> this.openFacebookPage('http://www.kaamtamam.com.pk')}
            >
              <Icon type="font-awesome"  name="book" color="orange" style={{alignSelf:'center',width:40}} />
              <Text style={{fontSize: 13, marginLeft: 15, color: '#000'}}>
                www.kaamtamam.com.pk
              </Text>
              </TouchableOpacity>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
