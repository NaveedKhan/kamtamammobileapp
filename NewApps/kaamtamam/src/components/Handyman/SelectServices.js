import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { CustomHeaderWithText } from '../Grocessry/Common/Header';
import Carousel from '../Grocessry/Carousel';
import SelectServiceGrid from './SelectServiceGrid';
import CustomFooter from '../common/CustomFooter';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const RenderButton = ({ icon, name, onPress }) => {
  return (
    <TouchableOpacity
      style={{
        backgroundColor: 'white',
        width: width * 0.5,
        height: height * 0.2,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 15,
      }}
      onPress={onPress}>
      <Icon name={icon} style={{ fontSize: 40, color: '#f48004' }} />
      <Text
        style={{
          fontSize: 20,
          color: '#f48004',
          fontWeight: 'bold',
          fontFamily: 'sens-serif',
        }}>
        {name}
      </Text>
    </TouchableOpacity>
  );
};

class SelectServices extends Component {
  constructor(props) {
    super(props);
    debugger;
  }
  
  render() {
    const { ServiceTypes } = this.props;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#fff',

        }}>
        <CustomHeaderWithText text="Select Service" />
        <ScrollView scrollEnabled>
          {/* <View style={{ height: 200, backgroundColor: 'transparent' }}>
            <Carousel />

          </View> */}
          <View style={{ flex: 1, backgroundColor: 'gray' }}>

            <SelectServiceGrid ServiceTypes={ServiceTypes} />
          </View>
        </ScrollView>
        <CustomFooter landingTo="H" />
      </View>
    );
  }
}
export default SelectServices;
