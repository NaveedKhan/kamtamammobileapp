import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Image,
  Modal,
  TouchableHighlight,
  ScrollView,
  Alert,
  Linking,
  Platform,
} from 'react-native';
import {Item, Input, Icon, Spinner, Toast} from 'native-base';
import {Rating, AirbnbRating} from 'react-native-elements';
import {CustomHeaderWithText} from '../../Grocessry/Common/Header';
import {Actions} from 'react-native-router-flux';
import {UpdateNotificationData} from '../../../actions';
import moment from 'moment';
import axios from 'axios';
import {connect} from 'react-redux';
import {config} from '../../../config';
//import {CustomFooterTab} from '../../Grocessry/Common/Footer';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class ServicesHistoryCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      reviewComment: '',
      reviewScore: 3,
      modalButtonLoading: false,
      selectedOrderToComplete: null,
      reviewModalVisible: false
    };
    this.renderRow = this.renderRow.bind(this);
  }

  timeBetweenDates(toDate) {
    debugger;
    var dateEntered = toDate;
    var now = new Date();
    var difference = now.getTime() - dateEntered.getTime();

    if (difference <= 0) {
      // Timer done
      //clearInterval(timer);
    } else {
      var seconds = Math.floor(difference / 1000);
      var minutes = Math.floor(seconds / 60);
      var hours = Math.floor(minutes / 60);
      var days = Math.floor(hours / 24);

      hours %= 24;
      minutes %= 60;
      seconds %= 60;

      if (hours > 0 || days > 0 || minutes > 5) {
        return true;
      } else {
        return false;
      }
      // this.setState({ countDownTime: days + 'D ' + hours + 'H ' + minutes + 'M ' + seconds + 'S' });
    }
  }

  renderRow(iconName, headingTitle, headingValue, detail) {
    return (
      <View style={styles.rowMain}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            //alignSelf: 'center',
            //  backgroundColor:'green',
            flex: 1,
          }}>
          <Icon name={iconName} style={styles.rowIcon} />
          <Text style={styles.rowHeadingList}>{headingTitle}:</Text>
          <Text style={styles.listItemText}>{headingValue}</Text>
        </View>
        {detail && (
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <Text style={{color: 'grey', paddingRight: 5}}>Details</Text>
            <Icon name="arrow-forward" style={{fontSize: 16, color: 'grey'}} />
          </TouchableOpacity>
        )}
      </View>
    );
  }

  ratingCompleted(rating) {
    this.setState({reviewScore: rating});
    console.log('Rating is: ' + rating);
  }

  submitReview() {
    // this.setState({ modalVisible: false })
    if (this.state.iscanceling) {
      this.senpApiCallToMarkCancel();
    } else {
      this.senpApiCallToMarkComplete();
    }
    //this.senpApiCallToMarkComplete();
  }

  senpApiCallToMarkCancel() {
    debugger;
     const { user ,DeviceID} = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
      ServiceRequestID: selectedOrderToComplete.serviceRequestID,
      IsMarkFinishedByUser: true,
      DeviceID:DeviceID,
      Reviews: {
        UserReview: reviewComment,
        UserReviewScore: reviewScore,
        UserID: selectedOrderToComplete.requestById,
        ServiceRequestID: selectedOrderToComplete.serviceRequestID,
        IsReviewedByUser: true,
      },
    };
    const {roleName, fullName, phoneNumber} = user; 
    let {GuestUserData} = this.props;
    numberArray = [];
    if(roleName == 'Guest')
    {

        GuestUserData.forEach(function(el, index) {
          numberArray.push(el.phoneNumber);
        });
        reviewModel.PhoneNumbers = [];//numberArray;
    }
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({modalButtonLoading: true});
    const {REST_API} = config;
    try {
      const response = axios
        .post(
          REST_API.ServiceRequest.CancelServiceRequestByUser,
          reviewModel,
          configs,
        )
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          debugger;
          console.log('api call' + response);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });

          me.props.UpdateNotificationData({
            prop: 'unReadNotificationCount',
            value: baseModel.data.unReadnotificationCount,
          });
          me.props.UpdateNotificationData({
            prop: 'totalpendingOrdercount',
            value: baseModel.data.totalpendingOrdercount,
          });
          Actions.reset('SelectJob');
          //Actions.tendersResult({TendersList:baseModel.data});
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({
        isLoading: false,
        modalButtonLoading: false,
        modalVisible: false,
      });
      //productFetchingFail(dispatch);
    }
  }

  senpApiCallToMarkComplete() {
    debugger;
    // const { Order } = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
      ServiceRequestID: selectedOrderToComplete.serviceRequestID,
      IsMarkFinishedByUser: true,
      Reviews: {
        UserReview: reviewComment,
        UserReviewScore: reviewScore,
        UserID: selectedOrderToComplete.requestById,
        ServiceRequestID: selectedOrderToComplete.serviceRequestID,
        IsReviewedByUser: true,
      },
    };
    // const { user } = this.props;
    // let id = user.id;
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({modalButtonLoading: true});
    const {REST_API} = config;
    try {
      const response = axios
        .post(
          REST_API.ServiceRequest.MarkServiceRequestCompleteByUser,
          reviewModel,
          configs,
        )
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          debugger;
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });

          me.props.UpdateNotificationData({
            prop: 'unReadNotificationCount',
            value: baseModel.data.unReadnotificationCount,
          });
          me.props.UpdateNotificationData({
            prop: 'totalpendingOrdercount',
            value: baseModel.data.totalpendingOrdercount,
          });
          Actions.reset('SelectJob');
          //this.setState({ modalVisible: false })
          // Actions.reset('SelectJob');
          //Actions.tendersResult({TendersList:baseModel.data});
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({
        isLoading: false,
        modalButtonLoading: false,
        modalVisible: false,
      });
      //productFetchingFail(dispatch);
    }
  }

  renderRatingModal() {
    debugger;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({modalVisible: false, modalButtonLoading: false});
        }}>
        <View style={styles.datecontainer}>
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              borderRadius: 15,
              justifyContent: 'flex-end',
              alignItems: 'center',
              alignSelf: 'flex-end',
              margin: 10,
              borderColor: '#fff',
              borderWidth: 1,
            }}
            onPress={() => this.setState({modalVisible: false})}>
            <Icon name="close" style={{color: '#fff'}} />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 15,
              margin: 5,
              alignSelf: 'center',
              color: 'white',
            }}>
            Please Add Review
          </Text>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 5,
            }}>
            <AirbnbRating
              count={5}
              reviews={[
                'Terrible (1)',
                'Bad (2)',
                'Normal (3)',
                'GOOD (4)',
                'Outstanding (5)',
              ]}
              defaultRating={3}
              onFinishRating={rat => this.ratingCompleted(rat)}
              size={40}
            />
          </View>

          <View
            style={{
              margin: 10,
              backgroundColor: '#fff',
              borderColor: 'gray',
              borderWidth: 1,
              padding: 5,
              borderRadius: 5,
            }}>
            <TextInput
              onChangeText={text => this.setState({reviewComment: text})}
              placeholder="Add any comment (optional but recomended)"
              placeholderTextColor="grey"
              maxLength={200}
              multiline={true}
              editable
              numberOfLines={5}
              style={{height: 150, justifyContent: 'flex-start'}}
            />
          </View>
          {this.state.modalButtonLoading ? (
            <Spinner />
          ) : (
            <TouchableOpacity
              onPress={() => this.submitReview()}
              style={{
                width: 250,
                height: 50,
                borderRadius: 30,
                margin: 10,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                borderColor: '#f48004',
                borderWidth: 1.7,
              }}>
              <Text style={{fontSize: 15, color: '#fff', fontWeight: 'bold'}}>
                {' '}
                SUBMIT NOW{' '}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </Modal>
    );
  }

  senpApiCallToAddUserReview() {
    debugger;
     let { services } = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
        UserReview: reviewComment,
        UserReviewScore: reviewScore,
        UserID: selectedOrderToComplete.requestById,
        ServiceRequestID: selectedOrderToComplete.serviceRequestID,
        IsReviewedByUser: true,
    };
    // const { user } = this.props;
    // let id = user.id;
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({modalButtonLoading: true});
    const {REST_API} = config;
    try {
      const response = axios
        .post(REST_API.ServiceRequest.AddUserReviewForService, reviewModel, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          debugger;
          console.log('api call' + response);
          var selectedorder = services.find(aa=> aa.serviceRequestID == selectedOrderToComplete.serviceRequestID);
          if(selectedorder!= null)
          {
           let {reviews} = selectedorder;
           if (reviews) {
             selectedorder.reviews.userReview= reviewComment;
             selectedorder.reviews.userReviewScore= reviewScore;
             selectedorder.reviews.userID= selectedOrderToComplete.orderByUserID;
             selectedorder.reviews.serviceRequestID= selectedOrderToComplete.serviceRequestID;
             selectedorder.reviews.isReviewedByUser= true;
           }
          // me.props.Order = Order;
          }

          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            reviewModalVisible: false,
          });

          Alert.alert('Confirmation','Dear valued customer thank you so much for your review.Your review is recorded and will be used to improve our service further!');
          debugger;       
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          // me.setState({ isLoading: true });
          Alert.alert('error', err);
          console.log('error', err);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            reviewModalVisible: false,
          });
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      // me.setState({ isLoading: true });
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({
        isLoading: false,
        modalButtonLoading: false,
        modalVisible: false,
      });
      //productFetchingFail(dispatch);
    }
  }

  renderReviewModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.reviewModalVisible}
        onRequestClose={() => {
          this.setState({reviewModalVisible: false, modalButtonLoading: false});
        }}>
        <View style={styles.datecontainer}>
          {/* <View style={{ margin: 10 }} /> */}
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              borderRadius: 15,
              justifyContent: 'flex-end',
              alignItems: 'center',
              alignSelf: 'flex-end',
              margin: 10,
              borderColor: '#fff',
              borderWidth: 1,
            }}
            onPress={() => this.setState({reviewModalVisible: false})}>
            <Icon name="close" style={{color: '#fff'}} />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 15,
              margin: 5,
              alignSelf: 'center',
              color: 'white',
            }}>
            How was our serive? Please add review.
          </Text>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 5,
            }}>
            <AirbnbRating
              count={5}
              reviews={[
                'Terrible (1)',
                'Bad (2)',
                'Normal (3)',
                'GOOD (4)',
                'Outstanding (5)',
              ]}
              defaultRating={3}
              onFinishRating={rat => this.ratingCompleted(rat)}
              size={40}
            />
          </View>

          <View
            style={{
              margin: 10,
              backgroundColor: '#fff',
              borderColor: 'gray',
              borderWidth: 1,
              padding: 5,
              borderRadius: 5,
            }}>
            <TextInput
              onChangeText={text => this.setState({reviewComment: text})}
              placeholder="Add any comment (optional but recomended)"
              placeholderTextColor="grey"
              maxLength={200}
              multiline={true}
              editable
              numberOfLines={5}
              style={{height: 150, justifyContent: 'flex-start'}}
            />
          </View>
          {this.state.modalButtonLoading ? (
            <Spinner />
          ) : (
            <TouchableOpacity
              onPress={() => {
                //this.setState({reviewModalVisible:false});
                this.senpApiCallToAddUserReview();
              }}
              style={{
                width: 250,
                height: 50,
                borderRadius: 30,
                margin: 10,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                borderColor: '#fff',
                borderWidth: 1.7,
              }}>
              <Text style={{fontSize: 15, color: '#fff', fontWeight: 'bold'}}>
                {' '}
                SUBMIT NOW{' '}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </Modal>
    );
  }


  showToast2() {
    return Toast.show({
      text: 'Order already marked completed!',
      buttonText: 'Okay',
      type: 'danger',
      duration: 3000,
    });
  }

  OnpressComplete(el, orderStatus) {
    debugger;
    if (
      orderStatus.toLowerCase() == 'Delivered'.toLowerCase() ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      this.showToast2();
    } else {
      debugger;
      this.ShowConfimrationAlert(
        el,
        'Are You sure your request is fulfilled and you want to close  it.',
      );
    }
  }

  markCancel(el) {
    debugger;
    if (
      el.orderStatus == 'Delivered' ||
      el.orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      this.showToast2();
    } else {
      let isexpire =
        el.orderStatus.toLowerCase() == 'initiated'.toLowerCase()
          ? false
          : this.timeBetweenDates(new Date(el.serviceAssignedToDriverTechDate));
      if (el.orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
        this.ShowConfimrationAlertForCancel(
          el,
          'Are you sure you really want to cancel the order',
        );
      } else {
        if (isexpire) {
          Alert.alert(
            'alert',
            'Please contact our help line or delivery guy to cancel the order.You canot cancel the order after 5 min',
          );
        } else {
          this.ShowConfimrationAlertForCancel(
            el,
            'Are you sure you really want to cancel the order',
          );
        }
      }
    }
  }

  markCancel(el, orderStatus) {
    debugger;
    if (
      orderStatus.toLowerCase() == 'Delivered'.toLowerCase() ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      this.showToast2();
    } else {
      let isexpire =
        orderStatus.toLowerCase() == 'initiated'.toLowerCase()
          ? false
          : this.timeBetweenDates(new Date(el.serviceAssignedToDriverTechDate));
      if (orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
        this.ShowConfimrationAlertForCancel(
          el,
          'Are you sure you really want to cancel the order',
        );
      } else {
        if (isexpire) {
          Alert.alert(
            'alert',
            'Please contact our help line or delivery guy to cancel the order.You canot cancel the order after 5 min',
          );
        } else {
          this.ShowConfimrationAlertForCancel(
            el,
            'Are you sure you really want to cancel the order',
          );
        }
      }
      //this.ShowConfimrationAlertForCancel(el, 'Are you sure you really want to cancel the order');
    }
  }

  ShowConfimrationAlertForCancel(el, message) {
    Alert.alert(
      'Confirmation!',
      message,
      [
        {
          text: 'Yes Continue',
          onPress: () => {
            //checkoutSucess(dispatch,baseModel.data)
            this.setState({selectedOrderToComplete: el});
            this.setState({iscanceling: true, modalVisible: true});
            //    this.senpApiCallToMarkCancel(el)
          },
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  ShowConfimrationAlert(el, message) {
    debugger;
    Alert.alert(
      'Confirmation!',
      'Are You sure your request is fulfilled and you want to close the it.',
      [
        {
          text: 'Yes,Continue',
          onPress: () => {
            //checkoutSucess(dispatch,baseModel.data)
            this.setState({selectedOrderToComplete: el});
            this.setState({iscanceling: false, modalVisible: true});
            //this.setState({ modalVisible: true })
          },
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  _renderUserReviews(score) {
    return (
      <View
        style={{
          // flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}>
        <Rating
          // showRating
          ratingCount={5}
          readonly
          // reviews={["Terrible (1)", "Bad (2)", "Normal (3)", "GOOD (4)", "Outstanding (5)"]}
          startingValue={score}
          //onFinishRating={(rat) => this.ratingCompleted(rat)}
          size={10}
        />
      </View>
    );
  }

  _renderWorkerReviews(score,isReviewedByUser,el) {
    return (
      <View
        style={{
          // flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}>
           {isReviewedByUser ? <View/> :   <TouchableOpacity
              onPress={() => this.setState({reviewModalVisible:true,selectedOrderToComplete:el})}
              style={[styles.button, {marginLeft: 5, width: 150}]}>
              <Text style={styles.buttonText}> Add Your Review </Text>
            </TouchableOpacity> }
        <Rating
          // showRating
          ratingCount={5}
          readonly
          // reviews={["Terrible (1)", "Bad (2)", "Normal (3)", "GOOD (4)", "Outstanding (5)"]}
          startingValue={score}
          //onFinishRating={(rat) => this.ratingCompleted(rat)}
          size={10}
        />      
      </View>
    );
  }

  _renderbothReviews(userscore, driverscore) {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 10,
          marginBottom: 20,
          padding: 5,
        }}>
        <Text>Your Review</Text>
        <Rating ratingCount={5} readonly startingValue={userscore} size={10} />
        <Text style={{marginTop: 5}}>Kaam Tamam Team Review</Text>
        <Rating
          ratingCount={5}
          readonly
          startingValue={driverscore}
          size={10}
        />
      </View>
    );
  }

  callTechnician(phone) {
    //let phone = '033323233232';
    console.log('callNumber ----> ', '033323233232');
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  }

  _renderButtons(orderStatus, isexpire, el) {
    if (
      orderStatus == 'Delivered' ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      let {reviews} = el;
      if (reviews) {
        let {
          userReviewScore,
          driverReviewScore,
          isReviewedByUser,
          isReviewedByDriverTech,
        } = reviews;

        var reviewbuttonstyle = {
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        };
        if (isReviewedByUser && !isReviewedByDriverTech) {
          return this._renderUserReviews(userReviewScore);
        } else if (!isReviewedByUser && isReviewedByDriverTech) {
          return this._renderWorkerReviews(driverReviewScore,isReviewedByUser,el);
        } else if (isReviewedByUser && isReviewedByDriverTech) {
          return this._renderbothReviews(userReviewScore, driverReviewScore);
        }
      }
    } else {
      if (orderStatus.toLowerCase() == 'initiated') {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() => this.markCancel(el, orderStatus)}
              style={[styles.button, {marginLeft: 5, width: 150}]}>
              <Text style={styles.buttonText}> Cancel Order</Text>
            </TouchableOpacity>
          </View>
        );
      } else {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() =>
                Actions.traceOrder({
                  requestAddressLat: parseFloat(el.requestAddressLat),
                  requestAddressLong: parseFloat(el.requestAddressLong),
                  Order: el,
                })
              }
              style={[styles.button, {marginLeft: 5, width: 150}]}>
              <Text style={styles.buttonText}> Track Order</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.callTechnician(el.assigneContactNo)}
              style={[styles.button, {marginLeft: 5, width: 150}]}>
              <Text style={styles.buttonText}> Call Technician</Text>
            </TouchableOpacity>
          </View>
        );
      }
    }
  }

  _renderBottomText(orderStatus, isexpire, el) {
    debugger;
    if (
      orderStatus.toLowerCase() == 'Delivered'.toLowerCase() ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      let {reviews} = el;
      let reviewstatis = '';
      if(reviews)
      {
         reviewstatis =
        reviews.isReviewedByUser && !reviews.isReviewedByDriverTech
          ? 'You have given the review right now'
          : !reviews.isReviewedByUser && reviews.isReviewedByDriverTech
          ? 'Kaam Tamam team has added the review'
          : !reviews.isReviewedByUser && !reviews.isReviewedByDriverTech
          ? 'No Review Added Yet'
          : 'Kaam Tamam and you have added the review';
      }
      
      return (
        <Text
          style={{color: 'red', alignSelf: 'center', margin: 5, fontSize: 13}}>
          {reviewstatis ? reviewstatis : ''}
        </Text>
      );
    } else {
      if (orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
        return (
          <Text
            style={{
              color: 'red',
              alignSelf: 'center',
              margin: 5,
              fontSize: 12,
              justifyContent: 'center',
              textAlign: 'center',
            }}>
            You can Cancel the Request until it is assign to worker
          </Text>
        );
      } else {
        return (
          <Text
            style={{
              color: 'red',
              alignSelf: 'center',
              margin: 5,
              fontSize: 12,
              justifyContent: 'center',
              textAlign: 'center',
            }}>
            You can Cancel the Request until it is assign to worker
          </Text>
        );
      }
    }
  }

  render() {
    const {services} = this.props;
    debugger;
    return (
      <View>
        {services.map(el => {
          const urdgency = el.isServiceSchedule
            ? 'Scheduled At: '
            : 'Request Urgency';
          // let isexpire = this.timeBetweenDates(new Date(el.serviceRequestDate));
          let isexpire =
            el.serviceStatusName.toLowerCase() == 'initiated'.toLowerCase()
              ? false
              : this.timeBetweenDates(
                  new Date(el.serviceAssignedToDriverTechDate),
                );
          return (
            <TouchableOpacity
              key={el.serviceRequestID}
              style={styles.StatusBorder}
              onPress={() => Actions.serviceDetails({selectedService: el})}>
              {this.renderRow(
                (iconName = 'paper'),
                (headingTitle = el.serviceTypeName + '  Request No'),
                (headingValue = el.serviceRequestID),
              )}

              {this.renderRow(
                (iconName = 'clock'),
                (headingTitle = 'Request Time'),
                (headingValue = moment(el.serviceRequestDate).format(
                  'MMMM Do YYYY, h:mm:ss a',
                )),
                (detail = false),
              )}

              {this.renderRow(
                (iconName = 'md-checkmark-circle-outline'),
                (headingTitle = 'Status'),
                (headingValue = el.serviceStatusName),
              )}

              {el.serviceStatusName.toLowerCase() == 'initiated' ? (
                <View />
              ) : (
                this.renderRow(
                  (iconName = 'contact'),
                  (headingTitle = 'Worker Name'),
                  (headingValue =
                    el.assigneeName == null
                      ? 'Not assigned to worker'
                      : el.assigneeName),
                )
              )}

              {this.renderRow(
                (iconName = 'contact'),
                (headingTitle = urdgency),
                (headingValue = el.isServiceSchedule
                  ? moment(el.serviceRequestScheduleDate).format(
                      'MMMM Do YYYY, h:mm:ss a',
                    )
                  : 'urgent order'),
              )}

              {this.renderRow(
                (iconName = 'construct'),
                (headingTitle = 'Total Chargea'),
                (headingValue = `Rs. ${el.serviceTotalCharges}`),
              )}

              {this._renderButtons(el.serviceStatusName, isexpire, el)}

              {this._renderBottomText(el.serviceStatusName, isexpire, el)}
            </TouchableOpacity>
          );
        })}
        {this.renderRatingModal()}
        {this.renderReviewModal()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 30,
  },
  datecontainer: {
    backgroundColor: 'green', //'#E9EBEE',
    borderRadius: 10,
    margin: 10,
    marginTop: 100,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  StatusBorder: {
    borderWidth: 3,
    borderColor: '#f48004',
    width: width * 0.9,
    // height: height * 0.68,
    margin: 10,
    borderRadius: 10,
  },
  listItemText: {
    fontSize: 12,
    color: 'green',
    fontFamily: 'sens-serif',
  },
  rowMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: width * 0.8,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  rowIcon: {
    color: '#f48004',
    margin: 10,
    marginRight: 20,
    marginBottom: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    margin: 20,
    marginRight: 10,
    marginLeft: 0,
    fontSize: 13,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  button: {
    marginTop: 3,
    marginLeft: 5,
    marginBottom: 5,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    padding: 7,
    borderRadius: 25,
    borderWidth: 1.5,
    borderColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 13,
    color: 'black',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

//export default ServicesHistoryCard;
const mapStateToProps = ({auth}) => {
  const {GuestUserData,DeviceID}= auth;
  return {user: auth.user,GuestUserData,DeviceID};
};
export default connect(
  mapStateToProps,
  {UpdateNotificationData},
)(ServicesHistoryCard);
