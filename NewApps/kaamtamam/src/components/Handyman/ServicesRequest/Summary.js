import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import {Item, Input, Icon} from 'native-base';
import {CustomHeaderWithText} from '../../Grocessry/Common/Header';
import CustomFooter from '../../common/CustomFooter';
const images = require('../../Grocessry/Asset/homeButton.png');
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class Summary extends Component {
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
  }
  renderRow(iconName, headingTitle, headingValue, detail) {
    return (
      <View style={styles.rowMain}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <Icon name={iconName} style={styles.rowIcon} />
          <Text style={styles.rowHeadingList}>{headingTitle}:</Text>
          <Text style={styles.listItemText}>{headingValue}</Text>
        </View>
      </View>
    );
  }
  render() {
    debugger;
    const {loading} = this.props;
    return (
      <View style={{flex: 1}}>
        <CustomHeaderWithText text="PLUMBER REQUEST" />
        <ScrollView scrollEnabled>
          <View style={{alignSelf: 'center', margin: 10}}>
            <Text style={styles.recentJobHeading}>SUMMARY</Text>
            <View style={styles.StatusBorder}>
              {this.renderRow((iconName = 'construct'), (headingTitle = 'Type of job'), (headingValue = 'Maintainance'))}
              {this.renderRow((iconName = 'clock'),(headingTitle = 'Job Time'),(headingValue = '11/12/19 - 12:05PM'))}
              {this.renderRow((iconName = 'pin'),(headingTitle = 'Address'),(headingValue = 'Current location'),)}
              {this.renderRow((iconName = 'document'), (headingTitle = 'Discription'),(headingValue = 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print'),
              )}
              {this.renderRow((iconName = 'md-images'),(headingTitle = 'Images'),)}
              {this.renderRow((iconName = 'pin'), (headingTitle = 'Location'))}
            </View>
          </View>
          <View style={styles.buttonMain}>
            <TouchableOpacity style={styles.button}>
              <Text style={styles.buttonText}>Confirm</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <CustomFooter/>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 30,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  StatusBorder: {
    borderWidth: 3,
    borderColor: 'orange',
    width: width * 0.9,
    height: height * 0.75,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#E9EBEE',
  },
  listItemText: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
    width: width * 0.5,
  },
  rowMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: width * 0.85,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  rowIcon: {
    color: '#f48004',
    margin: 20,
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    margin: 20,
    marginRight: 10,
    marginLeft: 10,
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
    marginBottom: 10,
  },
  button: {
    backgroundColor: '#f48004',
    padding: 10,
    borderRadius: 20,
    width: width * 0.35,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

export default Summary;
