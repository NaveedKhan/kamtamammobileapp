import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,Alert,Modal,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Item, Input, Icon, Spinner, List, ListItem, Left, CheckBox, Right, Thumbnail, Body, Button, avatar } from 'native-base';
import { CustomHeaderWithText } from '../../Grocessry/Common/Header';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import firebase from 'firebase';
import moment from "moment";
import { ServiceRequestUpdateForm, addServiceRequest } from '../../../actions';
import FormValidator from '../../HelperClasses/FormValidator';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class RequestDetail extends Component {
  constructor(props) {
    super(props);
    debugger;
    this.state = {
      currentDate: new Date(),
      markedDate: moment(new Date()),
      firebaseLoading: false,
      requesteename: '',
      requesteephone: ''
    };
  }

  storeData = async (key, value) => {
    debugger;
   try {
     await AsyncStorage.setItem(key,JSON.stringify(value) );
   } catch (e) {
       debugger;
     // saving error
   }
 };

  validateDataFirst() {
    const { roleName, fullName, phoneNumber } = this.props.user;
    const { requesteename, requesteephone } = this.state;
    var arrayerr = [];
    if (roleName == 'Guest') {

        if (requesteename == '' || requesteename == null) {
            arrayerr.push('Please enter  fullname');
        } else {
            var result = FormValidator.NameFieldValidation(requesteename);
            if (!result) {
                arrayerr.push('Please enter valid fullname');
            }
        }

        if (requesteephone == '' || requesteephone == null) {
            arrayerr.push('Please enter  phone');
        } else {
            var result = FormValidator.validatePhoneNumber(requesteephone);
            if (!result) {
                arrayerr.push('Please enter valid phone');
            }
        }
        if (arrayerr.length > 0) {
            Alert.alert('Invalid Data', arrayerr.toString())
        } else {
            this.addDataIntoFirebase()
        }

    } else {
        this.addDataIntoFirebase()
    }
}

  addDataIntoFirebase = () => {
    debugger;
    // const { Order } = this.props;
  //this.onConfirm('key');
  //  return;
    var UserModel = {
      userLat: 33.9976884, OrderId: 0,
      ServiceID: 0,
      userLong: 71.4695614,
    }
    // userCurrentLat: 33.9976884, userCurrentLong: 71.4695614, 
    var me = this;

    this.setState({ firebaseLoading: true });
    const dbref = firebase.database().ref('DriverLocation');
    var newLocref = dbref.push();
    var key = newLocref.getKey();
    newLocref.set(UserModel)
      .then(() => {
        debugger;
        me.setState({ firebaseLoading: false });
        me.onConfirm(key);

      })
      .catch((error) => {
        debugger;
        var err= String(error) == '' ? 'no error ' : String(error);
        Alert.alert('error',err);
        // me.setState({ isLoading: false });
        debugger;
        Alert.alert('alert!', 'something went wrong!');
        //  orderAssignedFail(dispatch, error);
        //me.showToast("an error occured, try again");
      });
  }

  componentDidMount() {
    //this.storeData('userData', JSON.stringify(SigninModel));
    const {roleName, fullName, phoneNumber} = this.props.user;
    // const {requesteename, requesteephone} = this.state;
    // var arrayerr = [];
    if (roleName == 'Guest') 
    {
        let {GuestUserData} = this.props;
        if(GuestUserData.length>0)
        {
            let firstelement = GuestUserData[0];
            let name = firstelement.name;
            let phone = firstelement.phoneNumber;
            this.setState({requesteename:name,requesteephone:phone});
        }
    }
  }

  swapButtonAndSpinner() {

    return this.props.loading ?
      <Spinner />
      :
      <TouchableOpacity style={{
        width: 250,
        height: 50,
        borderRadius: 30,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        borderColor: '#f48004',
        borderWidth: 1.7
      }} onPress={() => this.validateDataFirst()}>
        <Text style={{ fontSize: 15, color: '#f48004', fontWeight: 'bold' }} > SUBMIT REQUEST NOW </Text>
      </TouchableOpacity>
  }


  onConfirm(key) {
    debugger;
    const {
      serviceRequestModel, error, loading, RequestServiceType, RequestTime,
      RequestServicesList, RequestImages, RequestDescription, RequestAddress,
      maintainanceSelected, ServiceRequestScheduleDateString,
      installationSelected, isImmediateRequest, user, selectedServies,
      serviceRequestScheduleDate, imagesourcearray, handyServiceCharges, LocationTrackingID,DeviceTocken,DeviceID
    } = this.props;


    const { roleName, fullName, phoneNumber,id } = user;
    const { requesteename, requesteephone } = this.state;

    let {GuestUserData} = this.props;
    if(roleName == 'Guest')
    {
       
        let result = GuestUserData.find(aa=>aa.phoneNumber == requesteephone);
        if(result == null)
        {
            let guestUserdata = {name:requesteename, phoneNumber: requesteephone,DeviceTocken:DeviceTocken};
            GuestUserData.push(guestUserdata);
           // this.props.UpdateUserFormData({ prop: 'GuestUserData', value: GuestUserData });
         this.storeData('GuestUserData',GuestUserData);
        }
    }
    numberArray = [];
    GuestUserData.forEach(function(el, index) {
      numberArray.push(el.phoneNumber);
    });

    const { selectedApp } = this.props;
    const { userCurrentAddressDetail, userDeliveryAddressDetail, isCurrentLocatonSelected } = this.props;
    const { userCurrentLat, userCurrentLong, userCurrentAddress } = userCurrentAddressDetail;
    const { requsetLat, requetLong, requestSearchAddress } = userDeliveryAddressDetail;
    let totalchargeswithservice = parseInt(this.props.selectedServies.sum('ServiceTotalCharges') + handyServiceCharges);
    var RequestModel = {
      ImagesData: RequestImages,
      ServiceTypeName: selectedApp,
      LocationTrackingID: key,
      PhoneNumbers:[],
      DeviceTocken:DeviceTocken,
      DeviceID:DeviceID,
      // ServiceRequestScheduleDate: !isImmediateRequest ? serviceRequestScheduleDate : new Date().toLocaleString(),
      IsServiceSchedule: !isImmediateRequest,
      IsMaintainance: maintainanceSelected,
      IsInstallation: installationSelected,
      ServiceRequestDetail: RequestDescription,
      ServiceList: selectedServies,
      ServiceCharges:handyServiceCharges,
      ServiceRequestScheduleDateString: !isImmediateRequest ? ServiceRequestScheduleDateString : null,
      RequesteeAddress: isCurrentLocatonSelected ? userCurrentLat : userCurrentAddress,
      ServiceTotalCharges: totalchargeswithservice,
      ServiceTypeID: RequestServiceType,
      CurrentLocationLat: userCurrentLat,
      CurrentLocationLong: userCurrentLong,
      CurrentLocationAddress: userCurrentAddress,

      RequestAddressLat: isCurrentLocatonSelected ? userCurrentLat : requsetLat,
      RequestAddressLong: isCurrentLocatonSelected ? userCurrentLong : requetLong,
      RequestLocationAddress: isCurrentLocatonSelected ? userCurrentAddress : requestSearchAddress,

      RequestById: id,
      RequesteeName: roleName == "Guest" ? requesteename : fullName,
      RequesteeContactNo: roleName == "Guest" ? requesteephone : phoneNumber,
      IsGuestUser: roleName == "Guest" ? true : false,
    }


    this.props.addServiceRequest(RequestModel);

  }

  render() {
    debugger;
    const today = this.state.currentDate;
    const dateAfter72Hours = moment(today).add(3, 'days').format('MMMM Do YYYY, h:mm:ss a');
    const { roleName } = this.props.user;
    const {
      serviceRequestModel, error, loading, RequestServiceType, RequestTime,
      RequestServicesList, RequestImages, RequestDescription, RequestAddress,
      maintainanceSelected,
      installationSelected, isImmediateRequest, user, selectedServies,
      ServiceRequestScheduleDateString, imagesourcearray, handyServiceCharges
    } = this.props;
    const { firebaseLoading } = this.state;
    debugger;
    const { userCurrentAddressDetail, userDeliveryAddressDetail, isCurrentLocatonSelected } = this.props;
    const { userCurrentLat, userCurrentLong, userCurrentAddress } = userCurrentAddressDetail;
    const { requsetLat, requetLong, requestSearchAddress } = userDeliveryAddressDetail;
    const { selectedApp } = this.props;
    const { isLaundry } = selectedApp == "Laundry" ? true : false;
    var jobType = (maintainanceSelected && installationSelected) ? "Maintanance & Insatallation" :
      (maintainanceSelected && !installationSelected) ? "Maintanance " : " Insatallation";

    return (
      <View style={{ flex: 1 }}>
        <CustomHeaderWithText text="Request Detail" />
        {(firebaseLoading || loading) ? <View style={{
          width: width * 1, height: height * 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'gray', opacity: 0.8,
          position: 'absolute'
        }}>
          <Spinner />
          <Text style={{ color: '#fff', fontWeight: 'bold' }}>Uploading your detail.please wait!</Text>
          <Text style={{ color: 'red', fontWeight: 'bold', fontSize: 13, textAlign: 'center' }}>If you have uploaded images then it will take about a minute,
                    otherwise check your internet connection</Text>
        </View> :
          <ScrollView>
            <View style={styles.MainDetailwrapper}>
              <View style={styles.rowSelectType}>
                <Icon
                  name="construct"
                  color="#E9EBEE"
                  style={{ color: '#f48004' }}
                />
                <Text style={styles.rowHeadingList}>Job Detail</Text>
              </View>
              <View style={styles.rowList}>
                <Text style={styles.jobDetailLeft}>Job Type</Text>
                <Text style={styles.jobDetailReight}>{jobType}</Text>
              </View>
              <View style={styles.rowList}>
                <Text style={styles.jobDetailLeft}>Job Discription</Text>
                <Text style={styles.jobDetailReight}>
                  {RequestDescription == "" ? "No Description Provided" : RequestDescription}

                </Text>
              </View>
            </View>

            <View style={styles.MainDetailwrapper}>
              <View style={styles.rowSelectType}>
                <Icon
                  name="md-checkmark-circle-outline"
                  color="#E9EBEE"
                  style={{ color: '#f48004' }}
                />
                <Text style={styles.rowHeadingList}>Job Status</Text>
              </View>
              <View style={styles.rowList}>
                <Text style={styles.jobDetailLeft}>Status ({isImmediateRequest ? "Immediate Request" : "Schedule Request"})</Text>
                <Text style={styles.jobDetailReight}>Ready to intiate </Text>
              </View>
              <View style={styles.rowList}>
                <Text style={styles.jobDetailLeft}>Request Time</Text>
                <Text style={styles.jobDetailReight}>{
                  selectedApp == "Laundry" ? dateAfter72Hours :
                    !isImmediateRequest ?  moment(ServiceRequestScheduleDateString).format('MMMM Do YYYY, h:mm:ss a') : moment().format('MMMM Do YYYY, h:mm:ss a')}</Text>
                <Text style={styles.jobDetailLeft}>Delivery Time</Text>
                <Text style={styles.jobDetailReight}>{"Not yet specified"}</Text>
              </View>
            </View>

            <View style={styles.MainDetailwrapper}>
              <View style={styles.rowSelectType}>
                <Icon name="pin" color="#E9EBEE" style={{ color: '#f48004' }} />
                <Text style={styles.rowHeadingList}>Address Details</Text>
              </View>
              <View style={styles.rowList}>
                <Text style={styles.jobDetailLeft}>Delivery address</Text>
                <Text style={styles.jobDetailReight}>
                  {isCurrentLocatonSelected ? userCurrentAddress :
                    requestSearchAddress == '' ? 'Press plus Icon to add address ' : requestSearchAddress}
                </Text>
              </View>
            </View>

            {selectedApp == "Laundry" ? <View />
              :
              <View style={styles.MainDetailwrapper}>
                <View style={styles.rowSelectType}>
                  <Icon name="md-images" color="#E9EBEE" style={{ color: '#f48004' }} />
                  <Text style={styles.rowHeadingList}>Problem Images</Text>
                </View>

                <View style={styles.rowList}>
                  <View style={{ flexDirection: 'row', padding: 5 }}>
                    {
                      imagesourcearray.map(el => {
                        debugger;
                        //let image = el.replace('http://localhost', 'http://10.0.2.2');
                        return <Image key={el.uri} source={el} style={{ height: 50, width: 50, borderRadius: 10, overflow: 'hidden', marginRight: 10, borderColor: 'black', borderWidth: 1 }} />
                      })
                    }
                    {imagesourcearray.length == 0 ? <Text>No Images Given</Text> : <View />}
                  </View>
                </View>

              </View>


            }


            <View style={{
              borderWidth: 3,
              borderColor: '#f48004',
              backgroundColor: '#E9EBEE',
              width: width * 0.9,
              margin: 10,
              borderRadius: 10,
              alignSelf: 'center',
            }}>
              <View style={styles.rowSelectType}>
                <Icon name="md-card" color="#E9EBEE" style={{ color: '#f48004' }} />
                <Text style={styles.rowHeadingList}>Services Requested </Text>
              </View>

              <View style={{
                flex: 1, backgroundColor: '#E9EBEE', marginTop: 10, marginRight: 0, borderColor: '#f48004', borderWidth: 0, borderRadius: 10,
                overflow: 'hidden'
              }}>
                <List >

                  {this.props.selectedServies.map(item =>


                    <ListItem key={item.ServiceID}
                      style={{ backgroundColor: '#E9EBEE', borderBottomWidth: 1, borderBottomColor: "#f48004", marginLeft: 5 }}>

                      <Body style={{ borderWidth: 0, borderBottomWidth: 0 }} >
                        <Text style={{ fontSize: 13, fontWeight: 'bold',margin:5 }}>{item.ServiceName}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                          <Text style={{ fontSize: 12, margin: 5,color:'green' }} note numberOfLines={1}>{` Rs ${item.DiscountPrice == 0 || item.DiscountPrice == item.serviceCharges ? 
                          item.serviceCharges : item.DiscountPrice} / ${item.Unit} `}</Text>
                          <Text style={{ fontSize: 12, margin: 5, }} note numberOfLines={1}>{`Qty ${item.Quantity}`}</Text>
                        </View>
                      
                        {/* <Text note numberOfLines={1} style={{ fontSize: 12 }}>{` Rs. ${item.serviceCharges}/${item.Unit}                Qty ${item.Quantity}`}</Text> */}
                      </Body>
                      <Right style={{ borderWidth: 0 }}>
                        <Button transparent>
                          <Text style={{ fontSize: 11 ,color:'green'}}>RS. {item.ServiceTotalCharges}</Text>
                        </Button>
                      </Right>
                    </ListItem>





                  )}

                </List>
                {/* <View style={{ backgroundColor: 'orange', height: 7, margin: 8, borderRadius: 8 }} /> */}
              </View>
            </View>



            <View style={styles.MainDetailwrapper}>
              <View style={styles.rowSelectType}>
                <Icon name="md-card" color="#E9EBEE" style={{ color: '#f48004' }} />
                <Text style={styles.rowHeadingList}>Bill</Text>
              </View>
              <View style={styles.rowList}>
                <Text style={styles.jobDetailLeft}>SubTotal</Text>
                <Text style={styles.jobDetailReight}>{this.props.selectedServies.sum('ServiceTotalCharges')} -/RS</Text>
              </View>
              <View style={styles.rowList}>
                <Text style={styles.jobDetailLeft}>Service charges</Text>
                <Text style={styles.jobDetailReight}>Rs. {handyServiceCharges} -/day</Text>
              </View>
              <View style={styles.rowList}>
                <Text style={styles.jobDetailLeft}>Total bill</Text>
                <Text style={{ marginTop: 5, marginLeft: 20, color: 'red', fontSize: 20 }}>{parseInt(this.props.selectedServies.sum('ServiceTotalCharges') + handyServiceCharges)} -/RS</Text>
              </View>
            </View>


            {roleName == "Guest" ?
              <View style={styles.container}  >
                <View style={{
                  marginTop: 5, flexDirection: 'row', alignItems: 'center', borderColor: '#fff', borderBottomWidth: .7
                }}>
                  <Icon name='person' style={{ color: '#f48004', fontSize: 24, marginLeft: 10 }} />
                  <Input placeholder='Your Name' placeholderTextColor="#000"
                    onChangeText={text => this.setState({ requesteename: text })}
                    value={this.state.requesteename}
                    style={{ marginLeft: 10, fontSize: 14, color: '#000' }} />
                </View>
                <View style={{
                  marginTop: 5, flexDirection: 'row', alignItems: 'center', borderColor: '#fff', borderBottomWidth: .7
                }}>
                  <Icon name='person' style={{ color: '#f48004', fontSize: 24, marginLeft: 10 }} />
                  <Input placeholder='Phone Number' placeholderTextColor="#000"
                    onChangeText={text => this.setState({ requesteephone: text })}
                    value={this.state.requesteephone}
                    style={{ marginLeft: 10, fontSize: 14, color: '#000' }} />
                </View>
              </View>
              : <View />}

            <View style={styles.buttonMain}>
              {this.swapButtonAndSpinner()}
            </View>


            {/* {String(error) == '' ? <Text numberOfLines={4} style={{ margin: 10 }}>
              .
                                    </Text> :
              <View>
                <Text numberOfLines={4} style={{ margin: 10 }}>{String(error)}</Text>
                <Text numberOfLines={4} style={{ margin: 10 }}>{error.config.data}</Text>
              </View>
            } */}


          </ScrollView>
        }
        {/* <CustomFooterTab /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // height: Dimensions.get('window').height / 3.3,
    backgroundColor: '#e6e7e9',
    margin: 20,
    marginBottom: 5,
    borderRadius: 9,
    overflow: 'hidden',
    borderColor: '#f48004',
    borderWidth: 2
},
  jobDetailReight: {
    width: width * 0.7,
    color: 'green',
    marginTop: 5,
    marginLeft: 20,
  },
  jobDetailLeft: { color: 'black', marginTop: 5, marginLeft: 20 },
  MainDetailwrapper: {
    borderWidth: 3,
    borderColor: '#f48004',
    backgroundColor: '#E9EBEE',
    width: width * 0.9,
    margin: 10,
    borderRadius: 10,
    alignSelf: 'center',
    padding: 10,
  },
  rowSelectType: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
    marginTop: 10,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  StatusBorder: {
    borderWidth: 3,
    borderColor: 'orange',
    width: width * 0.9,
    height: height * 0.75,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#E9EBEE',
  },
  listItemText: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
    width: width * 0.5,
  },
  rowMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: width * 0.9,
    height: height * 0.1,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  rowIcon: {
    color: '#f48004',
    margin: 20,
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    marginRight: 5,
    marginLeft: 10,
    fontSize: 16,
    color: '#f48004',
    fontFamily: 'sens-serif',
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    backgroundColor: '#f48004',
    padding: 10,
    borderRadius: 20,
    width: width * 0.35,
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});



const mapStateToProps = ({ handyServices, auth, userAddress }) => {

  const { services, servicestype, serviceRequestModel, error, loading, RequestServiceType, RequestTime,
    RequestServicesList, RequestImages, RequestDescription, RequestAddress,
    maintainanceSelected, installationSelected, serviceRequestScheduleDate,
    isImmediateRequest, selectedServies, ServiceRequestScheduleDateString, LocationTrackingID
  } = handyServices;
  debugger;
  const { grocServiceChrges, handyServiceCharges, adverstisementsList ,GuestUserData} = auth;
  const { selectedApp,DeviceTocken,DeviceID } = auth;
  const { userCurrentAddressDetail, userDeliveryAddressDetail, isCurrentLocatonSelected } = userAddress;
  return {
    user: auth.user,
    services, servicestype, serviceRequestModel, error, loading, RequestServiceType, RequestTime,
    RequestServicesList, RequestImages, RequestDescription, RequestAddress, serviceRequestScheduleDate,
    maintainanceSelected, installationSelected, ServiceRequestScheduleDateString,DeviceTocken,DeviceID,
    userCurrentAddressDetail, userDeliveryAddressDetail, isCurrentLocatonSelected,GuestUserData,
    isImmediateRequest, selectedServies, selectedApp, handyServiceCharges, LocationTrackingID
  };
};
export default connect(mapStateToProps, { ServiceRequestUpdateForm, addServiceRequest })(RequestDetail);
//export default RequestDetail;

Array.prototype.sum = function (prop) {
  var total = 0
  for (var i = 0, _len = this.length; i < _len; i++) {
    total += this[i][prop]
  }
  return total
}