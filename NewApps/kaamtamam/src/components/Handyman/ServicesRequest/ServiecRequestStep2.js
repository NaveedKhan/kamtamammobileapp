import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
  TextInput,
  Platform,
  PermissionsAndroid,
  Alert,
} from 'react-native';
import {Item, Input, CheckBox, Icon, Spinner} from 'native-base';
import {CustomHeaderWithText} from '../../Grocessry/Common/Header';
//import {CustomFooterTab} from '../../Grocessry/Common/Footer';
import Geolocation from 'react-native-geolocation-service';
//import ImageResizer from 'react-native-image-resizer';
//import ImageCompressor from '@trunkrs/react-native-image-compressor'
import Geocode from 'react-geocode';
import axios from 'axios';
import {config} from '../../../config';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import {
  ServiceRequestUpdateForm,
  UpdateUserAddressDatail,
} from '../../../actions';
import {Button} from '../../common';

Geocode.setApiKey('AIzaSyCZ_GeN6VIBOgZqe9mZ568ygvB8eUNuSbc');
Geocode.setLanguage('en');
Geocode.enableDebug();

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';



const createFormData = (photo, body) => {
  const data = new FormData();

  data.append('File', {
    name: photo.fileName,
    type: photo.type,
    uri:
      Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
  });

  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });

  return data;
};

class ServiecRequestStep2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image1: '',
      image2: '',
      image3: '',
      image4: '',
      image5: '',
      image6: '',
      image1Source: '',
      image2Source: '',
      image4Source: '',
      image3Source: '',
      image5Source: '',
      image6Source: '',
      isGettingLocation: false,
    };
  }

  async requestocationPermission()
  {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Kaam Tamam Location Permission',
          message:
            'Kaam Tamam App needs access to your Location ' +
            'so you can place order.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        //this.getCurrentLocation();
      } else {
        console.log('Location permission denied');
        Alert.alert('Attention','You have denied the location permission so you will not be able to make an order '+
        'Kaam Tamam needs to access you location so that you can select a location for delivery')
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Kaam Tamam App Camera Permission',
          message:
            'Kaam Tamam App needs access to your camera ' +
            'so you can take problem pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the camera');
      } else {
        console.log('Camera permission denied');
        Alert.alert('Attenstion','You have denied photo and storage permission so you wil not be able to '+
        'take or upload problem pictures. to allow permission go to setting and permssion '+
        ' setting and allow photo and storage for Kaam Tamaam')
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async requestStoragePermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'Kaam Tamam App Camera Permission',
          message:
            'Kaam Tamam needs access to your camera ' +
            'so you can take  problem pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the camera');
      } else {
        Alert.alert('Attenstion','You have denied photo and storage permission so you wil not be able to '+
        'take or upload problem pictures. to allow permission go to setting and permssion '+
        ' setting and allow photo and storage for Kaam Tamaam')
      }
    } catch (err) {
      console.warn(err);
    }
  }

  componentDidMount() {
    if (Platform.OS == 'android') {
      this.requestCameraPermission();
      this.requestStoragePermission();
      this.requestocationPermission();
    }
  }

  handleUploadPhoto = () => {
    const {image1} = this.state;
    debugger;
    const imageModel = {source: image1.data};
    debugger;
    let apiconfig = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };

    const {REST_API} = config;
    axios
      .post(REST_API.Test.PostImage, imageModel, apiconfig)
      //.then(response => response.json())
      .then(response => {
        debugger;
        console.log('upload succes', response);
        //alert("Upload success!");
        this.setState({photo: null});
      })
      .catch(error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        Alert.alert('error', err);
        console.log('upload error', error);
        //alert("Upload failed!");
      });
  };

  onClickNext() {
    debugger;
    //let { selectedServies } = this.state;
    //var checkedService = selectedServies.filter(aa => aa.IsSelected == true);
    const {image1, image2, image3, image4, image5, image6} = this.state;
    const {
      image1Source,
      image2Source,
      image3Source,
      image4Source,
      image5Source,
      image6Source,
    } = this.state;

    var imageArray = [];
    var imagesourcearray = [];

    if (image1Source != '') imagesourcearray.push(image1Source);

    if (image2Source != '') imagesourcearray.push(image2Source);

    if (image3Source != '') imagesourcearray.push(image3Source);

    if (image4Source != '') imagesourcearray.push(image4Source);

    if (image5Source != '') imagesourcearray.push(image5Source);

    if (image6Source != '') imagesourcearray.push(image6Source);

    if (image1 != '') {
      const {fileName, type, height, width, fileSize, data} = image1;
      var imageModel = {
        FileName: fileName,
        Base64String: data,
        Size: fileSize,
        Width: width,
        Height: height,
        FileType: type,
      };
      imageArray.push(imageModel);
    }

    if (image2 != '') {
      const {fileName, type, height, width, fileSize, data} = image2;
      var imageModel = {
        FileName: fileName,
        Base64String: data,
        Size: fileSize,
        Width: width,
        Height: height,
        FileType: type,
      };
      imageArray.push(imageModel);
    }

    if (image3 != '') {
      const {fileName, type, height, width, fileSize, data} = image3;
      var imageModel = {
        FileName: fileName,
        Base64String: data,
        Size: fileSize,
        Width: width,
        Height: height,
        FileType: type,
      };
      imageArray.push(imageModel);
    }

    if (image4 != '') {
      const {fileName, type, height, width, fileSize, data} = image4;
      var imageModel = {
        FileName: fileName,
        Base64String: data,
        Size: fileSize,
        Width: width,
        Height: height,
        FileType: type,
      };
      imageArray.push(imageModel);
    }

    if (image5 != '') {
      const {fileName, type, height, width, fileSize, data} = image5;
      var imageModel = {
        FileName: fileName,
        Base64String: data,
        Size: fileSize,
        Width: width,
        Height: height,
        FileType: type,
      };
      imageArray.push(imageModel);
    }

    if (image6 != '') {
      const {fileName, type, height, width, fileSize, data} = image6;
      var imageModel = {
        FileName: fileName,
        Base64String: data,
        Size: fileSize,
        Width: width,
        Height: height,
        FileType: type,
      };
      imageArray.push(imageModel);
    }

    this.getCurrentLocation(imageArray, imagesourcearray);
    // Actions.PlumberRequestProceedPage({ imagesourcearray: imagesourcearray })
  }

  selectImage(buttonNo) {
    debugger;
    const options = {
      title: 'Select Avatar',
      customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
      quality: 0.3,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        Alert.alert('Attention','you have denied the gallery and photo permission.Please go to setting and allow the photo and storage permission '+
        ' Other wise you will not be able to take or upload problem pictures')
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        debugger;
        var path = response.path;
        debugger;
        this.updateimagesOnStates(response, source, buttonNo);
      }
    });
  }

  // async resize(image, imagesource, buttonNo) {
  //   // const { AccessTok } = this.props.homeState;
  //   // const { userId } = this.props.state;
  //   debugger;
  //   const result = await ImageCompressor.compress(image.data, {
  //     maxWidth: 500,
  //     maxHeight: 500
  //   });
  //   debugger;
  //   try {
  //     const resized = await ImageResizer.createResizedImage(image.uri, 500, 500, 'JPEG', 20);
  //     if (resized) {
  //       debugger;
  //       // const result = await ImageCompressor.compress(resized.path, {
  //       //   maxWidth: 500,
  //       //   maxHeight: 500
  //       // });
  //       debugger;
  //       this.setState({
  //         resizedImageUri: resized.uri,
  //       });
  //       if (Platform.OS === 'ios') {
  //         var resizeImage = {
  //           uri: imagesource,
  //           name: resized.name,
  //           data: image.data
  //         }
  //       } else {
  //         var resizeImage = {
  //           uri: imagesource,
  //           type: image.type,
  //           name: image.name,
  //           data: image.data
  //         }
  //       }

  //       this.updateimagesOnStates(resizeImage, imagesource, buttonNo);
  //       // this.props.state.isLoading = false;
  //     }
  //   } catch (err) {
  //     return Alert.alert('Please try again');
  //   }
  // }

  updateimagesOnStates(response, source, buttonNo) {
    debugger;
    if (buttonNo == 1) this.setState({image1: response, image1Source: source});
    else if (buttonNo == 2)
      this.setState({image2: response, image2Source: source});
    else if (buttonNo == 3)
      this.setState({image3: response, image3Source: source});
    else if (buttonNo == 4)
      this.setState({image4: response, image4Source: source});
    else if (buttonNo == 5)
      this.setState({image5: response, image5Source: source});
    else this.setState({image6: response, image6Source: source});
  }

  async getCurrentLocation(imageArray, imagesourcearray) {
    debugger;
    // userCurrentLat: 33.9976884, userCurrentLong: 71.4695614,
    var me = this;
    this.setState({isGettingLocation: true});
    // isGettingLocation:false
    await Geolocation.getCurrentPosition(
      position => {
        console.log(position);
        let lats =
          String(position.coords.latitude).indexOf('37.') == 0
            ? 33.9976884
            : position.coords.latitude;
        let longs =
          String(position.coords.longitude).indexOf('-122') == 0
            ? 71.4695614
            : position.coords.longitude;

        Geocode.fromLatLng(lats, longs)
          .then(
            response => {
              debugger;
              const address = response.results[0].formatted_address;
              //userCurrentLat: 33.9976884
              //userCurrentLong: 71.4695614
              //Board Bazar Stop, University latsRoad, Peshawar, Pakistan
              let userCurrentAddressDetail = {
                userCurrentLat: lats,
                userCurrentLong: longs,
                userCurrentAddress: address,
              };
              me.props.UpdateUserAddressDatail({
                prop: 'userCurrentAddressDetail',
                value: userCurrentAddressDetail,
              });
              me.props.UpdateUserAddressDatail({
                prop: 'isCurrentLocatonSelected',
                value: true,
              });
              me.setState({isGettingLocation: false});
              me.props.ServiceRequestUpdateForm({
                prop: 'RequestImages',
                value: imageArray,
              });
              Actions.PlumberRequestProceedPage({
                imagesourcearray: imagesourcearray,
              });
              console.log(address);
            },
            error => {
              debugger;
              var err = String(error) == '' ? 'no error ' : String(error);
              Alert.alert('error', err);
              me.setState({isGettingLocation: false});
              console.error(error);
            },
          )
          .catch(error => {
            debugger;
            var err = String(error) == '' ? 'no error ' : String(error);
            Alert.alert('error', err);
            me.setState({isGettingLocation: false});
            //loginUserFail(dispatch, error);
            console.log('error', error);
          });
      },
      error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        if(error)
        {
          const {code, message} = error;
          if(code == 1)
          {
            Alert.alert('Attention',
             'Dear customer you have denied the location permission.to poceed please '+
             ' got to setting and allow location access so that Kaam Tamam app can access the location service for you! Otherwise you will not be able to make an order'
             );
          }else
          {
            Alert.alert('error', message);
          }
          
        }
        me.setState({isGettingLocation: false});
        console.log(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 15000},
    );
  }

  render() {
    const {isGettingLocation} = this.state;
    return (
      <View style={{flex: 1}}>
        <CustomHeaderWithText text="STEP 2" />
        <ScrollView scrollEnabled>
          <View style={{alignSelf: 'center', margin: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'center',
                width: width * 0.85,
              }}>
              <View
                style={{
                  backgroundColor: '#f48004',
                  borderRadius: 5,
                  width: 10,
                  height: 10,
                  paddingRight: 10,
                }}
              />
              <Text style={styles.rowHeadingList}>
                Add Description (Optional)
              </Text>
            </View>
            <View style={styles.DiscriptionBorder}>
              <TextInput
                placeholder="Enter detail here....."
                style={{borderColor: 'gray'}}
                onChangeText={text =>
                  this.props.ServiceRequestUpdateForm({
                    prop: 'RequestDescription',
                    value: text,
                  })
                }
                multiline={true}
                value={this.props.RequestDescription}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'center',
                width: width * 0.85,
              }}>
              <View
                style={{
                  backgroundColor: '#f48004',
                  borderRadius: 5,
                  width: 10,
                  height: 10,
                  paddingRight: 10,
                  marginTop: 10,
                }}
              />
              <Text style={styles.rowHeadingList}>
                Upload Images (Optional)
              </Text>
            </View>

            <View
              style={{
                margin: 10,
                borderColor: '#f48004',
                borderWidth: 3,
                borderRadius: 10,
                padding: 5,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  marginBottom: 10,
                }}>
                <TouchableOpacity
                  onPress={() => this.selectImage(1)}
                  style={{
                    backgroundColor: '#E9EBEE',
                    width: 100,
                    height: 120,
                    borderRadius: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  {this.state.image1 == '' ? (
                    <Icon name="add" style={{fontSize: 40, marginTop: 15}} />
                  ) : (
                    <Image
                      source={this.state.image1Source}
                      style={{
                        height: 120,
                        width: 100,
                        borderRadius: 10,
                        overflow: 'hidden',
                      }}
                    />
                  )}
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.selectImage(2)}
                  style={{
                    backgroundColor: '#E9EBEE',
                    width: 100,
                    height: 120,
                    borderRadius: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  {this.state.image2 == '' ? (
                    <Icon name="add" style={{fontSize: 40, marginTop: 15}} />
                  ) : (
                    <View>
                      <Image
                        source={this.state.image2Source}
                        style={{
                          height: 120,
                          width: 100,
                          borderRadius: 10,
                          overflow: 'hidden',
                        }}
                      />
                    </View>
                  )}
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.selectImage(3)}
                  style={{
                    backgroundColor: '#E9EBEE',
                    width: 100,
                    height: 120,
                    borderRadius: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  {this.state.image3 == '' ? (
                    <Icon name="add" style={{fontSize: 40, marginTop: 15}} />
                  ) : (
                    <Image
                      source={this.state.image3Source}
                      style={{
                        height: 120,
                        width: 100,
                        borderRadius: 10,
                        overflow: 'hidden',
                      }}
                    />
                  )}
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                <TouchableOpacity
                  onPress={() => this.selectImage(4)}
                  style={{
                    backgroundColor: '#E9EBEE',
                    width: 100,
                    height: 120,
                    borderRadius: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  {this.state.image4 == '' ? (
                    <Icon name="add" style={{fontSize: 40, marginTop: 15}} />
                  ) : (
                    <Image
                      source={this.state.image4Source}
                      style={{
                        height: 120,
                        width: 100,
                        borderRadius: 10,
                        overflow: 'hidden',
                      }}
                    />
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.selectImage(5)}
                  style={{
                    backgroundColor: '#E9EBEE',
                    width: 100,
                    height: 120,
                    borderRadius: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  {this.state.image5 == '' ? (
                    <Icon name="add" style={{fontSize: 40, marginTop: 15}} />
                  ) : (
                    <Image
                      source={this.state.image5Source}
                      style={{
                        height: 120,
                        width: 100,
                        borderRadius: 10,
                        overflow: 'hidden',
                      }}
                    />
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.selectImage(6)}
                  style={{
                    backgroundColor: '#E9EBEE',
                    width: 100,
                    height: 120,
                    borderRadius: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  {this.state.image6 == '' ? (
                    <Icon name="add" style={{fontSize: 40, marginTop: 15}} />
                  ) : (
                    <Image
                      source={this.state.image6Source}
                      style={{
                        height: 120,
                        width: 100,
                        borderRadius: 10,
                        overflow: 'hidden',
                      }}
                    />
                  )}
                </TouchableOpacity>
              </View>
            </View>
            {/* <View style={styles.DiscriptionBorder}>

            </View> */}
          </View>
          {isGettingLocation ? (
            <Spinner />
          ) : (
            <TouchableOpacity
              style={{
                width: 250,
                height: 50,
                borderRadius: 30,
                margin: 10,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                borderColor: '#f48004',
                borderWidth: 1.7,
              }}
              onPress={() => this.onClickNext()}>
              <Text
                style={{fontSize: 15, color: '#f48004', fontWeight: 'bold'}}>
                STEP 3{' '}
              </Text>
            </TouchableOpacity>
          )}
        </ScrollView>

        {/* <CustomFooterTab /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 30,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  RequestBorder: {
    borderWidth: 3,
    borderColor: '#E9EBEE',
    backgroundColor: '#E9EBEE',
    width: width * 0.9,
    margin: 10,
    borderRadius: 10,
  },
  listItemText: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  rowList: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 10,
    borderBottomColor: 'white',
    width: width * 0.85,
    borderBottomWidth: 1,
    alignSelf: 'center',
    padding: 10,
  },
  rowIcon: {
    color: '#f48004',
    margin: 20,
    marginTop: 10,
    marginBottom: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    marginRight: 10,
    marginLeft: 10,
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
    marginTop: 10,
  },
  rowSelectType: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
    marginTop: 10,
  },
  rowText: {
    color: 'grey',
    alignSelf: 'center',
    marginTop: 5,
    marginLeft: 20,
  },
  DiscriptionBorder: {
    borderWidth: 3,
    borderColor: '#f48004',
    backgroundColor: 'white',
    width: width * 0.9,
    height: height * 0.25,
    margin: 10,
    borderRadius: 10,
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    backgroundColor: '#f48004',
    padding: 10,
    borderRadius: 20,
    width: width * 0.35,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

const mapStateToProps = ({handyServices}) => {
  const {
    services,
    servicestype,
    serviceRequestModel,
    error,
    loading,
    RequestServiceType,
    RequestTime,
    RequestServicesList,
    RequestImages,
    RequestDescription,
    RequestAddress,
    maintainanceSelected,
    installationSelected,
    isImmediateRequest,
  } = handyServices;
  debugger;
  return {
    services,
    servicestype,
    serviceRequestModel,
    error,
    loading,
    RequestServiceType,
    RequestTime,
    RequestServicesList,
    RequestImages,
    RequestDescription,
    RequestAddress,
    maintainanceSelected,
    installationSelected,
    isImmediateRequest,
  };
};
export default connect(
  mapStateToProps,
  {ServiceRequestUpdateForm, UpdateUserAddressDatail},
)(ServiecRequestStep2);
//export default ServiecRequestStep2;
