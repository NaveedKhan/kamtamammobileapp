import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  Modal,
  TextInput,
  Alert,
  ScrollView,
  Linking,
  Platform,
} from 'react-native';
import {
  Item,
  Input,
  Icon,
  Spinner,
  List,
  ListItem,
  Left,
  CheckBox,
  Right,
  Thumbnail,
  Toast,
  Body,
  Button,
  avatar,
} from 'native-base';
import {Rating, AirbnbRating} from 'react-native-elements';
import {CustomHeaderWithText} from '../../Grocessry/Common/Header';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import moment from 'moment';
import axios from 'axios';
import {config} from '../../../config';
import {
  ServiceRequestUpdateForm,
  addServiceRequest,
  UpdateNotificationData,
} from '../../../actions';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class ServiceDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      modalVisible: false,
      reviewComment: '',
      reviewScore: 0,
      modalButtonLoading: false,
      selectedOrderToComplete: null,
    };
  }

  callTechnician(phone) {
    // let phone = '033323233232';
    console.log('callNumber ----> ', '033323233232');
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  }

  ratingCompleted(rating) {
    this.setState({reviewScore: rating});
    console.log('Rating is: ' + rating);
  }

  submitReview() {
    // this.setState({ modalVisible: false })
    if (this.state.iscanceling) {
      this.senpApiCallToMarkCancel();
    } else {
      this.senpApiCallToMarkComplete();
    }
  }

  senpApiCallToMarkComplete() {
    debugger;
    // const { Order } = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
      ServiceRequestID: selectedOrderToComplete.serviceRequestID,
      IsMarkFinishedByUser: true,
      Reviews: {
        UserReview: reviewComment,
        UserReviewScore: reviewScore,
        UserID: selectedOrderToComplete.requestById,
        ServiceRequestID: selectedOrderToComplete.serviceRequestID,
        IsReviewedByUser: true,
      },
    };
    // const { user } = this.props;
    // let id = user.id;
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({modalButtonLoading: true});
    const {REST_API} = config;
    try {
      const response = axios
        .post(
          REST_API.ServiceRequest.MarkServiceRequestCompleteByUser,
          reviewModel,
          configs,
        )
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          debugger;
          console.log('api call' + response);

          me.props.UpdateNotificationData({
            prop: 'unReadNotificationCount',
            value: baseModel.data.unReadnotificationCount,
          });
          me.props.UpdateNotificationData({
            prop: 'totalpendingOrdercount',
            value: baseModel.data.totalpendingOrdercount,
          });
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
          Actions.reset('SelectJob');
          //Actions.tendersResult({TendersList:baseModel.data});
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({
        isLoading: false,
        modalButtonLoading: false,
        modalVisible: false,
      });
      //productFetchingFail(dispatch);
    }
  }

  senpApiCallToMarkCancel() {
    debugger;
    // const { Order } = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
      ServiceRequestID: selectedOrderToComplete.serviceRequestID,
      IsMarkFinishedByUser: true,
      Reviews: {
        UserReview: reviewComment,
        UserReviewScore: reviewScore,
        UserID: selectedOrderToComplete.requestById,
        ServiceRequestID: selectedOrderToComplete.serviceRequestID,
        IsReviewedByUser: true,
      },
    };
    // const { user } = this.props;
    // let id = user.id;
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({modalButtonLoading: true});
    const {REST_API} = config;
    try {
      const response = axios
        .post(
          REST_API.ServiceRequest.CancelServiceRequestByUser,
          reviewModel,
          configs,
        )
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          debugger;
          console.log('api call' + response);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });

          me.props.UpdateNotificationData({
            prop: 'unReadNotificationCount',
            value: baseModel.data.unReadnotificationCount,
          });
          me.props.UpdateNotificationData({
            prop: 'totalpendingOrdercount',
            value: baseModel.data.totalpendingOrdercount,
          });
          Actions.reset('SelectJob');
          //Actions.tendersResult({TendersList:baseModel.data});
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({
        isLoading: false,
        modalButtonLoading: false,
        modalVisible: false,
      });
      //productFetchingFail(dispatch);
    }
  }

  renderRatingModal() {
    debugger;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({modalVisible: false, modalButtonLoading: false});
        }}>
        <View style={styles.datecontainer}>
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              borderRadius: 15,
              justifyContent: 'flex-end',
              alignItems: 'center',
              alignSelf: 'flex-end',
              margin: 10,
              borderColor: '#fff',
              borderWidth: 1,
            }}
            onPress={() => this.setState({modalVisible: false})}>
            <Icon name="close" style={{color: '#fff'}} />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 15,
              margin: 5,
              alignSelf: 'center',
              color: 'white',
            }}>
            Please Add Review for better services
          </Text>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 5,
            }}>
            <AirbnbRating
              count={5}
              reviews={[
                'Terrible (1)',
                'Bad (2)',
                'Normal (3)',
                'GOOD (4)',
                'Outstanding (5)',
              ]}
              defaultRating={3}
              onFinishRating={rat => this.ratingCompleted(rat)}
              size={40}
            />
          </View>

          <View
            style={{
              margin: 10,
              backgroundColor: '#fff',
              borderColor: 'gray',
              borderWidth: 1,
              padding: 5,
              borderRadius: 5,
            }}>
            <TextInput
              onChangeText={text => this.setState({reviewComment: text})}
              placeholder="Add any comment (optional but recomended)"
              placeholderTextColor="grey"
              maxLength={200}
              multiline={true}
              editable
              numberOfLines={5}
              style={{height: 150, justifyContent: 'flex-start'}}
            />
          </View>
          {this.state.modalButtonLoading ? (
            <Spinner />
          ) : (
            <View>
              <TouchableOpacity
                onPress={() => this.submitReview()}
                style={{
                  width: 250,
                  height: 50,
                  borderRadius: 30,
                  margin: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  borderColor: '#f48004',
                  borderWidth: 1.7,
                }}>
                <Text style={{fontSize: 15, color: '#fff', fontWeight: 'bold'}}>
                  {' '}
                  SUBMIT NOW{' '}
                </Text>
              </TouchableOpacity>
              {/* <TouchableOpacity
                onPress={() => this.setState({ modalVisible: false })}
                style={{
                  width: 250,
                  height: 50,
                  borderRadius: 30,
                  margin: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  borderColor: '#f48004',
                  borderWidth: 1.7
                }} >
                <Text style={{ fontSize: 15, color: '#fff', fontWeight: 'bold' }} > CANCEL </Text>
              </TouchableOpacity> */}
            </View>
          )}
        </View>
      </Modal>
    );
  }

  showToast2() {
    return Toast.show({
      text: 'Order already marked completed!',
      buttonText: 'Okay',
      type: 'danger',
      duration: 3000,
    });
  }

  OnpressComplete(el) {
    debugger;
    if (el.serviceStatusName.toLowerCase() == 'Delivered'.toLowerCase()) {
      this.showToast2();
    } else {
      debugger;
      this.ShowConfimrationAlert(
        el,
        'Are You sure your request is fulfilled and you want to close  it.',
      );
    }
  }

  markCancel(el) {
    debugger;
    if (
      el.orderStatus == 'Delivered' ||
      el.orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      this.showToast2();
    } else {
      let isexpire =
        el.orderStatus.toLowerCase() == 'initiated'.toLowerCase()
          ? false
          : this.timeBetweenDates(new Date(el.serviceAssignedToDriverTechDate));
      if (el.orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
        this.ShowConfimrationAlertForCancel(
          el,
          'Are you sure you really want to cancel the order',
        );
      } else {
        if (isexpire) {
          Alert.alert(
            'alert',
            'Please contact our help line or delivery guy to cancel the order.You canot cancel the order after 5 min',
          );
        } else {
          this.ShowConfimrationAlertForCancel(
            el,
            'Are you sure you really want to cancel the order',
          );
        }
      }
    }
  }

  markCancel(el, orderStatus) {
    debugger;
    if (
      orderStatus.toLowerCase() == 'Delivered'.toLowerCase() ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      this.showToast2();
    } else {
      let isexpire =
        orderStatus.toLowerCase() == 'initiated'.toLowerCase()
          ? false
          : this.timeBetweenDates(new Date(el.serviceAssignedToDriverTechDate));
      if (orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
        this.ShowConfimrationAlertForCancel(
          el,
          'Are you sure you really want to cancel the order',
        );
      } else {
        if (isexpire) {
          Alert.alert(
            'alert',
            'Please contact our help line or delivery guy to cancel the order.You canot cancel the order after 5 min',
          );
        } else {
          this.ShowConfimrationAlertForCancel(
            el,
            'Are you sure you really want to cancel the order',
          );
        }
      }
      //this.ShowConfimrationAlertForCancel(el, 'Are you sure you really want to cancel the order');
    }
  }

  ShowConfimrationAlertForCancel(el, message) {
    Alert.alert(
      'Confirmation!',
      message,
      [
        {
          text: 'Yes Continue',
          onPress: () => {
            //checkoutSucess(dispatch,baseModel.data)
            // this.setState({  })
            this.setState({
              selectedOrderToComplete: el,
              iscanceling: true,
              modalVisible: true,
            });
            //    this.senpApiCallToMarkCancel(el)
          },
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  ShowConfimrationAlert(el, message) {
    debugger;
    Alert.alert(
      'Confirmation!',
      message,
      [
        {
          text: 'Yes,Continue',
          onPress: () => {
            //  this.setState({  })
            this.setState({
              selectedOrderToComplete: el,
              iscanceling: false,
              modalVisible: true,
            });
          },
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  _renderButtons(orderStatus, isexpire, el) {
    if (
      orderStatus == 'Delivered' ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      let {reviews} = el;
      let {
        userReviewScore,
        driverReviewScore,
        isReviewedByUser,
        isReviewedByDriverTech,
      } = reviews;

      var reviewbuttonstyle = {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
      };
      if (isReviewedByUser && !isReviewedByDriverTech) {
        return this._renderUserReviews(userReviewScore);
      } else if (!isReviewedByUser && isReviewedByDriverTech) {
        return this._renderUserReviews(driverReviewScore);
      } else if (isReviewedByUser && isReviewedByDriverTech) {
        return this._renderbothReviews(userReviewScore, driverReviewScore);
      }
    } else {
      if (orderStatus.toLowerCase() == 'initiated') {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() => this.markCancel(el, orderStatus)}
              style={[styles.button, {marginLeft: 5, width: 150}]}>
              <Text style={styles.buttonText}> Cancel Order</Text>
            </TouchableOpacity>
          </View>
        );
      } else {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 10,
              marginBottom: 10,
            }}>
            {/* <TouchableOpacity
              onPress={() => this.OnpressComplete(el, orderStatus)}
              style={styles.button}>
              <Text style={styles.buttonText}>Mark Complete</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.markCancel(el, orderStatus)}
              style={[styles.button, {marginLeft: 5}]}>
              <Text style={styles.buttonText}> Cancel Order</Text>
            </TouchableOpacity> */}
            {el.assigneeName == null && el.assigneContactNo == null ? (
              <View />
            ) : (
              <TouchableOpacity
                onPress={() => this.callTechnician(el.assigneContactNo)}
                style={[styles.button, {marginLeft: 5}]}>
                <Text style={styles.buttonText}> Call Technician</Text>
              </TouchableOpacity>
            )}
          </View>
        );
      }
    }
  }

  timeBetweenDates(toDate) {
    debugger;
    var dateEntered = toDate;
    var now = new Date();
    var difference = now.getTime() - dateEntered.getTime();

    if (difference <= 0) {
      // Timer done
      //clearInterval(timer);
    } else {
      var seconds = Math.floor(difference / 1000);
      var minutes = Math.floor(seconds / 60);
      var hours = Math.floor(minutes / 60);
      var days = Math.floor(hours / 24);

      hours %= 24;
      minutes %= 60;
      seconds %= 60;

      if (hours > 0 || days > 0 || minutes > 5) {
        return true;
      } else {
        return false;
      }
      // this.setState({ countDownTime: days + 'D ' + hours + 'H ' + minutes + 'M ' + seconds + 'S' });
    }
  }

  _renderUserReviews(score) {
    return (
      <View
        style={{
          // flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}>
        <Rating
          // showRating
          ratingCount={5}
          readonly
          // reviews={["Terrible (1)", "Bad (2)", "Normal (3)", "GOOD (4)", "Outstanding (5)"]}
          startingValue={score}
          //onFinishRating={(rat) => this.ratingCompleted(rat)}
          size={10}
        />
      </View>
    );
  }

  _renderbothReviews(userscore, driverscore) {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 10,
          marginBottom: 20,
          padding: 5,
        }}>
        <Text>Your Review</Text>
        <Rating ratingCount={5} readonly startingValue={userscore} size={10} />
        <Text style={{marginTop: 5}}>Kaam Tamam Team Review</Text>
        <Rating
          ratingCount={5}
          readonly
          startingValue={driverscore}
          size={10}
        />
      </View>
    );
  }

  _renderBottomText(orderStatus, el) {
    debugger;
    if (
      orderStatus.toLowerCase() == 'Delivered'.toLowerCase() ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      let {reviews} = el;
      let reviewstatis = '';
      if(reviews)
      {
       reviewstatis =
        reviews.isReviewedByUser && !reviews.isReviewedByDriverTech
          ? 'You have given the review right now'
          : !reviews.isReviewedByUser && reviews.isReviewedByDriverTech
          ? 'Kaam Tamam team has added the review'
          : !reviews.isReviewedByUser && !reviews.isReviewedByDriverTech
          ? 'No Review Added Yet'
          : 'Kaam Tamam and you have added the review';
      }
      return (
        <Text
          style={{
            color: 'red',
            alignSelf: 'center',
            marginTop: 5,
            fontSize: 13,
          }}>
          {reviewstatis}
        </Text>
      );
    } else {
      return (
        <Text
          style={{
            color: 'red',
            alignSelf: 'center',
            margin: 10,
            fontSize: 13,
            justifyContent: 'center',
            textAlign: 'center',
          }}>
          {'You can Cancel the Request until it is assign to worker.'}
        </Text>
      );
    }
  }

  render() {
    const {selectedService, handyServiceCharges} = this.props;
    const {
      requestAddressLat,
      requestAddressLong,
      assigneeImageURL,
      assigneContactNo,
      assigneeName,
      serviceStatusName,
      serviceReEstimatedTotalCharges,
      serviceCharges
    } = selectedService;
    debugger;
    let assImage_Http_URL = {uri: 'http://182.180.56.162:8088/404.png'};
    if (assigneeImageURL != null) {
      let imageass = assigneeImageURL.replace(
        'http://localhost',
        'http://10.0.2.2',
      );
      assImage_Http_URL = {uri: imageass};
    }

    let serviceRequestDate = new Date(selectedService.serviceRequestDate);

    var jobType =
      selectedService.isMaintainance && selectedService.isInstallation
        ? 'Maintanance & Insatallation'
        : selectedService.isMaintainance && !selectedService.isInstallation
        ? 'Maintanance '
        : ' Insatallation';

    return (
      <View style={{flex: 1}}>
        <CustomHeaderWithText text="Service Request Detail" />
        <ScrollView>
          {serviceStatusName.toLowerCase() != 'initiated' ? (
            assigneeName == null && assigneContactNo == null ? (
              <View style={{alignSelf: 'center', margin: 5}}>
                <Text style={{color: 'red'}}>
                  Service Was concelled before assigned
                </Text>
              </View>
            ) : (
              <View style={styles.thumbnailDiv}>
                <View style={styles.rowSelectType}>
                  <Icon
                    name="construct"
                    color="#E9EBEE"
                    style={{color: '#f48004'}}
                  />
                  <Text style={styles.rowHeadingList}>Technician Detail </Text>
                </View>
                <Image
                  source={assImage_Http_URL}
                  style={{
                    height: 90,
                    width: 90,
                    borderRadius: 45,
                    marginTop: 10,
                    overflow: 'hidden',
                    borderColor: 'black',
                    borderWidth: 1,
                  }}
                />
                <Text style={{fontSize: 13, color: 'green', marginTop: 3}}>
                  {assigneeName}
                </Text>
                <TouchableOpacity
                  onPress={() => this.callTechnician(assigneContactNo)}
                  style={{
                    borderColor: 'green',
                    borderWidth: 1,
                    width: 120,
                    height: 30,
                    margin: 5,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 15,
                  }}>
                  <Text style={{fontSize: 13, color: 'green'}}>
                    {assigneContactNo}
                  </Text>
                </TouchableOpacity>
              </View>
            )
          ) : (
            <View />
          )}

          <View style={styles.MainDetailwrapper}>
            <View style={styles.rowSelectType}>
              <Icon
                name="construct"
                color="#E9EBEE"
                style={{color: '#f48004'}}
              />
              <Text style={styles.rowHeadingList}>
                Request ID {selectedService.serviceRequestID}
              </Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Reqeust For</Text>
              <Text style={styles.jobDetailReight}>
                {selectedService.serviceTypeName}
              </Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Reqeust Type</Text>
              <Text style={styles.jobDetailReight}>{jobType}</Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Reqeust Discription</Text>
              <Text style={styles.jobDetailReight}>
                {selectedService.serviceRequestDetail == ''
                  ? 'No Description Provided'
                  : selectedService.serviceRequestDetail}
              </Text>
            </View>
          </View>

          <View style={styles.MainDetailwrapper}>
            <View style={styles.rowSelectType}>
              <Icon
                name="md-checkmark-circle-outline"
                color="#E9EBEE"
                style={{color: '#f48004'}}
              />
              <Text style={styles.rowHeadingList}>
                Reqeust Status ( {serviceStatusName} )
              </Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Request Urgency </Text>
              <Text style={styles.jobDetailReight}>
                {!selectedService.isServiceSchedule
                  ? 'Immediate Request'
                  : 'Schedule Request on : ' +
                  moment(selectedService.serviceRequestScheduleDate).format('MMMM Do YYYY, h:mm:ss a')}
              </Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Request Time</Text>
              <Text style={styles.jobDetailReight}>
                {  moment(selectedService.serviceRequestDate).format('MMMM Do YYYY, h:mm:ss a')}
              </Text>
              <Text style={styles.jobDetailLeft}>Request Delivery Time</Text>
              <Text style={styles.jobDetailReight}>
                {'Minimum 15 mins maximum 45 mins'}
              </Text>
            </View>
          </View>

          <TouchableOpacity
            style={styles.MainDetailwrapper}
            onPress={() => {
              if (serviceStatusName.toLowerCase() != 'initiated') {
                Actions.traceOrder({
                  requestAddressLat: parseFloat(requestAddressLat),
                  requestAddressLong: parseFloat(requestAddressLong),
                  Order: selectedService,
                });
              } else {
                Alert.alert(
                  'Alert!',
                  'Order Is not yet assigned , so you cannot track the order',
                );
              }
            }}>
            <View style={styles.rowSelectType}>
              <Icon name="pin" color="#E9EBEE" style={{color: '#f48004'}} />
              <Text style={styles.rowHeadingList}>
                Address Details (Click To track order)
              </Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Request Delivery address</Text>
              <Text style={styles.jobDetailReight}>
                {selectedService.requestLocationAddress
                  ? selectedService.requestLocationAddress
                  : 'empty'}
              </Text>
            </View>
          </TouchableOpacity>

          {selectedService.serviceTypeName == 'Laundry' ? (
            <View />
          ) : (
            <View style={styles.MainDetailwrapper}>
              <View style={styles.rowSelectType}>
                <Icon
                  name="md-images"
                  color="#E9EBEE"
                  style={{color: '#f48004'}}
                />
                <Text style={styles.rowHeadingList}>
                  Request Problem Images
                </Text>
              </View>

              <View style={styles.rowList}>
                <View style={{flexDirection: 'row'}}>
                  {selectedService.imagesSourceArray.map(el => {
                    debugger;
                    let image = el.replace(
                      'http://localhost',
                      'http://10.0.2.2',
                    );
                    let Image_Http_URL = {uri: image};
                    return (
                      <Image
                        key={el}
                        source={Image_Http_URL}
                        style={{
                          height: 50,
                          width: 50,
                          borderRadius: 10,
                          overflow: 'hidden',
                          marginRight: 10,
                          borderColor: 'black',
                          borderWidth: 1,
                        }}
                      />
                    );
                  })}
                  {selectedService.imagesSourceArray.length == 0 ? (
                    <Text style={{margin: 10}}>No Images Given</Text>
                  ) : (
                    <View />
                  )}
                </View>
              </View>
            </View>
          )}

        

          <View
            style={{
              flex: 1,
              backgroundColor: '#fff',
              margin: 20,
              borderColor: '#f48004',
              borderWidth: 3,
              borderRadius: 10,
              overflow: 'hidden',
            }}>
               <View style={styles.rowSelectType}>
              <Icon name="md-card" color="#E9EBEE" style={{color: '#f48004'}} />
              <Text style={styles.rowHeadingList}>Services Requested</Text>
            </View>
            <List>
              {selectedService.serviceList.map(item => (
                <ListItem
                  key={item.serviceID}
                  avatar
                  style={{
                    backgroundColor: '#fff',
                    borderBottomWidth: 1,
                    borderBottomColor: '#f48004',
                    marginLeft: 0,
                  }}>
                  {/* <Left style={{ paddingLeft: 5, alignItems: 'center', justifyContent: 'center' }}>
                    <Thumbnail small source={imagess} />
                  </Left> */}
                  <Body style={{borderWidth: 0, borderBottomWidth: 0}}>
                    <Text style={{fontWeight: 'bold'}}>
                      {item.handy_Services.serviceName}
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{fontSize: 12, margin: 5}}
                        note
                        numberOfLines={1}>{` Rs ${
                        item.handy_Services.serviceCharges
                      } / ${item.handy_Services.groc_Units.unitName} `}</Text>
                      <Text
                        style={{fontSize: 12, margin: 5, marginRight: 20}}
                        note
                        numberOfLines={1}>{`Qty ${item.quantity}`}</Text>
                    </View>
                    {/* <View style={{ flexDirection: 'row' }}>
                      <Text style={{ marginTop: 5, }} note numberOfLines={1}>{` Rs ${item.handy_Services.serviceCharges}/item`}</Text>
                      <Text style={{ marginTop: 5, }} note numberOfLines={1}>{`Qty ${item.quantity}`}</Text>
                    </View> */}
                  </Body>
                  <Right style={{borderWidth: 0}}>
                    <Button transparent>
                      <Text>Rs {item.serviceTotalCharges}</Text>
                    </Button>
                  </Right>
                </ListItem>
              ))}
            </List>
            <Text
              style={{
                alignSelf: 'center',
                margin: 5,
                color: 'green',
              }}>{`Service Chnarges : Rs. ${serviceCharges} /day`}</Text>
          </View>

          <View style={styles.MainDetailwrapper}>
            <View style={styles.rowSelectType}>
              <Icon name="md-card" color="#E9EBEE" style={{color: '#f48004'}} />
              <Text style={styles.rowHeadingList}>Bill</Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Total bill</Text>
              <Text style={styles.jobDetailReight}>
                {selectedService.serviceTotalCharges} -/RS
              </Text>
            </View>
          </View>

          {serviceReEstimatedTotalCharges == '0' ? (
            <View />
          ) : (
            <View
              style={{
                height: this.state.height / 10,
                backgroundColor: 'rgba(255,255,255,0.3)',
                flexDirection: 'row',
                fontFamily: 'Helvica',
              }}>
              <View
                style={{
                  flex: 5,
                  backgroundColor: 'transparent',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}>
                {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 18,
                    fontFamily: 'Helvica',
                  }}>
                  Final Total :
                </Text>
              </View>
              <View
                style={{
                  flex: 5,
                  backgroundColor: 'transparent',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text style={{fontSize: 16}}>
                  Rs. {serviceReEstimatedTotalCharges}
                </Text>
              </View>
            </View>
          )}

          
            {this._renderBottomText(serviceStatusName,selectedService)}
         
        </ScrollView>
        {this.renderRatingModal()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  thumbnailDiv: {
    //flex: 2.5,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    //marginTop: 10,
    margin: 10,
    borderWidth: 3,
    alignSelf: 'center',
    borderColor: '#f48004',
    backgroundColor: '#E9EBEE',
    width: width * 0.9,
    borderRadius: 10,
    //backgroundColor: 'green'
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 30,
  },
  datecontainer: {
    backgroundColor: 'green', //'#E9EBEE',
    borderRadius: 10,
    margin: 10,
    marginTop: 100,
  },
  jobDetailReight: {
    width: width * 0.7,
    color: 'green',
    marginTop: 5,
    marginLeft: 20,
    fontWeight: 'bold',
  },
  jobDetailLeft: {color: 'black', marginTop: 5, marginLeft: 20},
  MainDetailwrapper: {
    borderWidth: 3,
    borderColor: '#f48004',
    backgroundColor: '#E9EBEE',
    width: width * 0.9,
    margin: 10,
    borderRadius: 10,
    alignSelf: 'center',
    padding: 10,
  },
  rowSelectType: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
    marginTop: 10,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  StatusBorder: {
    borderWidth: 3,
    borderColor: 'orange',
    width: width * 0.9,
    height: height * 0.75,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#E9EBEE',
  },
  listItemText: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
    width: width * 0.5,
  },
  rowMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: width * 0.9,
    height: height * 0.1,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  rowIcon: {
    color: '#f48004',
    margin: 20,
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    marginRight: 5,
    marginLeft: 10,
    fontSize: 16,
    color: '#f48004',
    fontFamily: 'sens-serif',
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  button: {
    marginTop: 3,
    marginLeft: 5,
    marginBottom: 5,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    height: 45,
    width: 150,
    borderRadius: 25,
    borderWidth: 1.5,
    borderColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 13,
    color: 'black',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

const mapStateToProps = ({auth}) => {
  const {grocServiceChrges, handyServiceCharges, adverstisementsList} = auth;
  return {
    user: auth.user,
    handyServiceCharges,
  };
};
export default connect(
  mapStateToProps,
  {
    ServiceRequestUpdateForm,
    UpdateNotificationData,
    addServiceRequest,
  },
)(ServiceDetails);
//export default RequestDetail;

Array.prototype.sum = function(prop) {
  var total = 0;
  for (var i = 0, _len = this.length; i < _len; i++) {
    total += this[i][prop];
  }
  return total;
};
