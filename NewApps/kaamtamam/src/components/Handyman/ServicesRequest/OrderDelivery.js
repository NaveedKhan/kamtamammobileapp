import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import {Item, Input, Icon} from 'native-base';
import {CustomHeaderWithText} from '../../Grocessry/Common/Header';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const greyColor = '#A0A1A5';


class OrderDelivery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: 0,
      longitude: 0,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
      marker: {latitude: 0, longitude: 0},
    };
  }
  componentDidMount() {
    debugger;
    Geolocation.getCurrentPosition(
      position => {
        console.log(position);
        this.setState({
          latitude: position.coords.latitude,//33.6641233,//position.coords.latitude,
          longitude: position.coords.longitude//73.0505619//position.coords.longitude,
        });

      //  {latitude: 33.6641233, longitude: 73.0505619}
        this.setState({
          marker: {
            latitude: position.coords.latitude,//33.6641233,//position.coords.latitude,
            longitude: position.coords.longitude//73.0505619//position.coords.longitude,
          },
        });
      },
      error => {
        console.log(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 15000},
    );
  }

  render() {
    const {latitude, longitude} = this.state;
    return (
      <View>
        <CustomHeaderWithText text="Order Delivery" />
        <ScrollView>
          <View style={styles.StatusBorder}>
            {latitude > 0 && longitude > 0 ? (
              <MapView
              mapType={"standard"}
              showsCompass={true}
              showsMyLocationButton={true}
              showsTraffic={true}
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                onPress={e => {
                  debugger;
                  this.setState({
                    latitude: e.nativeEvent.coordinate.latitude,
                    longitude: e.nativeEvent.coordinate.longitude,
                    marker: e.nativeEvent.coordinate,
                  })
                }
                }
                region={{
                  latitude: latitude,//33.6641233,
                  longitude: longitude,//73.0505619,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121,
                }}>
                <Marker
                  coordinate={this.state.marker}
                  title={'restaurantName'}
                  description={'address'}
                />
              </MapView>
            ) : null}
          </View>
          <View style={styles.orderMain}>
            <Text style={styles.orderHeading}>Order No:</Text>
            <Text style={styles.orderValue}>TDMY2</Text>
          </View>
          <View style={{alignSelf: 'center', marginTop: 10}}>
            <TouchableOpacity style={styles.button}>
              <Text style={styles.buttonText}>Cancel Order</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button}>
              <Text style={styles.buttonText}>Mark Complete</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  StatusBorder: {
    borderBottomWidth: 3,
    borderBottomColor: 'orange',
    width: width * 1,
    height: height * 0.5,
  },

  orderMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 15,
  },
  orderHeading: {
    color: 'black',
    alignSelf: 'center',
    marginTop: 5,
    fontWeight: 'bold',
    fontSize: 16,
    fontFamily: 'sens-serif',
  },
  orderValue: {
    color: 'grey',
    alignSelf: 'center',
    marginTop: 5,
    fontSize: 16,
    fontFamily: 'sens-serif',
    paddingLeft: 10,
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    borderColor: '#f48004',
    backgroundColor: '#f48004',
    borderWidth: 2,
    padding: 10,
    borderRadius: 20,
    width: width * 0.5,
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

export default OrderDelivery;