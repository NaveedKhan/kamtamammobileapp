import React, {Component} from 'react';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Geocode from 'react-geocode';
import {Button} from 'native-base';
import {
  CustomHeaderWithText,
  CustomHeader,
} from '../../Grocessry/Common/Header';
import {
  ServiceRequestUpdateForm,
  SignupUpdateFormData,
  UpdateUserFormData,
  UpdateUserAddressDatail,
} from '../../../actions';
import {ActionConst, Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {Spinner} from '../../common';
//import { Button } from '../../common';
//https://medium.com/@princessjanf/react-native-maps-with-direction-from-current-location-ab1a371732c2
//https://github.com/bramus/react-native-maps-directions

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 30,
    left: 0,
    right: 0,
    bottom: 0,
    height: height * 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 90,
    left: 0,
    right: 0,
    bottom: 0,
  },
});

Geocode.setApiKey('AIzaSyCZ_GeN6VIBOgZqe9mZ568ygvB8eUNuSbc');
Geocode.setLanguage('en');
Geocode.enableDebug();
// userCurrentLat: 33.6641, userCurrentLong: 73.0506, : '',
class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // latitude: 0,
      // longitude: 0,
      // latitudeDelta: LATITUDE_DELTA,
      // longitudeDelta: LONGITUDE_DELTA,
      // marker: {latitude: 0, longitude: 0},
      isLocationSelectedFromMap: false,
      IsAddressFinding: false,
    };
  }

  renderButton() {
    var me = this;
    const {isLocationSelectedFromMap} = this.state;
    if (this.state.IsAddressFinding) {
      return (
        <View
          style={{
            width: 250,
            height: 50,
            borderRadius: 30,
            margin: 10,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            borderColor: '#f48004',
            borderWidth: 1.7,
          }}>
          <Spinner />
        </View>
      );
    } else {
      return (
        <TouchableOpacity
          style={{
            width: 250,
            height: 50,
            borderRadius: 30,
            margin: 10,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            borderColor: '#f48004',
            borderWidth: 1.7,
          }}
          onPress={() => {
            if(isLocationSelectedFromMap)
            {
              me.props.UpdateUserAddressDatail({
                prop: 'isCurrentLocatonSelected',
                value: false,
              });
            }
            Actions.pop();
          }}>
          <Text style={{fontSize: 15, color: '#f48004', fontWeight: 'bold'}}>
            DONE{' '}
          </Text>
        </TouchableOpacity>
      );
    }
  }

  render() {
    var me = this; //userCurrentAddress
    const {isNewUser, isGrocery} = this.props;
    const {isLocationSelectedFromMap} = this.state;
    const {
      userCurrentAddressDetail,
      userDeliveryAddressDetail,
      isCurrentLocatonSelected,
    } = this.props;
    const {
      userCurrentLat,
      userCurrentLong,
      userCurrentAddress,
    } = userCurrentAddressDetail;
    const {
      requsetLat,
      requetLong,
      requestSearchAddress,
    } = userDeliveryAddressDetail;
    debugger;
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <CustomHeader
              leftIcon={true}
              searchBar={true}
              headerHeight={{height: 130}}
              disabled={true}
              inputHeight={{height: 80}}
              rightIcon={true}
              inputPressed={() => Actions.searchArea()}
              searchBarValue={
                isCurrentLocatonSelected && !isLocationSelectedFromMap
                  ? userCurrentAddress
                  : requestSearchAddress
              }
            />
          </View>
          <View style={styles.container}>
            <MapView
              mapType={'standard'}
              showsCompass={true}
              showsUserLocation={true}
              loadingEnabled={true}
              cacheEnabled={true}
              moveOnMarkerPress={true}
              loadingBackgroundColor={'#000'}
              toolbarEnabled={true}
              // showsTraffic={true}
              onMarkerDragEnd={e => {
                me.setState({IsAddressFinding: true});
                let lats = e.nativeEvent.coordinate.latitude;
                let longs = e.nativeEvent.coordinate.longitude;
                Geocode.fromLatLng(lats, longs).then(
                  response => {
                    const address = response.results[0].formatted_address;
                    let userDeliveryAddressDetail = {
                      requsetLat: lats,
                      requetLong: longs,
                      requestSearchAddress: address,
                    };
                    console.log(lats, longs);

                    me.props.UpdateUserAddressDatail({
                      prop: 'userDeliveryAddressDetail',
                      value: userDeliveryAddressDetail,
                    });
                    me.setState({
                      IsAddressFinding: false,
                      isLocationSelectedFromMap: true,
                    });

                    console.log(address);
                  },
                  error => {
                    me.setState({IsAddressFinding: false});
                    console.error(error);
                  },
                );
              }}
              provider={PROVIDER_GOOGLE}
              style={styles.map}
              onPress={e => {
                debugger;
                me.setState({IsAddressFinding: true});
                let lats = e.nativeEvent.coordinate.latitude;
                let longs = e.nativeEvent.coordinate.longitude;
                Geocode.fromLatLng(lats, longs).then(
                  response => {
                    const address = response.results[0].formatted_address;
                    let userDeliveryAddressDetail = {
                      requsetLat: lats,
                      requetLong: longs,
                      requestSearchAddress: address,
                    };
                    console.log(lats, longs);

                    me.props.UpdateUserAddressDatail({
                      prop: 'userDeliveryAddressDetail',
                      value: userDeliveryAddressDetail,
                    });
                    me.setState({
                      IsAddressFinding: false,
                      isLocationSelectedFromMap: true,
                    });

                    console.log(address);
                  },
                  error => {
                    me.setState({IsAddressFinding: false});
                    console.error(error);
                  },
                );
              }}
              region={{
                latitude: isCurrentLocatonSelected
                  ? userCurrentLat
                  : requsetLat,
                longitude: isCurrentLocatonSelected
                  ? userCurrentLong
                  : requetLong,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              }}>
              <Marker
                draggable={true}
                coordinate={{
                  latitude:
                    isCurrentLocatonSelected && !isLocationSelectedFromMap
                      ? userCurrentLat
                      : requsetLat,
                  longitude:
                    isCurrentLocatonSelected && !isLocationSelectedFromMap
                      ? userCurrentLong
                      : requetLong,
                }}
                title={'Delivery Point'}
                description={'address'}
              />
            </MapView>
          </View>
        </View>

        {this.renderButton()}
      </View>
    );
  }
}

const mapStateToProps = ({handyServices, auth, userAddress}) => {
  const {
    services,
    servicestype,
    serviceRequestModel,
    error,
    loading,
    RequestServiceType,
    RequestTime,
    RequestServicesList,
    RequestImages,
    RequestDescription,
    RequestAddress,
    maintainanceSelected,
    installationSelected,
  } = handyServices;

  const {
    userCurrentAddressDetail,
    userDeliveryAddressDetail,
    isCurrentLocatonSelected,
  } = userAddress;
  debugger;
  return {
    services,
    servicestype,
    serviceRequestModel,
    error,
    loading,
    RequestServiceType,
    RequestTime,
    RequestServicesList,
    RequestImages,
    RequestDescription,
    RequestAddress,
    userCurrentAddressDetail,
    userDeliveryAddressDetail,
    isCurrentLocatonSelected,
    maintainanceSelected,
    installationSelected,
  };
};
export default connect(
  mapStateToProps,
  {
    ServiceRequestUpdateForm,
    SignupUpdateFormData,
    UpdateUserFormData,
    UpdateUserAddressDatail,
  },
)(Map);
//export default Map;
