import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  Alert,
  ScrollView,
  TouchableHighlight,
  PermissionsAndroid,
  Modal,
} from 'react-native';
import {
  Item,
  Input,
  CheckBox,
  Icon,
  Thumbnail,
  List,
  ListItem,
} from 'native-base';
import {CustomHeaderWithText} from '../../Grocessry/Common/Header';
//import {CustomFooterTab} from '../../Grocessry/Common/Footer';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {ServiceRequestUpdateForm} from '../../../actions';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

const statuseElec = [
  'Wiring',
  'Board Fitting',
  'Fan Fiiting',
  'Bulb Fitting',
  'Buttons Installation',
];
const plumberServices = [
  'Gizer fitting',
  'tooti fitting',
  'pipe leakage',
  'Tanke cleaning',
  'Shank Installation',
  'other',
];
const homeShifting = [
  'shifting only',
  'shifting+installation',
  'packing/unpacking',
];
const cleanerservices = [
  'Floor cleaning',
  'Finail cleaning',
  'spray',
  'dish washing',
  'full house cleaning',
  'Dust cleaning',
];
const painterServices = [
  'Room Painting',
  'Full House Painting',
  'Door Painting',
  'Furniture Polish',
];
const laundryServices = [
  'Cloth cleaning',
  'Cloth coloring',
  'Jacket',
  'Coat',
  'Sweater',
  'Blanket',
];

class ServiecRequestStep1 extends Component {
  constructor(props) {
    super(props);
    debugger;
    this.state = {
      selectedServies: [],
      modalVisible: false,
      details: '',
      detailsFor: '',
    };
  }

  UpdateFormData(propName, preVal) {
    debugger;
    // var preVal = this.props.maintainanceSelected;
    this.props.ServiceRequestUpdateForm({prop: propName, value: !preVal});
  }

  // async requestCameraPermission() {
  //   try {
  //     const granted = await PermissionsAndroid.request(
  //       PermissionsAndroid.PERMISSIONS.CAMERA,
  //       {
  //         title: 'Kaam Tamam App Camera Permission',
  //         message:
  //           'Kaam Tamam App needs access to your camera ' +
  //           'so you can take problem pictures.',
  //         buttonNeutral: 'Ask Me Later',
  //         buttonNegative: 'Cancel',
  //         buttonPositive: 'OK',
  //       },
  //     );
  //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //       console.log('You can use the camera');
  //     } else {
  //       console.log('Camera permission denied');
  //     }
  //   } catch (err) {
  //     console.warn(err);
  //   }
  // }

  // async requestStoragePermission() {
  //   try {
  //     const granted = await PermissionsAndroid.request(
  //       PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
  //       {
  //         title: 'Kaam Tamam App Camera Permission',
  //         message:
  //           'Kaam Tamam needs access to your camera ' +
  //           'so you can take  problem pictures.',
  //         buttonNeutral: 'Ask Me Later',
  //         buttonNegative: 'Cancel',
  //         buttonPositive: 'OK',
  //       },
  //     );
  //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //       console.log('You can use the camera');
  //     } else {
  //       console.log('Camera permission denied');
  //     }
  //   } catch (err) {
  //     console.warn(err);
  //   }
  // }

  componentDidMount() {
    const {serviceTypeID, services} = this.props;
    let selectSerArray = [];

    let selectedServies = services.filter(
      sa => sa.serviceTypeID == serviceTypeID,
    );
    selectedServies.forEach(function(item) {
      let ervModel = {
        ServiceID: item.serviceID,
        ServiceName: item.serviceName,
        ServiceDetail: item.serviceDetail,
        ImageName: item.imageName,
        Unit: item.groc_Units.unitName,
        serviceCharges: item.serviceCharges,
        ServiceTotalCharges: item.serviceCharges,
        Available: item.isAvailable,
        ServiceTypeName: item.handy_Servicetype.srviceTypeName,
        IsSelected: false,
        Quantity: 1,
        DiscountPrice: item.discountPrice,
        ServiceMinCharges: item.serviceMinCharges,
      };

      selectSerArray.push(ervModel);
      //copy.push(item*item);
    });

    this.setState({selectedServies: selectSerArray});
  }

  onSelectServicesCheckbox(serviceID, isSelected) {
    debugger;
    let {selectedServies} = this.state;
    var checkedService = selectedServies.find(aa => aa.ServiceID == serviceID);
    checkedService.IsSelected = !isSelected;

    debugger;
    this.setState({selectedServies: selectedServies});
  }

  addProduct(itemselected) {
    // var selectedProdcutMiodel = {
    //   ProductName: itemselected.itemName,
    //   ProductId: itemselected.itemID,
    //   ProductCount: 1,
    //   ProdcutPriceEach: itemselected.itemPrice,
    //   TotalPrice: itemselected.itemPrice,
    //   UnitName: itemselected.groc_Units.unitName
    // }
    debugger;
    //var orderincart = this.props.orderInCartList;
    const {selectedServies} = this.state;
    var result = selectedServies.find(
      aa => aa.ServiceID == itemselected.ServiceID,
    );
    if (result != undefined) {
      var quantity = result.Quantity;
      result.Quantity = quantity + 1;
      let isdiscount =
        result.DiscountPrice == 0 ||
        result.DiscountPrice == result.serviceCharges
          ? false
          : true;
      let ishavingminproce =
        result.ServiceMinCharges == 0 ||
        result.ServiceMinCharges == result.serviceCharges
          ? false
          : true;
      let totalcharges = isdiscount
        ? result.DiscountPrice * result.Quantity
        : result.serviceCharges * result.Quantity;
      result.ServiceTotalCharges = totalcharges;
    }
    //else {
    //orderincart.push(selectedProdcutMiodel);
    //}

    this.setState({selectedServies: selectedServies});
    // this.props.AddItem(orderincart)
  }

  deleteProduct(itemselected) {
    debugger;
    const {selectedServies} = this.state;
    var result = selectedServies.find(
      aa => aa.ServiceID == itemselected.ServiceID,
    );
    if (result != undefined) {
      var quantity = result.Quantity;
      if (quantity > 1) {
        result.Quantity = quantity - 1;
        result.ServiceTotalCharges = result.serviceCharges * result.Quantity;
      }

      this.setState({selectedServies: selectedServies});
    }

    // var orderincart = this.props.orderInCartList;
    // var result = orderincart.find(aa => aa.ProductId == itemselected.itemID);
    // if (result != undefined) {
    //   if (result.ProductCount > 1) {
    //     var index = orderincart.indexOf(itemselected);
    //     orderincart.splice(index, 1);
    //     this.setState({ numbersOfItems: this.state.numbersOfItems - 1 })
    //   }
    //   else {
    //     result.ProductCount = (result.ProductCount - 1);
    //     result.TotalPrice = result.ProdcutPriceEach * result.ProductCount;
    //   }
    //   this.props.RemoveItem(orderincart);
    // }

    // if (this.state.numbersOfItems > 0) {
    //   this.setState({ numbersOfItems: this.state.numbersOfItems - 1 })

    // }
  }

  ShowInvalidFormAlert(errorMessage) {
    debugger;
    Alert.alert(
      'InComplete Data!',
      errorMessage,
      [
        {
          text: 'OK',
          onPress: () => {
            //checkoutSucess(dispatch,baseModel.data)
            //this.setState({ selectedOrderToComplete: el })
            //  this.setState({ modalVisible: true })
          },
        },
      ],
      {cancelable: true},
    );
  }

  onClickNext() {
    debugger;
    const {
      serviceTypeID,
      maintainanceSelected,
      installationSelected,
    } = this.props;
    let {selectedServies} = this.state;
    var checkedService = selectedServies.filter(aa => aa.IsSelected == true);

    var errorMessage = '';
    if (
      this.props.selectServices != 'Laundry' &&
      this.props.selectServices != 'Home Shifting'
    ) {
      if (!maintainanceSelected && !installationSelected) {
        errorMessage =
          'Please Select One option from maintaince & installation \n';
      }
    }

    if (checkedService.length == 0) {
      errorMessage += ', Please Select at least one service from the list.';
    } else {
      checkedService.forEach(function(el, index) {
        debugger;
        let isdiscount =
          el.DiscountPrice == 0 || el.DiscountPrice == el.serviceCharges
            ? false
            : true;
        let totalcharges = isdiscount
          ? el.DiscountPrice * el.Quantity
          : el.serviceCharges * el.Quantity;
        el.ServiceTotalCharges = totalcharges;
      });
    }

    if (errorMessage.length > 5) {
      this.ShowInvalidFormAlert(errorMessage);
    } else {
      if (this.props.selectServices == 'Laundry') {
        this.props.ServiceRequestUpdateForm({
          prop: 'isImmediateRequest',
          value: false,
        });
        this.props.ServiceRequestUpdateForm({
          prop: 'RequestServiceType',
          value: serviceTypeID,
        });
        this.props.ServiceRequestUpdateForm({
          prop: 'selectedServies',
          value: checkedService,
        });
        Actions.PlumberRequestProceedPage({imagesourcearray: []});
      } else {
        this.props.ServiceRequestUpdateForm({
          prop: 'isImmediateRequest',
          value: true,
        });
        this.props.ServiceRequestUpdateForm({
          prop: 'RequestServiceType',
          value: serviceTypeID,
        });
        this.props.ServiceRequestUpdateForm({
          prop: 'selectedServies',
          value: checkedService,
        });
        Actions.requestDescription();
      }
    }
  }
  renderRatingModal() {
    debugger;
    const {details, detailsFor} = this.state;
    let arr = [];
    var ismany = details.indexOf(',');
    if (!ismany) {
      arr.push(details);
    } else {
      arr = details.split(',');
    }
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({modalVisible: false, modalButtonLoading: false});
        }}>
        <View style={styles.datecontainer}>
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              borderRadius: 15,
              justifyContent: 'flex-end',
              alignItems: 'center',
              alignSelf: 'flex-end',
              margin: 10,
              borderColor: '#fff',
              borderWidth: 1,
            }}
            onPress={() => this.setState({modalVisible: false})}>
            <Icon name="close" style={{color: '#fff'}} />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 15,
              margin: 5,
              alignSelf: 'center',
              color: 'white',
            }}>{`Services Provided In ${this.state.detailsFor}`}</Text>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 5,
            }}
          />

          <View
            style={{
              margin: 10,
              backgroundColor: '#fff',
              borderColor: 'gray',
              borderWidth: 1,
              padding: 5,
              borderRadius: 5,
            }}>
            <List>
              {arr.map(el => (
                <ListItem key={el}>
                  <Text style={{margin: 5}}>{`- ${el}`}</Text>
                </ListItem>
              ))}
            </List>
          </View>
        </View>
      </Modal>
    );
  }
  render() {
    const {selectedServies} = this.state;
    debugger;
    return (
      <View style={{flex: 1}}>
        <CustomHeaderWithText
          text={
            this.props.selectServices
              ? ` ${this.props.selectServices} Step1`
              : 'ahm ahm'
          }
        />
        <ScrollView scrollEnabled>
          <View style={{alignSelf: 'center', margin: 10, alignItems: 'center'}}>
            {this.props.selectServices == 'Laundry' ? (
              <View>
                <Text note style={{margin: 5, marginLeft: 10, marginRight: 10}}>
                  Your Laudry Order Will be deliverd to you in 72 hours
                </Text>
              </View>
            ) : this.props.selectServices == 'Home Shifting' ? (
              <View />
            ) : (
              <View style={styles.RequestBorder}>
                <View style={styles.rowSelectType}>
                  <Icon
                    name="radio-button-on"
                    color="#E9EBEE"
                    style={{color: '#f48004'}}
                  />
                  <Text style={styles.rowHeadingList}>
                    What is the nature of your problem?
                  </Text>
                </View>
                <TouchableOpacity
                  style={styles.rowList}
                  onPress={() =>
                    this.UpdateFormData(
                      'maintainanceSelected',
                      this.props.maintainanceSelected,
                    )
                  }>
                  <Text style={styles.rowText}>Maintenance</Text>
                  {this.props.maintainanceSelected ? (
                    <Icon
                      name="checkmark-circle"
                      style={{color: 'green', marginRight: 20}}
                    />
                  ) : (
                    <View />
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.rowList}
                  onPress={() =>
                    this.UpdateFormData(
                      'installationSelected',
                      this.props.installationSelected,
                    )
                  }>
                  <Text style={styles.rowText}>Installation</Text>
                  {this.props.installationSelected ? (
                    <Icon
                      name="checkmark-circle"
                      style={{color: 'green', marginRight: 20}}
                    />
                  ) : (
                    <View />
                  )}
                </TouchableOpacity>
              </View>
            )}

            <View
              style={{
                flex: 1,
                borderWidth: 3,
                borderColor: '#f48004',
                backgroundColor: '#E9EBEE',
                margin: 10,
                width: width * 0.9,
                //  marginLeft: 20,
                //  marginRight: 20,
                borderRadius: 10,
                paddingTop: 0,
              }}>
              <View style={styles.rowSelectType}>
                <Icon
                  name="radio-button-on"
                  color="#E9EBEE"
                  style={{color: '#f48004'}}
                />
                <Text style={styles.rowHeadingList}>Select Services </Text>
              </View>

              <View style={{margin: 0, backgroundColor: '#E9EBEE'}}>
                {selectedServies.map(x => {
                  // let imageass = x.ImageName.replace(
                  //   'http://localhost',
                  //   'http://10.0.2.2',
                  // );
                  // let assImage_Http_URL = {uri: imageass};
                  let isdiscount =
                    x.DiscountPrice == 0 || x.DiscountPrice == x.serviceCharges
                      ? false
                      : true;
                  let ishavingminproce =
                    x.ServiceMinCharges == 0 ||
                    x.ServiceMinCharges == x.serviceCharges
                      ? false
                      : true;
                  let totalchargesTag = isdiscount
                    ? x.DiscountPrice * x.Quantity
                    : x.serviceCharges * x.Quantity;
                  var pricetage = ishavingminproce
                    ? `Rs.  ${x.ServiceMinCharges} - ${x.serviceCharges} / ${
                        x.Unit
                      }`
                    : `Rs. ${x.serviceCharges} / ${x.Unit}`;
                  return (
                    <View
                      style={{
                        borderBottomColor: 'orange',
                        borderBottomWidth: 1,
                        backgroundColor: '#E9EBEE',
                      }}
                      key={x.ServiceID}>
                      <View style={{marginBottom: 5}}>
                        <TouchableOpacity
                          style={{
                            flexDirection: 'row',
                            alignItems: 'flex-start',
                            marginTop: 10,
                            marginBottom: 0,
                            justifyContent: 'flex-start',
                            backgroundColor: 'transparent',
                            //height: 60,
                            padding: 5,
                            marginLeft: 1,
                          }}>
                          {/* place to keep name and image of prodcut */}
                          <View
                            style={{
                              flex: 5,
                              flexDirection: 'column',
                              backgroundColor: 'transparent',
                            }}>
                            {/* <View style={{ marginRight: 0, height: 50, width: 50, borderRadius: 25, overflow: 'hidden' }}>
                            <Image source={assImage_Http_URL} style={{ height: 50, width: 50, borderRadius: 25 }} />
                          </View> */}

                            <Text
                              style={{
                                color: 'green',
                                //alignSelf: 'center',
                                marginLeft: 5,
                                fontWeight: 'bold',
                                //width: width * 0.45,
                                fontSize: 13,
                                fontFamily: 'sens-serif',
                                backgroundColor: 'transparent',
                              }}>
                              {x.ServiceName}
                            </Text>
                            <TouchableOpacity
                              onPress={() =>
                                this.setState({
                                  modalVisible: true,
                                  detailsFor: x.ServiceName,
                                  details: x.ServiceDetail,
                                })
                              }
                              style={{
                                marginTop: 3,
                                marginLeft: 5,
                                marginBottom: 5,
                                backgroundColor: 'transparent',
                                width: 70,
                                borderRadius: 20,
                                borderWidth: 1.5,
                                borderColor: '#f48004',
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}>
                              <Text
                                style={{
                                  fontSize: 10,
                                  textAlign: 'center',
                                  color: 'red',
                                  fontWeight: 'bold',
                                }}>
                                View Details
                              </Text>
                            </TouchableOpacity>
                          </View>

                          <TouchableOpacity
                            onPress={() =>
                              this.onSelectServicesCheckbox(
                                x.ServiceID,
                                x.IsSelected,
                              )
                            }
                            style={{
                              flex: 5,
                              flexDirection: 'row',
                              backgroundColor: 'transparent',
                              justifyContent: 'flex-end',
                              padding: 5,
                            }}>
                            <View>
                              <Text
                                style={{
                                  alignSelf: 'flex-end',
                                  backgroundColor: 'transparent',
                                  fontSize: 12,
                                  color: isdiscount ? 'red' : 'black',
                                  textDecorationLine: isdiscount
                                    ? 'line-through'
                                    : 'none',
                                }}>
                                {' '}
                                {pricetage}{' '}
                              </Text>

                              {isdiscount ? (
                                <Text
                                  style={{
                                    alignSelf: 'flex-end',
                                    marginRight: 3,
                                    fontSize: 11,
                                    marginBottom: 5,
                                    color: 'green',
                                  }}>
                                  {`Rs. ${x.DiscountPrice} / ${x.Unit}`}{' '}
                                </Text>
                              ) : (
                                <View />
                              )}
                            </View>

                            <CheckBox
                              checked={x.IsSelected}
                              color="#f48004"
                              style={{marginRight: 10, alignSelf: 'center'}}
                              onPress={() =>
                                this.onSelectServicesCheckbox(
                                  x.ServiceID,
                                  x.IsSelected,
                                )
                              }
                            />
                          </TouchableOpacity>
                        </TouchableOpacity>
                        {/* <View style={{ flexDirection: 'row' }}> */}
                        {/* <View>
                              <TouchableHighlight onPress={() => this.addProduct(x)}
                                style={{
                                   marginTop: 0, marginBottom: 5, backgroundColor: 'transparent',
                                  width: 70, borderRadius: 20, borderWidth: 1.5, borderColor: '#000', alignItems: 'center', justifyContent: 'center'
                                }} >
                                <Text style={{ fontSize: 10,textAlign:'center', color: '#000', fontWeight: 'bold' }}>View Details</Text>
                              </TouchableHighlight>
                            </View> */}
                        <View>
                          {/* {isdiscount
                              ?
                              <Text style={{ alignSelf: 'flex-end', marginRight: 30, fontSize: 11, marginBottom: 5, color: 'green' }}>{` ${x.DiscountPrice} / ${x.Unit}`} </Text>
                              :
                              <View />
                            } */}
                        </View>
                        {/* </View> */}
                      </View>

                      <View>
                        <View
                          style={{
                            backgroundColor: 'transparent',
                            alignItems: 'center',
                            justifyContent: 'space-evenly',
                            flexDirection: 'row',
                            margin: 10,
                            marginTop: 0,
                          }}>
                        
                        <TouchableHighlight
                            onPress={() => this.deleteProduct(x)}
                            style={{
                              height: 40,
                              marginTop: 5,
                              marginBottom: 5,
                              backgroundColor: 'transparent',
                              width: 70,
                              borderRadius: 20,
                              borderWidth: 1.5,
                              borderColor: '#000',
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}>
                            <Text
                              style={{
                                fontSize: 17,
                                color: '#000',
                                fontWeight: 'bold',
                              }}>
                              -
                            </Text>
                          </TouchableHighlight>
                                              
                          <Text
                            style={{textAlign: 'center', fontWeight: 'bold'}}>
                            {' '}
                            {x.Quantity}{' '}
                          </Text>
                          <TouchableHighlight
                            onPress={() => this.addProduct(x)}
                            style={{
                              height: 40,
                              marginTop: 5,
                              marginBottom: 5,
                              backgroundColor: 'transparent',
                              width: 70,
                              borderRadius: 20,
                              borderWidth: 1.5,
                              borderColor: '#000',
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}>
                            <Text
                              style={{
                                fontSize: 17,
                                color: '#000',
                                fontWeight: 'bold',
                              }}>
                              +
                            </Text>
                          </TouchableHighlight>
                         
                        </View>
                        <Text
                          style={{
                            alignSelf: 'flex-end',
                            margin: 5,
                            fontSize: 13,
                            marginRight: 10,
                            color: 'green',
                          }}>{`Total Price : Rs.${totalchargesTag}`}</Text>
                      </View>
                    </View>
                  );
                })}
              </View>
            </View>

            <TouchableOpacity
              style={{
                marginBottom: 20,
                width: 250,
                height: 50,
                borderRadius: 30,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#f48004',
                borderWidth: 1.7,
              }}
              onPress={() => this.onClickNext()}>
              <Text
                style={{fontSize: 18, color: '#f48004', fontWeight: 'bold'}}>
                NEXT
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        {this.renderRatingModal()}
        {/* <CustomFooterTab /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  datecontainer: {
    backgroundColor: 'green', //'#E9EBEE',
    borderRadius: 10,
    margin: 20,
    marginTop: 100,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 30,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  RequestBorder: {
    borderWidth: 3,
    borderColor: '#f48004',
    backgroundColor: '#E9EBEE',
    width: width * 0.9,
    margin: 10,
    borderRadius: 10,
  },
  listItemText: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  rowList: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 10,
    borderBottomColor: 'white',
    width: width * 0.85,
    borderBottomWidth: 1,
    alignSelf: 'center',
    padding: 10,
  },
  rowIcon: {
    color: '#f48004',
    margin: 20,
    marginTop: 10,
    marginBottom: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    marginRight: 10,
    marginLeft: 10,
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  rowSelectType: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
    marginTop: 10,
  },
  rowText: {
    color: 'grey',
    alignSelf: 'center',
    marginTop: 5,
    marginLeft: 20,
  },
  DiscriptionBorder: {
    borderWidth: 3,
    borderColor: '#E9EBEE',
    backgroundColor: 'white',
    margin: 10,
    borderRadius: 10,
    padding: 10,
    paddingTop: 0,
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    backgroundColor: '#f48004',
    padding: 10,
    borderRadius: 20,
    width: width * 0.35,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

const mapStateToProps = ({handyServices}) => {
  const {
    services,
    servicestype,
    serviceRequestModel,
    error,
    loading,
    RequestServiceType,
    RequestTime,
    RequestServicesList,
    RequestImages,
    RequestDescription,
    RequestAddress,
    maintainanceSelected,
    installationSelected,
  } = handyServices;
  debugger;
  return {
    services,
    servicestype,
    serviceRequestModel,
    error,
    loading,
    RequestServiceType,
    RequestTime,
    RequestServicesList,
    RequestImages,
    RequestDescription,
    RequestAddress,
    maintainanceSelected,
    installationSelected,
  };
};
export default connect(
  mapStateToProps,
  {ServiceRequestUpdateForm},
)(ServiecRequestStep1);

//export default ServiecRequestStep1;
