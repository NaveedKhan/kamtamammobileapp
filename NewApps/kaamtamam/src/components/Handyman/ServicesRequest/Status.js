import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import { Item, Input, Icon } from 'native-base';
import { CustomHeaderWithText } from '../../Grocessry/Common/Header';
import ServicesHistoryCard from './ServicesHistoryCard';
//import {CustomFooterTab} from '../../Grocessry/Common/Footer';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class Status extends Component {
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
  }
  renderRow(iconName, headingTitle, headingValue, detail) {
    return (
      <View style={styles.rowMain}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <Icon name={iconName} style={styles.rowIcon} />
          <Text style={styles.rowHeadingList}>{headingTitle}:</Text>
          <Text style={styles.listItemText}>{headingValue}</Text>
        </View>
        {detail && (
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <Text style={{ color: 'grey', paddingRight: 5 }}>Details</Text>
            <Icon name="arrow-forward" style={{ fontSize: 16, color: 'grey' }} />
          </TouchableOpacity>
        )}
      </View>
    );
  }
  render() {
    return (
      <View>
        <CustomHeaderWithText text="STATUS" />
        <ScrollView>
          <View style={{ alignSelf: 'center', margin: 10 }}>
            <Text style={styles.recentJobHeading}>RECENT JOBS</Text>


            <ServicesHistoryCard />
            <ServicesHistoryCard />
            <ServicesHistoryCard />




          </View>
        </ScrollView>
        {/* <CustomFooterTab /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 30,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  StatusBorder: {
    borderWidth: 3,
    borderColor: 'orange',
    width: width * 0.9,
    height: height * 0.68,
    margin: 10,
    borderRadius: 10,
  },
  listItemText: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  rowMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: width * 0.8,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  rowIcon: {
    color: '#f48004',
    margin: 20,
    marginTop: 10,
    marginBottom: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    margin: 20,
    marginRight: 10,
    marginLeft: 10,
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    backgroundColor: '#f48004',
    padding: 10,
    borderRadius: 20,
    width: width * 0.35,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

export default Status;
