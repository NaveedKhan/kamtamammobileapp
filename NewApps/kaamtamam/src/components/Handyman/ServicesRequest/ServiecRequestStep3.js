import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,Alert,
  Image, Modal,
  ScrollView, TouchableHighlight
} from 'react-native';
import { Item, Input, Icon, Picker, Spinner } from 'native-base';
//import ModalDatePicker from 'react-native-datepicker-modal';
import CalendarPicker from 'react-native-calendar-picker';
import { CustomHeaderWithText } from '../../Grocessry/Common/Header';
//import {CustomFooterTab} from '../Common/Footer';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import moment from "moment";
import firebase from 'firebase';
import { ServiceRequestUpdateForm, UpdateUserAddressDatail } from '../../../actions';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class ServiecRequestStep3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      selectedStartDate: null,
      selectedHour: '12',
      selectedMin: '00',
      selecteddayHalf: 'AM',
      currentDate: new Date(),
      markedDate: moment(new Date()),
      firebaseLoading: false
    };
    this.renderClender = this.renderClender.bind(this)
    this.onDateChange = this.onDateChange.bind(this);
  }

  UpdateFormData(propName, preVal) {
    debugger;
    // var preVal = this.props.maintainanceSelected;
    this.props.ServiceRequestUpdateForm({ prop: propName, value: !preVal })
  }
  onDateChange(date) {
    debugger;
    this.setState({
      selectedStartDate: date,
    });
  }
  renderClender() {
    const today = this.state.currentDate
    const todaydatetime = moment(today).format('MMMM Do YYYY');

    const { selectedStartDate, selectedHour, selectedMin, selecteddayHalf } = this.state;
    let currentime = new Date().toLocaleTimeString();
    const startDate = selectedStartDate ? new Date(selectedStartDate).toDateString() : todaydatetime;
    let selectedtime = ` ${selectedHour.padStart(2, '0')}:${selectedMin.padStart(2, '0')}:00 ${selecteddayHalf}`;
    let datetimeString = `${startDate} ${selectedtime}`;
    let hoursCount = [];
    let minsCount = [];
    for (i = 1; i <= 12; i++) {
      hoursCount.push(String(i).padStart(2, '0'));
    }

    for (i = 0; i <= 60; i++) {
      minsCount.push(String(i).padStart(2, '0'));
    }
    //
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({ modalVisible: false })
        }}>
        <View style={styles.datecontainer}>
          <View style={{ backgroundColor: 'transparent', width: width * 0.95, overflow: 'hidden' }}>
            <TouchableOpacity style={{
              width: 30, height: 30, borderRadius: 15, justifyContent: 'flex-end', alignItems: "center",
              alignSelf: "flex-end", margin: 10, borderColor: "#fff", borderWidth: 1
            }}
              onPress={() => {
                this.props.ServiceRequestUpdateForm({ prop: 'isImmediateRequest', value: true })
                this.setState({ modalVisible: false })
              }}>
              <Icon name="close" style={{ color: '#fff' }} />
            </TouchableOpacity>
            <View style={{ margin: 10 }}>
              <Text style={{ fontSize: 13, fontFamily: 'ariel', color: 'green' }}>{`SELECTED DATE :  ${datetimeString}`}</Text>
            </View>
          </View>

          {/* <TouchableOpacity style={{ width: 30, height: 30, borderRadius: 15, justifyContent: 'flex-end', alignItems: "center", alignSelf: "flex-end", margin: 10, borderColor: "black", borderWidth: 1 }}
            onPress={() => this.setState({ modalVisible: false })}>
            <Icon name="add" />
          </TouchableOpacity> */}
          <CalendarPicker onDateChange={this.onDateChange} />


          <Text style={{ fontSize: 15, margin: 5, alignSelf: 'center' }}>Please Select Time of delivery!</Text>
          <View style={{ flexDirection: 'row', backgroundColor: 'gray' }}>
            <Picker
              mode="dropdown"
              placeholder="Hours"
              style={{ height: 50, width: 70 }}
              iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
              headerStyle={{ backgroundColor: "#b95dd3" }}
              headerBackButtonTextStyle={{ color: "#fff" }}
              headerTitleStyle={{ color: "#fff" }}
              selectedValue={this.state.selectedHour}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ selectedHour: itemValue })
              }
            >

              {hoursCount.map(el =>
                <Picker.Item label={String(el)} value={String(el)} key={el} />
              )}
            </Picker>

            <Picker
              mode="dropdown"
              placeholder="Mins"
              style={{ height: 50, width: 70, borderColor: 'black', borderWidth: 1 }}
              iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
              headerStyle={{ backgroundColor: "#b95dd3" }}
              headerBackButtonTextStyle={{ color: "#fff" }}
              headerTitleStyle={{ color: "#fff" }}
              selectedValue={this.state.selectedMin}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ selectedMin: itemValue })
              }
            >

              {minsCount.map(el =>
                <Picker.Item label={String(el)} value={String(el)} key={el} />
              )}
            </Picker>

            <Picker
              mode="dropdown"
              style={{ height: 50, width: 90 }}
              iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
              headerStyle={{ backgroundColor: "#b95dd3" }}
              headerBackButtonTextStyle={{ color: "#fff" }}
              headerTitleStyle={{ color: "#fff" }}
              selectedValue={this.state.selecteddayHalf}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ selecteddayHalf: itemValue })
              }
            >
              <Picker.Item label="AM" value="AM" />
              <Picker.Item label="PM" value="PM" />
            </Picker>
          </View>
          <TouchableOpacity
            onPress={() => {
              debugger;
              if (selectedtime == '' || selectedtime == '00:00:00 AM') {
                this.setState({ modalVisible: false })
              } else {
                this.props.ServiceRequestUpdateForm({ prop: 'ServiceRequestScheduleDateString', value: datetimeString })
                this.props.ServiceRequestUpdateForm({ prop: 'isImmediateRequest', value: false })
                this.setState({ modalVisible: false })
              }

              //this.UpdateFormData('orderScheduleDate', datetimeString)
            }}
            style={{
              width: 250,
              height: 50,
              borderRadius: 30,
              margin: 10,
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              borderColor: '#f48004',
              borderWidth: 1.7
            }} >
            <Text style={{ fontSize: 15, color: '#fff', fontWeight: 'bold' }} > CONFIRM THE DATE </Text>
          </TouchableOpacity>
          {/* <View style={{ margin: 10 }}>
            <Text>{`SELECTED DATE :  ${datetimeString}`}</Text>
          </View> */}

        </View>
      </Modal>
    );
  };

  setModalVisible(visible) {
    debugger;
    this.setState({ modalVisible: visible });
  }

  addDataIntoFirebase = () => {
    debugger;
    // const { Order } = this.props;
    var me = this;
    //me.pressNext('ddadadadadasd');
    //return;
    var UserModel = {
      userLat: 33.9976884, OrderId: 0,
      ServiceID: 0,
      userLong: 71.4695614,
    }
    // userCurrentLat: 33.9976884, userCurrentLong: 71.4695614, 
    

    this.setState({ firebaseLoading: true });
    const dbref = firebase.database().ref('DriverLocation');
    var newLocref = dbref.push();
    var key = newLocref.getKey();
    newLocref.set(UserModel)
      .then(() => {
        debugger;
        me.setState({ firebaseLoading: false });
        me.pressNext(key);

      })
      .catch((error) => {
        debugger;
        var err= String(error) == '' ? 'no error ' : String(error);
        Alert.alert('error',err);
        // me.setState({ isLoading: false });
        debugger;
        Alert.alert('alert!', 'something went wrong!');
        //  orderAssignedFail(dispatch, error);
        //me.showToast("an error occured, try again");
      });
  }

  pressNext() {
    debugger;
    const {ServiceRequestScheduleDateString} = this.props;
    this.props.ServiceRequestUpdateForm({ prop: 'ServiceRequestScheduleDateString', value: ServiceRequestScheduleDateString })
    Actions.RequestDetail({ imagesourcearray: this.props.imagesourcearray })

  }

  render() {
    const today = this.state.currentDate;
    const dateAfter72Hours = moment(today).add(3, 'days').format('MMMM Do YYYY, h:mm:ss a');

    const { selectedStartDate, selectedHour, selectedMin, selecteddayHalf, firebaseLoading } = this.state;
    //const startDate = selectedStartDate ? new Date(selectedStartDate).toDateString() : '';

    const { userCurrentAddressDetail, userDeliveryAddressDetail, isCurrentLocatonSelected ,ServiceRequestScheduleDateString} = this.props;
    const { userCurrentLat, userCurrentLong, userCurrentAddress } = userCurrentAddressDetail;
    const { requsetLat, requetLong, requestSearchAddress } = userDeliveryAddressDetail;

    const { selectedApp } = this.props;

    // let selectedtime = selectedHour == '' || selectedMin == '' || selecteddayHalf == '' ? '' :
    //   ` ${selectedHour.padStart(2, '0')}:${selectedMin.padStart(2, '0')}:00 ${selecteddayHalf}`;
   // let datetimeString = `${startDate} ${selectedtime}`;
    return (
      <View style={{ flex: 1 }}>
        <CustomHeaderWithText text="FINAL STEP" />
        <ScrollView scrollEnabled>
          <View style={{ alignSelf: 'center', margin: 10 }}>
            <View style={styles.RequestBorder} pointerEvents={selectedApp == "Laundry" ? "none" : "auto"}>
              <View style={styles.rowSelectType}>
                <Icon name="time" color="#E9EBEE" style={{ color: '#f48004' }} />
                <Text style={styles.rowHeadingList}>Select time of job</Text>
              </View>
              <TouchableOpacity style={styles.rowList} onPress={() => 
                this.props.ServiceRequestUpdateForm({ prop: 'isImmediateRequest', value: true })
                }>
                <Text style={styles.rowText}>Need Immediate Service</Text>
                {
                  this.props.isImmediateRequest
                    ?
                    <Icon
                      name="checkmark-circle"
                      style={{ color: 'green', marginRight: 20 }}
                    /> :
                    <View />
                }

              </TouchableOpacity>
              <TouchableOpacity
                style={styles.rowList}
                onPress={() => {
                  this.setState({ modalVisible: !this.state.modalVisible })
                }}>
                <Text style={styles.rowText}>{
                  selectedApp == "Laundry" ? dateAfter72Hours :
                    !this.props.isImmediateRequest ? ServiceRequestScheduleDateString : "Schedule My Service"}</Text>
                {
                  !this.props.isImmediateRequest
                    ?
                    <Icon
                      name="md-calendar"
                      style={{ color: 'green', marginRight: 20 }}
                    /> :
                    <View />
                }

              </TouchableOpacity>
            </View>

            <View style={styles.AddressDetail}>
              <View style={styles.rowSelectType}>
                <Icon name="pin" color="#E9EBEE" style={{ color: '#f48004' }} />
                <Text style={styles.rowHeadingList}>Add address details</Text>
              </View>
              <TouchableOpacity
                onPress={() =>
                  // console.log('ohooo')
                  this.props.UpdateUserAddressDatail({ prop: 'isCurrentLocatonSelected', value: true })
                }
                style={styles.rowList}>
                <View style={{ flexDirection: 'column' }}>
                  <Text
                    style={{
                      color: 'green',
                      marginTop: 5,
                      marginLeft: 20,
                    }}>
                    Your Current Location address
                  </Text>
                  <Text
                    style={{
                      width: width * 0.4,
                      color: 'grey',
                      marginTop: 5,
                      marginLeft: 20,
                    }}>
                    {userCurrentAddress}
                    {/* 4820 Police line i10/4 islamabad near karachi compney */}
                  </Text>
                </View>
                {isCurrentLocatonSelected ? <Icon
                  name="checkmark-circle"
                  style={{ color: 'green', marginRight: 20 }}
                /> : <View />}

              </TouchableOpacity>
        
              <TouchableOpacity
                onPress={() => {
                  Actions.Map()
                }}
                style={styles.rowList} >
                <Text style={styles.rowText}>Add another delivery address from Map</Text>
                {!isCurrentLocatonSelected ? <Icon
                  name="checkmark-circle"
                  style={{ color: 'green', marginRight: 20 }}
                /> : <View />}
              </TouchableOpacity>





              <View style={{ backgroundColor: 'transparent', flex: 6.7, flexDirection: 'row' }}>
                <View style={{ flex: 8, backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' }} >
                  <View style={{ margin: 10, marginBottom:5,backgroundColor: 'gray', borderRadius: 5, padding: 10 }}>
                    <Text numberOfLines={4} style={{ fontFamily: 'sens-serif', fontSize: 15, color: 'orange' }}>
                      {isCurrentLocatonSelected ? userCurrentAddress :
                        requestSearchAddress == '' ? 'Press plus Icon to add address ' : requestSearchAddress}
                    </Text>
                  </View>
                </View>

              </View>
              <Text style={{alignSelf:'center',color:'red',fontSize:12,marginBottom:5}}>Your delivery address</Text>
            </View>
            {/* 
            <TouchableOpacity style={styles.button} onPress={() => this.pressNext()}>
              <Text style={{ color: 'white', fontSize: 20 }}>Next</Text>
            </TouchableOpacity> */}
            {firebaseLoading ? <Spinner /> :
              <TouchableOpacity style={{
                width: 250,
                height: 50,
                borderRadius: 30,
                margin: 10,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                borderColor: '#f48004',
                borderWidth: 1.7
              }} onPress={() => this.pressNext()}>
                <Text style={{ fontSize: 15, color: '#f48004', fontWeight: 'bold' }} > FINAL STEP </Text>
              </TouchableOpacity>
            }
          </View>
          {this.renderClender()}
        </ScrollView>
        {/* <CustomFooterTab /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  datecontainer: {
    backgroundColor: 'orange',//'#E9EBEE',
    borderRadius: 10,
    margin: 10,
    marginTop: 60,
  },
  container: {
    backgroundColor: 'white',
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    // marginVertical: spacing[1],
    // marginHorizontal: spacing[0],
    justifyContent: 'center',
    borderRadius: 2,
    height: 50,
  },
  placeholderText: {
    color: 'gray',
  },
  text: {
    width: '100%',
    // paddingHorizontal: spacing[1],
    // paddingVertical: spacing[0],
    // fontFamily: 'Montserrat',
    // fontSize: fontSize.medium,
    color: 'gray',
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },

  //margin: 10,
  //    borderColor:'#f48004',
  // borderWidth:3,
  //borderRadius: 10,
  //padding:5
  RequestBorder: {
    borderWidth: 3,
    borderColor: '#f48004',
    backgroundColor: '#E9EBEE',
    width: width * 0.9,
    height: height * 0.27,
    margin: 10,
    borderRadius: 10,
  },
  listItemText: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  rowList: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 10,
    borderBottomColor: 'white',
    width: width * 0.85,
    borderBottomWidth: 1,
    alignSelf: 'center',
    padding: 10,
  },
  rowIcon: {
    color: '#f48004',
    margin: 20,
    marginTop: 10,
    marginBottom: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    marginRight: 10,
    marginLeft: 10,
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  rowSelectType: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
    marginTop: 10,
  },
  rowText: {
    color: 'grey',
    alignSelf: 'center',
    marginTop: 5,
    marginLeft: 20,
  },
  AddressDetail: {
    borderWidth: 3,
    borderColor: '#f48004',
    backgroundColor: '#E9EBEE',
    width: width * 0.9,
    //height: height * 0.46,
    margin: 10,
    borderRadius: 10,
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    borderColor: '#f48004',
    borderWidth: 1,
    backgroundColor: '#f48004',
    width: width * 0.7,
    height: height * 0.09,
    justifyContent: 'center',
    marginTop: 10,
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 30,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});


const mapStateToProps = ({ handyServices, userAddress, auth }) => {

  const { services, servicestype, serviceRequestModel, error, loading, RequestServiceType, RequestTime,
    RequestServicesList, RequestImages, RequestDescription, RequestAddress,ServiceRequestScheduleDateString,
    maintainanceSelected, installationSelected, isImmediateRequest, requsetLat, requetLong, requestSearchAddress
  } = handyServices;
  const { selectedApp } = auth;
  const { userCurrentAddressDetail, userDeliveryAddressDetail, isCurrentLocatonSelected } = userAddress;
  debugger;
  return {
    services, servicestype, serviceRequestModel, error, loading, RequestServiceType, RequestTime,
    RequestServicesList, RequestImages, RequestDescription, RequestAddress, selectedApp,
    userCurrentAddressDetail, userDeliveryAddressDetail, isCurrentLocatonSelected,ServiceRequestScheduleDateString,
    maintainanceSelected, installationSelected, isImmediateRequest, requsetLat, requetLong, requestSearchAddress
  };
};
export default connect(mapStateToProps, { ServiceRequestUpdateForm, UpdateUserAddressDatail })(ServiecRequestStep3);
//export default ServiecRequestStep3;
