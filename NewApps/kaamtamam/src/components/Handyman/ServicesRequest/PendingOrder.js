import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import {Item, CheckBox, Icon} from 'native-base';
import {CustomHeaderWithText} from '../../Grocessry/Common/Header';
//import {CustomFooterTab} from '../../Grocessry/Common/Footer';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class PendingOrder extends Component {
  constructor(props) {
    super(props);
  }
  renderRow(iconName, headingTitle, headingValue, detail) {
    return (
      <View style={styles.rowMain}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <Icon name={iconName} style={styles.rowIcon} />
          <Text style={styles.rowHeadingList}>{headingTitle}:</Text>
          <Text style={styles.listItemText}>{headingValue}</Text>
        </View>
        {detail && (
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <Text style={{color: 'grey', paddingRight: 5}}>Details</Text>
            <Icon name="arrow-forward" style={{fontSize: 16, color: 'grey'}} />
          </TouchableOpacity>
        )}
      </View>
    );
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <CustomHeaderWithText text="Pending Orders" />
        <ScrollView>
          {this.renderRow(
            (iconName = 'paper'),
            (headingTitle = 'Order No'),
            (headingValue = 'PLM04'),
          )}
          {this.renderRow(
            (iconName = 'pin'),
            (headingTitle = 'Order Time'),
            (headingValue = '12:45  19/10/2019'),
          )}
          <View style={styles.orderMain}>
            <Text style={styles.orderHeading}>Item List</Text>
          </View>
          <View
            style={{
              flexDirection: 'column',
              borderColor: '#f48004',
              borderWidth: 2,
              borderRadius: 5,
              width: width * 0.9,
              //   height: height * 0.4,
              alignSelf: 'center',
              padding: 10,
              marginTop: 10,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}>
              <Text style={styles.ItemListRowText}>Banana</Text>
              <Text style={styles.ItemListRowText}>2KG</Text>
              <CheckBox
                checked={true}
                color="#f48004"
                style={styles.ItemListRowText}
              />
              <Text style={styles.ItemListRowText}>Rs. 100</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}>
              <Text style={styles.ItemListRowText}>Banana</Text>
              <Text style={styles.ItemListRowText}>2KG</Text>
              <CheckBox
                checked={true}
                color="#f48004"
                style={styles.ItemListRowText}
              />
              <Text style={styles.ItemListRowText}>Rs. 100</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}>
              <Text style={styles.ItemListRowText}>Banana</Text>
              <Text style={styles.ItemListRowText}>2KG</Text>
              <CheckBox
                checked={true}
                color="#f48004"
                style={styles.ItemListRowText}
              />
              <Text style={styles.ItemListRowText}>Rs. 100</Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: width * 0.6,
              justifyContent: 'space-between',
              alignSelf: 'center',
              borderBottomColor: 'grey',
              borderBottomWidth: 1,
              padding: 15,
            }}>
            <Text style={{fontSize: 16, color: 'grey', fontWeight: 'bold'}}>
              Total
            </Text>
            <Text style={{fontSize: 16, color: 'grey', fontWeight: 'bold'}}>
              Rs. 840
            </Text>
          </View>
          <View style={{alignSelf: 'center', marginTop: 10}}>
            <TouchableOpacity style={styles.button}>
              <Text style={styles.buttonText}>Start Delivery</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        {/* <CustomFooterTab /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 30,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  rowMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: width * 0.87,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  rowIcon: {
    color: '#f48004',
    margin: 20,
    marginTop: 10,
    marginBottom: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    margin: 20,
    marginRight: 10,
    marginLeft: 10,
    fontSize: 16,
    color: 'grey',
    fontFamily: 'sens-serif',
  },
  ItemListRowText: {
    fontSize: 18,
    color: '#f48004',
    fontWeight: 'bold',
    marginLeft: 5,
  },

  orderMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 15,
  },
  orderHeading: {
    color: 'grey',
    alignSelf: 'center',
    marginTop: 5,
    fontWeight: 'bold',
    fontSize: 16,
    fontFamily: 'sens-serif',
  },
  orderValue: {
    color: 'grey',
    alignSelf: 'center',
    marginTop: 5,
    fontSize: 16,
    fontFamily: 'sens-serif',
    paddingLeft: 10,
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    borderColor: '#f48004',
    backgroundColor: '#f48004',
    borderWidth: 2,
    padding: 10,
    borderRadius: 20,
    width: width * 0.5,
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

export default PendingOrder;
