import React, { Component } from 'react';
import { Image, Text } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Geocode from 'react-geocode';
import { View } from 'native-base';
import { connect } from 'react-redux';
import { ServiceRequestUpdateForm,UpdateUserAddressDatail } from '../actions';
import { Actions } from 'react-native-router-flux';
import { CustomHeaderWithText } from './Grocessry/Common/Header';


Geocode.setApiKey('AIzaSyCZ_GeN6VIBOgZqe9mZ568ygvB8eUNuSbc');
Geocode.setLanguage('en');
Geocode.enableDebug();
const homePlace = {
  description: 'Home',
  geometry: { location: { lat: 48.8152937, lng: 2.4597668 } },
};
const workPlace = {
  description: 'Work',
  geometry: { location: { lat: 48.8496818, lng: 2.2940881 } },
};

class SearchArea extends Component {
  render() {
    let me = this;
    return (
      <View style={{ flex: 1 }}>
        <CustomHeaderWithText text="Find Location" />

        <GooglePlacesAutocomplete
          placeholder="Search"
          minLength={1} // minimum length of text to search
          autoFocus={false}
          returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
          listViewDisplayed="auto" // true/false/undefined
          fetchDetails={true}
          renderDescription={row => row.description} // custom description render
          onPress={(data, details = null) => {
            debugger;
            console.log(data);
            console.log(details);
            console.log('latlon', details.geometry);
            Geocode.fromAddress(data.description).then(
              response => {
                const { lat, lng } = response.results[0].geometry.location;
                let userDeliveryAddressDetail = { requsetLat: lat, requetLong: lng, requestSearchAddress: data.description };
                console.log(lat, lng);
                me.props.UpdateUserAddressDatail({ prop: 'isCurrentLocatonSelected', value: false });
                me.props.UpdateUserAddressDatail({ prop: 'userDeliveryAddressDetail', value: userDeliveryAddressDetail });

                Actions.pop();
              },
              error => {
                console.error(error);
              },
            );
          }}
          getDefaultValue={() => {
            return ''; // text input default value
          }}
          query={{
            key: 'AIzaSyDrkNY7kg-7XWRo1Q-6s4zq94QemjGBwMk',
            language: 'en', // language of the results
            types: ['address', 'location', '(regions)'], // default: 'geocode'
            // location: '34.025917, 71.560135',
            components: 'country:pk',
            //strictbounds: true,
          }}
          styles={{
            textInputContainer: {
              width: '100%',
            },
            description: {
              fontWeight: 'bold',
            },
            predefinedPlacesDescription: {
              color: '#1faadb',
            },
          }}
         // currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
         // currentLocationLabel="Current location"
          nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
          GoogleReverseGeocodingQuery={
            {
              // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
            }
          }
          GooglePlacesSearchQuery={{
            // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
            rankby: 'distance',
            type: 'cafe',
          }}
          GooglePlacesDetailsQuery={{
            // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
            fields: 'formatted_address',
          }}
          filterReverseGeocodingByTypes={[
            'locality',
            'administrative_area_level_3',
          ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
        //  predefinedPlaces={[homePlace, workPlace]}
          debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
        />

      </View>
    );
  }
}


const mapStateToProps = ({ handyServices }) => {

  const { services, servicestype, serviceRequestModel, error, loading, RequestServiceType, RequestTime,
    RequestServicesList, RequestImages, RequestDescription, RequestAddress,
    maintainanceSelected, installationSelected, isImmediateRequest, requsetLat, requetLong, requestSearchAddress
  } = handyServices;
  debugger;
  return {
    services, servicestype, serviceRequestModel, error, loading, RequestServiceType, RequestTime,
    RequestServicesList, RequestImages, RequestDescription, RequestAddress,
    maintainanceSelected, installationSelected, isImmediateRequest, requsetLat, requetLong, requestSearchAddress
  };
};
export default connect(mapStateToProps, { ServiceRequestUpdateForm ,UpdateUserAddressDatail})(SearchArea);

 // export default SearchArea; 