import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Modal,
  Alert,
  Platform,
  StatusBar,
  ScrollView,
  PermissionsAndroid,
  Image,
  TouchableHighlight,
  TextInput,
  BackHandler,
} from 'react-native';
import {Icon, Spinner} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {Rating, AirbnbRating} from 'react-native-elements';
import {connect} from 'react-redux';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob'
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {CustomHeaderWithText, CustomHeader} from './Grocessry/Common/Header';
import Carousel from './Grocessry/Carousel';
import {UpdateUserFormData, UpdateNotificationData} from '../actions';
import SelectJobGrid from './SelectJobGrid';
import CustomFooter from './common/CustomFooter';
import {Button} from './common';

import {config} from '../config';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
class SelectJob extends Component {
  constructor(props) {
    super(props);
    //debugger;
    this.state = {
      routName: this.props.navigation.state.routeName,
      isLoading: false,
      JobsListArray: [],
      modalVisible: false,
      reviewComment: '',
      reviewScore: 0,
      checkinServiceName: '',
      apiError: false,
    };
  }

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Kaam Tamam Location Permission',
          message:
            'Kaam Tamam App needs access to your Location ' +
            'so you can place order.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the Location');
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }


  getDataFromAsynStorage(itemname)
  {
    var me = this;
    // me.setState({ isloading: true });
     try {
       const token = AsyncStorage.getItem(itemname).then((token) => {
         debugger;
         if (token != null) {
           let userObj = JSON.parse(token);
           debugger;
           this.props.UpdateUserFormData({ prop: 'GuestUserData', value: userObj });
         } else {
           debugger;
         }
       }).done();
 
     } catch (error) {
       var err= String(error) == '' ? 'no error ' : String(error);
       Alert.alert('error',err);
     //  me.setState({ isloading: false });
       return null;
 
     }
  }



  componentDidMount() {
    const {user} = this.props;
    this.setState({isLoading: true});
    // if (Platform.OS == 'android') {
    //   this.requestCameraPermission();
    // }
    debugger;
    if (user.roleName == 'Guest') {
      this.getDataFromAsynStorage('GuestUserData');
    }

    this.mapSercivesToMenus();
    this.setState({isLoading: false});
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  checkPendingOrders(selectedServicename) {
    debugger;
    var grocOrderSearchModel = {userId: 1};
    const {user} = this.props;
    let userid = user.id;
    //let apiUrl = '';
    const {REST_API} = config;

    this.setState({checkinServiceName: selectedServicename});

    // if (selectedServicename == 'Grocerry') {
    //   apiUrl = `${REST_API.Oders.GetPendingOrdersCount}${userid}`;
    // } else {
    //   apiUrl = `${
    //     REST_API.ServiceRequest.GetPendingHandyServicesCount
    //   }${userid}`;
    // }
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    try {
      const response = axios
        .get(`${REST_API.Oders.GetPendingOrdersCount}${userid}`, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          me.setState({checkinServiceName: ''});
          debugger;
          if (baseModel.success) {
            if (baseModel.count > 0) {
              me.ShowAlert(
                `you cannot request for more than one  ${selectedServicename} services at a time.Please complete the first one and then trya again`,
              );
            } else {
              me.props.UpdateUserFormData({
                prop: 'selectedApp',
                value: selectedServicename,
              });
              me.props.UpdateNotificationData({
                prop: 'totalpendingOrdercount',
                value: baseModel.totalpendingOrdercount,
              });
              Actions.landingPage();
            }
          } else {
            me.ShowAlert(
              `An erro occurred when checking the peding ${selectedServicename} status : error ` +
                baseModel.message,
            );
          }
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({checkinServiceName: ''});
        });
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({checkinServiceName: ''});
      return true;
    }
  }

  checkGuestPendingOrders(selectedServicename) {
    debugger;
  
    const {user,DeviceID} = this.props;
    let userid = user.id;
    //let apiUrl = '';
    const {GuestUserData} = this.props;
    numberArray = [];
    GuestUserData.forEach(function(el, index) {
      numberArray.push(el.phoneNumber);
    });

    var grocOrderSearchModel = {PhoneNumbers: [],DeviceID:DeviceID};

    const {REST_API} = config;

    this.setState({checkinServiceName: selectedServicename});


    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    try {
      const response = axios
        .post(`${REST_API.Oders.GetGuestPendingOrdersCount}`,grocOrderSearchModel, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          me.setState({checkinServiceName: ''});
          debugger;
          if (baseModel.success) {
            if (baseModel.count > 0) {
              me.ShowAlert(
                `you cannot request for more than one  ${selectedServicename} services at a time.Please complete the first one and then trya again`,
              );
            } else {
              me.props.UpdateUserFormData({
                prop: 'selectedApp',
                value: selectedServicename,
              });
              me.props.UpdateNotificationData({
                prop: 'totalpendingOrdercount',
                value: baseModel.totalpendingOrdercount,
              });
              Actions.landingPage();
            }
          } else {
            me.ShowAlert(
              `An erro occurred when checking the peding ${selectedServicename} status : error ` +
                baseModel.message,
            );
          }
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({checkinServiceName: ''});
        });
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({checkinServiceName: ''});
      return true;
    }
  }

  checkGuestPendingServics(selectedServicename, itemId) {
    debugger;

    const {user,DeviceID} = this.props;
    let userid = user.id;
    const {GuestUserData} = this.props;
    numberArray = [];
    GuestUserData.forEach(function(el, index) {
      numberArray.push(el.phoneNumber);
    });
    var handyServiceSearchModel = {ServiceTypeID: itemId,PhoneNumbers: [],DeviceID:DeviceID};
    const {REST_API} = config;
    this.setState({checkinServiceName: selectedServicename});

    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    try {
      const response = axios
        .post(
          REST_API.ServiceRequest.GetGuestPendingHandyServicesCount,
          handyServiceSearchModel,
          configs,
        )
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          me.setState({checkinServiceName: ''});
          debugger;
          if (baseModel.success) {
            if (baseModel.count > 0) {
              me.ShowAlert(
                `you cannot request for more than one  ${selectedServicename} services at a time.Please complete the first one and then trya again`,
              );
            } else {
              me.props.UpdateUserFormData({
                prop: 'selectedApp',
                value: selectedServicename,
              });
              me.props.UpdateNotificationData({
                prop: 'totalpendingOrdercount',
                value: baseModel.totalpendingOrdercount,
              });
              Actions.PlumberRequestLandingPage({
                selectServices: selectedServicename,
                serviceTypeID: itemId,
              });
            }
          } else {
            me.ShowAlert(
              `An erro occurred when checking the peding ${selectedServicename} status : error ` +
                baseModel.message,
            );
          }
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({checkinServiceName: ''});
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({checkinServiceName: ''});
      return true;
    }
  }

  checkPendingServics(selectedServicename, itemId) {
    debugger;

    const {user} = this.props;
    let userid = user.id;
    var handyServiceSearchModel = {ServiceTypeID: itemId, UserID: userid};
    const {REST_API} = config;
    this.setState({checkinServiceName: selectedServicename});

    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    try {
      const response = axios
        .post(
          REST_API.ServiceRequest.GetPendingHandyServicesCount,
          handyServiceSearchModel,
          configs,
        )
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          me.setState({checkinServiceName: ''});
          debugger;
          if (baseModel.success) {
            if (baseModel.count > 0) {
              me.ShowAlert(
                `you cannot request for more than one  ${selectedServicename} services at a time.Please complete the first one and then trya again`,
              );
            } else {
              me.props.UpdateUserFormData({
                prop: 'selectedApp',
                value: selectedServicename,
              });
              me.props.UpdateNotificationData({
                prop: 'totalpendingOrdercount',
                value: baseModel.totalpendingOrdercount,
              });
              Actions.PlumberRequestLandingPage({
                selectServices: selectedServicename,
                serviceTypeID: itemId,
              });
            }
          } else {
            me.ShowAlert(
              `An erro occurred when checking the peding ${selectedServicename} status : error ` +
                baseModel.message,
            );
          }
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({checkinServiceName: ''});
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({checkinServiceName: ''});
      return true;
    }
  }



  mapSercivesToMenus() {
    const {servicestype} = this.props;
    // let selectedServies = servicestype.find(sa => sa.srviceTypeName == "Laundry");
    let JobsList = [];

    JobsList.push({
      serviceID: 0,
      serviceName: 'Grocery',
      imageName: 'http://115.186.182.33:8088/grocerry.png',
    });

    servicestype.forEach(function(el) {
      JobsList.push({
        serviceID: el.serviceTypeId,
        serviceName: el.srviceTypeName,
        serviceTypeName:
          el.srviceTypeName == 'Laundry' ? 'Laundry' : el.srviceTypeName,
        imageName: el.imageURL,
      });
    });

    this.setState({JobsListArray: JobsList});
  }

  ratingCompleted(rating) {
    this.setState({reviewScore: rating});
    console.log('Rating is: ' + rating);
  }
  submitReview() {
    this.setState({modalVisible: false});
  }

  renderRatingModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({modalVisible: false});
        }}>
        <View style={styles.datecontainer}>
          <View style={{margin: 10}} />
          {/* <Text style={{ fontSize: 13, fontFamily: 'ariel', color: 'green' }}>{`SELECTED DATE : `}</Text>
          </View> */}
          {/* <TouchableOpacity style={{ width: 30, height: 30, borderRadius: 15, justifyContent: 'flex-end', alignItems: "center", alignSelf: "flex-end", margin: 10, borderColor: "black", borderWidth: 1 }}
            onPress={() => this.setState({ modalVisible: false })}>
            <Icon name="add" />
          </TouchableOpacity> */}

          <Text
            style={{
              fontSize: 15,
              margin: 5,
              alignSelf: 'center',
              color: 'white',
            }}>
            Please Add Review
          </Text>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 5,
            }}>
            <AirbnbRating
              count={5}
              reviews={[
                'Terrible (1)',
                'Bad (2)',
                'Normal (3)',
                'GOOD (4)',
                'Outstanding (5)',
              ]}
              defaultRating={3}
              onFinishRating={rat => this.ratingCompleted(rat)}
              size={40}
            />
          </View>

          <View
            style={{
              margin: 10,
              backgroundColor: '#fff',
              borderColor: 'gray',
              borderWidth: 1,
              padding: 5,
              borderRadius: 5,
            }}>
            <TextInput
              onChangeText={text => this.setState({reviewComment: text})}
              placeholder="Add any comment (optional but recomended)"
              placeholderTextColor="grey"
              maxLength={200}
              multiline={true}
              editable
              numberOfLines={5}
              style={{height: 150, justifyContent: 'flex-start'}}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.submitReview()}
            style={{
              width: 250,
              height: 50,
              borderRadius: 30,
              margin: 10,
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              borderColor: '#f48004',
              borderWidth: 1.7,
            }}>
            <Text style={{fontSize: 15, color: '#fff', fontWeight: 'bold'}}>
              {' '}
              SUBMIT NOW{' '}
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }

  isPendingServiceAlready(selectedServicename, itemId) {
    const {user} = this.props;
    if (selectedServicename == 'Grocery') {
      if(user.roleName == 'Guest')
      {
        this.checkGuestPendingOrders(selectedServicename, itemId);
      }else
      {
        this.checkPendingOrders(selectedServicename, itemId);
      }
     
    } else {
      if(user.roleName == 'Guest')
      {
        this.checkGuestPendingServics(selectedServicename, itemId);
      }else{
        this.checkPendingServics(selectedServicename, itemId);
      }
      
    }
  }

  ShowAlert(errorMessage) {
    debugger;
    Alert.alert(
      'Alert!',
      errorMessage,
      [
        {
          text: 'OK',
          onPress: () => {
            //checkoutSucess(dispatch,baseModel.data)
            //this.setState({ selectedOrderToComplete: el })
            //this.setState({ modalVisible: true })
          },
        },
      ],
      {cancelable: true},
    );
  }

  onPressItemCategory(name, itemId) {
    debugger;
    this.isPendingServiceAlready(name, itemId);
  }

  _renderGridColums(image, name, itemId) {
    var imagess = require('./Grocessry/Asset/Categoris/BrandedFood.jpg');
    let imageass = image.replace('http://localhost', 'http://10.0.2.2');
    let assImage_Http_URL = {uri: imageass};
    const {checkinServiceName} = this.state;
    return (
      <View style={styles.ListStyleColum}>
        {checkinServiceName == name ? (
          <Spinner />
        ) : (
          <TouchableHighlight
            onPress={() => this.onPressItemCategory(name, itemId)}
            style={styles.ListColumImageStyle}>
            <Image
              source={assImage_Http_URL}
              style={styles.ListColumImageStyle}
            />
          </TouchableHighlight>
        )}

        <Text style={styles.ListColumnTextStyle}>{name}</Text>
      </View>
    );
  }

  _renderGrid(Services) {
    //const { Services } = this.props;
    //debugger;
    const categoriesDB = Services;
    let count = Math.ceil(categoriesDB.length);
    let numberofrows = Math.ceil(count / 2);
    var numberofrowsArray = [];
    for (i = 0; i < numberofrows; i++) {
      numberofrowsArray.push(i);
    }

    return (
      <View style={{flexDirection: 'column', backgroundColor: '#fff'}}>
        {numberofrowsArray.map(el => {
          var firstIndex = el * 2 + 0;
          var secondIndex = el * 2 + 1;

          var name1 = categoriesDB[firstIndex].serviceName;
          var image1 = categoriesDB[firstIndex].imageName;
          var id1 = categoriesDB[firstIndex].serviceID;
          // let ids = el.id;

          var name2 =
            categoriesDB[secondIndex] == undefined
              ? ''
              : categoriesDB[secondIndex].serviceName;
          var image2 =
            categoriesDB[secondIndex] == undefined
              ? ''
              : categoriesDB[secondIndex].imageName;
          var id2 =
            categoriesDB[secondIndex] == undefined
              ? 0
              : categoriesDB[secondIndex].serviceID;
          return (
            <View style={styles.ListStyle} key={el}>
              {this._renderGridColums(image1, name1, id1)}
              {categoriesDB[secondIndex] == undefined ? (
                <View />
              ) : (
                this._renderGridColums(image2, name2, id2)
              )}
            </View>
          );
        })}
      </View>
    );
  }

  render() {
    const routName = this.props.navigation.state.routeName;
    const {adverstisementsList} = this.props;
    debugger;

    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#fff',
        }}>
        {/* //inputPressed={() => Actions.searchProducts()} */}
        <CustomHeader
          leftIcon={true}
          searchBar={false}
          rightIcon={true}
          disabled={true}
        />
        <StatusBar hidden />
        <ScrollView scrollEnabled>
          <View style={{height: 200, backgroundColor: 'transparent'}}>
            <Carousel adverstisementsList={adverstisementsList} />
          </View>
          <View style={{flex: 1, backgroundColor: '#fff'}}>
            {this.state.isLoading && this.state.JobsListArray.length < 1 ? (
              <Spinner />
            ) : (
              this._renderGrid(this.state.JobsListArray)
            )}
          </View>
        </ScrollView>
        {this.renderRatingModal()}
        <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 2 }}>
            <AdMobBanner
              adSize="FULL_BANNER" adUnitID="ca-app-pub-3940256099942544/6300978111" testDevices={[AdMobBanner.simulatorId]} onAdFailedToLoad={error => console.error(error)} />

          </View>
        <CustomFooter isHomePage={true} />
      </View>
    );
  }
}

//export default SelectJob;

const mapStateToProps = ({handyServices, notificationsReducer, auth}) => {
  const {
    grocServiceChrges,
    handyServiceCharges,
    adverstisementsList,
    user,
    GuestUserData,
    DeviceID
  } = auth;
  const {services, servicestype} = handyServices;
  const {totalpendingOrdercount} = notificationsReducer;
  // debugger;
  return {
    services,
    servicestype,
    totalpendingOrdercount,
    adverstisementsList,
    user,
    GuestUserData,
    DeviceID
  };
};
export default connect(
  mapStateToProps,
  {UpdateUserFormData, UpdateNotificationData},
)(SelectJob);

const styles = {
  ListStyle: {
    height: Dimensions.get('window').height / 5.5,
    //backgroundColor:'green',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  yellowback: {
    backgroundColor: 'transparent',
  },
  graywback: {
    backgroundColor: 'transparent',
  },
  brownback: {
    backgroundColor: 'transparent',
  },
  ListStyleColum: {
    flex: 0.5,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginRight: 5,
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 2,
      width: 3,
    },
  },
  ListColumImageStyle: {
    height: Dimensions.get('window').height / 6.7,
    width: Dimensions.get('window').width / 2.4,
    borderRadius: 10,
    borderWidth: 2,
    resizeMode:'contain',
    overflow: 'hidden',
    borderColor: 'gray',
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 2,
      width: 3,
    },
  },
  ListColumnTextStyle: {
    color: '#000',
    fontSize: 14,
    fontWeight: 'bold',
    justifyContent: 'center',
    textAlign: 'center',
    // backgroundColor:'yellow',
    margin: 3,
  },
  datecontainer: {
    backgroundColor: 'green', //'#E9EBEE',
    borderRadius: 10,
    margin: 10,
    marginTop: 100,
  },
};
