import React, { Component } from 'react';
import { View, Text, Button, Dimensions, Image, StatusBar, ScrollView, TouchableHighlight } from 'react-native';
import SelectJobGridColum from './SelectJobGridColum';

class SelectJobGrid extends Component {
    constructor(props) {
        super(props);
      

       
           // "Fruits","Vegetables","Meat","Drinks/Beverages","Retail Items","Baked Items","Beauty Items","Medicines","Branded Food"};


        this.state = {
           // cate: Categories22,
           // productCategories: this.props.productCategories
        }

    };

    _renderCategoriesData() {
   
    }

    render() {
       const  {Services} = this.props;
        debugger
      //  const categories = this.state.cate;
        const categoriesDB = Services;//this.state.ServiceTypes;
        let count = Math.ceil(categoriesDB.length);
        let numberofrows = Math.ceil(count / 2);
        var numberofrowsArray = []
        for (i = 0; i < numberofrows; i++) {
            numberofrowsArray.push(i)
        }
        
        return (
            <View style={{ flexDirection: 'column', backgroundColor: '#fff'}}>

                {
                    numberofrowsArray.map(el => {
                        var firstIndex = ((el * 2) + 0);
                        var secondIndex = ((el * 2) + 1);

                        var name1 = categoriesDB[firstIndex].serviceName;
                        var image1 = categoriesDB[firstIndex].imageName;
                         var id1 = categoriesDB[firstIndex].serviceID
                        // let ids = el.id;

                        var name2 = categoriesDB[secondIndex] == undefined ? "" : categoriesDB[secondIndex].serviceName;
                        var image2 = categoriesDB[secondIndex] == undefined ? "" : categoriesDB[secondIndex].imageName;
                        var id2 = categoriesDB[secondIndex] == undefined ? 0 : categoriesDB[secondIndex].serviceID;
                        return (
                            <View style={styles.ListStyle} key={el}>

                                <SelectJobGridColum image={image1} name={name1} itemId={id1} />

                                {
                                    categoriesDB[secondIndex] == undefined ?
                                        <View /> :
                                        <SelectJobGridColum image={image2} name={name2} itemId={id2} />
                                }
                            </View>
                        )
                    })

                }


            </View>
        );
    }
}

export default SelectJobGrid;

const styles = {
    ListStyle: {
        height: Dimensions.get('window').height / 5.5,
        //backgroundColor:'green',
        flexDirection: 'row',
        alignItems:'center',
        marginTop: 10
    },
    yellowback: {
        backgroundColor: 'transparent'
    },
    graywback: {
        backgroundColor: 'transparent'
    },
    brownback: {
        backgroundColor: 'transparent'
    },
    ListStyleColum: {
        flex: 0.5,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    }
    ,
    ListColumImageStyle: {
        height: Dimensions.get('window').height / 6.7,
        width: Dimensions.get('window').width / 2.4,
        borderRadius: 10,
        borderWidth: 2,
        overflow: 'hidden',
        borderColor: "black",
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 2,
            width: 3
        }
    },
    ListColumnTextStyle: {
        color: '#000',
        fontSize: 14
    }
};