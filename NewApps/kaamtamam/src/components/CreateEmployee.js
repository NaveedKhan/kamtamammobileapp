import React, { Component } from 'react';
import { View, Text,Picker } from 'react-native';
import { EmployeeUpdate,EmployeeCreateButton } from '../actions';
import {connect} from 'react-redux';
import { Card, CardSection, Button, Input } from './common';

class CreateEmployee extends Component {

    onCreatePress()
    {
        debugger;
        const {name,phone,shift} = this.props;
        this.props.EmployeeCreateButton({name,phone,shift: shift || 'monday'});
    }

    render(){
        return(
            <Card>
                <CardSection>
                    <Input
                    label="Name"
                    value={this.props.name}
                    placeholder="your name"
                    onChangeText={text=> this.props.EmployeeUpdate({prop:'name',value:text})}
                    />
                </CardSection>
            
                <CardSection>
                <Input
                    value={this.props.phone}
                label="phone"
                onChangeText={text=> this.props.EmployeeUpdate({prop:'phone',value:text})}
                placeholder="0345694903"
                />
                </CardSection>
            
                <CardSection>
                <Input
                    value={this.props.shift}
                    label="shift"
                    onChangeText={text=> this.props.EmployeeUpdate({prop:'shift',value:text})}
                placeholder="12 pm"
                />
                </CardSection>
            
                <CardSection>
                <Button onPress={()=>this.onCreatePress()}>
                    Create
                </Button>
                </CardSection>
            
            </Card>
        )
    }
}

const mapStateToProps = (state) => {
    const { name, phone, shift } = state.employee;
   
    return { name, phone, shift };
  };

export default connect(mapStateToProps,{ EmployeeUpdate,EmployeeCreateButton })(CreateEmployee);