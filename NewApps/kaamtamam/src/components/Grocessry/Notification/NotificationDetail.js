import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Spinner } from 'native-base';
import { CommonHeader, SubHeader, CustomHeader, CustomHeaderWithText } from '../Common/Header';
import axios from 'axios';
import { connect } from 'react-redux';
import { UpdateNotificationData } from '../../../actions';
import { config } from '../../../config';


 class NotificationDetail extends Component {
  constructor(props) {
    super(props);
    debugger;
    this.state = {
      notification: this.props.notification,
      isLoading: false
    }
  }
  componentDidMount() {
    //MarkNoificationRead
    debugger;
    const { notification } = this.state;
    var users = this.props.user;
    const notModel = { NotificationID: notification.notificationID };
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    };
    const { REST_API } = config;
    this.setState({ isLoading: true });
    var me = this;
    axios.post(REST_API.Notifications.MarkNoificationRead, notModel, configs)
      .then(response => {
        debugger;
        var baseModel = response.data;
        if (baseModel.success) {
          var notList = baseModel.unReadnotificationCount;
          me.props.UpdateNotificationData({ prop: 'unReadNotificationCount', value: notList });
          me.setState({ isLoading: false })

        } else {
          me.setState({ isLoading: true });
        }
        // console.log('api call' + response);
      }).catch(error => {
        debugger;
        var err= String(error) == '' ? 'no error ' : String(error);
        me.setState({ isLoading: true });
        Alert.alert('error',err);
      //  me.setState({ isLoading: true });
        console.log("error", error)
      });
  }

  render() {
    const { detail,
      isRead,
      notificationID,
      notoficationToDriverId,
      notoficationToUserId,
      messageForUser,
      orderID ,
      serviceID} = this.props.notification;
      const {isGroc} = this.props;
      let notificationheading = (orderID==0 && serviceID == 0) ? `Notication` :
      (isGroc) ? `Notification about Grocery Order No ${orderID}` : `Notification about Handyman Service No ${serviceID}` 

    return (
      <Container>
        <CustomHeaderWithText text="Notification Detail" />
        <Content>
          {
            this.state.isLoading ? <Spinner /> :
              <Card>
                <CardItem header>
                  <Text>{notificationheading}</Text>
                </CardItem>
                <CardItem>
                  <Body>
                    <Text note numberOfLines={10}>
                      {messageForUser}
                    </Text>
                  </Body>
                </CardItem>
                <CardItem footer>
                  {/* <Text>{isRead ? "Read" : "un read"}</Text> */}
                </CardItem>
              </Card>
          }
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({ auth }) => {

  return { user: auth.user };
};
export default connect(mapStateToProps, {UpdateNotificationData})(NotificationDetail);
//export default NotificationDetail;