import React, {Component} from 'react';
import {Dimensions, Alert} from 'react-native';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  Spinner,
  Tab,
  Tabs,
  View,
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import {connect} from 'react-redux';
import {config} from '../../../config';
import {
  CommonHeader,
  SubHeader,
  CustomHeader,
  CustomHeaderWithText,
} from '../Common/Header';

class Notification extends Component {
  constructor(props) {
    super(props);
    debugger;
    this.state = {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      isLoading: false,
      notificationsArray: [],
    };
  }

  fetchNotifications() {
    var users = this.props.user;
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    const {REST_API} = config;
    this.setState({isLoading: true});
    var me = this;
    axios
      .get(REST_API.Notifications.GetNotByUserID + `/${users.id}`, configs)
      .then(response => {
        debugger;
        var baseModel = response.data;
        if (baseModel.success) {
          var notList = baseModel.notifications;
          me.setState({isLoading: false, notificationsArray: notList});
        } else {
          me.setState({isLoading: true});
        }
        // console.log('api call' + response);
      })
      .catch(error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        me.setState({isLoading: true});
        Alert.alert('error', err);
        console.log('error', err);
      });
  }

onPressDetail(not,isGroc)
{
   let {notificationsArray} = this.state;
  // var selectedNot = notificationsArray.find(aa=>aa.notificationID == not.notificationID);
  // if(selectedNot)
  // {
  //   selectedNot.isRead = true;
  // }
   not.isRead = true;
   this.setState({notificationsArray:notificationsArray});
 
  Actions.notificationDetail({notification: not,isGroc:isGroc})
}

  componentDidMount() {
    debugger;
    var users = this.props.user;
    if(users.roleName != "Guest")
    {
        this.fetchNotifications();
    }
  }

  render() {
    var users = this.props.user;
    let {notificationsArray} = this.state;
    let grocerryNots = notificationsArray.filter(
      aa => aa.serviceID == 0 && aa.orderID != 0,
    );
    let handymanNots = notificationsArray.filter(
      aa => aa.orderID == 0 && aa.serviceID != 0,
    );
    let globalNots = notificationsArray.filter(
      aa => aa.orderID == 0 && aa.serviceID == 0,
    );
    let globTitle =
      globalNots.length > 0 ? `Global(${globalNots.length})` : 'Global';
    let handyTitle =
      handymanNots.length > 0 ? `Handyman(${handymanNots.length})` : 'Handyman';
    let grocerryTitle =
      grocerryNots.length > 0 ? `Grocery(${grocerryNots.length})` : 'Grocery';

    return (
      <Container>
        <CustomHeaderWithText text="Notifications" hideMenu={true} />

        <Tabs
          tabBarBackgroundColor="#fff"
          tabBarUnderlineStyle={{backgroundColor: '#f48004'}}>
          <Tab
            heading={grocerryTitle}
            tabStyle={{backgroundColor: '#fff'}}
            textStyle={{color: '#666363'}}
            activeTextStyle={{color: '#f48004', fontWeight: 'bold'}}
            activeTabStyle={{backgroundColor: '#fff'}}>
            <Content>
              <List>
                {this.state.isLoading ? (
                  <Spinner />
                ) : grocerryNots.length < 1 ? (
                  <Text numberOfLines={2} style={{textAlign: 'center', marginTop: 15,fontSize:13 }}>
                   {users.roleName != "Guest" ? 'There is no notification for you yet!'
                   :'To get notification please register/signup your account'} 
                  </Text>
                ) : (
                  grocerryNots.map(not => {
                    var backcolor = not.isRead ? 'white' : '#e0e0e0';
                    return (
                      <ListItem
                        onPress={() =>
                          this.onPressDetail(not,true)
                        }
                        key={not.notificationID}
                        thumbnail
                        style={{
                          backgroundColor: backcolor,
                          marginLeft: 0,
                          borderBottomColor: '#f48004',
                          borderBottomWidth: 1,
                        }}>
                        <Body>
                          <Text>Order No {not.orderID}</Text>
                          <Text note numberOfLines={2}>
                            {not.messageForUser}
                          </Text>
                        </Body>
                        <Right>
                          <Button
                            transparent
                            onPress={() =>
                              this.onPressDetail(not,true)                             
                            }>
                            <Text>View</Text>
                          </Button>
                        </Right>
                      </ListItem>
                    );
                  })
                )}
              </List>
            </Content>
          </Tab>
          <Tab
            heading={handyTitle}
            tabStyle={{backgroundColor: '#fff'}}
            textStyle={{color: '#666363'}}
            activeTextStyle={{color: '#f48004', fontWeight: 'bold'}}
            activeTabStyle={{backgroundColor: '#fff'}}>
            <Content>
              <List>
                {this.state.isLoading ? (
                  <Spinner />
                ) : handymanNots.length < 1 ? (
                  <Text numberOfLines={2} style={{textAlign: 'center', marginTop: 15,fontSize:13 }}>
                      {users.roleName != "Guest" ? 'There is no notification for you yet!'
                   :'To get notification please register/signup your account'}
                  </Text>
                ) : (
                  handymanNots.map(not => {
                    var backcolor = not.isRead ? 'white' : '#e0e0e0';
                    return (
                      <ListItem
                        onPress={() =>
                          this.onPressDetail(not,false)  
                        }
                        key={not.notificationID}
                        thumbnail
                        style={{
                          backgroundColor: backcolor,
                          marginLeft: 0,
                          borderBottomColor: '#f48004',
                          borderBottomWidth: 1,
                        }}>
                        <Body>
                          <Text>Request ID {not.serviceID}</Text>
                          <Text note numberOfLines={2}>
                            {not.messageForUser}
                          </Text>
                        </Body>
                        <Right>
                          <Button
                            transparent
                            onPress={() =>
                              this.onPressDetail(not,false)  
                            }>
                            <Text>View</Text>
                          </Button>
                        </Right>
                      </ListItem>
                    );
                  })
                )}
              </List>
            </Content>
          </Tab>
          <Tab
            heading={globTitle}
            tabStyle={{backgroundColor: '#fff'}}
            textStyle={{color: '#666363'}}
            activeTextStyle={{color: '#f48004', fontWeight: 'bold'}}
            activeTabStyle={{backgroundColor: '#fff'}}>
            <Content>
              <List>
                {this.state.isLoading ? (
                  <Spinner />
                ) : globalNots.length < 1 ? (
                  <Text numberOfLines={2} style={{textAlign: 'center', marginTop: 15,fontSize:13 }}>
                       {users.roleName != "Guest" ? 'There is no notification for you yet!'
                   :'To get notification please register/signup your account'}
                  </Text>
                ) : (
                  globalNots.map(not => {
                    var backcolor = not.isRead ? 'white' : '#e0e0e0';
                    return (
                      <ListItem
                        onPress={() =>
                          this.onPressDetail(not)
                        }
                        key={not.notificationID}
                        thumbnail
                        style={{
                          backgroundColor: backcolor,
                          marginLeft: 0,
                          borderBottomColor: '#f48004',
                          borderBottomWidth: 1,
                        }}>
                        <Body>
                          <Text>Notification No {not.notificationID}</Text>
                          <Text note numberOfLines={2}>
                            {not.detail}
                          </Text>
                        </Body>
                        <Right>
                          <Button
                            transparent
                            onPress={() =>
                              this.onPressDetail(not)
                            }>
                            <Text>View</Text>
                          </Button>
                        </Right>
                      </ListItem>
                    );
                  })
                )}
              </List>
            </Content>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

const mapStateToProps = ({auth}) => {
  return {user: auth.user};
};
export default connect(
  mapStateToProps,
  null,
)(Notification);
