import React, { Component } from 'react';
import { View, Text, Button, Image, Dimensions } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { checkoutButton, AddItem, RemoveItem } from '../../../actions';
import { CommonHeader, SubHeader } from '../Common/Header';

class CartList extends Component {
    constructor(props) {
        super(props);
        //this.state = { itemvalue: 0 }
        debugger;
        let numbersOfItems = 0;
        let cart = this.props.item;

        numbersOfItems = cart.ProductCount;

        this.state = {
            numbersOfItems
        }
    }

    addProduct(itemselected) {
        var selectedProdcutMiodel = {
            ProductName: itemselected.itemName,
            ProductId: itemselected.itemID,
            ImageName:itemselected.imageName,
            ProductCount: 1,
            ProdcutPriceEach: itemselected.itemPrice,
            TotalPrice: itemselected.itemPrice
        }
        debugger;
        var orderincart = this.props.orderInCartList;
        var result = orderincart.find(aa => aa.ProductId == itemselected.ProductId);
        if (result != undefined) {
            result.ProductCount = (result.ProductCount + 1);
            result.TotalPrice = result.ProdcutPriceEach * result.ProductCount;
        } else {
            orderincart.push(selectedProdcutMiodel);
        }

        this.setState({ numbersOfItems: this.state.numbersOfItems + 1 })
        this.props.AddItem(orderincart)

    }

    deleteProduct(itemselected) {

        var orderincart = this.props.orderInCartList;
        var result = orderincart.find(aa => aa.ProductId == itemselected.ProductId);
        if (result != undefined) {
            if (result.ProductCount <= 1) {
                var index = orderincart.indexOf(itemselected);
                orderincart.splice(index, 1);
            }
            else {
                result.ProductCount = (result.ProductCount - 1);
                result.TotalPrice = result.ProdcutPriceEach * result.ProductCount;
            }
            this.props.RemoveItem(orderincart);
        }


        if (this.state.numbersOfItems > 0) {
            this.setState({ numbersOfItems: this.state.numbersOfItems - 1 })

        }
    }

    render() {
        const { item } = this.props;
        const {ImageName} = item;
        debugger;
        let imageass = ImageName.replace('http://localhost', 'http://10.0.2.2');
        let assImage_Http_URL = { uri: imageass };
        return (
            <View style={{ flex: 1, borderBottomColor: 'orange', borderColor: '#f48004', borderWidth: 1, borderRadius: 10, marginBottom: 3 }}>
                <View style={{ height: Dimensions.get('window').height / 6, backgroundColor: 'transparent', marginBottom: 5, flexDirection: 'row' }}>
                    <View style={{
                        backgroundColor: 'transparent', flex: 8, margin: 2, flexDirection: 'row', borderRadius: 8, overflow: 'hidden', borderWidth: 1,
                        borderColor: '#f2eded', shadowColor: '#f2eded', shadowOffset: { width: 1, height: 1 }
                    }}>
                        <View style={{ backgroundColor: 'transparent', flex: 4, alignContent: 'center', justifyContent: 'center', marginLeft: 2 }}>
                            <Image source={assImage_Http_URL} style={styles.Imagestyle} />
                        </View>

                        <View style={{ //backgroundColor: '#f2eded',
                            backgroundColor: '#f2eded', flex: 6, flexDirection: 'column', marginLeft: 5
                        }}>
                            <View style={{ backgroundColor: 'transparent', flex: 5, justifyContent: 'center', padding: 5, marginLeft: 10 }} >
                                <Text style={{ fontWeight: 'bold', fontSize: 13 }}>
                                    {item.ProductName}
                                </Text>

                                <Text style={{ fontSize: 12 }}>
                                Rs. {item.ProdcutPriceEach} / {item.UnitName}
                                    </Text>
                            </View>

                            <View style={{ backgroundColor: 'transparent', flex: 5, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }} >
                                 <Button title=" - " style={{ height: 10, marginTop: 5, marginBottom: 5, borderRadius: 5 }} color="#f48004"
                                    onPress={() => this.deleteProduct(item)}
                                />
                                <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 17 }}> {this.state.numbersOfItems} </Text>
                                <Button title=" + " style={{ height: 10, marginTop: 5, marginBottom: 5, borderRadius: 5 }} color="#f48004"
                                    onPress={() => this.addProduct(item)}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={{ backgroundColor: 'transparent', flex: 2, alignItems: 'center', justifyContent: 'center' }} >
                        <Text style={{ fontSize: 15 }}>
                            Rs/- {item.TotalPrice}
                        </Text>
                    </View>
                </View>

            </View>
        );
    }
}

const mapStateToProps = ({ cart }) => {
    const { user, isloading, selectedCategory, selectedProduct, totalItems, orderInCartList } = cart;
    //  debugger;
    return { user, isloading, selectedCategory, selectedProduct, totalItems, orderInCartList };
};
export default connect(mapStateToProps, { checkoutButton, AddItem, RemoveItem })(CartList);
//export default CartList;

const styles = {
    ListStyle: {
        height: Dimensions.get('window').height / 5.5,
        //backgroundColor:'green',
        flexDirection: 'row'
    },
    ListStyleColum: {
        flex: 0.5,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Imagestyle:
    {
        width: 128,
        height: 128,
        // backgroundColor: 'red',
        // borderRadius:( Dimensions.get('window').width/1.8)/2,
        resizeMode: 'contain',
    },
}