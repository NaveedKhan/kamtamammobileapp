import React, { Component } from 'react';


import { View, Text, Button, Dimensions, Image, ScrollView, TouchableHighlight, StatusBar } from 'react-native';
import {
    Content,

    List,
    ListItem,
    Icon,
    Container,
    Left,
    Right,
    Badge
} from "native-base";
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { addClicked, AddItem, RemoveItem, OnItemClick } from '../../../actions';
import { CommonHeader, SubHeader, CustomHeader ,CustomHeaderWithText} from '../Common/Header';
import { CustomFooterTab } from '../Common/Footer';
import CustomFooter from '../../common/CustomFooter';
import { products, productsCat } from '../../../components/common/Cities.json';
import ProductListGrid from './ProductListGrid';

class ProductDetail extends Component {
    constructor(props) {
        super(props);
        let numbersOfItems = 0;
        const cart = this.props.orderInCartList;
        var resultpr = cart.find(aa => aa.ProductId == this.props.item.itemID);
        if (resultpr != undefined) {
            numbersOfItems = resultpr.ProductCount;
        }
        debugger;
        this.state = {
            numbersOfItems: numbersOfItems == undefined ? 0 : numbersOfItems
        }

    }

    addProduct(itemselected) {
        var selectedProdcutMiodel = {
            ProductName: itemselected.itemName,
            ProductId: itemselected.itemID,
            ImageName:itemselected.imageName,
            ProductCount: 1,
            ProdcutPriceEach: itemselected.itemPrice,
            TotalPrice: itemselected.itemPrice
        }
        debugger;
        var orderincart = this.props.orderInCartList;
        var result = orderincart.find(aa => aa.ProductId == itemselected.itemID);
        if (result != undefined) {
            result.ProductCount = (result.ProductCount + 1);
            result.TotalPrice = result.ProdcutPriceEach * result.ProductCount;
        } else {
            orderincart.push(selectedProdcutMiodel);
        }

        this.setState({ numbersOfItems: this.state.numbersOfItems + 1 })
        this.props.AddItem(orderincart)

    }

    deleteProduct(itemselected) {

        var orderincart = this.props.orderInCartList;
        var result = orderincart.find(aa => aa.ProductId == itemselected.itemID);
        if (result != undefined) {
            if (result.ProductCount <= 1) {
                var index = orderincart.indexOf(itemselected);
                orderincart.splice(index, 1);
                this.setState({ numbersOfItems: this.state.numbersOfItems - 1 })
            }
            else {
                result.ProductCount = (result.ProductCount - 1);
                result.TotalPrice = result.ProdcutPriceEach * result.ProductCount;
            }
            this.props.RemoveItem(orderincart);
        }


        if (this.state.numbersOfItems > 0) {
            this.setState({ numbersOfItems: this.state.numbersOfItems - 1 })
        }
    }

    _renderProductImage() {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent' }} >
                <Image source={require('../Asset/Categoris/Vegetables.png')} style={styles.Imagestyle} />
            </View>
        )
    }
    render() {
        const { itemName, itemPrice ,itemDetail,groc_Units,imageName} = this.props.item;
        let imageass = imageName.replace('http://localhost', 'http://10.0.2.2');
        let assImage_Http_URL = { uri: imageass };
        return (
            <View style={{ fontSize: 20, textAlign: 'center', flex: 1, marginTop: 0 }}>
                <StatusBar hidden />
                {/* <CommonHeader text="Product List" /> */}
                <CustomHeaderWithText text="Detail" />
                <View >
                    <SubHeader mainText={itemName} />
                </View>


                {/* product detatil grid  */}
                <View style={{ flex: 1, margin: 5 }}>
                    <View style={{ flex: 1 }}>

                        <View style={{ flex: 4, justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent', margin: 5, borderRadius: 10, overflow: 'hidden' }} >
                            <Image source={assImage_Http_URL} style={styles.Imagestyle} />
                        </View>

                        <View style={{ flex: 1.2, margin: 5, flexDirection: 'row', borderColor: 'black', borderWidth: 0, backgroundColor: 'transparent' }}>
                            <View style={{ flex: 5, borderWidth: 0, justifyContent: 'center', margin: 5 }} >
                                <Text style={{ margin: 5, fontSize: 18 }}>
                                    {itemName.toUpperCase()}
                                </Text>
                            </View>
                            <View style={{ flex: 5, borderWidth: 0, justifyContent: 'center', alignItems: 'flex-end', margin: 5 }} >
                                <Text style={{ margin: 5, fontSize: 18 }}>
                                    {`Rs . ${itemPrice} / ${groc_Units.unitName}`}
                                </Text>
                            </View>
                        </View>

                        <View style={{ flex: 1.3, margin: 5, borderColor: 'black', borderWidth: 0, backgroundColor: '#fcd0ab', borderRadius: 10 }}>
                            <View style={{ flex: 1, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'space-evenly', flexDirection: 'row' }} >
                                <TouchableHighlight onPress={() => this.addProduct(this.props.item)}
                                    style={{
                                        height: 40, marginTop: 5, marginBottom: 5, backgroundColor: 'transparent',
                                        width: 70, borderRadius: 21, borderWidth: 1.5, borderColor: 'orange', alignItems: 'center', justifyContent: 'center'
                                    }} >
                                    <Text style={{ fontSize: 20, color: '#000' }}>+</Text>
                                </TouchableHighlight>
                                <Text style={{ textAlign: 'center', fontSize: 22 }}>  {this.state.numbersOfItems}  </Text>
                                <TouchableHighlight onPress={() => this.deleteProduct(this.props.item)}
                                    style={{
                                        height: 40, marginTop: 5, marginBottom: 5, backgroundColor: 'transparent',
                                        width: 70, borderRadius: 21, borderWidth: 1.5, borderColor: 'orange', alignItems: 'center', justifyContent: 'center'
                                    }} >
                                    <Text style={{ fontSize: 20, color: '#000' }}>-</Text>
                                </TouchableHighlight>
                            </View>
                        </View>

                        <View style={{ flex: 3.5, backgroundColor: 'transparent' }} >
                            <Text style={{ margin: 10, fontSize: 20 }}>Details</Text>
                            <Text style={{ margin: 10, marginTop: 5, fontSize: 16 }}>{itemDetail}  </Text>
                        </View>
                    </View>
                </View>
                <CustomFooter landingTo="G" selectedApp="Grocerry" cartRequestsCount= {this.props.orderInCartList.length} />
                {/* <CustomFooterTab cartLength={this.props.orderInCartList.length} notifications={2} /> */}
            </View>
        );
    }
}

const mapStateToProps = ({ productList }) => {
    const { user, isloading, selectedCategory, selectedCategoryName, selectedProduct, totalItems, orderInCartList } = productList;
    //debugger;
    return { user, isloading, selectedCategory, selectedCategoryName, selectedProduct, totalItems, orderInCartList };
};
export default connect(mapStateToProps, { AddItem, RemoveItem, OnItemClick })(ProductDetail);

// const mapStateToProps = ({ productDetail }) => {
//     const { user, isloading, selectedCategory, selectedProduct, totalItems } = productDetail;
//     debugger;
//     return { user, isloading, selectedCategory, selectedProduct, totalItems };
// };
// export default connect(mapStateToProps, { addClicked })(ProductDetail);

const styles = {
    Imagestyle:
    {
        width: Dimensions.get('window').width / 1.2,
        height: Dimensions.get('window').width / 2.2,
        // borderRadius:( Dimensions.get('window').width/1.8)/2,
        resizeMode: 'contain',
        //backgroundColor:'red',
        opacity: 0.9
    },
    ProductGridColumn: {
        backgroundColor: '#fcfcfc',
        flex: 5,
        flexDirection: 'column',
        margin: 5,
        borderWidth: 1,
        overflow: 'hidden',
        borderRadius: 6//, shadowOffset: { width: 1, height: 1 }
    },


}