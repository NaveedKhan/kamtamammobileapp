import React, { Component } from 'react';
import { View, TouchableHighlight, Dimensions } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
import { connect } from 'react-redux';
import { searchProductsByKeyword } from '../../../actions';
import { CustomHeaderWithText, IconInput } from '../Common/Header';
import { Spinner } from '../../common';
import { Actions } from 'react-native-router-flux';
import { ScrollView } from 'react-native-gesture-handler';

class SearchProducts extends Component {
    onTextChange(text) {
        debugger;
        this.props.searchProductsByKeyword(text);
        debugger;
    }
    render() {
        debugger;
        return (
            <View style={{ flex: 1, marginTop: 0 }}>
                <CustomHeaderWithText text="Search Products" />
                <TouchableHighlight style={styles.centerDiv} >
                    <IconInput placeholder={"Type any keyword"} onChangeText={text => this.onTextChange(text)} />
                </TouchableHighlight>
                <ScrollView scrollEnabled>
                <List style={{margin:5}}>
                    {
                        this.props.isloading ? <Spinner  style={{marginTop:50}}/> :
                            this.props.searchedProductsList.map(el => {
                                let assImage_Http_URL = { uri: 'http://182.180.56.162:8088/404.png' };
                                if (el.imageName != null) {
                                    let imageass = el.imageName.replace('http://localhost', 'http://10.0.2.2');
                                    assImage_Http_URL = { uri: imageass };
                                }
                              return  <ListItem onPress={() => Actions.productDetail({ item: el })} key={el.itemID} avatar style={{ marginLeft: 0, borderColor: 'orange', borderWidth: 1 }}>
                                    <Left>
                                        <Thumbnail source={assImage_Http_URL} />
                                    </Left>
                                    <Body>
                                        <Text style={{fontSize:13}}>{el.itemName}</Text>
                                        <Text style={{fontSize:12}}>{el.itemDetail}</Text>
                                    </Body>
                                    <Right>
                                        <Text style={{ color: '#f48004' }} note>{`Rs. ${el.itemPrice} /${el.groc_Units.unitName} `}</Text>
                                    </Right>

                                </ListItem>
                            })
                    }

                </List>
                </ScrollView>
            </View>
        );
    }
}

//searchedProductsList
//onPress={() => Actions.productDetail({item:this.props.item})}
const mapStateToProps = ({ productList }) => {
    const { isloading, searchedProductsList } = productList;
    //debugger;

    return { isloading, searchedProductsList };
};
export default connect(mapStateToProps, { searchProductsByKeyword })(SearchProducts);
//export default SearchProducts;
const styles = {
    headerMainDiv: {
        backgroundColor: '#e2e2e2',

        borderBottomColor: '#f48004',
        borderBottomWidth: 0
    },
    leftIconDiv: {
        backgroundColor: 'transparent',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    centerDiv: { backgroundColor: '#fff', justifyContent: 'flex-start' },
    centerTextFieldDiv: {
        flexDirection: 'row', alignItems: 'center', borderColor: 'gray',
        backgroundColor: '#fff', borderWidth: 1, marginLeft: 10, marginRight: 10, borderRadius: 26
    },
    righIconDiv: { backgroundColor: 'transparent', flex: 1, justifyContent: 'center', alignItems: 'center' }
};