import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  TouchableHighlight,
  Modal,
  TouchableOpacity,
  Alert,
  TextInput,
} from 'react-native';
import {
  List,
  ListItem,
  Left,
  Thumbnail,
  CheckBox,
  Right,
  Icon,
  Picker,
  Body,
  Spinner,
  Input,
} from 'native-base';
import axios from 'axios';
import {config} from '../../../config';
import CheckoutItemsList from './CheckoutItemsList';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'firebase';
import {
  confirmButtonClicked,
  addressChanged,
  UpdateCheckoutFormData,
  UpdateUserAddressDatail,
  UpdateUserFormData,
} from '../../../actions';
import {CommonHeader, CustomHeaderWithText} from '../Common/Header';
import {Actions} from 'react-native-router-flux';
import CustomFooter from '../../common/CustomFooter';
import moment from 'moment';
import CalendarPicker from 'react-native-calendar-picker/CalendarPicker';
import FormValidator from '../../HelperClasses/FormValidator';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class Checkout extends Component {
  constructor(props) {
    super(props);
    debugger;
    this.state = {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      swipedAllCards: false,
      swipeDirection: '',
      cardIndex: 0,
      modalVisible: false,
      selectedStartDate: null,
      selectedHour: '12',
      selectedMin: '00',
      selecteddayHalf: 'AM',
      isCurentLocation: false,
      firebaseLoading: false,
      requesteename: '',
      requesteephone: '',
      currentDate: new Date(),
    };
    this.renderClender = this.renderClender.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
  }

  storeData = async (key, value) => {
    debugger;
    try {
      await AsyncStorage.setItem(key, JSON.stringify(value));
    } catch (e) {
      debugger;
      // saving error
    }
  };

  getDataFromAsynStorage(itemname) {
    var me = this;
    // me.setState({ isloading: true });
    try {
      const token = AsyncStorage.getItem(itemname)
        .then(token => {
          debugger;
          if (token != null) {
            let userObj = JSON.parse(token);
            debugger;
          } else {
            debugger;
            //this.props.fetchingProducts();
            //  me.setState({ isloading: false });
          }
        })
        .done();
    } catch (error) {
      var err = String(error) == '' ? 'no error ' : String(error);
      Alert.alert('error', err);
      //  me.setState({ isloading: false });
      return null;
    }
  }

  componentDidMount() {
    //this.storeData('userData', JSON.stringify(SigninModel));
    const {roleName, fullName, phoneNumber} = this.props.user;
    // const {requesteename, requesteephone} = this.state;
    // var arrayerr = [];
    if (roleName == 'Guest') {
      let {GuestUserData} = this.props;
      if (GuestUserData.length > 0) {
        let firstelement = GuestUserData[0];
        let name = firstelement.name;
        let phone = firstelement.phoneNumber;
        this.setState({requesteename: name, requesteephone: phone});
      }
    }
  }

  validateDataFirst() {
    const {roleName, fullName, phoneNumber} = this.props.user;
    const {requesteename, requesteephone} = this.state;
    var arrayerr = [];
    if (roleName == 'Guest') {
      if (requesteename == '' || requesteename == null) {
        arrayerr.push('Please enter  fullname');
      } else {
        var result = FormValidator.NameFieldValidation(requesteename);
        if (!result) {
          arrayerr.push('Please enter valid fullname');
        }
      }

      if (requesteephone == '' || requesteephone == null) {
        arrayerr.push('Please enter  phone');
      } else {
        var result = FormValidator.validatePhoneNumber(requesteephone);
        if (!result) {
          arrayerr.push('Please enter valid phone');
        }
      }
      if (arrayerr.length > 0) {
        Alert.alert('Invalid Data', arrayerr.toString());
      } else {
        this.addDataIntoFirebase();
      }
    } else {
      this.addDataIntoFirebase();
    }
  }

  addDataIntoFirebase = () => {
    debugger;
    // const { Order } = this.props;
   // this.onConfirmPressed('key');
   // return;
    var UserModel = {
      userLat: 33.9976884,
      OrderId: 0,
      ServiceID: 0,
      userLong: 71.4695614,
    };
    // userCurrentLat: 33.9976884, userCurrentLong: 71.4695614,
    var me = this;

    this.setState({firebaseLoading: true});
    const dbref = firebase.database().ref('DriverLocation');
    var newLocref = dbref.push();
    var key = newLocref.getKey();
    newLocref
      .set(UserModel)
      .then(() => {
        debugger;
        me.setState({firebaseLoading: false});
        me.onConfirmPressed(key);
      })
      .catch(error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        Alert.alert('error', err);
        me.setState({firebaseLoading: false});
        // me.setState({ isLoading: false });
        debugger;
        // Alert.alert('alert!', 'something went wrong!');
        //  orderAssignedFail(dispatch, error);
        //me.showToast("an error occured, try again");
      });
  };

  onConfirmPressed(key) {
    debugger;
    const {
      user,
      orderInCartList,
      isOrderSchedule,
      orderScheduleDate,
      OrderScheduleDateString,
      grocServiceChrges,
    } = this.props;
    const {
      userCurrentAddressDetail,
      userDeliveryAddressDetail,
      isCurrentLocatonSelected,
    } = this.props;
    const {
      userCurrentLat,
      userCurrentLong,
      userCurrentAddress,
    } = userCurrentAddressDetail;
    const {
      requsetLat,
      requetLong,
      requestSearchAddress,
    } = userDeliveryAddressDetail;
    const {roleName, fullName, phoneNumber} = user;
    const {requesteename, requesteephone} = this.state;

    let {GuestUserData, DeviceTocken,DeviceID} = this.props;

    numberArray = [];

    if (roleName == 'Guest') {
      let result = GuestUserData.find(aa => aa.phoneNumber == requesteephone);
      if (result == null) {
        let guestUserdata = {
          name: requesteename,
          phoneNumber: requesteephone,
          DeviceTocken: DeviceTocken,
        };
        GuestUserData.push(guestUserdata);
        // this.props.UpdateUserFormData({ prop: 'GuestUserData', value: GuestUserData });
        this.storeData('GuestUserData', GuestUserData);
      }
      GuestUserData.forEach(function(el, index) {
        numberArray.push(el.phoneNumber);
      });
    }

    let totalchargeswithservice = parseInt(
      orderInCartList.sum('TotalPrice') + grocServiceChrges,
    );

    let id = user.id;
    let {RequestDescription} = this.props;
    debugger;
    var OrderdetailModel = {
      UserId: id,
      OrderDetail: RequestDescription ? RequestDescription : '',
      PhoneNumbers: [],//numberArray,
      LocationTrackingID: key,
      OrderVM: orderInCartList,
      DeviceTocken: DeviceTocken,
      DeviceID:DeviceID,
      OrderTotalCharges: totalchargeswithservice,
      CurrentLocationLat: userCurrentLat,
      CurrentLocationLong: userCurrentLong,
      ServiceCharges: grocServiceChrges,
      CurrentLocationAddress: userCurrentAddress,
      IsScheduleOrder: isOrderSchedule,
      OrderScheduleDateString: isOrderSchedule ? OrderScheduleDateString : null,
      // OrderScheduleDate: isOrderSchedule ? orderScheduleDate : new Date().toLocaleString(),
      RequestAddressLat: isCurrentLocatonSelected ? userCurrentLat : requsetLat,
      RequestAddressLong: isCurrentLocatonSelected
        ? userCurrentLong
        : requetLong,
      RequestLocationAddress: isCurrentLocatonSelected
        ? userCurrentAddress
        : requestSearchAddress,
      RequesteeName: roleName == 'Guest' ? requesteename : fullName,
      RequesteeContactNo: roleName == 'Guest' ? requesteephone : phoneNumber,
      IsGuestUser: roleName == 'Guest' ? true : false,
    };
    this.props.confirmButtonClicked(OrderdetailModel);
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  onDateChange(date) {
    debugger;
    this.setState({
      selectedStartDate: date,
    });
  }
  renderClender() {
    const {
      selectedStartDate,
      selectedHour,
      selectedMin,
      selecteddayHalf,
    } = this.state;
    //const startDate = selectedStartDate ? new Date(selectedStartDate).toDateString() : 'Select Any date';
    //let currentime = new Date().toLocaleTimeString();

    const today = this.state.currentDate;
    const todaydatetime = moment(today).format('MMMM Do YYYY');
    const startDate = selectedStartDate
      ? new Date(selectedStartDate).toDateString()
      : todaydatetime;
    let selectedtime = ` ${selectedHour.padStart(
      2,
      '0',
    )}:${selectedMin.padStart(2, '0')}:00 ${selecteddayHalf}`;
    let datetimeString = `${startDate} ${selectedtime}`;
    let hoursCount = [];
    let minsCount = [];
    for (i = 1; i <= 12; i++) {
      hoursCount.push(String(i).padStart(2, '0'));
    }

    for (i = 0; i <= 60; i++) {
      minsCount.push(String(i).padStart(2, '0'));
    }
    //
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          debugger;
          this.setState({modalVisible: false});
          this.props.UpdateCheckoutFormData({
            prop: 'OrderScheduleDateString',
            value: datetimeString,
          });
        }}>
        <View style={styles.datecontainer}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              overflow: 'hidden',
            }}>
            <View
              style={{
                backgroundColor: 'transparent',
                width: width * 0.95,
                overflow: 'hidden',
              }}>
              <TouchableOpacity
                style={{
                  width: 30,
                  height: 30,
                  borderRadius: 15,
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                  alignSelf: 'flex-end',
                  margin: 10,
                  borderColor: '#fff',
                  borderWidth: 1,
                }}
                onPress={() => {
                  this.props.UpdateCheckoutFormData({
                    prop: 'isOrderSchedule',
                    value: false,
                  });
                  this.setState({modalVisible: false});
                }}>
                <Icon name="close" style={{color: '#fff'}} />
              </TouchableOpacity>
              <View style={{margin: 10}}>
                <Text
                  style={{
                    fontSize: 13,
                    fontFamily: 'ariel',
                    color: 'green',
                  }}>{`SELECTED DATE :  ${datetimeString}`}</Text>
              </View>
            </View>
            {/* <TouchableOpacity style={{ width: 30, height: 30, borderRadius: 15, justifyContent: 'flex-end', alignItems: "center", alignSelf: "flex-end", margin: 10, borderColor: "black", borderWidth: 1 }}
                            onPress={() => {
                                debugger;
                                this.setState({ modalVisible: false })
                                this.props.UpdateCheckoutFormData({ prop: 'OrderScheduleDateString', value: datetimeString })
                                //this.UpdateFormData('orderScheduleDate', datetimeString)
                            }
                            }>
                            <Icon name="add" color='green' />
                        </TouchableOpacity> */}
          </View>
          <CalendarPicker onDateChange={this.onDateChange} />

          <Text style={{fontSize: 15, margin: 5, alignSelf: 'center'}}>
            Please Select Time of delivery!
          </Text>
          <View style={{flexDirection: 'row', backgroundColor: 'gray'}}>
            <Picker
              mode="dropdown"
              placeholder="Hours"
              style={{height: 50, width: 70}}
              iosIcon={
                <Icon
                  name="arrow-dropdown-circle"
                  style={{color: '#007aff', fontSize: 25}}
                />
              }
              headerStyle={{backgroundColor: '#b95dd3'}}
              headerBackButtonTextStyle={{color: '#fff'}}
              headerTitleStyle={{color: '#fff'}}
              selectedValue={this.state.selectedHour}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({selectedHour: itemValue})
              }>
              {hoursCount.map(el => (
                <Picker.Item label={String(el)} value={String(el)} key={el} />
              ))}
            </Picker>

            <Picker
              mode="dropdown"
              placeholder="Mins"
              style={{
                height: 50,
                width: 70,
                borderColor: 'black',
                borderWidth: 1,
              }}
              iosIcon={
                <Icon
                  name="arrow-dropdown-circle"
                  style={{color: '#007aff', fontSize: 25}}
                />
              }
              headerStyle={{backgroundColor: '#b95dd3'}}
              headerBackButtonTextStyle={{color: '#fff'}}
              headerTitleStyle={{color: '#fff'}}
              selectedValue={this.state.selectedMin}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({selectedMin: itemValue})
              }>
              {minsCount.map(el => (
                <Picker.Item label={String(el)} value={String(el)} key={el} />
              ))}
            </Picker>

            <Picker
              mode="dropdown"
              style={{height: 50, width: 90}}
              iosIcon={
                <Icon
                  name="arrow-dropdown-circle"
                  style={{color: '#007aff', fontSize: 25}}
                />
              }
              headerStyle={{backgroundColor: '#b95dd3'}}
              headerBackButtonTextStyle={{color: '#fff'}}
              headerTitleStyle={{color: '#fff'}}
              selectedValue={this.state.selecteddayHalf}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({selecteddayHalf: itemValue})
              }>
              <Picker.Item label="AM" value="AM" />
              <Picker.Item label="PM" value="PM" />
            </Picker>
          </View>

          <TouchableOpacity
            onPress={() => {
              debugger;
              this.setState({modalVisible: false});
              // this.UpdateFormData('isOrderSchedule', true)
              this.props.UpdateCheckoutFormData({
                prop: 'isOrderSchedule',
                value: true,
              });
              this.props.UpdateCheckoutFormData({
                prop: 'OrderScheduleDateString',
                value: datetimeString,
              });
              //this.UpdateFormData('orderScheduleDate', datetimeString)
            }}
            style={{
              width: 250,
              height: 50,
              borderRadius: 30,
              margin: 10,
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              borderColor: '#fff',
              borderWidth: 1.7,
            }}>
            <Text style={{fontSize: 15, color: '#fff', fontWeight: 'bold'}}>
              {' '}
              CONFIRM THE DATE{' '}
            </Text>
          </TouchableOpacity>
          {/* <View style={{ margin: 10 }}>
                        <Text style={{ fontSize: 15, fontFamily: 'ariel', color: 'green' }}>{`SELECTED DATE :  ${datetimeString}`}</Text>
                    </View> */}
        </View>
      </Modal>
    );
  }

  UpdateFormData(propName, preVal) {
    debugger;
    // var preVal = this.props.maintainanceSelected;
    this.props.UpdateCheckoutFormData({prop: propName, value: !preVal});
  }

  showmodal() {}

  render() {
    debugger;
    const {roleName} = this.props.user;
    const {
      orderInCartList,
      iscurrentLocSelected,
      isloading,
      error,
      grocServiceChrges,
    } = this.props;
    const {
      userCurrentAddressDetail,
      userDeliveryAddressDetail,
      isCurrentLocatonSelected,
    } = this.props;
    const {
      userCurrentLat,
      userCurrentLong,
      userCurrentAddress,
    } = userCurrentAddressDetail;
    const {
      requsetLat,
      requetLong,
      requestSearchAddress,
    } = userDeliveryAddressDetail;
    const {firebaseLoading} = this.state;
    //let currentime = new Date().toLocaleTimeString();
    const {width, height} = this.state;
    // const { selectedStartDate, selectedHour, selectedMin, selecteddayHalf } = this.state;
    //const startDate = selectedStartDate ? new Date(selectedStartDate).toDateString() : '';
    //let selectedtime = selectedHour == '' || selectedMin == '' || selecteddayHalf == '' ? currentime :
    //` ${selectedHour.padStart(2, '0')}:${selectedMin.padStart(2, '0')}:00 ${selecteddayHalf}`;
    //let datetimeString = `${startDate} ${selectedtime}`;

    return (
      <View style={{backgroundColor: '#fff', flex: 1, marginTop: 0}}>
        <View>
          <CustomHeaderWithText text="Products Checkout" />
        </View>
        <ScrollView scrollEnabled style={{paddingBottom: 10}}>
          <View style={styles.container}>
            <View
              style={{
                flex: 2.8,
                backgroundColor: 'transparent',
                margin: 5,
                overflow: 'hidden',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Icon
                name="map-marker"
                type="FontAwesome"
                style={{color: 'orange', fontSize: 23, marginLeft: 20}}
              />
              <Text
                style={{
                  padding: 5,
                  fontWeight: 'bold',
                  fontSize: 19,
                  marginLeft: 10,
                }}>
                Delivery Address
              </Text>
            </View>

            <List>
              <ListItem
                avatar
                style={{marginLeft: 0}}
                onPress={() =>
                  // console.log('ohooo')
                  this.props.UpdateUserAddressDatail({
                    prop: 'isCurrentLocatonSelected',
                    value: true,
                  })
                }>
                <TouchableOpacity
                  style={{
                    width: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignContent: 'stretch',
                    alignSelf: 'stretch',
                    margin: 5,
                  }}>
                  <CheckBox
                    onPress={() =>
                      //  console.log('ohooo')
                      this.props.UpdateUserAddressDatail({
                        prop: 'isCurrentLocatonSelected',
                        value: true,
                      })
                    }
                    color="green"
                    checked={isCurrentLocatonSelected}
                    style={{
                      height: 20,
                      width: 20,
                      alignItems: 'center',
                      fontSize: 30,
                      justifyContent: 'center',
                    }}
                  />
                </TouchableOpacity>

                <Body>
                  <Text style={{color: 'green'}}>My Current Location</Text>
                  <Text note>
                    {isCurrentLocatonSelected
                      ? userCurrentAddress
                      : requestSearchAddress == ''
                      ? 'Press Plus icon to select address'
                      : requestSearchAddress}
                  </Text>
                </Body>
                <Right>
                  <Text note>View</Text>
                </Right>
              </ListItem>

              <ListItem onPress={() => Actions.Map({isGrocery: true})}>
                <View
                  style={{
                    flex: 2,
                    backgroundColor: 'transparent',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TouchableHighlight
                    onPress={() => Actions.Map({isGrocery: true})}
                    style={{
                      backgroundColor: '#fff',
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: this.state.width / 8,
                      width: this.state.width / 8,
                      borderRadius: this.state.width / 16,
                      shadowColor: 'black',
                      shadowOffset: {width: 0, height: 4},
                      shadowRadius: 4,
                      elevation: 8,
                    }}>
                    <Icon
                      name="plus"
                      type="FontAwesome"
                      style={{color: 'orange', fontSize: 26}}
                    />
                  </TouchableHighlight>
                  <Text>Add another delivery address from Map</Text>
                </View>
              </ListItem>
            </List>

            <View
              style={{
                backgroundColor: 'transparent',
                flex: 6.7,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    margin: 10,
                    marginBottom: 5,
                    backgroundColor: 'gray',
                    borderRadius: 5,
                    padding: 10,
                  }}>
                  <Text
                    numberOfLines={4}
                    style={{
                      fontFamily: 'sens-serif',
                      fontSize: 15,
                      color: 'orange',
                    }}>
                    {isCurrentLocatonSelected
                      ? userCurrentAddress
                      : requestSearchAddress == ''
                      ? 'Press plus Icon to add address '
                      : requestSearchAddress}
                  </Text>
                </View>
              </View>
            </View>
            <Text style={{alignSelf: 'center', color: 'red', fontSize: 12}}>
              Your delivery address
            </Text>

            <View
              style={{
                backgroundColor: 'orange',
                flex: 0.2,
                margin: 8,
                borderRadius: 8,
              }}
            />
          </View>

          <View style={styles.container}>
            <View
              style={{
                flex: 2.8,
                backgroundColor: 'transparent',
                margin: 0,
                overflow: 'hidden',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Icon
                name="map-marker"
                type="FontAwesome"
                style={{color: 'orange', fontSize: 23, marginLeft: 20}}
              />
              <Text
                style={{
                  padding: 5,
                  fontWeight: 'bold',
                  fontSize: 19,
                  margin: 10,
                }}>
                Order Urgency
              </Text>
            </View>
            <TouchableOpacity
              style={styles.rowList}
              onPress={() =>
                this.props.UpdateCheckoutFormData({
                  prop: 'isOrderSchedule',
                  value: false,
                })
              }>
              <Text style={styles.rowText}>Need Immediate Service</Text>
              {!this.props.isOrderSchedule ? (
                <Icon
                  name="checkmark-circle"
                  style={{color: 'green', marginRight: 20}}
                />
              ) : (
                <View />
              )}
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.rowList}
              onPress={() => {
                this.setState({modalVisible: !this.state.modalVisible});
                // if (!this.props.isOrderSchedule)
                // this.UpdateFormData('isOrderSchedule', this.props.isOrderSchedule)
              }}>
              <Text style={styles.rowText}>
                {this.props.isOrderSchedule
                  ? this.props.OrderScheduleDateString
                  : 'Schedule My Service'}
              </Text>
              {this.props.isOrderSchedule ? (
                <Icon
                  name="md-calendar"
                  style={{color: 'green', marginRight: 20}}
                />
              ) : (
                <View />
              )}
            </TouchableOpacity>
            <View
              style={{
                backgroundColor: 'orange',
                flex: 0.2,
                margin: 8,
                borderRadius: 8,
              }}
            />
          </View>

          <View style={styles.container}>
            <View style={{height: 50, margin: 20, marginTop: 10}}>
              <View
                style={{
                  flex: 1,
                  margin: 0,
                  overflow: 'hidden',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                <Icon
                  name="check-circle"
                  type="FontAwesome"
                  style={{color: 'orange', fontSize: 23, marginLeft: 20}}
                />
                <Text
                  style={{
                    padding: 5,
                    fontWeight: 'bold',
                    fontSize: 19,
                    marginLeft: 10,
                  }}>
                  Order Summary
                </Text>
              </View>
            </View>

            <ScrollView scrollEnabled style={{marginBottom: 10}}>
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'transparent',
                  marginLeft: 20,
                  marginRight: 20,
                }}>
                <List>
                  {orderInCartList.map(order => (
                    <CheckoutItemsList key={order.ProductId} item={order} />
                  ))}
                </List>
              </View>
            </ScrollView>
          </View>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 14,
              color: 'green',
              fontWeight: 'bold',
            }}>
            {' '}
            SERVICE CHANRGES : Rs. {grocServiceChrges}
          </Text>
          {/* {String(error) == '' ? (
            <Text numberOfLines={4} style={{margin: 10}}>
              
            </Text>
          ) : (
            <View>
              <Text numberOfLines={12} style={{margin: 10}}>
                {String(error)}
              </Text>
              <Text numberOfLines={8} style={{margin: 10}}>
                {error.config.data}
              </Text>
            </View>
          )} */}
          {this.props.RequestDescription == '' ? (
            <View />
          ) : (
            <View>
              <Text
                style={{
                  margin: 5,
                  marginLeft: 20,
                  fontWeight: 'bold',
                  fontSize: 15,
                  color: 'green',
                  marginBottom: 1,
                }}>
                Extra Detail{' '}
              </Text>
              <View style={styles.DiscriptionBorder}>
                <TextInput
                  placeholder="Enter Extra detail or items for your orders......."
                  style={{borderColor: 'gray'}}
                  editable={false}
                  multiline={true}
                  style={{color: '#000'}}
                  value={this.props.RequestDescription}
                />
              </View>
            </View>
          )}

          {roleName == 'Guest' ? (
            <View style={[styles.container, {marginTop: 0}]}>
              <Text
                style={{
                  margin: 5,
                  marginLeft: 20,
                  fontWeight: 'bold',
                  fontSize: 15,
                  color: 'green',
                  marginBottom: 1,
                }}>
                Enter Your Name and Phone{' '}
              </Text>
              <View
                style={{
                  marginTop: 5,
                  flexDirection: 'row',
                  alignItems: 'center',
                  borderColor: '#fff',
                  borderBottomWidth: 0.7,
                }}>
                <Icon
                  name="person"
                  style={{color: '#f48004', fontSize: 24, marginLeft: 10}}
                />
                <Input
                  placeholder="Your Name"
                  placeholderTextColor="#000"
                  onChangeText={text => this.setState({requesteename: text})}
                  value={this.state.requesteename}
                  style={{marginLeft: 10, fontSize: 14, color: '#000'}}
                />
              </View>
              <View
                style={{
                  marginTop: 5,
                  flexDirection: 'row',
                  alignItems: 'center',
                  borderColor: '#fff',
                  borderBottomWidth: 0.7,
                }}>
                <Icon
                  name="person"
                  style={{color: '#f48004', fontSize: 24, marginLeft: 10}}
                />
                <Input
                  placeholder="Phone Number"
                  placeholderTextColor="#000"
                  onChangeText={text => this.setState({requesteephone: text})}
                  value={this.state.requesteephone}
                  style={{marginLeft: 10, fontSize: 14, color: '#000'}}
                />
              </View>
            </View>
          ) : (
            <View />
          )}
          <Text
            style={{
              color: 'red',
              fontWeight: 'bold',
              margin: 10,
              marginBottom: 0,
            }}>
            Attention!
          </Text>
          <Text
            style={{
              color: 'red',
              fontSize: 12,
              margin: 10,
              alignSelf: 'center',
              textAlign: 'center',
              marginTop: 0,
            }}>
            Some of the items charges could be different from the above
            mentioned prices.it can be less or more due to price fluctuation in
            the market. Our worker will re estimate the total price if prices
            are changed!
          </Text>
        </ScrollView>

        {firebaseLoading || isloading ? (
          <View
            style={{
              width: width * 1,
              height: height * 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'gray',
              opacity: 0.8,
              position: 'absolute',
            }}>
            <Spinner />
            <Text style={{color: '#fff', fontWeight: 'bold'}}>
              Uploading your detail.please wait!
            </Text>
          </View>
        ) : (
          <View
            style={{
              height: this.state.height / 9,
              backgroundColor: '#e2e2e2',
              flexDirection: 'row',
              fontFamily: 'Helvica',
            }}>
            <View
              style={{
                flex: 5,
                backgroundColor: 'transparent',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-evenly',
              }}>
              {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 20,
                  fontFamily: 'Helvica',
                }}>
                Total :{' '}
              </Text>
              <Text style={{fontSize: 18}}>
                Rs.{' '}
                {parseInt(
                  orderInCartList.sum('TotalPrice') + grocServiceChrges,
                )}{' '}
              </Text>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: 'transparent',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableHighlight
                onPress={() => this.validateDataFirst()}
                style={{
                  borderRadius: 30,
                  backgroundColor: '#e8a035',
                  height: this.state.height / 14,
                  width: this.state.width / 2.5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                color="#e8a035"
                title="Checkouts">
                <Text
                  style={{textAlign: 'center', color: '#fff', fontSize: 16}}>
                  SUBMIT ORDER
                </Text>
              </TouchableHighlight>
            </View>
          </View>
        )}

        {this.renderClender()}
      </View>
    );
  }
}

const mapStateToProps = ({checkout, cart, auth, userAddress}) => {
  const {
    user,
    isloading,
    selectedCategory,
    selectedProduct,
    totalItems,
    orderScheduleDate,
    isOrderSchedule,
    OrderScheduleDateString,
    error,
    iscurrentLocSelected,
  } = checkout;
  const {orderInCartList} = cart;
  const {
    grocServiceChrges,
    handyServiceCharges,
    adverstisementsList,
    GuestUserData,
    DeviceTocken,
    DeviceID
  } = auth;
  // const { userCurrentAddress } = auth;
  const {
    userCurrentAddressDetail,
    userDeliveryAddressDetail,
    isCurrentLocatonSelected,
  } = userAddress;
  debugger;
  return {
    user: auth.user,
    isloading,
    selectedCategory,
    error,
    isCurrentLocatonSelected,
    userDeliveryAddressDetail,
    OrderScheduleDateString,
    orderScheduleDate,
    selectedProduct,
    isOrderSchedule,
    totalItems,
    userCurrentAddressDetail,
    iscurrentLocSelected,
    orderInCartList,
    grocServiceChrges,
    GuestUserData,
    DeviceTocken,
    DeviceID
  };
};
export default connect(
  mapStateToProps,
  {
    confirmButtonClicked,
    addressChanged,
    UpdateUserAddressDatail,
    UpdateCheckoutFormData,
    UpdateUserFormData,
  },
)(Checkout);

const styles = {
  container: {
    // height: Dimensions.get('window').height / 3.3,
    backgroundColor: '#e6e7e9',
    margin: 20,
    marginBottom: 5,
    borderRadius: 9,
    overflow: 'hidden',
    borderColor: '#f48004',
    borderWidth: 2,
  },
  DiscriptionBorder: {
    borderWidth: 3,
    borderColor: '#f48004',
    backgroundColor: 'white',
    width: width * 0.9,
    height: height * 0.25,
    margin: 10,
    borderRadius: 10,
    alignSelf: 'center',
  },
  datecontainer: {
    backgroundColor: 'orange', //'#E9EBEE',
    borderRadius: 10,
    margin: 10,
    marginTop: 60,
  },
  rowList: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 10,
    borderBottomColor: 'white',
    width: width * 0.85,
    borderBottomWidth: 1,
    alignSelf: 'center',
    padding: 10,
  },
  rowIcon: {
    color: '#f48004',
    margin: 20,
    marginTop: 10,
    marginBottom: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    marginRight: 10,
    marginLeft: 10,
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  rowSelectType: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
    marginTop: 10,
  },
  rowText: {
    color: 'grey',
    alignSelf: 'center',
    marginTop: 5,
    marginLeft: 20,
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent',
  },
  done: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
    backgroundColor: 'transparent',
  },
};
