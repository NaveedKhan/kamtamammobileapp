import React, { Component } from 'react';
import { View, Text, Platform, ScrollView, Dimensions, TouchableHighlight } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Left, Body, Right, Button, avatar } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { confirmButtonClicked, addressChanged } from '../../../actions';

class CheckoutItemsList extends Component {
    constructor(props) {
        super(props);
        //this.state = { itemvalue: 0 }
        debugger;
        let numbersOfItems = 0;
        let cart = this.props.item;

        numbersOfItems = cart.ProductCount;
    }
    render() {
        const { item } = this.props;
        const { ImageName } = item;
        let imageass = ImageName.replace('http://localhost', 'http://10.0.2.2');
        let assImage_Http_URL = { uri: imageass };
        return (


            <ListItem avatar style={{ backgroundColor: 'transparent', borderBottomWidth: 1, borderBottomColor: "#f48004", marginLeft: 0 }}>
                <Left style={{ paddingLeft: 0, alignItems: 'center', justifyContent: 'center' }}>
                    <Thumbnail small source={assImage_Http_URL} />
                </Left>
                <Body style={{ borderWidth: 0, borderBottomWidth: 0 }} >
                    <Text>{item.ProductName}</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
                        <Text style={{ fontSize: 12, margin: 5, }} note numberOfLines={1}>{` Rs ${item.ProdcutPriceEach} / ${item.UnitName} `}</Text>
                        <Text style={{ fontSize: 12, margin: 5, marginRight: 20 }} note numberOfLines={1}>{`Qty ${item.ProductCount}`}</Text>
                    </View>
                    {/* <Text note numberOfLines={1}>{` RS. ${item.ProdcutPriceEach}/kg                Qty ${item.ProductCount}`}</Text> */}
                </Body>
                <Right style={{ borderWidth: 0 }}>

                    <Text>RS. {item.TotalPrice}</Text>

                </Right>
            </ListItem>


        );
    }
}

export default CheckoutItemsList;