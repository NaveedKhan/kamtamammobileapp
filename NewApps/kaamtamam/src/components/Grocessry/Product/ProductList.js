import React, { Component } from 'react';
import { View, Text, Dimensions, Image, ScrollView, TouchableHighlight, StatusBar } from 'react-native';
import {
    Content,
    Button,
    List,
    ListItem,
    Icon,
    Container,
    Left,
    Right,
    Badge
} from "native-base";
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { AddItem, RemoveItem, OnItemClick } from '../../../actions';
import { CommonHeader, SubHeader, CustomHeader } from '../Common/Header';
import { CustomFooterTab } from '../Common/Footer';
import CustomFooter from '../../common/CustomFooter';
import { products, productsCat } from '../../../components/common/Cities.json';
import ProductListGrid from './ProductListGrid';
import ProductListGridColum from './ProductListGridColum';
import { Spinner } from '../../common';



class ProductList extends Component {
    constructor(props) {
        super(props);
        debugger;
        this.state = {
            itemId: this.props.selectedCategory,
            categoryname: this.props.selectedCategoryName,
            numberofrowsArray: [],
            filterDataArray: [],
            isLoading: false
        }
    }

    componentDidMount() {
        debugger;
        const { itemId } = this.state;
        this.setState({ isLoading: true });
        const { ProductsData } = this.props;
        const { products } = ProductsData;
        let filterData = products.filter(aa => aa.itemCategoryID == itemId);
        let count = Math.ceil(filterData.length);
        let numberofrows = Math.ceil(count / 2);
        var numberofrowsArray = []
        for (i = 0; i < numberofrows; i++) {
            numberofrowsArray.push(i)
        }
        this.setState({ numberofrowsArray: numberofrowsArray, filterDataArray: filterData, isLoading: false });
    }


    _renderUpperIcon() {
        return (
            <View style={{ backgroundColor: '#fcfcfc', flex: .3, margin: 5 }} >
                <Icon name='heart' type="AntDesign" style={{ color: "gray", fontSize: 20, width: 20 }} />
            </View>
        )
    }

    _renderProductImage(imagename) {
        let imageass = imagename.replace('http://localhost', 'http://10.0.2.2');
        let assImage_Http_URL = { uri: imageass };
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent' }} >
                <Image source={assImage_Http_URL} style={styles.Imagestyle} />
            </View>
        )
    }

    _renderProductPriceAndButtons(item) {
        const { itemName, itemPrice, groc_Units } = item;
         let numbersOfItems = 0;
         const cart = this.props.orderInCartList;
         var resultpr = cart.find(aa => aa.ProductId == item.itemID);
         if (resultpr != undefined) {
             numbersOfItems = resultpr.ProductCount;
         }
        // debugger;
        // this.state = {
        //     numbersOfItems:numbersOfItems == undefined? 0 : numbersOfItems
        // }
        {/* product detail grid column inner third row for item name price and add button */ }
        return <View style={{ backgroundColor: '#f78320', flex: .45, flexDirection: 'column' }} >

            {/* product detail grid column inner third row title and price */}
            <View style={{ backgroundColor: 'transparent', flex: 55, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'stretch' }} >
                <View style={{ flex: 7, backgroundColor: 'transparent' }}>
                    <Text numberOfLines={3}
                        style={{
                            marginTop:5,
                            fontSize: 12, padding: 5,
                            fontWeight: '900', fontFamily: 'Arial', alignItems: 'center', color: '#000'
                        }}>
                        {itemName}
                    </Text>
                </View>

                <View  >
                    <Text  numberOfLines={2} style={{ color: '#000', padding: 5, textAlign: 'right', marginRight: 3, fontSize: 12, fontFamily: 'Arial', backgroundColor: 'transparent', alignSelf: 'stretch', justifyContent: 'space-around' }}>
                        {`Rs. ${itemPrice}/${groc_Units.unitName}`}
                    </Text>
                </View>

            </View>

            {/* product detail grid column inner third row add buttons */}
            <View style={{ backgroundColor: '#f78320', flex: 45, alignItems: 'center', justifyContent: 'space-evenly', flexDirection: 'row' }} >
               
            <TouchableHighlight onPress={() => this.deleteProduct(item)}
                    style={{
                        height: 40, marginTop: 5, marginBottom: 5, backgroundColor: 'transparent',
                        width: 70, borderRadius: 20, borderWidth: 1.5, borderColor: '#fff', alignItems: 'center', justifyContent: 'center'
                    }} >
                    <Text style={{ fontSize: 17, color: '#fff', fontWeight: 'bold' }}>-</Text>
                </TouchableHighlight>              
                <Text style={{ textAlign: 'center',fontWeight:'bold' }}> {numbersOfItems} </Text>
                <TouchableHighlight onPress={() => this.addProduct(item)}
                    style={{
                        height: 40, marginTop: 5, marginBottom: 5, backgroundColor: 'transparent',
                        width: 70, borderRadius: 20, borderWidth: 1.5, borderColor: '#fff', alignItems: 'center', justifyContent: 'center'
                    }} >
                    <Text style={{ fontSize: 17, color: '#fff', fontWeight: 'bold' }}>+</Text>
                </TouchableHighlight>
            </View>

        </View>
    }

    addProduct(itemselected) {
        debugger;
        var selectedProdcutMiodel = {
            ProductName: itemselected.itemName,
            ImageName:itemselected.imageName,
            ProductId: itemselected.itemID,
            ProductCount: 1,
            ProdcutPriceEach: itemselected.itemPrice,
            TotalPrice: itemselected.itemPrice,
            UnitName:itemselected.groc_Units.unitName
        }
        debugger;
        var orderincart = this.props.orderInCartList;
        var result = orderincart.find(aa => aa.ProductId == itemselected.itemID);
        if (result != undefined) {
            result.ProductCount = (result.ProductCount + 1);
            result.TotalPrice = result.ProdcutPriceEach * result.ProductCount;
        } else {
            orderincart.push(selectedProdcutMiodel);
        }

        this.setState({ numbersOfItems: this.state.numbersOfItems + 1 })
        this.props.AddItem(orderincart)

    }

    deleteProduct(itemselected) {

        var orderincart = this.props.orderInCartList;
        var result = orderincart.find(aa => aa.ProductId == itemselected.itemID);
        if (result != undefined) {
            if (result.ProductCount <= 1) {
                var index = orderincart.indexOf(itemselected);
                orderincart.splice(index, 1);
                this.setState({ numbersOfItems: this.state.numbersOfItems - 1 })
            }
            else {
                result.ProductCount = (result.ProductCount - 1);
                result.TotalPrice = result.ProdcutPriceEach * result.ProductCount;
            }
            this.props.RemoveItem(orderincart);
        }


        if (this.state.numbersOfItems > 0) {
            this.setState({ numbersOfItems: this.state.numbersOfItems - 1 })

        }
    }

    _renderProdictGridColums(item) {
     const { imageName } = item;
        return (
            <View style={styles.ProductGridColumn}>

                {/* product detail grid column inner first row  */}
                <View style={{ backgroundColor: '#fcfcfc', flex: .1, flexDirection: 'row' }}>
                    {this._renderUpperIcon()}
                </View>

                {/* product detail grid column inner second row for item image */}
                <View style={{ backgroundColor: '#fcfcfc', flex: .50 }} >
                    <TouchableHighlight onPress={() => Actions.productDetail({ item: item })} style={{ backgroundColor: '#fcfcfc', flex: .55 }}>
                        {
                            this._renderProductImage(imageName)
                        }
                    </TouchableHighlight>
                </View>

                {this._renderProductPriceAndButtons(item)}

            </View>

        );
    }

    _renderProductGrid(item1, item2) {
        return (
            <View key={item1.itemID} style={styles.ProductGridRowStyle}>

                <View style={styles.ProductGridInnerRow}>

                    {/* product detail grid left side column */}
                    {
                    item1 == undefined ? <View /> : 
                    this. _renderProdictGridColums(item1)
                 
                    }

                    {/* product detail grid right  side column */}
                    {
                    item2 == undefined ? <View /> 
                    : 
                    this. _renderProdictGridColums(item2)
                    }
                </View>
            </View>
        );
    }


    render() {
        debugger;
        const { numberofrowsArray, filterDataArray } = this.state;

        debugger

        return (
            <View style={{ fontSize: 20, textAlign: 'center', flex: 1, marginTop: 0 }}>
                <StatusBar hidden />
                {/* <CommonHeader text="Product List" /> */}
                <CustomHeader leftIcon={true} searchBar={true} rightIcon={true} inputPressed={() => Actions.searchProducts()} disabled={true} />
                <View>
                    <SubHeader mainText={this.props.selectedCategoryName} />
                </View>

                <ScrollView scrollEnabled>
                    {/* product detatil grid  */}
                    <View style={{ flex: 1, margin: 5 }}>
                        {
                            this.state.isLoading ? <Spinner /> :

                                numberofrowsArray.map(el => {
                                    var firstIndex = ((el * 2) + 0);
                                    var secondIndex = ((el * 2) + 1);
                                    var item1 = filterDataArray[firstIndex];
                                    var item2 = filterDataArray[secondIndex];
                                    return this._renderProductGrid(item1, item2);
                                    // return (
                                    //     <ProductListGrid key={el} item1={filterDataArray[firstIndex]} item2={filterDataArray[secondIndex]} />
                                    // )
                                })
                        }
                    </View>
                </ScrollView>

                {/* product detatil grid row */}
                {/* <CustomFooterTab cartLength = {this.props.orderInCartList.length} notifications={2}/> */}
                <CustomFooter landingTo="G" selectedApp="Grocerry" cartRequestsCount={this.props.orderInCartList.length} />
            </View>
        );
    }
}
const mapStateToProps = ({ productList, landingPageReducer }) => {
    const { user, isloading, selectedCategory, selectedCategoryName, selectedProduct, totalItems, orderInCartList } = productList;
    //debugger;
    const { ProductsData } = landingPageReducer;
    return { user, isloading, selectedCategory, selectedCategoryName, selectedProduct, totalItems, orderInCartList, ProductsData };
};
export default connect(mapStateToProps, { AddItem, RemoveItem, OnItemClick })(ProductList);

const styles = {
    ProductGridRowStyle: {
        height: Dimensions.get('window').height / 2.3,
        backgroundColor: 'yellow'
    },
    ProductGridInnerRow: {
        backgroundColor: '#fcfcfc',//'#fcfcfc',
        flexDirection: 'row',
        //flex: 1,
        height: Dimensions.get('window').height / 2.3,
        alignItems: 'center'
    },
    Imagestyle:
    {
        width: Dimensions.get('window').width / 2.2,
        height: Dimensions.get('window').width / 2.2,
        // borderRadius:( Dimensions.get('window').width/1.8)/2,
        resizeMode: 'contain',
        //backgroundColor:'red',
        opacity: 0.9
    },
    ProductGridColumn: {
        backgroundColor: '#fcfcfc',
        flex: 5,
        flexDirection: 'column',
        margin: 5,
        borderColor: '#f48004',
        borderWidth: 2,
        overflow: 'hidden',
        borderRadius: 6//, shadowOffset: { width: 1, height: 1 }
    }
}