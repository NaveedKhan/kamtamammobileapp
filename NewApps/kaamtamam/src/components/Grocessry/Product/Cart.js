import React, {Component} from 'react';
import {
  View,
  Text,
  Button,
  Image,
  Dimensions,
  ScrollView,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  Alert,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import {Icon, Spinner} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import Geolocation from 'react-native-geolocation-service';
import Geocode from 'react-geocode';
import {
  checkoutButton,
  AddItem,
  UpdateUserAddressDatail,
} from '../../../actions';
import {
  CommonHeader,
  SubHeader,
  CustomHeader,
  CustomHeaderWithText,
} from '../Common/Header';
import {CustomFooterTab} from '../Common/Footer';
import CustomFooter from '../../common/CustomFooter';
import CartList from './CartList';

Geocode.setApiKey('AIzaSyCZ_GeN6VIBOgZqe9mZ568ygvB8eUNuSbc');
Geocode.setLanguage('en');
Geocode.enableDebug();


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;


class Cart extends Component {
  constructor(props) {
    super(props);
    debugger;
    this.state = {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      isGettingLocation: false,
      RequestDescription: '',
      // var selectedProdcutMiodel = {
      //     ProductName: itemselected.itemName,
      //     ProductId: itemselected.itemID,
      //     ProductCount: 1,
      //     ProdcutPriceEach: itemselected.itemPrice,
      //     TotalPrice: itemselected.itemPrice
      // }
    };
  }

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Kaam Tamam Location Permission',
          message:
            'Kaam Tamam App needs access to your Location ' +
            'so you can place order.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the Location');
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

async requestocationPermissionFirstANdproceed()
{
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Kaam Tamam Location Permission',
        message:
          'Kaam Tamam App needs access to your Location ' +
          'so you can place order.',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      this.getCurrentLocation();
    } else {
      console.log('Location permission denied');
      Alert.alert('Attention','You have denied the location permission so you will not be able to make an order '+
      'Kaam Tamam needs to access you location so that you can select a location for delivery')
    }
  } catch (err) {
    console.warn(err);
  }
}

  componentDidMount() {
    debugger;
    //this.getCurrentLocation();
    if (Platform.OS == 'android') {
      this.requestCameraPermission();
    }
  }

  async getCurrentLocation() {
    debugger;
    var me = this;
    this.setState({isGettingLocation: true});
    await Geolocation.getCurrentPosition(
      position => {
        console.log(position);
        let lats =
          String(position.coords.latitude).indexOf('37.') == 0
            ? 33.9976884
            : position.coords.latitude;
        let longs =
          String(position.coords.longitude).indexOf('-122') == 0
            ? 71.4695614
            : position.coords.longitude;

        Geocode.fromLatLng(lats, longs)
          .then(
            response => {
              debugger;
              const address = response.results[0].formatted_address;
              //userCurrentLat: 33.9976884
              //userCurrentLong: 71.4695614
              //Board Bazar Stop, University latsRoad, Peshawar, Pakistan
              let userCurrentAddressDetail = {
                userCurrentLat: lats,
                userCurrentLong: longs,
                userCurrentAddress: address,
              };
              me.props.UpdateUserAddressDatail({
                prop: 'userCurrentAddressDetail',
                value: userCurrentAddressDetail,
              });
              me.props.UpdateUserAddressDatail({
                prop: 'isCurrentLocatonSelected',
                value: true,
              });
              me.setState({isGettingLocation: false});
              Actions.checkout({RequestDescription:me.state.RequestDescription});
              console.log(address);
            },
            error => {
              debugger;
              me.setState({isGettingLocation: false});
              var err = String(error) == '' ? 'no error ' : String(error);
              // me.setState({ isLoading: false });
             
             
              console.error(error);
            },
          )
          .catch(error => {
            debugger;
            var err = String(error) == '' ? 'no error ' : String(error);
            //me.setState({ isLoading: false });
            Alert.alert('error', err);
            me.setState({isGettingLocation: false});
            //loginUserFail(dispatch, error);
            console.log('error', error);
          });
      },
      error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        // me.setState({ isLoading: false });
        if(error)
        {
          const {code, message} = error;
          if(code == 1)
          {
            Alert.alert('Attention',
             'Dear customer you have denied the location permission.to poceed please '+
             ' got to setting and allow location access so that Kaam Tamam app can access the location service for you! Otherwise you will not be able to make an order'
             );
          }else
          {
            Alert.alert('error', message);
          }
          
        }
        me.setState({isGettingLocation: false});
        console.log(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 15000},
    );
  }

  goToCheckout() {
    if (Platform.OS == 'android') {
      this.requestocationPermissionFirstANdproceed();
    }else
    {
      this.getCurrentLocation();
    }
  }

  render() {
    const {orderInCartList} = this.props;
    const {isGettingLocation} = this.state;

    return (
      <View style={{flex: 1, marginTop: 0}}>
        {/* <CommonHeader text="Cart" /> */}
        <CustomHeaderWithText text="CART" />
        {/* <View>
          <SubHeader mainText="Cart Items" />
        </View> */}
        <ScrollView scrollEnabled>
        {orderInCartList.length < 1 ? (
              <View/>
            ) : 
             <View>
                <Text style={{margin:5, marginLeft:10, fontSize:15, color:'green',fontWeight:'bold'}}>Others Detail </Text>
          <View style={styles.DiscriptionBorder}>
            <TextInput
              placeholder="Enter Extra detail or items for your orders......."
              style={{borderColor: 'gray'}}
              onChangeText={text => this.setState({RequestDescription: text})}
              multiline={true}
              value={this.state.RequestDescription}
            />
          </View>
             </View>
              
            }
       {orderInCartList.length < 1 ? <View/>:
          <Text style={{margin:5, marginLeft:10, fontSize:15, color:'green',fontWeight:'bold'}}>Ordered Items List</Text>
       }
          <View style={{flex: 1, margin: 5}}>
            {orderInCartList.length < 1 ? (
              <Text style={{textAlign: 'center', marginTop: 15}}>
                There is no iten in the Cart!
              </Text>
            ) : (
              orderInCartList.map(order => (
                <CartList key={order.ProductId} item={order} />
              ))
            )}
          </View>
        </ScrollView>
        {orderInCartList.length < 1 ? (
          <View
            style={{
              height: this.state.height / 9,
              backgroundColor: '#e2e2e2',
              flexDirection: 'row',
            }}
          />
        ) : (
          <View
            style={{
              height: this.state.height / 9,
              backgroundColor: '#e2e2e2',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 5,
                backgroundColor: 'transparent',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-evenly',
              }}>
              <Text style={{fontWeight: 'bold', fontSize: 20}}>Total : </Text>
              <Text style={{fontSize: 18}}>
                Rs. {orderInCartList.sum('TotalPrice')}{' '}
              </Text>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: 'transparent',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              {isGettingLocation ? (
                <Spinner />
              ) : (
                <TouchableOpacity
                  onPress={() => this.goToCheckout()}
                  style={{
                    height: this.state.height / 14,
                    width: this.state.width / 2.5,
                    borderRadius: 30,
                    margin: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                    borderColor: '#f48004',
                    borderWidth: 1.7,
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      color: '#f48004',
                      fontWeight: 'bold',
                    }}>
                    {' '}
                    CHECKOUT{' '}
                  </Text>
                </TouchableOpacity>
              )
              // <TouchableHighlight onPress={() => this.goToCheckout()}
              //     style={{
              //         borderRadius: 40, backgroundColor: '#f48004', height: this.state.height / 14,
              //         width: this.state.width / 2.5,
              //         justifyContent: 'center', alignItems: 'center'
              //     }} color="#f48004" title="Checkouts">
              //     <Text style={{ textAlign: 'center', color: '#fff', fontSize: 15 }}> CHECKOUT</Text>
              // </TouchableHighlight>
              }
            </View>
          </View>
        )}
      </View>
    );
  }
}
const mapStateToProps = ({cart}) => {
  const {
    user,
    isloading,
    selectedCategory,
    selectedProduct,
    totalItems,
    orderInCartList,
  } = cart;
  // debugger;
  return {
    user,
    isloading,
    selectedCategory,
    selectedProduct,
    totalItems,
    orderInCartList,
  };
};
export default connect(
  mapStateToProps,
  {checkoutButton, AddItem, UpdateUserAddressDatail},
)(Cart);

const styles = {
  ListStyle: {
    height: Dimensions.get('window').height / 5.5,
    //backgroundColor:'green',
    flexDirection: 'row',
  },
  ListStyleColum: {
    flex: 0.5,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  Imagestyle: {
    width: 128,
    height: 128,
    // backgroundColor: 'red',
    // borderRadius:( Dimensions.get('window').width/1.8)/2,
    resizeMode: 'contain',
  },
  DiscriptionBorder: {
    borderWidth: 1,
    borderColor: '#f48004',
    backgroundColor: 'white',
    width: width * 0.95,
    height: height * 0.25,
    margin: 10,
    borderRadius: 10,
    alignSelf:'center'
  },
};

Array.prototype.sum = function(prop) {
  var total = 0;
  for (var i = 0, _len = this.length; i < _len; i++) {
    total += this[i][prop];
  }
  return total;
};
