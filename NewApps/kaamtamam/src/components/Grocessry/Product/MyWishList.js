import React, { Component } from 'react';
import { View, TouchableHighlight, Dimensions } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
import { connect } from 'react-redux';
import { searchProductsByKeyword } from '../../../actions';
import { CustomHeaderWithText, IconInput } from '../Common/Header';
import { Spinner } from '../../common';
import { Actions } from 'react-native-router-flux';

class MyWishList extends Component {
    onTextChange(text) {
        debugger;
        this.props.searchProductsByKeyword(text);
        debugger;
    }
    render() {
        debugger;
        return (
            <View style={{ flex: 1, marginTop: 0 }}>
                <CustomHeaderWithText text="My Wish List" />
                <List>
              
                                <ListItem  style={{ marginLeft: 0, borderColor: 'orange', borderWidth: 1 ,alignItems:'center',justifyContent:'center'}}>
                                  
                                        <Text style={{alignSelf:'center'}} note>No favourite product yet</Text>
                                </ListItem>
                        

                </List>
            </View>
        );
    }
}

//searchedProductsList
//onPress={() => Actions.productDetail({item:this.props.item})}
const mapStateToProps = ({ productList }) => {
    const { isloading, searchedProductsList } = productList;
    //debugger;

    return { isloading, searchedProductsList };
};
export default connect(mapStateToProps, { searchProductsByKeyword })(MyWishList);
//export default SearchProducts;
const styles = {
    headerMainDiv: {
        backgroundColor: '#e2e2e2',

        borderBottomColor: '#f48004',
        borderBottomWidth: 0
    },
    leftIconDiv: {
        backgroundColor: 'transparent',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    centerDiv: { backgroundColor: '#fff', justifyContent: 'flex-start' },
    centerTextFieldDiv: {
        flexDirection: 'row', alignItems: 'center', borderColor: 'gray',
        backgroundColor: '#fff', borderWidth: 1, marginLeft: 10, marginRight: 10, borderRadius: 26
    },
    righIconDiv: { backgroundColor: 'transparent', flex: 1, justifyContent: 'center', alignItems: 'center' }
};