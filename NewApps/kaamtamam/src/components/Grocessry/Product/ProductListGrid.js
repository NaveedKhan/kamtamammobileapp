import React, { Component } from 'react';
import { View, Text, Button, Dimensions, Image, ScrollView, TouchableHighlight } from 'react-native';
import {
    Content,

    List,
    ListItem,
    Icon,
    Container,
    Left,
    Right,
    Badge
} from "native-base";
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { AddItem, RemoveItem, OnItemClick } from '../../../actions';
import { CommonHeader, SubHeader } from '../Common/Header';
import ProductGridColumn from './ProductListGridColum';

class ProductListGrid extends Component {
    debugger;
    render() {
        return (
            <View style={styles.ProductGridRowStyle}>

                <View style={styles.ProductGridInnerRow}>
                 
                        {/* product detail grid left side column */}
                     {this.props.item1 == undefined ? <View/> :<ProductGridColumn item={this.props.item1} />}   
                   
                        {/* product detail grid right  side column */}
                        {this.props.item2 == undefined ? <View/> :<ProductGridColumn  item={this.props.item2}/>}  
                </View>
            </View>
        );
    }
}

export default ProductListGrid;

const styles = {


    ProductGridRowStyle: {
        height: Dimensions.get('window').height / 2.45,
        backgroundColor: 'yellow'
    },
    ProductGridInnerRow: {
        backgroundColor: '#fcfcfc',//'#fcfcfc',
        flexDirection: 'row',
        //flex: 1,
        height: Dimensions.get('window').height / 2.45,
        alignItems: 'center'
    }
}