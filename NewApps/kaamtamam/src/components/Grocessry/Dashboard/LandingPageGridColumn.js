import React, { Component } from 'react';
import { View, Text, Button, Dimensions, Image, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { callDataFetch, Datafecthed, ProdcutSelected } from '../../../actions';
import { connect } from 'react-redux';

class LandingPageGridColumn extends Component {

    onPressItemCategory(id, name) {
        debugger;
        this.props.ProdcutSelected(id, name);
        Actions.productList({ id: id, categoryname: name })
    }

    render() {
        debugger;
        var imageaa = require('../Asset/Categoris/Beverages.png');
        let imageass = this.props.image.replace('http://localhost', 'http://10.0.2.2');
        let assImage_Http_URL = { uri: imageass };
        return (
            <View style={styles.ListStyleColum}>
                <TouchableHighlight onPress={() => this.onPressItemCategory(this.props.itemId, this.props.name)} style={styles.ListColumImageStyle}>
                    <Image source={assImage_Http_URL} style={styles.ListColumImageStyle} />
                </TouchableHighlight>
                <Text style={styles.ListColumnTextStyle}>{this.props.name}</Text>
            </View>
        );
    }
}


const mapStateToProps = ({ landingPageReducer }) => {
    const { user, isloading, selectedCategory, ProductsData } = landingPageReducer;
    // debugger;
    return { user, isloading, selectedCategory, ProductsData };
};
export default connect(mapStateToProps, { Datafecthed, callDataFetch, ProdcutSelected })(LandingPageGridColumn);

const styles = {
    ListStyle: {
        height: Dimensions.get('window').height / 5.5,
        //backgroundColor:'green',
        flexDirection: 'row',
        marginTop: 10
    },
    yellowback: {
        backgroundColor: 'transparent'
    },
    graywback: {
        backgroundColor: 'transparent'
    },
    brownback: {
        backgroundColor: 'transparent'
    },
    ListStyleColum: {
        flex: 0.5,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    }
    ,
    ListColumImageStyle: {
        height: Dimensions.get('window').height / 6.7,
        width: Dimensions.get('window').width / 2.4,
        borderRadius: 10,
        borderWidth: 2,
        overflow: 'hidden',
        borderColor: "gray",
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 2,
            width: 3
        }
    },
    ListColumnTextStyle: {
        color: '#000',
        fontSize: 13,
       // fontSize: 16,
        fontWeight: 'bold',
    }
};