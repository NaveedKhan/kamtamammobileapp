import React, { Component } from 'react';
import { View, Text, Button, Dimensions, Image, StatusBar, ScrollView, TouchableHighlight } from 'react-native';
import LandingPageGridColumn from './LandingPageGridColumn';

class LandingPageCategories extends Component {
    constructor(props) {
        super(props);
        const Categories22 = [
            { 'name': 'Fruits  & Vegetables', 'image': require('../Asset/Categoris/Fruits.jpg'), "id": 1 },
            { 'name': 'Beverages', 'image': require('../Asset/Categoris/Beverages.png'), "id": 2 },
            { 'name': 'Branded Food', 'image': require('../Asset/Categoris/BrandedFood.jpg'), "id": 3 },
            { 'name': 'Dairy,Bakery', 'image': require('../Asset/Categoris/Dairy.jpg'), "id": 4 },
            { 'name': 'Meats', 'image': require('../Asset/Categoris/Beverages.png'), "id": 5 },
            { 'name': 'Home Care', 'image': require('../Asset/Categoris/HomeCare.png'), "id": 6 },
            { 'name': 'Medicines', 'image': require('../Asset/Categoris/Dairy.jpg'), "id": 7 },
            { 'name': 'Beauty Items', 'image': require('../Asset/Categoris/HomeCare.png'), "id": 8 }
        ];



        // "Fruits","Vegetables","Meat","Drinks/Beverages","Retail Items","Baked Items","Beauty Items","Medicines","Branded Food"};


        this.state = {
            //cate: Categories22,
            productCategories: this.props.productCategories
        }

    };

    _renderCategoriesData() {

    }

    render() {
      //  debugger
        //const categories = this.state.cate;
        const categoriesDB = this.props.productCategories;
        let count = Math.ceil(categoriesDB.length);
        let numberofrows = Math.ceil(count / 2);
        var numberofrowsArray = []
        for (i = 0; i < numberofrows; i++) {
            numberofrowsArray.push(i)
        }

        return (
            <View style={{ flexDirection: 'column', backgroundColor: '#fff' }}>

                {
                    numberofrowsArray.map(el => {
                       // debugger;
                        var firstIndex = ((el * 2) + 0);
                        var secondIndex = ((el * 2) + 1);

                        var name1 = categoriesDB[firstIndex] == undefined ? "" : categoriesDB[firstIndex].itemCategoryName;
                        var image1 = categoriesDB[firstIndex] == undefined ? "" : categoriesDB[firstIndex].imageName;
                        var id1 = categoriesDB[firstIndex] == undefined ? "" : categoriesDB[firstIndex].itemCategoryID
                        // let ids = el.id;

                        var name2 = categoriesDB[secondIndex] == undefined ? "" : categoriesDB[secondIndex].itemCategoryName;
                        var image2 = categoriesDB[secondIndex] == undefined ? "" : categoriesDB[secondIndex].imageName;
                        var id2 = categoriesDB[secondIndex] == undefined ? "" : categoriesDB[secondIndex].itemCategoryID
                       // debugger;
                        return (
                            <View style={styles.ListStyle} key={el}>

                                <LandingPageGridColumn image={image1} name={name1} itemId={id1} />

                                {
                                    categoriesDB[secondIndex] == undefined ?
                                        <View /> :
                                        <LandingPageGridColumn image={image2} name={name2} itemId={id2} />
                                }
                            </View>
                        )
                    })

                }


            </View>
        );
    }
}

export default LandingPageCategories;

const styles = {
    ListStyle: {
        height: Dimensions.get('window').height / 5.5,
        //backgroundColor:'green',
        flexDirection: 'row',
        marginTop: 10
    },
    yellowback: {
        backgroundColor: 'transparent'
    },
    graywback: {
        backgroundColor: 'transparent'
    },
    brownback: {
        backgroundColor: 'transparent'
    },
    ListStyleColum: {
        flex: 0.5,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    }
    ,
    ListColumImageStyle: {
        height: Dimensions.get('window').height / 6.7,
        width: Dimensions.get('window').width / 2.4,
        borderRadius: 10,
        borderWidth: 2,
        overflow: 'hidden',
        borderColor: "black",
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 2,
            width: 3
        }
    },
    ListColumnTextStyle: {
        color: '#000',
        fontSize: 14
    }
};