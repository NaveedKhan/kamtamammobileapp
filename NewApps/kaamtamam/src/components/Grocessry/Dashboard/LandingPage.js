import React, { Component } from 'react';
import { View, Text, Button, Dimensions, Image, StatusBar, ScrollView } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Icon, Input, ScrollableTab } from 'native-base';
import { Actions } from 'react-native-router-flux';
//import Slideshow from 'react-native-image-slider-show';
import { callDataFetch, Datafecthed } from '../../../actions';
import { connect } from 'react-redux';
import { CommonHeader, CustomHeader } from '../Common/Header';
import Carousel from '../Carousel'
import LandingPageCategories from './LandingPageCategories';
import { products, productsCat } from '../../../components/common/Cities.json';
import { CustomFooterTab } from '../Common/Footer';
import CustomFooter from '../../common/CustomFooter';
class LandingPage extends Component {
  constructor(props) {
    super(props);
    debugger;
    this.state = {
      productsList: this.props.ProductsData.products,
      productCategories: this.props.ProductsData.productsCat,
    };
  }



  componentDidMount() {
    // this.props.Datafecthed();

  }





  render() {
    return (

      <View style={{ flex: 1 }}>
        <StatusBar hidden />
        <CustomHeader leftIcon={true} searchBar={true} rightIcon={true} inputPressed={() => Actions.searchProducts()} disabled={true} />
        <ScrollView scrollEnabled={true} style={{ flex: 1 }} >
          {/* <View style={{ height: 200, backgroundColor: 'transparent' }}>
            <Carousel />

          </View> */}

          <LandingPageCategories productCategories={this.state.productCategories} />
          {/* <Tabs style={{ flex: 1 }} tabBarUnderlineStyle={{ backgroundColor: '#f48004' }} renderTabBar={() => <ScrollableTab />}>

            <Tab heading="CATEGORIES" tabStyle={{ backgroundColor: '#fff' }}
              textStyle={{ color: '#666363' }} activeTextStyle={{ color: '#f48004', fontWeight: 'bold' }} activeTabStyle={{ backgroundColor: '#fff' }}>
              <LandingPageCategories productCategories={this.state.productCategories} />
            </Tab>
       

          </Tabs> */}

        </ScrollView>
        <CustomFooter landingTo="G" selectedApp="Grocerry" cartRequestsCount={this.props.orderInCartList.length} />
      </View>
    );
  }
}
const mapStateToProps = ({ landingPageReducer, productList }) => {
  const { user, isloading, selectedCategory, ProductsData } = landingPageReducer;
  const { orderInCartList } = productList;

  return { user, isloading, selectedCategory, ProductsData, orderInCartList };
};
export default connect(mapStateToProps, { Datafecthed, callDataFetch })(LandingPage);

const styles = {
  ListStyle: {
    height: Dimensions.get('window').height / 5.5,
    //backgroundColor:'green',
    flexDirection: 'row'
  },
  yellowback: {
    backgroundColor: 'transparent'
  },
  graywback: {
    backgroundColor: 'transparent'
  },
  brownback: {
    backgroundColor: 'transparent'
  },
  ListStyleColum: {
    flex: 0.5,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  }
  ,
  ListColumImageStyle: {
    height: 90,
    width: Dimensions.get('window').width / 2.4,
    borderRadius: 10,
    borderWidth: 2,
    overflow: 'hidden',
    borderColor: "black"
  },
  ListColumnTextStyle: {
    color: '#fff',
    fontSize: 15
  }
};