import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TouchableOpacity } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Button, Input, Icon, Thumbnail,Spinner } from 'native-base';
import { logOut } from '../../../actions';
import { CommonHeader, CustomHeader, CustomHeaderWithText } from '../Common/Header';

class MyAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
        }
        debugger;
        //login page
    }
    _renderItem({ item, index }) {
        return (
            <View style={styles.slide}>
                <Text style={styles.title}>{item.title}</Text>
            </View>
        );
    }
    logoutUser() {
        this.props.logOut();
    }
    render() {
        //0088A6
        const images = require('../Asset/Kaam_Tamam_Logo.png');
       // debugger;
        const { user } = this.props;
        return (
            <View style={styles.mainContainerDiv}>


                <CustomHeaderWithText text="My Account " />

                <View style={styles.contentDiv}>

                    <View style={styles.thumbnailDiv}>
                        <Thumbnail large source={images} style={{ height: 110, width: 110,resizeMode:'contain', borderRadius: 60, overflow: 'hidden', backgroundColor:'#f48004' }} />
                        <Button transparent rounded style={styles.editButtonStyle}>
                            <Text style={styles.editButtonTextStylel} >EDIT</Text>
                        </Button>
                    </View>

                    <View style={{ flex: 7.5, justifyContent: 'flex-start', alignItems: 'stretch', backgroundColor: 'transparent' }} >
                        <View style={{ flex: 1, backgroundColor: 'transparent', margin: 10 }}>
                            <View style={{  borderBottomColor: '#f48004', borderBottomWidth: 1, alignItems: 'center', margin: 3 }} >

                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{(user && user.fullName) ? user.fullName.toUpperCase() : ''}</Text>
                                <Text style={{ fontWeight: 'normal', fontSize: 16 }}>{(user && user.phoneNumber) ? user.phoneNumber : ''}</Text>
                                {/* <Text style={{ fontWeight: 'normal', fontSize: 16 }}> {(user && user.email) ? user.email : ''}</Text> */}
                            </View>


                            {/* <View style={{ height: 70, borderBottomColor: '#f48004', borderBottomWidth: 1, alignItems: 'center', margin: 3 }} >
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Address</Text>
                                <Text numberOfLines={2} style={{ marginLeft: 20, marginRight: 20, fontWeight: 'normal', fontSize: 16, textAlign: 'center' }}>This is my her are spme s sds addess you are going to check </Text>
                            </View> */}


                            <View style={{ height: 60, borderBottomColor: '#f48004', borderBottomWidth: 1, alignItems: 'center', margin: 3, justifyContent: 'center' }} >

                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name='key' style={{ marginLeft: 10, color: '#000', fontSize: 32 }} />
                                    <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 8 }}>Chnge Password</Text>
                                </View>

                            </View>


                            <View style={{ height: 60, borderBottomColor: '#f48004', borderBottomWidth: 1, alignItems: 'center', margin: 3, justifyContent: 'center' }} >

                                {
                                this.props.loading ? <Spinner /> :
                                    <TouchableOpacity onPress={() => this.logoutUser()} style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name='log-out' type="Entypo" style={{ marginLeft: 10, color: '#000', fontSize: 22 }} />
                                        <Text style={{ fontWeight: 'bold', fontSize: 16, marginLeft: 8 }}>SignOut</Text>
                                    </TouchableOpacity>
                                    }

                            </View>


                            <View style={{ height: 200 }}>

                            </View>



                        </View>
                    </View>

                </View>

            </View>
        );
    }
}
const mapStateToProps = ({ auth }) => {
    const { loading } = auth;

    return { user: auth.user, loading };
};
export default connect(mapStateToProps, { logOut })(MyAccount);
//export default MyAccount;

const styles = {
    mainContainerDiv: {
        flex: 1,
        backgroundColor: '#fff'
    },
    contentDiv: {
        flex: 1,
        // backgroundColor: 'blue'
    },
    thumbnailDiv: {
        flex: 2.5,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        marginTop: 10,
         backgroundColor: 'transparent'
    },
    editButtonStyle: {
        width: 70,
        height: 25,
        justifyContent: 'center',
        borderColor: '#f48004',
        borderWidth: 1,
        backgroundColor: 'transparent'
    },
    editButtonTextStyle: {
        fontSize: 13,
        color: '#000',
        fontWeight: 'bold'
    },

    errorTextStyel: {
        fontSize: 17,
        alignSelf: 'center',
        color: 'red'
    },
    Imagestyle:
    {
        width: Dimensions.get('window').width / 1.5,
        height: Dimensions.get('window').width / 1.5,
        // borderRadius:( Dimensions.get('window').width/1.8)/2,
        resizeMode: 'contain',

        // opacity: 0.9
    }
};