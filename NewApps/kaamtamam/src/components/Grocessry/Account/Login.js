import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableHighlight,
  StatusBar,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Button, Input, Icon, Toast, Spinner} from 'native-base';
import {
  emailChanged,
  passwordChanged,
  loginUser,
  UpdateUserFormData,
} from '../../../actions';
import FormValidator from '../../HelperClasses/FormValidator';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: Dimensions.get('window').height,
      width: Dimensions.get('window').width,
    };
    debugger;
    //login page
  }

  updateFormData(fieldname, value) {
    this.props.UpdateUserFormData({prop: fieldname, value: value});
  }

  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  onButtnPressed() {
    const {email, password, error,DeviceTocken,DeviceID} = this.props;
    debugger;
    // Actions.drawerOpen();
    var arrayerr = [];
    if (email == '' || email == null) {
      arrayerr.push('Please enter Email address');
    }

    if (password == '' || password == null) {
      arrayerr.push('Please enter password');
    }
    //  else {
    //     var result = FormValidator.validatePassword(password);
    //     if (!result) {
    //         arrayerr.push('Please enter valid password');
    //     }
    // }
    if (arrayerr.length > 0) {
      Alert.alert('invalid form', arrayerr.toString());
    } else {
      this.props.loginUser({
        email: email.trim(),
        password: password.trim(),
        fromSplashscreen: false ,
        token:DeviceTocken,
        deviceId:DeviceID
      });
    }
  }

//   componentDidMount() {
//     if (this.props.error != '') {
//       Toast.show({
//         text: 'Wrong password!',
//         buttonText: 'Okay',
//         buttonTextStyle: {color: '#008000'},
//         buttonStyle: {backgroundColor: '#5cb85c'},
//       });
//     }
//   }
  showToast2() {
    return Toast.show({
      text: 'Order already marked completed!',
      buttonText: 'Okay',
      type: 'danger',
      duration: 3000,
    });
  }
  swapButtonAndSpinner() {
    return this.props.loading ? (
      <Spinner />
    ) : (
      <TouchableHighlight
        style={styles.buttonStyle}
        onPress={() => this.onButtnPressed()}>
        {/* <Button bordered warning rounded style={{ width: 250, justifyContent: 'center', borderColor: '#fff', borderWidth: 2 }} onPress={() => this.onButtnPressed()}> */}
        <Text style={{fontSize: 15, color: '#ffff', fontWeight: 'bold'}}>
          SIGN IN
        </Text>
      </TouchableHighlight>
    );
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#f78320'}}>
        <StatusBar hidden />

        <View
          style={{
            flex: 4,
            justifyContent: 'center',
            alignItems: 'center',
            margin: 10,
          }}>
          <Image
            source={require('../Asset/Kaam_Tamam_Logo.png')}
            style={styles.Imagestyle}
          />
        </View>

        <View
          style={{
            flex: 3,
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
          }}>
          <View
            style={{
              marginTop: 5,
              flexDirection: 'row',
              alignItems: 'center',
              borderColor: '#fff',
              borderBottomWidth: 0.7,
              width: Dimensions.get('window').width / 1.3,
            }}>
            <Icon
              name="person"
              style={{color: '#fff', fontSize: 24, marginLeft: 4}}
            />
            <Input
              placeholder="Username"
              placeholderTextColor="#fff"
              onChangeText={text => this.updateFormData('email', text)}
              value={this.props.email}
              style={{marginLeft: 20, fontSize: 18, color: '#fff'}}
            />
          </View>
          <View
            style={{
              marginTop: 5,
              flexDirection: 'row',
              alignItems: 'center',
              borderColor: '#fff',
              borderBottomWidth: 0.7,
              width: Dimensions.get('window').width / 1.3,
            }}>
            <Icon
              name="lock"
              style={{color: '#fff', fontSize: 24, marginLeft: 4}}
            />
            <Input
              placeholder="Password"
              placeholderTextColor="#fff"
              secureTextEntry={true}
              style={{marginLeft: 20, fontSize: 18, color: '#fff'}}
              onChangeText={text => this.updateFormData('password', text)}
              value={this.props.password}
            />
          </View>
        </View>

        <View
          style={{
            flex: 2,
            justifyContent: 'space-around',
            alignItems: 'center',
            backgroundColor: 'transparent',
          }}>
          {this.swapButtonAndSpinner()}

          <Button
            rounded
            transparent
            style={{width: 250, justifyContent: 'center'}}
            onPress={() => Actions.signUp()}>
            <Text
              style={{
                fontSize: 13,
                color: '#ffff',
                textDecorationLine: 'underline',
              }}>
              Dont have account? Signup
            </Text>
          </Button>
        </View>

        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'bltransparentue',
          }}
        />
      </View>
    );
  }
}
const mapStateToProps = ({auth}) => {
  const {email, password, error, loading,DeviceTocken,DeviceID} = auth;

  return {email, password, error, loading,DeviceTocken,DeviceID};
};
export default connect(
  mapStateToProps,
  {emailChanged, passwordChanged, loginUser, UpdateUserFormData},
)(Login);

const styles = {
  errorTextStyel: {
    fontSize: 17,
    alignSelf: 'center',
    color: 'red',
  },
  Imagestyle: {
    width: Dimensions.get('window').width / 1.6,
    height: Dimensions.get('window').width / 1.6,
    // borderRadius:( Dimensions.get('window').width/1.8)/2,
    resizeMode: 'contain',

    // opacity: 0.9
  },
  buttonStyle: {
    width: 250,
    height: 50,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#fff',
    borderWidth: 1.7,
  },
  buttonsArea: {
    flex: 3,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
};
