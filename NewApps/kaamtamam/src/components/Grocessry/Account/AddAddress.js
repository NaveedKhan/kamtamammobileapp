import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Text, ScrollView, Dimensions} from 'react-native';
import {
  Container,
  Header,
  Content,
  Item,
  Input,
  Icon,
  Button,
  Spinner,
} from 'native-base';
import {
  emailsignupChanged,
  passwordSignupChanged,
  usernameChanged,
  phoneChanged,
  fullnameChanged,
  signupUser,
} from '../../../actions';

import {CommonHeader, CustomHeaderWithText} from '../Common/Header';
class AddAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: Dimensions.get('window').height,
      width: Dimensions.get('window').width,
    };
  }

  fullnamechangeSignup(txt) {
    this.props.fullnameChanged(txt);
  }

  emailhangeSignup(txt) {
    this.props.emailsignupChanged(txt);
  }

  passwordchangeSignup(txt) {
    this.props.passwordSignupChanged(txt);
  }

  phonechangeSignup(txt) {
    this.props.phoneChanged(txt);
  }

  usernamechangeSignup(txt) {
    this.props.usernameChanged(txt);
  }

  signupClicked() {
    const {
      email,
      password,
      fullname,
      phone,
      error,
      username,
      loading,
    } = this.props;
    //  this.props.signupUser({ email, password, fullname, phone, error, username });
  }

  swapButtonAndSpinner() {
    return this.props.loading ? (
      <Spinner />
    ) : (
      <Button
        rounded
        block
        warnings
        style={{backgroundColor: '#f48004'}}
        onPress={() => this.signupClicked()}>
        <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 19}}>
          ADD LOCATION
        </Text>
      </Button>
    );
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <CustomHeaderWithText text="Address" showMenu={false} />
        <ScrollView>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignContent: 'center',
              margin: 20,
              marginTop: 5,
            }}>
            <View
              style={{
                height: this.state.height / 9.5,
                alignContent: 'center',
                justifyContent: 'center',
                margin: 10,
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 22, fontWeight: 'bold'}}>
                {' '}
                Enter Address Details
              </Text>
            </View>

            <View
              style={{
                marginTop: 15,
                flexDirection: 'row',
                alignItems: 'center',
                borderColor: 'gray',
                borderBottomWidth: 0.7,
              }}>
              <Icon
                name="pin"
                type="Entypo"
                style={{color: '#f48004', fontSize: 22}}
              />
              <Input
                placeholder="Street"
                style={{marginLeft: 20, fontSize: 15}}
                onChangeText={this.fullnamechangeSignup.bind(this)}
                value={this.props.fullname}
              />
            </View>

            <View
              style={{
                marginTop: 15,
                flexDirection: 'row',
                alignItems: 'center',
                borderColor: 'gray',
                borderBottomWidth: 0.7,
              }}>
              <Icon
                name="home"
                type="AntDesign"
                style={{color: '#f48004', fontSize: 22}}
              />
              <Input
                placeholder="House No"
                style={{marginLeft: 20, fontSize: 15}}
                onChangeText={this.phonechangeSignup.bind(this)}
                value={this.props.phone}
              />
            </View>

            <View
              style={{
                marginTop: 15,
                flexDirection: 'row',
                alignItems: 'center',
                borderColor: 'gray',
                borderBottomWidth: 0.7,
              }}>
              <Icon
                name="map"
                type="Entypo"
                style={{color: '#f48004', fontSize: 22}}
              />
              <Input
                placeholder="Block/Sector"
                style={{marginLeft: 20, fontSize: 15}}
                onChangeText={this.usernamechangeSignup.bind(this)}
                value={this.props.username}
              />
            </View>

            <View
              style={{
                marginTop: 15,
                flexDirection: 'row',
                alignItems: 'center',
                borderColor: 'gray',
                borderBottomWidth: 0.7,
              }}>
              <Icon name="mail" style={{color: '#f48004', fontSize: 22}} />
              <Input
                placeholder="Nearest Place"
                style={{marginLeft: 20, fontSize: 15}}
                onChangeText={this.emailhangeSignup.bind(this)}
                value={this.props.email}
              />
            </View>

            <View
              style={{
                alignContent: 'stretch',
                justifyContent: 'space-around',
                alignItems: 'center',
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
              }}>
              <Button
                iconLeft
                transparent
                bordered
                block
                rounded
                style={{borderColor: 'orange', borderWidth: 2}}>
                <Icon name="pin" type="Entypo" />
                <Text style={{marginLeft: 10}}>Use My Location</Text>
              </Button>
            </View>

            <View
              style={{
                flex: 0.2,
                marginTop: 30,
                alignContent: 'stretch',
                justifyContent: 'space-around',
                alignItems: 'center',
                marginLeft: 30,
                marginRight: 30,
              }}>
              {this.swapButtonAndSpinner()}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({signUp}) => {
  const {email, password, fullname, phone, error, username, loading} = signUp;

  return {email, password, fullname, phone, error, username, loading};
};

export default connect(
  mapStateToProps,
  {
    emailsignupChanged,
    passwordSignupChanged,
    usernameChanged,
    phoneChanged,
    fullnameChanged,
    signupUser,
  },
)(AddAddress);

const styles = {
  container: {
    height: 200,
    backgroundColor: '#F5FCFF',
  },
  backgroundOrange: {
    backgroundColor: 'orange',
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent',
  },
  done: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
    backgroundColor: 'transparent',
  },
};
