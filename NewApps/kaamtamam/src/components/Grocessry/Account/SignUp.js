import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  StatusBar,
  PermissionsAndroid,
  Alert,
} from 'react-native';
import {Input, Icon, Button, Spinner, Toast} from 'native-base';
import {
  emailsignupChanged,
  passwordSignupChanged,
  UpdateUserAddressDatail,
  usernameChanged,
  phoneChanged,
  fullnameChanged,
  signupUser,
  SignupUpdateFormData,
} from '../../../actions';
import Geolocation from 'react-native-geolocation-service';
import FormValidator from '../../HelperClasses/FormValidator';
import Geocode from 'react-geocode';
import {CustomHeaderWithText} from '../Common/Header';
import {Actions} from 'react-native-router-flux';

Geocode.setApiKey('AIzaSyCZ_GeN6VIBOgZqe9mZ568ygvB8eUNuSbc');
Geocode.setLanguage('en');
Geocode.enableDebug();

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: Dimensions.get('window').height,
      width: Dimensions.get('window').width,

      showToast: false,
    };
  }

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Camera Permission',
          message:
            'Kaam Tamam App needs access to your camera ' +
            'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        await this.getCurrentLocation();
        console.log('You can use the camera');
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  componentDidMount() {
    debugger;
   // this.requestCameraPermission();
  }

  async getCurrentLocation() {
    var me = this;
    await Geolocation.getCurrentPosition(
      position => {
        console.log(position);
        let lats =
          String(position.coords.latitude).indexOf('37.') == 0
            ? 33.6641233
            : position.coords.latitude;
        let longs =
          String(position.coords.longitude).indexOf('-122') == 0
            ? 73.0505619
            : position.coords.longitude;

        Geocode.fromLatLng(lats, longs)
          .then(
            response => {
              debugger;
              const address = response.results[0].formatted_address;
              //userCurrentLat: 33.9976884
              //userCurrentLong: 71.4695614
              //Board Bazar Stop, University latsRoad, Peshawar, Pakistan
              let userCurrentAddressDetail = {
                userCurrentLat: lats,
                userCurrentLong: longs,
                userCurrentAddress: address,
              };
              me.props.UpdateUserAddressDatail({
                prop: 'userCurrentAddressDetail',
                value: userCurrentAddressDetail,
              });
              me.props.UpdateUserAddressDatail({
                prop: 'isCurrentLocatonSelected',
                value: true,
              });
              console.log(address);
            },
            error => {
              debugger;
              console.error(error);
            },
          )
          .catch(error => {
            debugger;
            //loginUserFail(dispatch, error);
            console.log('error', error);
          });
      },
      error => {
        console.log(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 15000},
    );
  }

  updateFormData(fieldname, value) {
    this.props.SignupUpdateFormData({prop: fieldname, value: value});
  }

  // fullnamechangeSignup(txt) {
  //     this.props.fullnameChanged(txt);
  // }

  // emailhangeSignup(txt) {
  //     this.props.emailsignupChanged(txt);
  // }

  // passwordchangeSignup(txt) {
  //     this.props.passwordSignupChanged(txt);
  // }

  // phonechangeSignup(txt) {
  //     this.props.phoneChanged(txt);
  // }

  // usernamechangeSignup(txt) {
  //     this.props.usernameChanged(txt);
  // }

  signupClicked() {
    debugger;
    const {
      email,
      password,
      fullname,
      phone,
      error,
      username,
      loading,
    } = this.props;
    var arrayerr = ["Please enter  "];

    if (fullname == '' || fullname == null) {
      arrayerr.push(' fullname');
    } else {
      var result = FormValidator.NameFieldValidation(fullname.trim());
      if (!result) {
        arrayerr.push(' valid fullname');
      }
    }

    if (username == '' || username == null) {
      arrayerr.push(' username');
    } else {
      var result = FormValidator.UserNameFieldValidation(username.trim());
      if (!result) {
        arrayerr.push(' valid username');
      }
    }

    if (phone == '' || phone == null) {
      arrayerr.push('  phone');
    } else {
      var result = FormValidator.validatePhoneNumber(phone.trim());
      if (!result) {
        arrayerr.push(' valid phone');
      }
    }
    // if (email == '' || email == null) {
    //   arrayerr.push('Please enter Email address');
    // } else {
    //   var result = FormValidator.validateEmail(email.trim());
    //   if (!result) {
    //     arrayerr.push('Please enter valid email');
    //   }
    // }

    if (password == '' || password == null) {
      arrayerr.push(' password');
    } else {
      var result = FormValidator.validatePassword(password.trim());
      if (!result) {
        arrayerr.push(' valid alpha numeric password');
      }
    }

    if (arrayerr.length > 1) {
      Alert.alert('invalid form', arrayerr.toString());
    } else {
      this.props.signupUser({
        email:email.trim(),
        password:password.trim(),
        fullname:fullname.trim(),
        phone:phone.trim(),
        error,
        username:username.trim(),
      });
    }

    //this.showToast2();
  }

  showToast2() {
    return Toast.show({
      text: 'Wrong password!',
      buttonText: 'Okay',
      type: 'danger',
      duration: 3000,
    });
  }

  swapButtonAndSpinner() {
    return this.props.loading ? (
      <Spinner />
    ) : (
      <Button
        rounded
        block
        warnings
        style={{backgroundColor: '#f48004'}}
        onPress={() => this.signupClicked()}>
        <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 19}}>
          SING UP
        </Text>
      </Button>
    );
  }

  render() {
    const {
      userCurrentAddressDetail,
      userDeliveryAddressDetail,
      isCurrentLocatonSelected,
    } = this.props;
    const {
      userCurrentLat,
      userCurrentLong,
      userCurrentAddress,
    } = userCurrentAddressDetail;
    const {
      requsetLat,
      requetLong,
      requestSearchAddress,
    } = userDeliveryAddressDetail;

    return (
      <View style={{flex: 1}}>
        <StatusBar hidden />
        <CustomHeaderWithText text="SIGNUP" showMenu={false} />
        <ScrollView>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              justifyContent: 'center',
              margin: 20,
              marginTop: 5,
            }}>
            <View
              style={{
                height: this.state.height / 9.5,
                alignContent: 'center',
                justifyContent: 'center',
                margin: 10,
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 22, fontWeight: 'bold'}}>
                {' '}
                Personal Details
              </Text>
            </View>

            <View
              style={{
                marginTop: 15,
                flexDirection: 'row',
                alignItems: 'center',
                borderColor: 'gray',
                borderBottomWidth: 0.5,
              }}>
              <Icon name="person" style={{color: '#f48004', fontSize: 22}} />
              <Input
                placeholder="Full Name"
                style={{marginLeft: 20, fontSize: 15}}
                onChangeText={text => this.updateFormData('fullname', text)}
                value={this.props.fullname}
              />
            </View>

            <View
              style={{
                marginTop: 15,
                flexDirection: 'row',
                alignItems: 'center',
                borderColor: 'gray',
                borderBottomWidth: 0.5,
              }}>
              <Icon name="call" style={{color: '#f48004', fontSize: 22}} />
              <Input
                placeholder="Phone"
                style={{marginLeft: 20, fontSize: 15}}
                onChangeText={text => this.updateFormData('phone', text)}
                value={this.props.phone}
              />
            </View>

            <View
              style={{
                marginTop: 15,
                flexDirection: 'row',
                alignItems: 'center',
                borderColor: 'gray',
                borderBottomWidth: 0.5,
              }}>
              <Icon name="calendar" style={{color: '#f48004', fontSize: 22}} />
              <Input
                placeholder="username"
                style={{marginLeft: 20, fontSize: 15}}
                onChangeText={text => this.updateFormData('username', text)}
                value={this.props.username}
              />
            </View>

            {/* <View
              style={{
                marginTop: 15,
                flexDirection: 'row',
                alignItems: 'center',
                borderColor: 'gray',
                borderBottomWidth: 0.5,
              }}>
              <Icon name="mail" style={{color: '#f48004', fontSize: 22}} />
              <Input
                placeholder="Email"
                style={{marginLeft: 20, fontSize: 15}}
                onChangeText={text => this.updateFormData('email', text)}
                value={this.props.email}
              />
            </View> */}

            <View
              style={{
                marginTop: 15,
                flexDirection: 'row',
                alignItems: 'center',
                borderColor: 'gray',
                borderBottomWidth: 0.5,
              }}>
              <Icon name="lock" style={{color: '#f48004', fontSize: 22}} />
              <Input
                placeholder="Password"
                style={{marginLeft: 20, fontSize: 15}}
                secureTextEntry={true}
                onChangeText={text => this.updateFormData('password', text)}
                value={this.props.password}
              />
            </View>
            <Text style={{color:'green', fontSize:12}}>Password must contain at least one character and one digit. e.g abcd123 </Text>

            <View
              style={{
                flex: 0.2,
                marginTop: 30,
                alignContent: 'stretch',
                justifyContent: 'space-around',
                alignItems: 'center',
                marginLeft: 30,
                marginRight: 30,
              }}>
              {this.swapButtonAndSpinner()}
            </View>
            <View
              style={{
                height: this.state.height / 10,
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <Text
                numberOfLines={2}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignContent: 'center',
                  textAlign: 'center',
                  width: 230,
                }}>
                By continuing you agree to read and confirm to our terms and
                conditions
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({signUp, auth, userAddress}) => {
  const {
    email,
    password,
    fullname,
    phone,
    error,
    username,
    loading,
    houseNo,
    streetNo,
    blockNo,
    fullAddress,
  } = signUp;
  const {userCurrentLat, userCurrentLong, userCurrentAddress} = auth;
  // debugger;
  const {
    userCurrentAddressDetail,
    userDeliveryAddressDetail,
    isCurrentLocatonSelected,
  } = userAddress;
  return {
    email,
    password,
    fullname,
    phone,
    error,
    username,
    houseNo,
    streetNo,
    blockNo,
    fullAddress,
    loading,
    userCurrentLat,
    userCurrentLong,
    userCurrentAddress,
    userCurrentAddressDetail,
    userDeliveryAddressDetail,
    isCurrentLocatonSelected,
  };
};

export default connect(
  mapStateToProps,
  {
    emailsignupChanged,
    passwordSignupChanged,
    usernameChanged,
    phoneChanged,
    fullnameChanged,
    signupUser,
    SignupUpdateFormData,
    UpdateUserAddressDatail,
  },
)(SignUp);

const styles = {
  container: {
    backgroundColor: '#e6e7e9',
    marginTop: 15,
    marginBottom: 5,
    borderRadius: 9,
    overflow: 'hidden',
  },
  backgroundOrange: {
    backgroundColor: 'orange',
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent',
  },
  done: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
    backgroundColor: 'transparent',
  },
};
