import React, {Component} from 'react';
import {View, Text, Dimensions, TouchableHighlight} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {
  Container,
  Header,
  Content,
  Tab,
  Tabs,
  Icon,
  Input,
  ScrollableTab,
  Button,
} from 'native-base';
import {IconInput} from '.';
import {TouchableOpacity} from 'react-native-gesture-handler';

class CustomHeaderWithText extends Component {
  clickc() {
    debugger;
    Actions.drawerOpen();
  }

  backpressed() {
    debugger;

    Actions.pop();
  }

  render() {
    const {
      leftIcon,
      searchBar,
      rightIcon,
      refreshButton,
      refreshButtonClicked,
    } = this.props;
    return (
      <View style={styles.headerMainDiv}>
        <TouchableHighlight
          style={styles.leftIconDiv}
          onPress={() => this.backpressed()}>
          <Icon
            name="arrow-back"
            style={{color: '#f48004', fontSize: 35, marginLeft: 10}}
          />
        </TouchableHighlight>
        <View style={styles.centerDiv}>
          <Text
            style={{
              fontSize: 22,
              color: '#f48004',
              alignSelf: 'center',
              fontWeight: 'bold',
            }}>
            {this.props.text}
          </Text>
        </View>
        {refreshButton ? (
          <TouchableHighlight
          style={styles.righIconDiv}
            onPress={refreshButtonClicked}>
            <View > 
              <Icon
                name="refresh"
                type={'FontAwesome'}
                style={{color: '#f48004', fontSize: 35,alignSelf:'center'}}
              />
              <Text>Refresh</Text>
            </View>
          </TouchableHighlight>
        ) : (
          <View />
        )}
        {this.props.showMenu ? (
          <TouchableHighlight
            style={styles.righIconDiv}
            onPress={() => this.clickc()}>
            <Icon name="menu" style={{color: '#f48004', fontSize: 27}} />
          </TouchableHighlight>
        ) : (
          <View style={{flex: 1}} />
        )}
      </View>
    );
  }
}

export {CustomHeaderWithText};

const styles = {
  headerMainDiv: {
    height: Dimensions.get('window').height / 8,
    backgroundColor: '#e2e2e2', //'#f48004',
    flexDirection: 'row',
  },
  leftIconDiv: {
    backgroundColor: 'transparent',
    width: Dimensions.get('window').width /5,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  centerDiv: {
    backgroundColor: 'transparent',
    width: Dimensions.get('window').width /1.7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerTextFieldDiv: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: 'gray',
    backgroundColor: '#fff',
    borderWidth: 1,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 26,
  },
  righIconDiv: {
    width: Dimensions.get('window').width /5,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
};
