import React, {
    Component
} from 'react';
import {
    Text,
    View,StyleSheet, Image,Dimensions
} from 'react-native';
import { Container, Header, Content,Left,Right,Body,Title, Item, Input, Icon,Button,Badge  } from 'native-base';
import { Actions } from 'react-native-router-flux';
//import { config } from "../../src/config";
//import axios from "axios";


// const CommonHeader = ({text,UserCompany,orgid})=>
// {
class CommonHeader extends Component {
    constructor(props) {
      super(props);
      this.state = {
        //starCount: 3.5,
      //  modalVisible: this.props.isVisible
      };
}



   

  render(){
   const {text,styles,rightButton} = this.props;
    return(
        <Header   style={[{backgroundColor:"#f48004",marginLeft:10,marginLeft:0},styles]}>
        <Left>
          <Button transparent  onPress={()=> Actions.pop()} >
            <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} />
          </Button>
        </Left>
        <Body style={{backgroundColor:'transparent',alignItems:'center',justifyContent:'center',alignContent:'center'}}>
          <Title style={styless.headerStyle}>{text}</Title>
        </Body>
        {
         rightButton? 
        
        <Right>
          <Button transparent badge vertical onPress={()=> console.log('back clicked')}>
            <Badge style={{height:20,width:20,justifyContent:'center',alignItems:'center'}}><Text style={{fontSize:11}}>2</Text></Badge>
            <Icon type="FontAwesome"  name="shopping-cart"  style={{fontSize:30, color: '#fff'}}/>
          </Button>
        </Right>
        :
        <Right/>
        }
      </Header>
    )
  }
}

export  {CommonHeader};

const styless = StyleSheet.create({
    containerStyle: {
        flex:1,justifyContent: 'flex-start', flexDirection: 'column',  alignContent:'flex-start'
    },
    contentStyle:
    {
        flex: 1,backgroundColor: '#3f8db4', flexDirection: 'column'
    },
    headerStyle:{
        color:'#fff',fontSize:16,width:200,fontWeight:'bold',
        textAlign:'center',
        justifyContent:'center',
        alignSelf:'center'
    }
  });