import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableHighlight, BackHandler } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Tab, Tabs, Icon, Input, ScrollableTab, Button } from 'native-base';
import { IconInput } from './';


class CustomHeader extends Component {

  clickc() {
    debugger;
      Actions.drawerOpen()
  }

backpress()
{
  debugger;
  if (Actions.currentScene == 'SelectJob') {
    BackHandler.exitApp();
    return true;
  } else {
    Actions.pop()
  }

}

  render() {
    const { leftIcon, searchBar, rightIcon, inputPressed, searchBarValue, headerHeight, inputHeight, disabled } = this.props;
    return (

      <View style={[styles.headerMainDiv, headerHeight]}>

        {
          leftIcon ? <TouchableHighlight style={styles.leftIconDiv} onPress={() => this.backpress()}>
            <Icon name='arrow-back' style={{ color: '#f48004', fontSize: 34 }} />
          </TouchableHighlight> : <View style={styles.leftIconDiv} />
        }

        {
          searchBar
            ?
            <TouchableHighlight style={styles.centerDiv} onPress={inputPressed}>
              <IconInput placeholder={searchBarValue == '' ? "Search" : searchBarValue} inputHeight={inputHeight} disabled={disabled} />
            </TouchableHighlight>
            :
            <View style={styles.centerDiv}>
              <Text style={{ color: '#f48004', fontSize: 25, fontWeight: 'bold', fontFamily: 'sans-serif' }}>KAAM TAMAAM</Text>
            </View>
        }


        {/* <View style={styles.righIconDiv}> */}
        <TouchableHighlight style={styles.righIconDiv} onPress={() => this.clickc()}>
          <Icon name='menu' style={{ color: '#f48004', fontSize: 27 }} />
        </TouchableHighlight>
        {/* </View> */}
      </View>

    );
  }
}

export { CustomHeader };

const styles = {
  headerMainDiv: {
    height: Dimensions.get('window').height / 7.7,
    backgroundColor: '#e2e2e2',
    flexDirection: 'row',
    borderBottomColor: '#f48004',
    borderBottomWidth: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  leftIconDiv: {
    backgroundColor: 'transparent',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  centerDiv: { backgroundColor: 'transparent', flex: 8, height: 80, justifyContent: 'center', alignItems: 'center' },
  centerTextFieldDiv: {
    flexDirection: 'row', alignItems: 'center', borderColor: 'gray',
    backgroundColor: '#fff', borderWidth: 1, marginLeft: 10, marginRight: 10, borderRadius: 26
  },
  righIconDiv: { backgroundColor: 'transparent', flex: 1, justifyContent: 'center', alignItems: 'center' }
};