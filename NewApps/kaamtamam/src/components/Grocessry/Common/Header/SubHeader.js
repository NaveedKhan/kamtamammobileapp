import React, {
  Component
} from 'react';
import {
  Text,
  View, StyleSheet, Image, Dimensions
} from 'react-native';
import { Container, Header, Content, Left, Right, Body, Title, Item, Input, Icon, Button, Badge } from 'native-base';
import { Actions } from 'react-native-router-flux';
//import { config } from "../../src/config";
//import axios from "axios";


// const CommonHeader = ({text,UserCompany,orgid})=>
// {
class SubHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //starCount: 3.5,
      //  modalVisible: this.props.isVisible
    };
  }





  render() {
    const { text } = this.props;
    return (
      <View style={{ backgroundColor: '#fcfcfc', height: Dimensions.get('window').height / 13, flexDirection: 'row' }}>

        <View style={{ flex: .85, backgroundColor: '#fcfcfc', justifyContent: 'center', alignItems: 'flex-start' }}>
          <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 5 }}> {this.props.mainText}</Text>
        </View>

        <View style={{ flex: .15, backgroundColor: '#fcfcfc', flexDirection: 'row' }} >
          <View style={{ flex: .5, backgroundColor: '#fcfcfc', justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ backgroundColor: '#fcfcfc', alignItems: 'center', justifyContent: 'center' }}>
              <Icon

                name="grid"
                style={{ color: "#777", fontSize: 26, width: 40 }}
              />
            </View>

          </View>
          <View style={{ flex: .5, backgroundColor: '#fcfcfc', justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ backgroundColor: '#fcfcfc', alignItems: 'center', justifyContent: 'space-evenly' }}>
             
              <Icon
                name="shoppingcart" type="AntDesign"
                style={{ color: "#777", fontSize: 26, width: 40 }}
              />
            </View>
          </View>

        </View>
      </View>
    )
  }
}

export { SubHeader };