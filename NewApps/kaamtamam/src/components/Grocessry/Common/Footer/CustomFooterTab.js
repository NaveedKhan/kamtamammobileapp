import React, { Component } from 'react';
import { Dimensions, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Badge, Thumbnail, View } from 'native-base';
import { IconInput } from './';


class CustomFooterTab extends Component {

    clickc() {
        debugger;
        //Actions.drawerMenu()

    }
    componentDidMount() {
        debugger;
        // var users = this.props.user;
        // let configs = {
        //     headers: {
        //         'Content-Type': 'application/json; charset=UTF-8'
        //     }
        // };
        // const { REST_API } = config;
        // this.setState({ isLoading: true });
        // var me = this;
        // axios.get(REST_API.Notifications.GetNotByUserID + `/${users.user.id}`, configs)
        //     .then(response => {
        //         debugger;
        //         var baseModel = response.data;
        //         if (baseModel.success) {
        //             var notList = baseModel.notifications;
        //             me.setState({ isLoading: false, notificationsArray: notList })

        //         } else {
        //             me.setState({ isLoading: true });
        //         }
        //         // console.log('api call' + response);
        //     }).catch(error => {
        //         debugger;
        //         me.setState({ isLoading: true });
        //         console.log("error", error)
        //     });
    }


    render() {
        const { cartLength, notifications } = this.props;
        const images = require('../../Asset/homeButton.png')
        return (

            <Footer style={{ backgroundColor: 'red' }}>
                <FooterTab style={{ backgroundColor: '#f48004', borderTopColor: 'black', borderTopWidth: 1 }}>


                    <Button badge vertical onPress={() => Actions.notification()}>
                        <Badge><Text>{notifications}</Text></Badge>
                        <Icon name="apps" style={{ color: '#fff' }} />
                        <Text style={{ color: '#fff', fontSize: 10 }}>Notofications</Text>
                    </Button>

                    <TouchableHighlight onPress={() => Actions.reset('landingPage')} style={{ height: 50, width: 50, borderRadius: 25, overflow: 'hidden' }}>
                        <Thumbnail large source={images} style={{ height: 50, width: 50, borderRadius: 25, overflow: 'hidden' }} />
                    </TouchableHighlight>
                    {/* <Button vertical onPress={()=> Actions.MyAccount()}>
                        <Icon name="person" style={{color:'#f78320'}}/>
                        <Text style={{color:'gray'}}>Account</Text>
                    </Button> */}

                    <Button badge vertical onPress={() => Actions.cart()} >
                        {
                            cartLength < 1 ? <View style={{marginTop:18}}/> :
                                <Badge ><Text>{cartLength}</Text></Badge>
                        }
                        <Icon name="shoppingcart" type="AntDesign" style={{ color: '#fff' }} />
                        <Text style={{ color: '#fff', fontSize: 10 }}>My Cart</Text>
                    </Button>

                </FooterTab>
            </Footer>


        );
    }
}

export { CustomFooterTab };

const styles = {
    headerMainDiv: {
        height: Dimensions.get('window').height / 7.7,
        backgroundColor: '#f48004',
        flexDirection: 'row'
    },
    leftIconDiv: {
        backgroundColor: 'transparent',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    centerDiv: { backgroundColor: 'transparent', flex: 8, justifyContent: 'center', alignItems: 'center' },
    centerTextFieldDiv: {
        flexDirection: 'row', alignItems: 'center', borderColor: 'gray',
        backgroundColor: '#fff', borderWidth: 1, marginLeft: 10, marginRight: 10, borderRadius: 26
    },
    righIconDiv: { backgroundColor: 'transparent', flex: 1, justifyContent: 'center', alignItems: 'center' }
};