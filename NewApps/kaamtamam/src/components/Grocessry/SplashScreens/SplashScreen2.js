import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableHighlight,
  StatusBar,
  Alert,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import {loginUser,UpdateUserFormData,loginGuest} from '../../../actions';
import {Button, Spinner} from 'native-base';
import DeviceInfo from 'react-native-device-info';


class SplashScreen2 extends Component {
  constructor(props) {
    super(props);
    // ...
    this.state = {
      height: Dimensions.get('window').height,
      width: Dimensions.get('window').width,
      latitude: 0,
      longitude: 0,
      deviceId: ''
    };
    debugger;
  }

  swapButtonAndSpinner() {
    debugger;
    return this.props.loading ? (
      <View style={styles.buttonsArea}>
        <Spinner />
      </View>
    ) : (
      <View style={styles.buttonsArea}>
        <TouchableHighlight
          style={styles.buttonStyle}
          onPress={() => Actions.login()}>
          <Text style={{fontSize: 15, color: '#ffff'}}>LOGIN</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.buttonStyle}
          onPress={() => Actions.signUp()}>
          <Text style={{fontSize: 15, color: '#ffff'}}>SIGNUP</Text>
        </TouchableHighlight>
        {/* <View style={{height:50}}/> */}
        <Button
          rounded
          transparent
          style={{width: 250, justifyContent: 'center'}}
          onPress={() => this.onLoginGuestClick()}>
          <Text
            style={{
              fontSize: 15,
              color: '#ffff',
              textDecorationLine: 'underline',
            }}>
            CONTINUE AS GUEST
          </Text>
        </Button>
      </View>
    );
  }

  getdeviceId = () => {
    //Getting the Unique Id from here
    var id = DeviceInfo.getUniqueId();
    this.setState({ deviceId: id, });
    this.props.UpdateUserFormData({
      prop: 'DeviceID',
      value: id,
    });
  };

  componentDidMount() {
    // const {user} = this.props;
    this.setState({isLoading: true});
    debugger;
    this.getdeviceId();
    //if (user.roleName == 'Guest') {
    this.getDataFromAsynStorage('GuestUserData');

    // }
  }

  getDataFromAsynStorage(itemname) {
    var me = this;
    me.setState({isloading: true});
    try {
      const token = AsyncStorage.getItem(itemname)
        .then(token => {
          debugger;
          me.setState({isloading: false});
          if (token != null) {
            let userObj = JSON.parse(token);
            debugger;
            this.props.UpdateUserFormData({
              prop: 'GuestUserData',
              value: userObj,
            });
          } else {
            debugger;
          }
        })
        .done();
    } catch (error) {
      var err = String(error) == '' ? 'no error ' : String(error);
      Alert.alert('error', err);
      me.setState({isloading: false});
      return null;
    }
  }

  onLoginGuestClick() {
    debugger;
    const {GuestUserData,DeviceTocken,DeviceID} = this.props;
    numberArray = [];
    GuestUserData.forEach(function(el, index) {
      numberArray.push(el.phoneNumber);
    });
    debugger;
    this.props.loginGuest({
      email: 'guest@gmail.com',
      password: 'Test@123',
      phoneNumbers: [],
      DeviceTocken:DeviceTocken,
      DeviceID:DeviceID
    });
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#f78320'}}>
        <StatusBar hidden />
        <View
          style={{
            flex: 4,
            justifyContent: 'center',
            alignItems: 'center',
            margin: 10,
          }}>
          <Image
            source={require('../Asset/Kaam_Tamam_Logo.png')}
            style={styles.Imagestyle}
          />
        </View>
        <View style={{flex: 2, justifyContent: 'center', alignItems: 'center'}}>
          <Text
            numberOfLines={2}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: 250,
              textAlign: 'center',
              color: '#fff',
              fontSize: 18,
            }}>
            One Stop Solution to all your household needs...!{' '}
          </Text>
        </View>

        {this.swapButtonAndSpinner()}

        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'bltransparentue',
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = ({auth}) => {
  const {loading, GuestUserData,DeviceTocken,DeviceID} = auth;
  //debugger;
  return {user: auth.user, loading, GuestUserData,DeviceTocken,DeviceID};
};
export default connect(
  mapStateToProps,
  {loginUser,UpdateUserFormData,loginGuest},
)(SplashScreen2);
//export default SplashScreen2;

const styles = {
  errorTextStyel: {
    fontSize: 17,
    alignSelf: 'center',
    color: 'red',
  },
  Imagestyle: {
    width: Dimensions.get('window').width / 1.6,
    height: Dimensions.get('window').width / 1.6,
    // borderRadius:( Dimensions.get('window').width/1.8)/2,
    resizeMode: 'contain',

    // opacity: 0.9
  },
  buttonStyle: {
    width: 250,
    height: 50,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#fff',
    borderWidth: 1.7,
  },
  buttonsArea: {
    flex: 3,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
};
