import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView, TouchableHighlight } from 'react-native';
import { Icon, Thumbnail, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { checkoutButton, AddItem } from '../../../actions';
import { CommonHeader, SubHeader, CustomHeader, CustomHeaderWithText } from '../Common/Header';
import { CustomFooterTab } from '../Common/Footer';

class OrderGridImageRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            ordersList: [],
            item: this.props.item
        }
    }
    render() {
        const { item ,orderTotalItems} = this.props;
        const firstthreeitems = item.length < 3 ? item : item.slice(0, 3);
        return (
            <View style={{ borderBottomWidth: 1, borderBottomColor: 'gray', height: Dimensions.get('window').height / 7.5, flexDirection: 'row' }} >
                <View style={{ borderWidth: 0, borderColor: 'black', flex: 2, alignItems: 'center', justifyContent: 'center' }} >
                    <Icon type={this.props.iconType} name={this.props.iconName}  style={{ color: '#000', fontSize: 22 }} />
                </View>
                <View style={{ borderWidth: 0, borderColor: 'red', flex: 8, justifyContent: 'flex-start', paddingLeft: 10 }} >

                    <View style={{ flex: 3 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{`${orderTotalItems} Item `}</Text>
                    </View>

                    <View style={{ flex: 7, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>

                        {firstthreeitems.map(el =>
                            <View key={el.productId} style={{ marginLeft: 10 }}>
                                <Thumbnail small source={require('../Asset/Apple.jpg')} />
                                <Text style={{ fontSize: 15 }}>{`${el.productCount} ${el.productName}`}</Text>
                            </View>
                        )}
                       

                    </View>
                </View>
            </View>

        );
    }
}

export default OrderGridImageRow;