import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView, TouchableHighlight } from 'react-native';
import { Icon, Thumbnail, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { checkoutButton, AddItem } from '../../../actions';
import { CommonHeader, SubHeader, CustomHeader, CustomHeaderWithText } from '../Common/Header';
import { CustomFooterTab } from '../Common/Footer';
import OrderGridNormalRow from './OrderGridNormalRow';
import OrderGridHeader from './OrderGridHeader';
import OrderGridImageRow from './OrderGridImageRow';

class OrderGrid extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            ordersList: [],
            item:this.props.item
        }
    }
    render() {
        debugger;
        const {item} = this.props;
        let date =  new Date(item.orderDate).toLocaleDateString(); 
        let status = item.orderStatus;
        let orderID = item.orderID;
        let orderTotalItems = item.orderTotalItems;
        let orderTotalPrice = item.orderTotalPrice;

        return (

            <View style={{ borderWidth: 4, borderColor: 'orange', height: Dimensions.get('window').height / 1.8, margin:10, borderRadius: 10 }} >


                <OrderGridHeader styleHeader={{ backgroundColor: 'orange' }} heading={`Order No ${orderID}`} value="" iconName="infocirlceo" iconType="AntDesign"/>
                <OrderGridNormalRow heading="11 new Morroco Coat America" value={`Order Date : ${date}`}  iconName="home" iconType="AntDesign" />
                <OrderGridNormalRow heading="Distance 5km" value={`Order is : ${status}`}   iconName="pushpino" iconType="AntDesign"/>
                <OrderGridNormalRow heading="Cost" value={`${orderTotalPrice} /Rs`}  iconName="credit" iconType="Entypo" />

                <OrderGridImageRow orderTotalItems={orderTotalItems} item={item.orderItems} iconName="shoppingcart" iconType="AntDesign"/>

                <View style={{
                    height: Dimensions.get('window').height / 9.8, flexDirection: 'row',
                    justifyContent: 'space-around', alignItems: 'center', padding: 5, alignContent: 'center'
                }}>
                    <View>
                        <Button block rounded warning style={{ width: 150 }}>
                            <Text>
                                Mark Complete
                                    </Text>
                        </Button>
                    </View>

                    <View>
                    <TouchableHighlight style={styles.buttonStyle} onPress={() => Actions.orderDetail({Order:item})}>
                            <Text style={{ fontSize: 15, color: '#000', fontWeight: 'bold' }} > View Detail</Text>
                        </TouchableHighlight>
                    </View>

                </View>

            </View>

        );
    }
}

export default OrderGrid;

const styles = {
    ListStyle: {
        height: Dimensions.get('window').height / 5.5,
        //backgroundColor:'green',
        flexDirection: 'row'
    },
    ListStyleColum: {
        flex: 0.5,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Imagestyle:
    {
        width: 128,
        height: 128,
        // backgroundColor: 'red',
        // borderRadius:( Dimensions.get('window').width/1.8)/2,
        resizeMode: 'contain',
    },
}