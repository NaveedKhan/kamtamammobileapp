import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  Alert,
  Modal,
  Image,
  Linking,
  Platform,
} from 'react-native';
import {Rating, AirbnbRating} from 'react-native-elements';
import {
  List,
  ListItem,
  Left,
  CheckBox,
  Right,
  Icon,
  Thumbnail,
  Body,
  Button,
  avatar,
  Toast,
  Spinner,
} from 'native-base';
import axios from 'axios';
import {config} from '../../../config';
import {UpdateNotificationData} from '../../../actions';
//import CheckoutItemsList from './CheckoutItemsList'
import {connect} from 'react-redux';
//import { checkoutButton, assignOrder } from '../../actions';
import {
  confirmButtonClicked,
  addressChanged,
  assignOrder,
} from '../../../actions';
import {CommonHeader, CustomHeaderWithText} from '../Common/Header';
import {Actions} from 'react-native-router-flux';
import moment from 'moment';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class OrderDetail extends Component {
  constructor(props) {
    super(props);
    debugger;
    this.state = {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      swipedAllCards: false,
      swipeDirection: '',
      cardIndex: 0,
      modalVisible: false,
    };
  }

  timeBetweenDates(toDate) {
    debugger;
    var dateEntered = toDate;
    var now = new Date();
    var difference = now.getTime() - dateEntered.getTime();

    if (difference <= 0) {
      // Timer done
      //clearInterval(timer);
    } else {
      var seconds = Math.floor(difference / 1000);
      var minutes = Math.floor(seconds / 60);
      var hours = Math.floor(minutes / 60);
      var days = Math.floor(hours / 24);

      hours %= 24;
      minutes %= 60;
      seconds %= 60;

      if (hours > 0 || days > 0 || minutes > 5) {
        return true;
      } else {
        return false;
      }
      // this.setState({ countDownTime: days + 'D ' + hours + 'H ' + minutes + 'M ' + seconds + 'S' });
    }
  }

  markCancel(el) {
    if (
      el.orderStatus == 'Delivered' ||
      el.orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      this.showToast2();
    } else {
      let isexpire =
        el.orderStatus.toLowerCase() == 'initiated'.toLowerCase()
          ? false
          : this.timeBetweenDates(new Date(el.orderAssignedToDriverTechDate));
      if (el.orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
        this.ShowConfimrationAlertForCancel(
          el,
          'Are you sure you really want to cancel the order',
        );
      } else {
        if (isexpire) {
          Alert.alert(
            'alert',
            'Please contact our help line or delivery guy to cancel the order.You canot cancel the order after 5 min',
          );
        } else {
          this.ShowConfimrationAlertForCancel(
            el,
            'Are you sure you really want to cancel the order',
          );
        }
      }
    }
  }
  // markCancel(el) {
  //     // this.ShowConfimrationAlert(el, 'Are You sure your request is fulfilled and you want to close  it.')
  //     this.ShowConfimrationAlertForCancel(el, 'Are you sure you really want to cancel the order');
  // }
  markComplete(el) {
    if (el.orderStatus.toLowerCase() == 'Delivered'.toLowerCase()) {
      this.showToast2();
    } else {
      this.ShowConfimrationAlert(
        el,
        'Are You sure your request is fulfilled and you want to close  it.',
      );
    }
  }
  onConfirmPressed() {
    debugger;
    // const { user, orderInCartList } = this.props;
    // let id = user.user.id;
    // this.props.confirmButtonClicked(id, orderInCartList);

    // const { Order, user, relatedOrders } = this.props;
    // let id = user.id;
    // this.props.assignOrder(Order.orderID, id, relatedOrders);
  }

  ratingCompleted(rating) {
    this.setState({reviewScore: rating});
    console.log('Rating is: ' + rating);
  }

  submitReview() {
    // this.setState({ modalVisible: false })
    if (this.state.iscanceling) {
      this.senpApiCallToMarkCancel();
    } else {
      this.senpApiCallToMarkComplete();
    }
  }

  senpApiCallToMarkComplete() {
    debugger;
    // const { Order } = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
      OrderID: selectedOrderToComplete.orderID,
      IsMarkFinishedByUser: true,
      Reviews: {
        UserReview: reviewComment,
        UserReviewScore: reviewScore,
        UserID: selectedOrderToComplete.orderByUserID,
        OrderID: selectedOrderToComplete.orderID,
        IsReviewedByUser: true,
      },
    };
    // const { user } = this.props;
    // let id = user.id;
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({modalButtonLoading: true});
    const {REST_API} = config;
    try {
      const response = axios
        .post(REST_API.Oders.MarkOrderCompleted, reviewModel, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          debugger;
          console.log('api call' + response);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });

          me.props.UpdateNotificationData({
            prop: 'unReadNotificationCount',
            value: baseModel.data.unReadnotificationCount,
          });
          me.props.UpdateNotificationData({
            prop: 'totalpendingOrdercount',
            value: baseModel.data.totalpendingOrdercount,
          });
          Actions.reset('SelectJob');
          //Actions.tendersResult({TendersList:baseModel.data});
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          // me.setState({ isLoading: true });
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      // me.setState({ isLoading: true });
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({
        isLoading: false,
        modalButtonLoading: false,
        modalVisible: false,
      });
      //productFetchingFail(dispatch);
    }
  }

  senpApiCallToMarkCancel() {
    debugger;
    // const { Order } = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
      OrderID: selectedOrderToComplete.orderID,
      IsMarkFinishedByUser: true,
      Reviews: {
        UserReview: reviewComment,
        UserReviewScore: reviewScore,
        UserID: selectedOrderToComplete.orderByUserID,
        OrderID: selectedOrderToComplete.orderID,
        IsReviewedByUser: true,
      },
    };
    // const { user } = this.props;
    // let id = user.id;
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({modalButtonLoading: true});
    const {REST_API} = config;
    try {
      const response = axios
        .post(REST_API.Oders.MarkOrderCancelByUser, reviewModel, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          debugger;
          console.log('api call' + response);

          me.props.UpdateNotificationData({
            prop: 'unReadNotificationCount',
            value: baseModel.data.unReadnotificationCount,
          });
          me.props.UpdateNotificationData({
            prop: 'totalpendingOrdercount',
            value: baseModel.data.totalpendingOrdercount,
          });
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
          Actions.reset('SelectJob');
          //Actions.tendersResult({TendersList:baseModel.data});
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          // me.setState({ isLoading: true });
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      // me.setState({ isLoading: true });
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({
        isLoading: false,
        modalButtonLoading: false,
        modalVisible: false,
      });
      //productFetchingFail(dispatch);
    }
  }

  ShowConfimrationAlert(el, message) {
    Alert.alert(
      'Confirmation!',
      message,
      [
        {
          text: 'Yes Continue',
          onPress: () =>
            //checkoutSucess(dispatch,baseModel.data)
            this.setState({
              iscanceling: false,
              modalVisible: true,
              selectedOrderToComplete: el,
            }),
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  ShowConfimrationAlertForCancel(el, message) {
    Alert.alert(
      'Confirmation!',
      message,
      [
        {
          text: 'Yes Continue',
          onPress: () => {
            //checkoutSucess(dispatch,baseModel.data)
            this.setState({
              iscanceling: true,
              modalVisible: true,
              selectedOrderToComplete: el,
            });
            //    this.senpApiCallToMarkCancel(el)
          },
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  renderRatingModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({modalVisible: false, modalButtonLoading: false});
        }}>
        <View style={styles.datecontainer}>
          {/* <View style={{ margin: 10 }} /> */}
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              borderRadius: 15,
              justifyContent: 'flex-end',
              alignItems: 'center',
              alignSelf: 'flex-end',
              margin: 10,
              borderColor: '#fff',
              borderWidth: 1,
            }}
            onPress={() => this.setState({modalVisible: false})}>
            <Icon name="close" style={{color: '#fff'}} />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 15,
              margin: 5,
              alignSelf: 'center',
              color: 'white',
            }}>
            Please Add Review
          </Text>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 5,
            }}>
            <AirbnbRating
              count={5}
              reviews={[
                'Terrible (1)',
                'Bad (2)',
                'Normal (3)',
                'GOOD (4)',
                'Outstanding (5)',
              ]}
              defaultRating={3}
              onFinishRating={rat => this.ratingCompleted(rat)}
              size={40}
            />
          </View>

          <View
            style={{
              margin: 10,
              backgroundColor: '#fff',
              borderColor: 'gray',
              borderWidth: 1,
              padding: 5,
              borderRadius: 5,
            }}>
            <TextInput
              onChangeText={text => this.setState({reviewComment: text})}
              placeholder="Add any comment (optional but recomended)"
              placeholderTextColor="grey"
              maxLength={200}
              multiline={true}
              editable
              numberOfLines={5}
              style={{height: 150, justifyContent: 'flex-start'}}
            />
          </View>
          {this.state.modalButtonLoading ? (
            <Spinner />
          ) : (
            <TouchableOpacity
              onPress={() => this.submitReview()}
              style={{
                width: 250,
                height: 50,
                borderRadius: 30,
                margin: 10,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                borderColor: '#fff',
                borderWidth: 1.7,
              }}>
              <Text style={{fontSize: 15, color: '#fff', fontWeight: 'bold'}}>
                {' '}
                SUBMIT NOW{' '}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </Modal>
    );
  }

  showToast2() {
    return Toast.show({
      text: 'Order already marked completed!',
      buttonText: 'Okay',
      type: 'danger',
      duration: 3000,
    });
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  componentDidMount() {
    // let configs = {
    //     headers: {
    //         'Content-Type': 'application/json; charset=UTF-8'
    //     }
    // };
    // const { REST_API } = config;
    // const { UserCompany, orgid, UserData } = this.state;
    // axios.get(REST_API.Test.GetSimple, configs)
    //     .then(response => {
    //         debugger;
    //         console.log('api call' + response);
    //     }).catch(error => {
    //         debugger;
    //         console.log("error", error)
    //     });
  }

  showmodal() {}

  _renderUserReviews(score) {
    return (
      <View
        style={{
          // flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}>
        <Rating
          // showRating
          ratingCount={5}
          readonly
          // reviews={["Terrible (1)", "Bad (2)", "Normal (3)", "GOOD (4)", "Outstanding (5)"]}
          startingValue={score}
          //onFinishRating={(rat) => this.ratingCompleted(rat)}
          size={10}
        />
      </View>
    );
  }

  _renderbothReviews(userscore, driverscore) {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 10,
          marginBottom: 20,
          padding: 5,
        }}>
        <Text>Your Review</Text>
        <Rating ratingCount={5} readonly startingValue={userscore} size={10} />
        <Text style={{marginTop: 5}}>Kaam Tamam Team Review</Text>
        <Rating
          ratingCount={5}
          readonly
          startingValue={driverscore}
          size={10}
        />
      </View>
    );
  }

  _renderButtons(orderStatus, isexpire, el) {
    debugger;
    if (
      orderStatus == 'Delivered' ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      let {reviews} = el;
      let {
        userReviewScore,
        driverReviewScore,
        isReviewedByUser,
        isReviewedByDriverTech,
      } = reviews;
      let reviewscore = isReviewedByUser && isReviewedByDriverTech;

      var reviewbuttonstyle = {
        // flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
      };
      if (isReviewedByUser && !isReviewedByDriverTech) {
        return this._renderUserReviews(userReviewScore);
      } else if (!isReviewedByUser && isReviewedByDriverTech) {
        return this._renderUserReviews(driverReviewScore);
      } else if (isReviewedByUser && isReviewedByDriverTech) {
        return this._renderbothReviews(userReviewScore, driverReviewScore);
      }
    } else {
      if (orderStatus.toLowerCase() == 'initiated') {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() => this.markCancel(el)}
              style={[styles.button, {marginLeft: 5, width: 150}]}>
              <Text style={styles.buttonText}> Cancel Order</Text>
            </TouchableOpacity>
          </View>
        );
      } else {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 10,
            }}>
            {/* <TouchableOpacity onPress={() => this.markComplete(el)} style={styles.button}>
                        <Text style={styles.buttonText}>Mark Complete</Text>
                    </TouchableOpacity> 
            {el.assigneeName == null && el.assigneContactNo == null ? (
              <View />
            ) : (
              <TouchableOpacity
                onPress={() => this.callTechnician(el.assigneContactNo)}
                style={[styles.button, {marginLeft: 5}]}>
                <Text style={styles.buttonText}> Call Technician</Text>
              </TouchableOpacity>
            )}
            {/* <TouchableOpacity onPress={() => this.callTechnician(assigneContactNo)} style={[styles.button, { marginLeft: 5 }]}>
                        <Text style={styles.buttonText}> Call Technician</Text>
                    </TouchableOpacity> */}
          </View>
        );
      }
    }
  }

  _renderBottomText(orderStatus, el) {
    if (
      orderStatus == 'Delivered' ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      let {reviews} = el;
      let reviewstatis = '';
      if(reviews)
      {
       reviewstatis =
        reviews.isReviewedByUser && !reviews.isReviewedByDriverTech
          ? 'You have given the review right now'
          : !reviews.isReviewedByUser && reviews.isReviewedByDriverTech
          ? 'Kaam Tamam team has added the review'
          : !reviews.isReviewedByUser && !reviews.isReviewedByDriverTech
          ? 'No Review Added Yet'
          : 'Kaam Tamam and you have added the review';
      }
      return (
        <Text
          style={{color: 'red', alignSelf: 'center', margin: 5, fontSize: 13}}>
          {`${reviewstatis}`}
        </Text>
      );
    } else {
      if (orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
        return (
          <Text
            style={{
              color: 'red',
              alignSelf: 'center',
              margin: 5,
              fontSize: 12,
              justifyContent: 'center',
              textAlign: 'center',
            }}>
            {'You can cancel the Order until it is assigned to  delivery guy'}
          </Text>
        );
      } else {
        return (
          <Text
            style={{
              color: 'red',
              alignSelf: 'center',
              margin: 5,
              fontSize: 12,
              justifyContent: 'center',
              textAlign: 'center',
            }}>
            {'You can cancel the Order until it is assigned to  delivery guy'}
          </Text>
        );
      }
    }
  }

  callTechnician(phone) {
    //let phone = '033323233232';
    console.log('callNumber ----> ', '033323233232');
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  }

  render() {
    debugger;
    const {Order, grocServiceChrges} = this.props;
    const {width, height} = this.state;
    const {
      orderID,
      orderDate,
      orderCompletionDate,
      orderScheduleDate,
      isScheduleOrder,
      orderTotalCharges,
      requestAddressLat,
      requestAddressLong,
      requestLocationAddress,
      orderByUserID,
      orderByUserFullName,
      locationTrackingID,
      orderStatus,
      orderTotalItems,
      orderTotalPrice,
      orderItems,
      assigneeImageURL,
      assigneContactNo,
      assigneeName,
      orderAssignedToDriverTechDate,
      orderReEstimatedTotalCharges,
      serviceCharges,
      orderDetail
    } = Order;

    let assImage_Http_URL = {uri: 'http://182.180.56.162:8088/404.png'};
    if (assigneeImageURL != null) {
      let imageass = assigneeImageURL.replace(
        'http://localhost',
        'http://10.0.2.2',
      );
      assImage_Http_URL = {uri: imageass};
    }

    const urdgency = isScheduleOrder ? 'Scheduled for ' : 'Urgent Order';
    // const todaydatetime = moment(today).format('MMMM Do YYYY, h:mm:ss a');
    const date = isScheduleOrder
      ? moment(orderScheduleDate).format('MMMM Do YYYY, h:mm:ss a')
      : moment(orderDate).format('MMMM Do YYYY, h:mm:ss a');

    // const todaydatetime = moment(today).format('MMMM Do YYYY, h:mm:ss a');
    let isexpire =
      orderStatus.toLowerCase() == 'initiated'
        ? false
        : this.timeBetweenDates(new Date(orderAssignedToDriverTechDate));

    return (
      <View style={{backgroundColor: '#fff', flex: 1, marginTop: 0}}>
        <View>
          <CustomHeaderWithText text="Order Detail" />
        </View>
        <ScrollView scrollEnabled>
          {orderStatus.toLowerCase() != 'initiated' ? (
            assigneeName == null && assigneContactNo == null ? (
              <View style={{alignSelf: 'center', margin: 5}}>
                <Text style={{color: 'red'}}>
                  Order Was concelled before assigned
                </Text>
              </View>
            ) : (
              <View style={styles.thumbnailDiv}>
                <View style={styles.rowSelectType}>
                  <Text style={styles.rowHeadingList}>
                    Delivery Guy Detail{' '}
                  </Text>
                </View>
                <Image
                  source={assImage_Http_URL}
                  style={{
                    height: 90,
                    width: 90,
                    borderRadius: 45,
                    marginTop: 10,
                    overflow: 'hidden',
                    borderColor: 'black',
                    borderWidth: 1,
                  }}
                />
                <Text style={{fontSize: 13, color: 'green', marginTop: 3}}>
                  {assigneeName}
                </Text>
                <TouchableOpacity
                  onPress={() => this.callTechnician(assigneContactNo)}
                  style={{
                    borderColor: 'green',
                    borderWidth: 1,
                    width: 120,
                    height: 30,
                    margin: 5,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 15,
                  }}>
                  <Text style={{fontSize: 13, color: 'green'}}>
                    {assigneContactNo}
                  </Text>
                </TouchableOpacity>
              </View>
            )
          ) : (
            <View />
          )}
          <View style={styles.container}>
            <View
              style={{
                flex: 2.8,
                backgroundColor: 'transparent',
                margin: 0,
                overflow: 'hidden',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Icon
                name="check-circle"
                type="FontAwesome"
                style={{color: 'orange', fontSize: 23, marginLeft: 20}}
              />
              <Text
                style={{
                  padding: 5,
                  fontWeight: 'bold',
                  fontSize: 19,
                  marginLeft: 10,
                }}>
                Delivery Address
              </Text>
            </View>

            <View
              style={{
                backgroundColor: 'transparent',
                flex: 3,
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {
                  if (orderStatus.toLowerCase() != 'initiated') {
                    Actions.traceOrder({
                      requestAddressLat: parseFloat(requestAddressLat),
                      requestAddressLong: parseFloat(requestAddressLong),
                      Order: Order,
                    });
                  } else {
                    Alert.alert(
                      'Alert!',
                      'Order Is not yet assigned , so you cannot track the order',
                    );
                  }
                }}
                style={{
                  flex: 9.5,
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{margin: 10, marginBottom: 0, textAlign: 'center'}}>
                  <Text
                    numberOfLines={5}
                    style={{
                      fontFamily: 'sens-serif',
                      fontSize: 13,
                      margin: 10,
                      color: 'green',
                      fontWeight: 'bold',
                    }}>
                    {Order.requestLocationAddress
                      ? Order.requestLocationAddress
                      : 'Nushta address'}
                  </Text>
                </View>
              </TouchableOpacity>
              <View
                style={{
                  flex: 0.5,
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              />
            </View>
            <View
              style={{
                backgroundColor: 'orange',
                flex: 0.2,
                margin: 8,
                borderRadius: 8,
              }}
            />
          </View>
          <Text
            style={{
              alignSelf: 'center',
              color: 'red',
              marginBottom: 5,
              fontSize: 12,
            }}>
            Click on the address to track your order
          </Text>

          <View
            style={{
              backgroundColor: '#e6e7e9',
              margin: 20,
              marginTop: 10,
              borderRadius: 10,
              borderWidth: 3,
              borderColor: '#f48004',
              overflow: 'hidden',
            }}>
            <View
              style={{
                flex: 1,
                backgroundColor: '#e6e7e9',
                margin: 0,
                overflow: 'hidden',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Icon
                name="check-circle"
                type="FontAwesome"
                style={{color: 'orange', fontSize: 23, marginLeft: 20}}
              />
              <Text
                style={{
                  padding: 5,
                  fontWeight: 'bold',
                  fontSize: 19,
                  marginLeft: 10,
                }}>
                Order Urgency
              </Text>
            </View>
            <Text
              style={{
                margin: 10,
                marginLeft: 20,
                color: 'green',
                fontWeight: 'bold',
              }}>{`${urdgency} ${date}`}</Text>
          </View>

          {orderDetail == '' || orderDetail == null ?
          <View/>:
         <View>
            < Text style={{margin:5,marginLeft:20,fontWeight:'bold',fontSize:15,color:'green', marginBottom:1}}>Extra Detail </Text>
       <View style={styles.DiscriptionBorder}>
            <TextInput
              placeholder="Enter Extra detail or items for your orders......."
              style={{borderColor: 'gray'}}
               editable={false}
              multiline={true}
              style={{color:'#000'}}
              value={orderDetail}
            />
          </View>
         </View>
          }

          <View
            style={{
              flex: 1,
              backgroundColor: '#e6e7e9',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 3,
              borderColor: '#f48004',
              borderRadius: 10,
            }}>
            <View
              style={{
                height: 50,
                backgroundColor: '#e6e7e9',
                margin: 0,
                marginTop: 10,
                marginBottom: 0,
                borderRadius: 10,
                borderBottomLeftRadius: 0,
                borderBottomRightRadius: 0,
                overflow: 'hidden',
              }}>
              <View
                style={{
                  flex: 1,
                  backgroundColor: '#e6e7e9',
                  margin: 0,
                  overflow: 'hidden',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                <Icon
                  name="check-circle"
                  type="FontAwesome"
                  style={{color: 'orange', fontSize: 23, marginLeft: 20}}
                />
                <Text
                  style={{
                    padding: 5,
                    fontWeight: 'bold',
                    fontSize: 19,
                    marginLeft: 10,
                  }}>
                  Ordered Items List
                </Text>
              </View>
            </View>

            <List>
              {Order.orderItems.map(item => {
                let image = item.productImage.replace(
                  'http://localhost',
                  'http://10.0.2.2',
                );
                let Image_Http_URL = {uri: image};

                return (
                  <ListItem
                    key={item.productId}
                    avatar
                    style={{
                      backgroundColor: '#e6e7e9',
                      borderBottomWidth: 1,
                      borderBottomColor: '#f48004',
                      marginLeft: 0,
                    }}>
                    <Left
                      style={{
                        paddingLeft: 5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Thumbnail small source={Image_Http_URL} />
                    </Left>
                    <Body style={{borderWidth: 0, borderBottomWidth: 0}}>
                      <Text style={{fontWeight: 'bold'}}>
                        {item.productName}
                      </Text>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text
                          style={{fontSize: 12, margin: 5}}
                          note
                          numberOfLines={1}>{` Rs ${item.prodcutPriceEach} / ${
                          item.unitName
                        } `}</Text>
                        <Text
                          style={{fontSize: 12, margin: 5, marginRight: 20}}
                          note
                          numberOfLines={1}>{`Qty ${item.productCount}`}</Text>
                      </View>
                      {/* <Text note numberOfLines={1}>{` Rs. ${item.prodcutPriceEach}/${item.unitName}                Qty ${item.productCount}`}</Text> */}
                    </Body>
                    <Right style={{borderWidth: 0}}>
                      <Button transparent>
                        <Text>Rs. {item.totalPrice}</Text>
                      </Button>
                    </Right>
                  </ListItem>
                );
              })}
            </List>
            <View
              style={{
                backgroundColor: 'orange',
                height: 7,
                margin: 8,
                borderRadius: 8,
              }}
            />
            <Text
              style={{
                alignSelf: 'center',
                margin: 5,
                color: 'green',
              }}>{`Service Chnarges : Rs. ${serviceCharges}`}</Text>
          </View>

          <View
            style={{
              height: this.state.height / 10,
              backgroundColor: 'rgba(255,255,255,0.3)',
              flexDirection: 'row',
              fontFamily: 'Helvica',
            }}>
            <View
              style={{
                flex: 5,
                backgroundColor: 'transparent',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-evenly',
              }}>
              {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 18,
                  fontFamily: 'Helvica',
                }}>
                Total :{' '}
              </Text>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: 'transparent',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{fontSize: 16}}>Rs. {Order.orderTotalCharges} </Text>
            </View>
          </View>
          {orderReEstimatedTotalCharges == '0' ? (
            <View />
          ) : (
            <View
              style={{
                height: this.state.height / 10,
                backgroundColor: 'rgba(255,255,255,0.3)',
                flexDirection: 'row',
                fontFamily: 'Helvica',
              }}>
              <View
                style={{
                  flex: 5,
                  backgroundColor: 'transparent',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}>
                {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 18,
                    fontFamily: 'Helvica',
                  }}>
                  Final Total :{' '}
                </Text>
              </View>
              <View
                style={{
                  flex: 5,
                  backgroundColor: 'transparent',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text style={{fontSize: 16}}>
                  Rs. {orderReEstimatedTotalCharges}{' '}
                </Text>
              </View>
            
            </View>
          )}

<Text style={{color:'red',fontWeight:'bold',margin:10, marginBottom:0}}>Attention!</Text>
<Text style={{color:'red',fontSize:12,margin:10,alignSelf:'center',textAlign:'center',marginTop:0}}>
Some of the items charges could be different from the above mentioned prices.it can be less or more due to price 
fluctuation in the market. Our worker will re estimate the total price if prices are changed!
</Text>

          {this._renderBottomText(orderStatus, Order)}
        </ScrollView>

        {this.renderRatingModal()}
      </View>
    );
  }
}

const mapStateToProps = ({checkout, cart, auth}) => {
  //const {  relatedOrders } = orders;
  const {isloading, selectedCategory, selectedProduct, totalItems} = checkout;
  const {orderInCartList} = cart;
  const {grocServiceChrges} = auth;
  debugger;
  return {
    user: auth.user,
    isloading,
    selectedCategory,
    selectedProduct,
    totalItems,
    orderInCartList,
    grocServiceChrges,
  };
};
export default connect(
  mapStateToProps,
  {confirmButtonClicked, addressChanged, assignOrder, UpdateNotificationData},
)(OrderDetail);

const styles = {
  datecontainer: {
    backgroundColor: 'green', //'#E9EBEE',
    borderRadius: 10,
    margin: 10,
    marginTop: 100,
  },
  DiscriptionBorder: {
    borderWidth: 3,
    borderColor: '#f48004',
    backgroundColor: 'white',
    width: width * 0.9,
    height: height * 0.25,
    margin: 10,
    borderRadius: 10,
    alignSelf:'center'
  },
  thumbnailDiv: {
    //flex: 2.5,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    //marginTop: 10,
    margin: 10,
    borderWidth: 3,
    alignSelf: 'center',
    borderColor: '#f48004',
    backgroundColor: '#E9EBEE',
    width: width * 0.9,
    borderRadius: 10,
    //backgroundColor: 'green'
  },
  container: {
    height: Dimensions.get('window').height / 3.3,
    backgroundColor: '#e6e7e9',
    margin: 20,
    borderWidth: 3,
    borderColor: '#f48004',
    marginBottom: 5,
    borderRadius: 9,
    overflow: 'hidden',
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  button: {
    marginTop: 3,
    marginLeft: 5,
    marginBottom: 5,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    height: height / 17,
    width: 150,
    borderRadius: 25,
    borderWidth: 1.5,
    borderColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 13,
    color: 'black',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent',
  },
  done: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
    backgroundColor: 'transparent',
  },
};

Array.prototype.sum = function(prop) {
  var total = 0;
  for (var i = 0, _len = this.length; i < _len; i++) {
    total += this[i][prop];
  }
  return total;
};
