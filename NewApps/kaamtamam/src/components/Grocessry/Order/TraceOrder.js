import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
  Alert,
} from 'react-native';
import { Item, Input, Icon, Spinner } from 'native-base';
import { CustomHeaderWithText } from '../../Grocessry/Common/Header';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import MapViewDirections from 'react-native-maps-directions';
import firebase from 'firebase';
import { Button } from '../../common';
import { Actions } from 'react-native-router-flux';
//import { Spinner } from '../../common';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0622;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const greyColor = '#A0A1A5';
const GOOGLE_MAPS_APIKEY = 'AIzaSyCZ_GeN6VIBOgZqe9mZ568ygvB8eUNuSbc';
//const origin = { latitude: 34.0025363, longitude: 71.5130443 };
//const destination = { latitude: 33.682270, longitude: 73.049566 };

class TraceOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      latitude: 0,
      longitude: 0,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
      destinationData: { latitude: 0, longitude: 0 },
      marker: { latitude: 0, longitude: 0 },
      origin: { latitude: this.props.requestAddressLat, longitude: this.props.requestAddressLong },
      destination: { latitude: 33.9976884, longitude: 71.4695614 },
      duration: 0,
      distance: ''
    };
    this.mapView = null;
  }
  componentDidMount() {
    debugger;
   // this.setState({ isLoading: true })
    this.GetDataFromFireBase();
  }

  GetDataFromFireBase() {
    debugger;
    var me = this;
    const {Order} = this.props;
    this.setState({ isLoading: true })
    let locationTrackingID= Order.locationTrackingID == null ? '-M4uyXDmUGa4VFmJKuSJ' : Order.locationTrackingID;
    firebase.database().ref().child('DriverLocation').child(locationTrackingID)
      .on("value", function (snapshot) {
        debugger;
        me.setState({  })
        console.log(snapshot.val());
        var res = snapshot.val()
        if (res != null) {
          debugger;
          var userlat = res.userLat;
          var userlong = res.userLong;
          const destination = { latitude: userlat, longitude: userlong };
          me.setState({ isLoading: false,destination: destination });

        }
      }
      );

  }

  refershClicked()
  {
    const {requestAddressLat,requestAddressLong,Order} = this.props;
    this.GetDataFromFireBase();
    Actions.refresh({
      requestAddressLat: requestAddressLat,
      requestAddressLong:requestAddressLong,
      Order: Order,
    })
  }

  render() {
    var me = this;
    const { isLoading, origin, destination } = this.state;
    const {requestAddressLat,requestAddressLong} = this.props;
    debugger;
    return (
      <View style={{ flex: 1 }}>
        <CustomHeaderWithText text="Track Order"    refreshButton={true}
            refreshButtonClicked={()=>this.refershClicked()}/>
        <ScrollView scrollEnabled>
          <View style={styles.StatusBorder}>
            {!isLoading ? (
              <MapView
                ref={c => this.mapView = c}
                mapType={"standard"}
                showsUserLocation={true}
                showsCompass={true}
                //  showsMyLocationButton={true}
                //showsTraffic={true}
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                region={{
                  latitude: requestAddressLat,//33.6641233,
                  longitude: requestAddressLong,//73.0505619,
                  latitudeDelta: LATITUDE_DELTA,
                  longitudeDelta: LONGITUDE_DELTA,
                }}>

                <Marker
                  pinColor = {'#332edb'}
                  coordinate={origin}
                  title={'Delivery'}
                  description={'Address'}
                />

                <Marker
                  coordinate={destination}
                  title={'Worker'}
                  description={'Location'}
                />

                <MapViewDirections
                  origin={origin}
                  mode='DRIVING'
                  language='en'
                  destination={destination}
                  apikey={GOOGLE_MAPS_APIKEY}
                  strokeWidth={3}
                  strokeColor="hotpink"
                  resetOnChange={true}
                  optimizeWaypoints={true}
                  onError = {(err)=>{
                    debugger;
                    if(err == "Error on GMAPS route request: ZERO_RESULTS")
                       console.log('erro',err)
                  } }
                  onStart={() => console.log('started')}
                  onReady={(detail) => {
                    debugger;
                    if (detail) 
                    {
                      me.setState({ duration: parseInt(detail.duration), distance: detail.distance });
                     // me.mapView.fitToCoordinates(detail.coordinates)
                      //   ,
                      //    {
                      //   edgePadding: {
                      //     right: (width / 20),
                      //     bottom: (height / 20),
                      //     left: (width / 20),
                      //     top: (height / 20),
                      //   }
                      // }
                      // );
                    }
                   
                    console.log('ready');
                  }}
                />

              </MapView>
            ) : <Spinner />}
          </View>
          <View style={styles.orderMain}>
            <Text style={styles.orderHeading}>{this.state.duration} Minutes Away</Text>
            <Text style={styles.orderValue}>Distance : {this.state.distance} KM</Text>
          </View>
          {/* <Button onPress={()=> Actions.refresh()}>
            Refresh
          </Button> */}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  StatusBorder: {
    borderBottomWidth: 3,
    borderBottomColor: 'orange',
    width: width * 1,
    height: height * 0.76,
  },

  orderMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 15,
  },
  orderHeading: {
    color: 'black',
    alignSelf: 'center',
    marginTop: 5,
    fontWeight: 'bold',
    fontSize: 16,
    fontFamily: 'sens-serif',
  },
  orderValue: {
    color: 'grey',
    alignSelf: 'center',
    marginTop: 5,
    fontSize: 16,
    fontFamily: 'sens-serif',
    paddingLeft: 10,
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    borderColor: '#f48004',
    backgroundColor: '#f48004',
    borderWidth: 2,
    padding: 10,
    borderRadius: 20,
    width: width * 0.5,
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

export default TraceOrder;