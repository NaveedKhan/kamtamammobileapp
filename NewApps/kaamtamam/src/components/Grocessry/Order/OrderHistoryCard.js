import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  Modal,
  Alert,
  ScrollView,
  TextInput,
  Linking,
  Platform,
} from 'react-native';
import {Item, Input, Icon, Toast, Spinner} from 'native-base';
import {Rating, AirbnbRating} from 'react-native-elements';
//import { CustomHeaderWithText } from '../../Grocessry/Common/Header';
import axios from 'axios';
import {config} from '../../../config';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import moment from 'moment';

//import { Spinner } from '../../common';
import {UpdateNotificationData} from '../../../actions';
//import {CustomFooterTab} from '../../Grocessry/Common/Footer';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class OrderHistoryCard extends Component {
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
    this.state = {
      modalVisible: false,
      reviewModalVisible: false,
      reviewComment: '',
      reviewScore: 3,
      modalButtonLoading: false,
      selectedOrderToComplete: null,
    };
  }
  renderRow(iconName, headingTitle, headingValue, detail) {
    return (
      <View style={styles.rowMain}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            //alignSelf: 'center',
            //  backgroundColor:'green',
            flex: 1,
          }}>
          <Icon name={iconName} style={styles.rowIcon} />
          <Text style={styles.rowHeadingList}>{headingTitle}:</Text>
          <Text style={styles.listItemText}>{headingValue}</Text>
        </View>
        {detail && (
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <Text style={{color: 'grey', paddingRight: 5}}>Details</Text>
            <Icon name="arrow-forward" style={{fontSize: 16, color: 'grey'}} />
          </TouchableOpacity>
        )}
      </View>
    );
  }

  timeBetweenDates(toDate) {
    debugger;
    var dateEntered = toDate;
    var now = new Date();
    var difference = now.getTime() - dateEntered.getTime();

    if (difference <= 0) {
      // Timer done
      //clearInterval(timer);
    } else {
      var seconds = Math.floor(difference / 1000);
      var minutes = Math.floor(seconds / 60);
      var hours = Math.floor(minutes / 60);
      var days = Math.floor(hours / 24);

      hours %= 24;
      minutes %= 60;
      seconds %= 60;

      if (hours > 0 || days > 0 || minutes > 5) {
        return true;
      } else {
        return false;
      }
      // this.setState({ countDownTime: days + 'D ' + hours + 'H ' + minutes + 'M ' + seconds + 'S' });
    }
  }

  ratingCompleted(rating) {
    this.setState({reviewScore: rating});
    console.log('Rating is: ' + rating);
  }

  submitReview() {
    //this.setState({ modalVisible: false })
    if (this.state.iscanceling) {
      this.senpApiCallToMarkCancel();
    } else {
      this.senpApiCallToMarkComplete();
    }
  }

  senpApiCallToMarkComplete() {
    debugger;
    // const { Order } = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
      OrderID: selectedOrderToComplete.orderID,
      IsMarkFinishedByUser: true,
      Reviews: {
        UserReview: reviewComment,
        UserReviewScore: reviewScore,
        UserID: selectedOrderToComplete.orderByUserID,
        OrderID: selectedOrderToComplete.orderID,
        IsReviewedByUser: true,
      },
    };
    // const { user } = this.props;
    // let id = user.id;
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({modalButtonLoading: true});
    const {REST_API} = config;
    try {
      const response = axios
        .post(REST_API.Oders.MarkOrderCompleted, reviewModel, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          debugger;
          console.log('api call' + response);

          me.props.UpdateNotificationData({
            prop: 'unReadNotificationCount',
            value: baseModel.data.unReadnotificationCount,
          });
          me.props.UpdateNotificationData({
            prop: 'totalpendingOrdercount',
            value: baseModel.data.totalpendingOrdercount,
          });
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
          Actions.reset('SelectJob');
          //Actions.tendersResult({TendersList:baseModel.data});
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          // me.setState({ isLoading: true });
          Alert.alert('error', err);
          console.log('error', err);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      // me.setState({ isLoading: true });
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({
        isLoading: false,
        modalButtonLoading: false,
        modalVisible: false,
      });
      //productFetchingFail(dispatch);
    }
  }


  senpApiCallToAddUserReview() {
    debugger;
     let { Order,DeviceID } = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
        UserReview: reviewComment,
        UserReviewScore: reviewScore,
        UserID: selectedOrderToComplete.orderByUserID,
        OrderID: selectedOrderToComplete.orderID,
        IsReviewedByUser: true,
    };
    // const { user } = this.props;
    // let id = user.id;
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({modalButtonLoading: true});
    const {REST_API} = config;
    try {
      const response = axios
        .post(REST_API.Oders.AddUserReviewForOrder, reviewModel, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          debugger;
          console.log('api call' + response);
          var selectedorder = Order.find(aa=> aa.orderID == selectedOrderToComplete.orderID);
          if(selectedorder!= null)
          {
           let {reviews} = selectedorder;
           if (reviews) {
             selectedorder.reviews.userReview= reviewComment;
             selectedorder.reviews.userReviewScore= reviewScore;
             selectedorder.reviews.userID= selectedOrderToComplete.orderByUserID;
             selectedorder.reviews.orderID= selectedOrderToComplete.orderID;
             selectedorder.reviews.isReviewedByUser= true;
           }
          // me.props.Order = Order;
          }

          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            reviewModalVisible: false,
          });

          Alert.alert('Confirmation','Dear valued customer thank you so much for your review.Your review is recorded and will be used to improve our service further!');
          debugger;       
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          // me.setState({ isLoading: true });
          Alert.alert('error', err);
          console.log('error', err);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            reviewModalVisible: false,
          });
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      // me.setState({ isLoading: true });
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({
        isLoading: false,
        modalButtonLoading: false,
        modalVisible: false,
      });
      //productFetchingFail(dispatch);
    }
  }


  ShowConfimrationAlert(el, message) {
    Alert.alert(
      'Confirmation!',
      message,
      [
        {
          text: 'Yes Continue',
          onPress: () =>
            //checkoutSucess(dispatch,baseModel.data)
            this.setState({
              iscanceling: false,
              modalVisible: true,
              selectedOrderToComplete: el,
            }),
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  ShowConfimrationAlertForCancel(el, message) {
    Alert.alert(
      'Confirmation!',
      message,
      [
        {
          text: 'Yes Continue',
          onPress: () => {
            //checkoutSucess(dispatch,baseModel.data)
            this.setState({
              iscanceling: true,
              modalVisible: true,
              selectedOrderToComplete: el,
            });
            //    this.senpApiCallToMarkCancel(el)
          },
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  renderRatingModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({modalVisible: false, modalButtonLoading: false});
        }}>
        <View style={styles.datecontainer}>
          {/* <View style={{ margin: 10 }} /> */}
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              borderRadius: 15,
              justifyContent: 'flex-end',
              alignItems: 'center',
              alignSelf: 'flex-end',
              margin: 10,
              borderColor: '#fff',
              borderWidth: 1,
            }}
            onPress={() => this.setState({modalVisible: false})}>
            <Icon name="close" style={{color: '#fff'}} />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 15,
              margin: 5,
              alignSelf: 'center',
              color: 'white',
            }}>
            Please Add Review
          </Text>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 5,
            }}>
            <AirbnbRating
              count={5}
              reviews={[
                'Terrible (1)',
                'Bad (2)',
                'Normal (3)',
                'GOOD (4)',
                'Outstanding (5)',
              ]}
              defaultRating={3}
              onFinishRating={rat => this.ratingCompleted(rat)}
              size={40}
            />
          </View>

          <View
            style={{
              margin: 10,
              backgroundColor: '#fff',
              borderColor: 'gray',
              borderWidth: 1,
              padding: 5,
              borderRadius: 5,
            }}>
            <TextInput
              onChangeText={text => this.setState({reviewComment: text})}
              placeholder="Add any comment (optional but recomended)"
              placeholderTextColor="grey"
              maxLength={200}
              multiline={true}
              editable
              numberOfLines={5}
              style={{height: 150, justifyContent: 'flex-start'}}
            />
          </View>
          {this.state.modalButtonLoading ? (
            <Spinner />
          ) : (
            <TouchableOpacity
              onPress={() => this.submitReview()}
              style={{
                width: 250,
                height: 50,
                borderRadius: 30,
                margin: 10,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                borderColor: '#fff',
                borderWidth: 1.7,
              }}>
              <Text style={{fontSize: 15, color: '#fff', fontWeight: 'bold'}}>
                {' '}
                SUBMIT NOW{' '}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </Modal>
    );
  }


  renderReviewModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.reviewModalVisible}
        onRequestClose={() => {
          this.setState({reviewModalVisible: false, modalButtonLoading: false});
        }}>
        <View style={styles.datecontainer}>
          {/* <View style={{ margin: 10 }} /> */}
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              borderRadius: 15,
              justifyContent: 'flex-end',
              alignItems: 'center',
              alignSelf: 'flex-end',
              margin: 10,
              borderColor: '#fff',
              borderWidth: 1,
            }}
            onPress={() => this.setState({reviewModalVisible: false})}>
            <Icon name="close" style={{color: '#fff'}} />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 15,
              margin: 5,
              alignSelf: 'center',
              color: 'white',
            }}>
            How was our serive? Please add review.
          </Text>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 5,
            }}>
            <AirbnbRating
              count={5}
              reviews={[
                'Terrible (1)',
                'Bad (2)',
                'Normal (3)',
                'GOOD (4)',
                'Outstanding (5)',
              ]}
              defaultRating={3}
              onFinishRating={rat => this.ratingCompleted(rat)}
              size={40}
            />
          </View>

          <View
            style={{
              margin: 10,
              backgroundColor: '#fff',
              borderColor: 'gray',
              borderWidth: 1,
              padding: 5,
              borderRadius: 5,
            }}>
            <TextInput
              onChangeText={text => this.setState({reviewComment: text})}
              placeholder="Add any comment (optional but recomended)"
              placeholderTextColor="grey"
              maxLength={200}
              multiline={true}
              editable
              numberOfLines={5}
              style={{height: 150, justifyContent: 'flex-start'}}
            />
          </View>
          {this.state.modalButtonLoading ? (
            <Spinner />
          ) : (
            <TouchableOpacity
              onPress={() => {
                //this.setState({reviewModalVisible:false});
                this.senpApiCallToAddUserReview();
              }}
              style={{
                width: 250,
                height: 50,
                borderRadius: 30,
                margin: 10,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                borderColor: '#fff',
                borderWidth: 1.7,
              }}>
              <Text style={{fontSize: 15, color: '#fff', fontWeight: 'bold'}}>
                {' '}
                SUBMIT NOW{' '}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </Modal>
    );
  }



  showToast2() {
    return Toast.show({
      text: 'Order already marked completed!',
      buttonText: 'Okay',
      type: 'danger',
      duration: 3000,
    });
  }

  markCancel(el) {
    if (
      el.orderStatus == 'Delivered' ||
      el.orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      this.showToast2();
    } else {
      let isexpire =
        el.orderStatus.toLowerCase() == 'initiated'.toLowerCase()
          ? false
          : this.timeBetweenDates(new Date(el.orderAssignedToDriverTechDate));
      if (el.orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
        this.ShowConfimrationAlertForCancel(
          el,
          'Are you sure you really want to cancel the order',
        );
      } else {
        if (isexpire) {
          Alert.alert(
            'Attention',
            'Please contact our help line or delivery guy to cancel the order.You canot cancel the order after 5 min',
          );
        } else {
          this.ShowConfimrationAlertForCancel(
            el,
            'Are you sure you really want to cancel the order!wddddddddddw',
          );
        }
      }
    }
  }

  senpApiCallToMarkCancel() {
    debugger;
     const { user,DeviceID } = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
      OrderID: selectedOrderToComplete.orderID,
      IsMarkFinishedByUser: true,
      DeviceID:DeviceID,
      Reviews: {
        UserReview: reviewComment,
        UserReviewScore: reviewScore,
        UserID: selectedOrderToComplete.orderByUserID,
        OrderID: selectedOrderToComplete.orderID,
        IsReviewedByUser: true,
      },
    };

    const {roleName, fullName, phoneNumber} = user;   

    let {GuestUserData} = this.props;
    numberArray = [];
    if(roleName == 'Guest')
    {
        GuestUserData.forEach(function(el, index) {
          numberArray.push(el.phoneNumber);
        });
        reviewModel.PhoneNumbers = [];//numberArray;
    }

    // const { user } = this.props;
    // let id = user.id;
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({modalButtonLoading: true});
    const {REST_API} = config;
    try {
      const response = axios
        .post(REST_API.Oders.MarkOrderCancelByUser, reviewModel, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          // const orderlist = baseModel.ordersDetailList;
          // const servicesList = baseModel.handyServices;
          debugger;
          console.log('api call' + response);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
          //this.setState({ modalVisible: false })
          me.props.UpdateNotificationData({
            prop: 'unReadNotificationCount',
            value: baseModel.data.unReadnotificationCount,
          });
          me.props.UpdateNotificationData({
            prop: 'totalpendingOrdercount',
            value: baseModel.data.totalpendingOrdercount,
          });
          Actions.reset('SelectJob');
          //Actions.tendersResult({TendersList:baseModel.data});
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          // me.setState({ isLoading: true });
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({
            isLoading: false,
            modalButtonLoading: false,
            modalVisible: false,
          });
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      // me.setState({ isLoading: true });
      Alert.alert('error', err);
      console.log('error', error);
      me.setState({
        isLoading: false,
        modalButtonLoading: false,
        modalVisible: false,
      });
      //productFetchingFail(dispatch);
    }
  }

  markComplete(el) {
    if (
      el.orderStatus.toLowerCase() == 'Delivered'.toLowerCase() ||
      el.orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      this.showToast2();
    } else {
      this.ShowConfimrationAlert(
        el,
        'Are You sure your request is fulfilled and you want to close  it.',
      );
    }
  }

  _renderWorkerReviews(score,isReviewedByUser,el) {
    return (
      <View
        style={{
          // flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}>
           {isReviewedByUser ? <View/> :   <TouchableOpacity
              onPress={() => this.setState({reviewModalVisible:true,selectedOrderToComplete:el})}
              style={[styles.button, {marginLeft: 5, width: 150}]}>
              <Text style={styles.buttonText}> Add Your Review </Text>
            </TouchableOpacity> }
        <Rating
          // showRating
          ratingCount={5}
          readonly
          // reviews={["Terrible (1)", "Bad (2)", "Normal (3)", "GOOD (4)", "Outstanding (5)"]}
          startingValue={score}
          //onFinishRating={(rat) => this.ratingCompleted(rat)}
          size={10}
        />      
      </View>
    );
  }

  _renderUserReviews(score) {
    return (
      <View
        style={{
          // flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}>
        <Rating
          // showRating
          ratingCount={5}
          readonly
          // reviews={["Terrible (1)", "Bad (2)", "Normal (3)", "GOOD (4)", "Outstanding (5)"]}
          startingValue={score}
          //onFinishRating={(rat) => this.ratingCompleted(rat)}
          size={10}
        />
      </View>
    );
  }

  _renderbothReviews(userscore, driverscore) {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 10,
          marginBottom: 20,
          padding: 5,
        }}>
        <Text>Your Review</Text>
        <Rating ratingCount={5} readonly startingValue={userscore} size={10} />
        <Text style={{marginTop: 5}}>Kaam Tamam Team Review</Text>
        <Rating
          ratingCount={5}
          readonly
          startingValue={driverscore}
          size={10}
        />
      </View>
    );
  }

  callTechnician(phone) {
    debugger;
    //let phone = '033323233232';
    console.log('callNumber ----> ', '033323233232');
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  }

  _renderButtons(orderStatus, isexpire, el) {
    debugger;
    const {assigneeImageURL, assigneContactNo, assigneeName} = el;
    if (
      orderStatus == 'Delivered' ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      let {reviews} = el;
      if (reviews) {
        let {
          userReviewScore,
          driverReviewScore,
          isReviewedByUser,
          isReviewedByDriverTech,
        } = reviews;

        var reviewbuttonstyle = {
          // flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        };
        if (isReviewedByUser && !isReviewedByDriverTech) {
          return this._renderUserReviews(userReviewScore);
        } else if (!isReviewedByUser && isReviewedByDriverTech) {       
          return this._renderWorkerReviews(driverReviewScore,isReviewedByUser,el);
        } else if (isReviewedByUser && isReviewedByDriverTech) {
          return this._renderbothReviews(userReviewScore, driverReviewScore);
        }
      }
    } else {
      if (orderStatus.toLowerCase() == 'initiated') {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() => this.markCancel(el)}
              style={[styles.button, {marginLeft: 5, width: 150}]}>
              <Text style={styles.buttonText}> Cancel Order</Text>
            </TouchableOpacity>
          </View>
        );
      } else {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() =>
                Actions.traceOrder({
                  requestAddressLat: parseFloat(el.requestAddressLat),
                  requestAddressLong: parseFloat(el.requestAddressLong),
                  Order: el,
                })
              }
              style={[styles.button, {marginLeft: 5, width: 150}]}>
              <Text style={styles.buttonText}>Track Order</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity onPress={() => this.markCancel(el)} style={[styles.button, { marginLeft: 5, width: 150 }]}>
                        <Text style={styles.buttonText}> Cancel Order</Text>
                    </TouchableOpacity> */}
            <TouchableOpacity
              onPress={() => this.callTechnician(el.assigneContactNo)}
              style={[styles.button, {marginLeft: 5, width: 150}]}>
              <Text style={styles.buttonText}> Call Technician</Text>
            </TouchableOpacity>
          </View>
        );
      }
    }
  }

  _renderBottomText(orderStatus, isexpire, el) {
    if (
      orderStatus == 'Delivered' ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      let {reviews} = el;
      let reviewstatis = '';
      if(reviews)
      {
         reviewstatis =
        reviews.isReviewedByUser && !reviews.isReviewedByDriverTech
          ? 'You have given the review right now'
          : !reviews.isReviewedByUser && reviews.isReviewedByDriverTech
          ? 'Kaam Tamam team has added the review'
          : !reviews.isReviewedByUser && !reviews.isReviewedByDriverTech
          ? 'No Review Added Yet'
          : 'Kaam Tamam and you have added the review';
      }
    
      return (
        <Text
          style={{color: 'red', alignSelf: 'center', margin: 5, fontSize: 13}}>
          {`${reviewstatis ? reviewstatis : ''}`}
        </Text>
      );
    } else {
      if (orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
        return (
          <Text
            style={{
              color: 'red',
              alignSelf: 'center',
              margin: 5,
              fontSize: 12,
              justifyContent: 'center',
              textAlign: 'center',
            }}>
            You can cancel the Order until it is assigned to delivery guy
          </Text>
        );
      } else {
        return (
          <Text
            style={{
              color: 'red',
              alignSelf: 'center',
              margin: 5,
              fontSize: 12,
              justifyContent: 'center',
              textAlign: 'center',
            }}>
            You can cancel the Order until it is assigned to delivery guy
          </Text>
        );
      }
    }
  }

  render() {
    /// const { services } = this.props;
    const {Order} = this.props;

    debugger;
    return (
      <View>
        {Order.map(el => {
          let isexpire =
            el.orderStatus.toLowerCase() == 'initiated'
              ? false
              : this.timeBetweenDates(
                  new Date(el.orderAssignedToDriverTechDate),
                );
          const urdgency = el.isScheduleOrder
            ? 'Scheduled for '
            : 'Urgent Order';
          // const todaydatetime = moment(today).format('MMMM Do YYYY, h:mm:ss a');
          const date = el.isScheduleOrder
            ? moment(el.orderScheduleDate).format('MMMM Do YYYY, h:mm:ss a')
            : moment(el.orderDate).format('MMMM Do YYYY, h:mm:ss a');
          return (
            <TouchableOpacity
              key={el.orderID}
              style={styles.StatusBorder}
              onPress={() => Actions.orderDetail({Order: el})}>
              {this.renderRow(
                (iconName = 'paper'),
                (headingTitle = 'Order No'),
                (headingValue = el.orderID),
              )}
              {this.renderRow(
                (iconName = 'clock'),
                (headingTitle = urdgency),
                (headingValue = date),
                (detail = false),
              )}
              {this.renderRow(
                (iconName = 'md-checkmark-circle-outline'),
                (headingTitle = 'Status'),
                (headingValue = el.orderStatus),
              )}
              {el.orderStatus.toLowerCase() == 'initiated' ? (
                <View />
              ) : (
                this.renderRow(
                  (iconName = 'contact'),
                  (headingTitle = 'Delivery Boy Name'),
                  (headingValue =
                    el.assigneeName == null
                      ? 'Not assigned to Worker'
                      : el.assigneeName),
                )
              )}
              {this.renderRow(
                (iconName = 'contact'),
                (headingTitle = 'Total Items'),
                (headingValue = el.orderTotalItems),
              )}
              {this.renderRow(
                (iconName = 'construct'),
                (headingTitle = 'Total Cost '),
                (headingValue = `Rs. ${el.orderTotalCharges}`),
              )}

              {this._renderButtons(el.orderStatus, isexpire, el)}

              {this._renderBottomText(el.orderStatus, isexpire, el)}

              {/* <Text style={{ color: 'red', alignSelf: 'center', marginTop: 5, fontSize: 13 }}>
                            You can Cancel the Request within 5 min.
                        </Text> */}
            </TouchableOpacity>
          );
        })}

        {this.renderRatingModal()}
        {this.renderReviewModal()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 30,
  },
  datecontainer: {
    backgroundColor: 'green', //'#E9EBEE',
    borderRadius: 10,
    margin: 10,
    marginTop: 100,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  StatusBorder: {
    borderWidth: 3,
    borderColor: '#f48004',
    width: width * 0.9,
    // height: height * 0.68,
    margin: 10,
    borderRadius: 10,
  },
  listItemText: {
    fontSize: 12,
    color: 'green',
    fontFamily: 'sens-serif',
  },
  rowMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: width * 0.8,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  rowIcon: {
    color: '#f48004',
    margin: 10,
    marginRight: 20,
    marginBottom: 5,
    //  backgroundColor:'red'
  },
  rowHeadingList: {
    fontWeight: 'bold',
    margin: 20,
    marginRight: 10,
    marginLeft: 0,
    fontSize: 13,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  button: {
    marginTop: 3,
    marginLeft: 5,
    marginBottom: 5,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    padding: 7,
    borderRadius: 25,
    borderWidth: 1.5,
    borderColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 12,
    color: 'black',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

//export default OrderHistoryCard;
const mapStateToProps = ({auth}) => {
  const {GuestUserData,DeviceID}= auth;
  return {user: auth.user,GuestUserData,DeviceID};
};
export default connect(
  mapStateToProps,
  {UpdateNotificationData},
)(OrderHistoryCard);
