import React, { Component } from 'react';
import { View, Text, Button, Image, Dimensions, ScrollView, TouchableHighlight } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { checkoutButton, AddItem } from '../../../actions';
import { CommonHeader, SubHeader, CustomHeader, CustomHeaderWithText } from '../Common/Header';
import { CustomFooterTab } from '../Common/Footer';

class OrderGridNormalRow extends Component {
    render() {
        return (
            <View style={{ borderBottomWidth: 1, borderbottomColor: 'gray', height: Dimensions.get('window').height / 12, flexDirection: 'row' }} >
                <View style={{ borderWidth: 0, borderColor: 'black', flex: 2, alignItems: 'center', justifyContent: 'center' }} >
                    <Icon type={this.props.iconType} name={this.props.iconName}  style={{ color: '#000', fontSize: 22 }} />
                </View>
                <View style={{ borderWidth: 0, borderColor: 'red', flex: 8, justifyContent: 'center', paddingLeft: 10 }} >
                    <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{this.props.heading}</Text>
                    <Text>{this.props.value}</Text>

                </View>
            </View>
        );
    }
}

export default OrderGridNormalRow;