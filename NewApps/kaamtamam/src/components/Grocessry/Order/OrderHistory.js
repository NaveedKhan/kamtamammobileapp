import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  ScrollView,
  TouchableHighlight,
  Alert,
} from 'react-native';
import {
  Icon,
  Thumbnail,
  Button,
  Spinner,
  Tab,
  Tabs,
  ScrollableTab,
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import axios from 'axios';
import {config} from '../../../config';

import {checkoutButton, AddItem,UpdateNotificationData} from '../../../actions';
import {
  CommonHeader,
  SubHeader,
  CustomHeader,
  CustomHeaderWithText,
} from '../Common/Header';
import {CustomFooterTab} from '../Common/Footer';
import OrderGridNormalRow from './OrderGridNormalRow';
import OrderGridHeader from './OrderGridHeader';
import OrderGridImageRow from './OrderGridImageRow';
import OrderGrid from './OrderGrid';
import ServicesHistoryCard from '../../Handyman/ServicesRequest/ServicesHistoryCard';
import OrderHistoryCard from './OrderHistoryCard';

class OrderHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      ordersList: [],
      servicesList: [],
      groceryTitle: 'Grocery',
      handyTitle: 'HandyMan',
    };
    this.interval = null;
  }

  sendApiCall() {
    const {user, IsRecentOrders} = this.props;
    let id = user.id;
    const {REST_API} = config;
    var requestedURL = IsRecentOrders
      ? REST_API.Oders.GetRecentOrderServicesOfUser + id
      : REST_API.Oders.GetOrderByUserID + id;
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({isLoading: true});

    try {
      const response = axios
        .get(requestedURL, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          const orderlist = baseModel.ordersDetailList;
          const servicesList = baseModel.handyServices;
          debugger;
          let groceryTitleNew =
            orderlist.length < 1
              ? 'Grocery'
              : `Grocery (${orderlist.length})`;
          let handyTitleNew =
            servicesList.length < 1
              ? 'HandyMan'
              : `HandyMan (${servicesList.length})`;
          console.log('api call' + response);
          me.props.UpdateNotificationData({
            prop: 'totalpendingOrdercount',
            value: baseModel.totalpendingOrdercount,
          });
          me.setState({
            isLoading: false,
            ordersList: orderlist,
            servicesList: servicesList,
            groceryTitle: groceryTitleNew,
            handyTitle: handyTitleNew,
          });
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({isLoading: false});
        });
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      me.setState({isLoading: false});
      Alert.alert('error', err);
      console.log('error', error);
    }
  }

  sendApiCallForGuest()
  {
    debugger;
    const {GuestUserData,DeviceTocken,DeviceID} = this.props;
    let numberArray = [];
      GuestUserData.forEach(function(el, index) {
        numberArray.push(el.phoneNumber);
      });

    
  

    const guestVM = {PhoneNumbers:[],DeviceID:DeviceID}
    const {user, IsRecentOrders} = this.props;
    let id = user.id;
    const {REST_API} = config;
    var requestedURL = IsRecentOrders
      ? REST_API.Oders.GetRecentOrdersOfGuest
      : REST_API.Oders.GetOrderHistoryOfGuest
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    var me = this;
    this.setState({isLoading: true});

    try {
      const response = axios
        .post(requestedURL,guestVM, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          const orderlist = baseModel.ordersDetailList;
          const servicesList = baseModel.handyServices;
          debugger;
          let groceryTitleNew =
            orderlist.length < 1
              ? 'Grocery'
              : `Grocery (${orderlist.length})`;
          let handyTitleNew =
            servicesList.length < 1
              ? 'HandyMan'
              : `HandyMan (${servicesList.length})`;
          console.log('api call' + response);
          me.props.UpdateNotificationData({
            prop: 'totalpendingOrdercount',
            value: baseModel.totalpendingOrdercount,
          });
          me.setState({
            isLoading: false,
            ordersList: orderlist,
            servicesList: servicesList,
            groceryTitle: groceryTitleNew,
            handyTitle: handyTitleNew,
          });
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          Alert.alert('error', err);
          console.log('error', error);
          me.setState({isLoading: false});
        });
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      me.setState({isLoading: false});
      Alert.alert('error', err);
      console.log('error', error);
    }
  }


  getUserOrders() {
    if (Actions.currentScene == 'orderHistory') {
      this.sendApiCall();
    }
  }

  componentDidMount() {
    debugger;
     const {user} = this.props;
     if(user.roleName == 'Guest')
     {
      this.sendApiCallForGuest()
     }else
     {
      this.sendApiCall();
     }
  
    
    //}
  }

  refershClicked() {
    debugger;
    const {user} = this.props;
    if(user.roleName == 'Guest')
    {
     this.sendApiCallForGuest()
    }else
    {
     this.sendApiCall();
    }
  }



  render() {
    const {
      ordersList,
      servicesList,
      groceryTitle,
      handyTitle,
      isLoading,
    } = this.state;
    const {IsRecentOrders} = this.props;
    return (
      <View style={{flex: 1, borderWidth: 1, borderColor: 'black'}}>
        <View style={{borderWidth: 1, borderColor: 'black'}}>
          <CustomHeaderWithText
            text={IsRecentOrders? "Recent Orders":"Orders History"}
            refreshButton={true}
            refreshButtonClicked={()=>this.refershClicked()}
          />
        </View>
        <Tabs
          style={{flex: 1}}
          tabBarBackgroundColor="#fff"
          tabBarUnderlineStyle={{backgroundColor: '#f48004'}}
          renderTabBar={() => <ScrollableTab />}>
          <Tab
            heading={groceryTitle}
            tabStyle={{backgroundColor: '#fff'}}
            textStyle={{color: '#666363'}}
            activeTextStyle={{color: '#f48004', fontWeight: 'bold'}}
            activeTabStyle={{backgroundColor: '#fff'}}>
            <ScrollView scrollEnabled>
              <View style={{backgroundColor: '#fff', alignItems: 'center'}}>
                {isLoading ? (
                  <Spinner />
                ) : ordersList.length < 1 ? (
                  <Text style={{textAlign: 'center', marginTop: 15}}>
                  {IsRecentOrders ? "You dont have any recent orders!":"You dont have any order in the history"}  
                  </Text>
                ) : (
                  <OrderHistoryCard Order={ordersList} />
                )
                // )
                }
              </View>
            </ScrollView>
          </Tab>
          <Tab
            heading={handyTitle}
            tabStyle={{backgroundColor: '#fff'}}
            textStyle={{color: '#666363'}}
            activeTextStyle={{color: '#f48004', fontWeight: 'bold'}}
            activeTabStyle={{backgroundColor: '#fff'}}>
            <ScrollView scrollEnabled>
              <View style={{alignItems: 'center'}}>
                {isLoading ? (
                  <Spinner />
                ) : servicesList.length < 1 ? (
                  <Text style={{textAlign: 'center', marginTop: 15}}>
                  {IsRecentOrders ? "You dont have any recent orders!":"You dont have any order in the history"}  
                  </Text>
                ) : (
                  <ServicesHistoryCard services={servicesList} />
                )
                // )
                }
              </View>
            </ScrollView>
          </Tab>
        </Tabs>
      </View>
    );
  }
}

const mapStateToProps = ({auth}) => {
  const {GuestUserData,DeviceTocken,DeviceID} = auth;
  return {user: auth.user,GuestUserData,DeviceTocken,DeviceID};
};
export default connect(
  mapStateToProps,
  {UpdateNotificationData},
)(OrderHistory);
//export default OrderHistory;

const styles = {
  ListStyle: {
    height: Dimensions.get('window').height / 5.5,
    //backgroundColor:'green',
    flexDirection: 'row',
  },
  ListStyleColum: {
    flex: 0.5,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  Imagestyle: {
    width: 128,
    height: 128,
    // backgroundColor: 'red',
    // borderRadius:( Dimensions.get('window').width/1.8)/2,
    resizeMode: 'contain',
  },
};
