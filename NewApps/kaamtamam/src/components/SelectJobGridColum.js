import React, { Component } from 'react';
import { View, Text, Button, Dimensions, Image, TouchableHighlight, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';
import { connect } from 'react-redux';
import { UpdateUserFormData } from '../actions';
// import ddf  from './Grocessry/Asset';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;


const RenderButton = ({ icon, name, onPress }) => {
  return (
    <TouchableOpacity
      style={{
        backgroundColor: '#f48004',
        borderColor: 'black',
        borderWidth: 0,
        height: height / 5.5,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',

      }}
      onPress={onPress}>
      <Icon name={icon} style={{ fontSize: 40, color: '#fff' }} />
      <Text
        style={{
          fontSize: 20,
          color: '#fff',
          fontWeight: 'bold',
          fontFamily: 'sens-serif',
        }}>
        {name}
      </Text>
    </TouchableOpacity>
  );
};

class SelectJobGridColum extends Component {

  onPressItemCategory(name) {
    debugger;
    this.props.UpdateUserFormData({ prop: 'selectedApp', value: name });
    if (name == "Grocerry") {
      Actions.landingPage();
    }
    //  else if (name == "HandyMan") {
    //   Actions.selectServices({ ServiceTypes: this.props.servicestype });
    // } 
    else {
      Actions.PlumberRequestLandingPage({ selectServices: this.props.name, serviceTypeID: this.props.itemId });
      //Actions.selectServices({ServiceTypes: this.props.servicestype});
    }
    //this.props.ProdcutSelected(id, name);
    //Actions.productList({ id: id, categoryname: name })
  }

  render() {
    var imagess = require('./Grocessry/Asset/Categoris/BrandedFood.jpg');
    return (
      <View style={styles.ListStyleColum}>
        <TouchableHighlight onPress={() => this.onPressItemCategory(this.props.name)} style={styles.ListColumImageStyle}>
          <Image source={imagess} style={styles.ListColumImageStyle} />
        </TouchableHighlight>
        <Text style={styles.ListColumnTextStyle}>{this.props.name}</Text>
      </View>
    );
  }
}


const mapStateToProps = ({ handyServices }) => {

  const { services, servicestype } = handyServices;
  //debugger;
  return { services, servicestype };
};
export default connect(mapStateToProps, {UpdateUserFormData})(SelectJobGridColum);

const styles = {
  ListStyle: {
    height: Dimensions.get('window').height / 5.5,
    //backgroundColor:'green',
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center'
  },
  yellowback: {
    backgroundColor: 'transparent'
  },
  graywback: {
    backgroundColor: 'transparent'
  },
  brownback: {
    backgroundColor: 'transparent'
  },
  ListStyleColum: {
    flex: 0.5,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginRight: 5
  }
  ,
  ListColumImageStyle: {
    height: Dimensions.get('window').height / 6.7,
    width: Dimensions.get('window').width / 2.4,
    borderRadius: 10,
    borderWidth: 2,
    overflow: 'hidden',
    borderColor: "black",
    shadowColor: "#000000",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 2,
      width: 3
    }
  },
  ListColumnTextStyle: {
    color: '#000',
    fontSize: 16,
    fontWeight: 'bold',
    justifyContent: 'center',
    textAlign: 'center',
    // backgroundColor:'yellow',
    margin: 3
  }
};