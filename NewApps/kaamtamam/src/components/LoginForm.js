import React, { Component} from 'react';
import { Text, View } from 'react-native';
import { Card, CardSection, Input, Button, Spinner } from './common';
import { connect }  from 'react-redux';
import { emailChanged, passwordChanged, loginUser } from '../actions';
import axios from 'axios'
import {config} from '../config';
class LoginForm extends Component {
   constructor(props) {
    super(props)
    debugger;
    // ...
  }
  onEmailChange(text) {
      this.props.emailChanged(text);
  }
  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }
  onButtnPressed() {
    const {email, password, error } = this.props;
    debugger;
    this.props.loginUser({email,password});
  }
getLog(){
  
  let configs = {
    headers: {
    'Content-Type': 'application/json; charset=UTF-8'
}};
const { REST_API } = config;

return axios.get(REST_API.Test.GetSimple)
.then(response => {
  debugger;
     console.log('api call'+ response);
}).catch(error => {
  debugger;          
  console.log("error", error)        
});
}
renderSpinner() {
  debugger;
if(this.props.loading){
  return <Spinner size="large" />
}else {
  {
    return (
      <Button onPress={()=>this.getLog()}>
       Login
    </Button>
  );
  }
}

}
render(){
  return (
    <Card>
    <CardSection>
    <Input
label="Email"
placeholder="email@gmail.com"
onChangeText={this.onEmailChange.bind(this)}
value={this.props.email}
    />
    </CardSection>

    <CardSection>
    <Input
label="Password"
placeholder="******"
secureTextEntry
onChangeText={this.onPasswordChange.bind(this)}
    value={this.props.password}
    />
    </CardSection>

<Text style={styles.errorTextStyel}>
{this.props.error}
</Text>

    <CardSection>
  {this.renderSpinner()}
    </CardSection>
    </Card>
  );
     }
}

// const mapStateToProps = state => {
//   return {
//     email: state.auth.email,
//     password: state.auth.password
//     error: state.auth.error
//   };
// };
const mapStateToProps = ({ auth }) => {
  const { email, password, error, loading } = auth;
  debugger;
  return { email, password, error, loading };
};
export default connect(mapStateToProps, {emailChanged, passwordChanged, loginUser})(LoginForm);

const styles = {
  errorTextStyel: {
    fontSize: 17,
    alignSelf: 'center',
    color: 'red'
  }
};
