import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Badge } from 'native-base';
import { Actions } from 'react-native-router-flux';
const images = require('../Grocessry/Asset/homeButton.png');
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';
//const CustomFooter = () => {
class CustomFooter extends Component {
  constructor(props) {
    super(props);
    debugger;

  }
  onPressCartRequests() {
    const { isHomePage, selectedApp, cartRequestsCount } = this.props;
    if (isHomePage) {
      Actions.orderHistory({ IsRecentOrders: true })
    }
    if (selectedApp == "Grocerry") {
      Actions.cart();
    }
  }
  //const images = require('../Grocessry/Asset/Kaam_Tamam_Logo.png')
  render() {
    var landingTo = this.props.landingTo == "G" ? "landingPage" : "selectServices";
    const { isHomePage, selectedApp, cartRequestsCount, unReadNotificationCount ,totalpendingOrdercount} = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          height: height * 0.1,
          margin: 5,
          marginLeft: 0,
          paddingLeft: 5,
          paddingRight: 5,
          marginRight: 0,
          height: 70,
          borderTopColor: greyColor,
          borderTopWidth: 1,
          backgroundColor: 'white'
        }}>
        <TouchableOpacity style={{ alignItems: 'center' }}>
          <Icon name="search" style={{ fontSize: 34, color: greyColor }} />
          <Text style={{ fontSize: 14, color: greyColor }}>Search</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => this.onPressCartRequests()}>
          {
            isHomePage ? totalpendingOrdercount == 0 ? <View /> :
              <View>
                <Text style={{ position: 'absolute', zIndex: 1, color: 'white', backgroundColor: 'red', fontSize: 11, borderRadius: 8, height: 16, width: 16, textAlign: "center" }}>
                  {totalpendingOrdercount}
                </Text>
              </View> :
              cartRequestsCount == undefined || cartRequestsCount == 0 ? <View />
                :
                <View>
                  <Text style={{ position: 'absolute', zIndex: 1, color: 'white', backgroundColor: 'red', fontSize: 12, borderRadius: 8, height: 16, width: 16, textAlign: "center" }}>
                    {cartRequestsCount}
                  </Text>
                </View>
          }

          <Icon name="person" style={{ fontSize: 34, color: greyColor }} />
          <Text style={{ fontSize: 14, color: greyColor }}>
            {selectedApp == "Grocerry" ? "My Cart" : "My Request"}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => { debugger; isHomePage ? Actions.reset('SelectJob') : Actions.popTo('SelectJob') }}
          style={{
            borderColor: '#f48004',
            borderRadius: 30,
            borderWidth: 2,

            width: 60,
            height: 60,
            alignItems: 'center',
            justifyContent: 'center'
          }}>
          <Image resizeMode="contain" source={images} style={{ width: 50, height: 50, alignSelf: 'center' }} />
        </TouchableOpacity>

        <TouchableOpacity style={{ alignItems: 'center' }}>
          <Icon name="heart" style={{ fontSize: 34, color: greyColor }} />
          <Text style={{ fontSize: 14, color: greyColor }}>Saved</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => Actions.notification()}>
          {
            unReadNotificationCount == 0 ? <View /> :
              <View>
                <Text style={{ position: 'absolute', zIndex: 1, color: 'white', backgroundColor: 'red', fontSize: 11, borderRadius: 8, height: 16, width: 16, textAlign: "center" }}>
                  {unReadNotificationCount}
                </Text>
              </View>
          }

          <Icon name="notifications" style={{ fontSize: 34, color: greyColor }} />
          <Text style={{ fontSize: 14, color: greyColor }}>Notification</Text>
        </TouchableOpacity>
      </View>
    );
  }
};

//export default CustomFooter;
const mapStateToProps = ({ notificationsReducer }) => {
  const { unReadNotificationCount, totalpendingOrdercount } = notificationsReducer;
  return { unReadNotificationCount, totalpendingOrdercount };
};
export default connect(mapStateToProps, null)(CustomFooter);