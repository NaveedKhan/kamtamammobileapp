import React, {
    Component
} from 'react';
import {
    Text,
    View,StyleSheet, Image,Dimensions,ScrollView,TextInput,Alert
} from 'react-native';
import { Container, Header, Content,Left,Right,Body,Title, Item, Input, Icon,Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
//import { config } from "../../src/config";
//import axios from "axios";


// const CommonHeader = ({text,UserCompany,orgid})=>
// {
class CommonHeader extends Component {
    constructor(props) {
      super(props);
      this.state = {
        //starCount: 3.5,
      //  modalVisible: this.props.isVisible
      };
}



    

  render(){
   
    return(
        <Header   style={{backgroundColor:'#d67908'}}>
        <Left>
          <Button transparent onPress={()=> console.log('back clicked')} >
            <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:25, color: '#fff'}} />
          </Button>
        </Left>
        <Body>
          <Title style={styles.headerStyle}>{text}</Title>
        </Body>
        <Right>
          <Button transparent onPress={()=> console.log('back clicked')}>
            {/* <Icon name='menu' /> */}
            <Icon type="FontAwesome"  name="sign-out"  style={{fontSize:25, color: '#fff'}}/>
          </Button>
        </Right>
      </Header>
    )
  }
}

export  {CommonHeader};

const styles = StyleSheet.create({
    containerStyle: {
        flex:1,justifyContent: 'flex-start', flexDirection: 'column',  alignContent:'flex-start'
    },
    contentStyle:
    {
        flex: 1,backgroundColor: '#3f8db4', flexDirection: 'column'
    },
    headerStyle:{
        color:'#fff',fontSize:13,width:200
    }
  });