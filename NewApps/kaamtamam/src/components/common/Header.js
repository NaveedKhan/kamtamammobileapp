import React from 'react';
import { Text, View } from 'react-native';

const Header = (props) => {
  const { textStyle, viewStyle } = styles;
 return (
<View style={viewStyle}>
   <Text style={textStyle}>{props.headerText}</Text>
</View>
 );
};

// Header.PropsDefault() {
//   headerText: "Header New"
// };
//Make it availbel for main View
export { Header };

const styles = {
  textStyle: {
    fontSize: 20
  //  backgroundColor: 'orange'
  //  alignItems: 'center'
  },
  viewStyle: {
    backgroundColor: '#F8F8F8',
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 }, //length width of shadow
    shadowOpacity: 0.5, // darkness heavy
    elevation: 2,
    position: 'relative',
    paddingTop: 10
  }
};
