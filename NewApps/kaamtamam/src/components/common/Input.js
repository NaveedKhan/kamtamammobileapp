import React from 'react';
import { View, Text, TextInput } from 'react-native';

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry }) => (
<View style={styles.containerStyle}>
  <Text style={styles.labelStyle}>{label}</Text>
  <TextInput
  secureTextEntry={secureTextEntry}
  autoCorrect={false}
  placeholder={placeholder}
  placeholderColor="gray"
  value={value}
  onChangeText={onChangeText}
  style={ styles.inputStyle}
  />
</View>
 );

export { Input };

const styles = {
labelStyle: {
  fontSize: 18,
  paddingLeft: 10,
  textAlign: 'center',
  flex: 1
},
inputStyle: {
  color: '#000',
  paddingRight: 5,
  paddingLeft: 10,
  fontSize: 18,
  lineHeight: 23,
  flex: 2
},

containerStyle: {
  height: 40,
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center'
}

};
