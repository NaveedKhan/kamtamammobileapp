
//const ROOT_URL = 'http://hanif268t.nayatel.net:8091/api/';
//const ROOT_URL = "http://10.0.2.2:8080/api/";


export const ROOT_URL = "http://115.186.182.33:8080/api/";
//export const ROOT_URL = "http://10.0.2.2:59430/api/";
//https://stackoverflow.com/questions/45229579/could-not-run-adb-reverse-react-native
export const config = {
  REST_API: {
    Account: {
      SignOut: `${ROOT_URL}Auth/SignOut`,
      Login: `${ROOT_URL}Auth/LoginUser`,
      LoginGuest: `${ROOT_URL}Auth/LoginGuest`,
      SignUp: `${ROOT_URL}Auth/SignUp`,
    },
    Oders: {
      AddOrder: `${ROOT_URL}Orders/AddOrder`,
      GetOrderByUserID: `${ROOT_URL}Orders/GetOrderByUserID/`,
      GetOrderHistoryOfGuest: `${ROOT_URL}Orders/GetOrderHistoryOfGuest/`,
      GetRecentOrdersOfGuest:`${ROOT_URL}Orders/GetRecentOrdersOfGuest/`,
      GetRecentOrderServicesOfUser: `${ROOT_URL}Orders/GetRecentOrderServicesOfUser/`,
      MarkOrderCompleted: `${ROOT_URL}Orders/MarkOrderComplete/`,
      AddUserReviewForOrder: `${ROOT_URL}Orders/AddUserReviewForOrder/`,
      MarkOrderCancelByUser:`${ROOT_URL}Orders/MarkOrderCancelByUser/`,
      GetPendingOrdersCount:`${ROOT_URL}Orders/GetPendingOrdersCount/`,
      GetGuestPendingOrdersCount:`${ROOT_URL}Orders/GetGuestPendingOrdersCount/`,

    },
    GrocProducts:{
      SearchGrocProductsByKeyword:`${ROOT_URL}Groc_Items/SearchGrocProductsByKeyword`,
    },
    Test: {
      GetProducts: `${ROOT_URL}values`,
      PostImage: `${ROOT_URL}values/Test`,
    },
    Notifications: {
      GetNotByDriverID: `${ROOT_URL}GrocNotification/GetNotByDriverID`,
      GetNotByUserID: `${ROOT_URL}GrocNotification/GetNotByUserID`,
      MarkNoificationRead: `${ROOT_URL}GrocNotification/MarkNoificationRead`,
    },
    ServiceRequest: {
      AddServiceRequest: `${ROOT_URL}Handy_ServiceRequest/`,
      GetServiceRequests: `${ROOT_URL}Handy_ServiceRequest/GetNotByUserID`,
      MarkServiceRequestCompleteByUser: `${ROOT_URL}Handy_ServiceRequest/MarkServiceRequestCompleteByUser`,
      CancelServiceRequestByUser: `${ROOT_URL}Handy_ServiceRequest/CancelServiceRequestByUser`,
      GetPendingHandyServicesCount: `${ROOT_URL}Handy_ServiceRequest/GetPendingHandyServicesCount`,
      GetGuestPendingHandyServicesCount: `${ROOT_URL}Handy_ServiceRequest/GetGuestPendingHandyServicesCount`,
      AddUserReviewForService: `${ROOT_URL}Handy_ServiceRequest/AddUserReviewForService`,
      
    }
  },
};
