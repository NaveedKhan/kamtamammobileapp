import {
    PRODUCT_CHECKOUT_ADDRESS_SELECTED, PRODUCT_CHECKOUT_CONFIRM_CLICKED,
    PRODUCT_CHECKOUT_ADD_ADDRESS_CLICKED, PRODUCT_CHECKOUT_CONFIRM_SUCCESSFUL,
     PRODUCT_CHECKOUT_CONFIRM_FAIL,
     PRODUCT_CHECKOUT_UPDATE_FORM
} from '../actions/types';
import { Actions } from 'react-native-router-flux';

const INITIAL_STATE = { user: '', isloading: false, selectedCategory: '', selectedProduct: '', error: '',
 totalItems: 0, orderInCartList: [], orderDetail: {},isOrderSchedule:false,orderScheduleDate:'' ,OrderScheduleDateString:'',
iscurrentLocSelected:true,LocationTrackingID:''};

export default (state = INITIAL_STATE, action) => {
    //debugger;
    switch (action.type) {
        case PRODUCT_CHECKOUT_UPDATE_FORM:
            return { ...state, [action.payload.prop]: action.payload.value }
        case PRODUCT_CHECKOUT_ADDRESS_SELECTED:
            return { ...state, [action.payload.prop]: action.payload.value }
        case PRODUCT_CHECKOUT_CONFIRM_CLICKED:
            return { ...state, isloading: true }
        case PRODUCT_CHECKOUT_CONFIRM_FAIL:
            return { ...state, isloading: false, error: action.payload }
        case PRODUCT_CHECKOUT_CONFIRM_SUCCESSFUL:
            return INITIAL_STATE;
        case PRODUCT_CHECKOUT_ADD_ADDRESS_CLICKED:
            return INITIAL_STATE;
        default:
            return state;
    }
};