import { PRODUCT_DETAIL_ADD_CLICKED,PRODUCT_DETAIL_ADD_ITEM,PRODUCT_DETAIL_REMOVE_ITEM } from '../actions/types';
import { Actions } from 'react-native-router-flux';

const INITIAL_STATE ={ user: '', isloading:false, selectedCategory:'',selectedProduct:'',totalItems:0 };

export default(state = INITIAL_STATE, action ) => {
    //debugger;
    switch(action.type)
    {
        case PRODUCT_DETAIL_ADD_CLICKED:
            return {...state ,selectedCategory : 'Fruits' ,selectedProduct:'Banana'}
        case PRODUCT_DETAIL_ADD_ITEM:
                return {...state ,selectedCategory : 'selected wala',totalItems:state.totalItems+1 }
        case PRODUCT_DETAIL_REMOVE_ITEM:
                    return {...state ,user : 'naveed',isloading:false,totalItems:state.totalItems-1}       
        default:
            return state;
    }
}