import {
    PRODUCT_LIST_ITEM_SELECTED, PRODUCT_LIST_ADD_ITEM, PRODUCT_LIST_CART_CLICKED, PRODUCT_LIST_CART_CLEANED,
    PRODUCT_LIST_REMOVE_ITEM, PRODUCT_LIST_SEARCH_START, PRODUCT_LIST_SEARCH_SUCCESS, PRODUCT_LIST__SEARCH_FAILED
} from '../actions/types';
import { Actions } from 'react-native-router-flux';

const INITIAL_STATE = {
    user: '', isloading: false, error: '',
    selectedCategoryName: '', selectedCategory: '',
    selectedProduct: '', totalItems: 0, orderInCartList: [], searchedProductsList: []
};

export default (state = INITIAL_STATE, action) => {
    //debugger;
    switch (action.type) {
        case PRODUCT_LIST_ITEM_SELECTED:
            return { ...state, selectedCategory: action.payload.catid, selectedCategoryName: action.payload.catname }
        case PRODUCT_LIST_ADD_ITEM:
            return { ...state, selectedCategory: 'selected wala', totalItems: state.totalItems + 1, orderInCartList: action.payload }
        case PRODUCT_LIST_CART_CLICKED:
            return { ...state, selectedCategory: 'selected wala' }
        case PRODUCT_LIST_CART_CLEANED:
            return { ...state, orderInCartList: [], error: '' }
        case PRODUCT_LIST_REMOVE_ITEM:
            return { ...state, user: 'naveed', isloading: false, totalItems: state.totalItems - 1, orderInCartList: action.payload }

        case PRODUCT_LIST_SEARCH_START:
            return { ...state, isloading: true, }
        case PRODUCT_LIST_SEARCH_SUCCESS:
            return { ...state, isloading: false, searchedProductsList: action.payload }
        case PRODUCT_LIST__SEARCH_FAILED:
            return { ...state, isloading: false, error: action.payload }

        default:
            return state;
    }
}

