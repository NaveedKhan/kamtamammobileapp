import {
    HANDY_SERVICES_FETCHING, HANDY_SERVICES_FETECHED_FAILED, HANDY_SERVICES_FETECHED_SUCCESS, HANDY_SERVICES_REQUEST, HANDY_SERVICES_REQUEST_ADD_REVIEW,
    HANDY_SERVICES_REQUEST_ADD_REVIEW_FAILED, HANDY_SERVICES_REQUEST_ADD_REVIEW_SUCCESS, HANDY_SERVICES_REQUEST_FAILED, HANDY_SERVICES_REQUEST_MARK_COMPLETE,
    HANDY_SERVICES_REQUEST_MARK_COMPLETE_FAILED, HANDY_SERVICES_REQUEST_MARK_COMPLETE_SUCCESS, HANDY_SERVICES_REQUEST_SUCCESS,HANDY_SERVICES_REQUEST_FORMDATA,
    HANDY_SERVICES_REQUEST_STEP1,HANDY_SERVICES_REQUEST_STEP2,HANDY_SERVICES_REQUEST_STEP3
} from '../actions/types';

const INITIAL_STATE = { services: [], servicestype: [], serviceRequestModel: {}, error: '', loading: false, selectedServies:[],
 RequestServiceType:'', RequestTime:'',ServiceRequestScheduleDateString:'',
RequestServicesList:[],RequestImages:[],RequestDescription:'',RequestAddress:'',
maintainanceSelected:false, installationSelected:false , serviceRequestScheduleDate:'',
isImmediateRequest:true, requsetLat: 33.6641, requetLong:73.0506, requestSearchAddress:'' ,LocationTrackingID:''};


export default (state = INITIAL_STATE, action) => {
    //debugger;
    switch (action.type) {
        case HANDY_SERVICES_REQUEST_FORMDATA:
            return {...state , [action.payload.prop] : action.payload.value }

        case HANDY_SERVICES_FETCHING:
            return { ...state, loading: true };
        case HANDY_SERVICES_FETECHED_SUCCESS:
            return { ...state, services: action.payload.services, servicestype: action.payload.servicestype };
        case HANDY_SERVICES_FETECHED_FAILED:
            debugger;
            return { ...state, error: action.payload, loading: false };

        case HANDY_SERVICES_REQUEST:
            debugger;
            return { ...state, loading: true };
        case HANDY_SERVICES_REQUEST_SUCCESS:
            debugger;
            return { ...state,isImmediateRequest:true, loading: false };
        case HANDY_SERVICES_REQUEST_FAILED:
            debugger;
            return { ...state, loading: false ,error: action.payload};

        case HANDY_SERVICES_REQUEST_ADD_REVIEW:
            debugger;
            return { ...state, ...INITIAL_STATE, user: action.payload, loading: false };
        case HANDY_SERVICES_REQUEST_ADD_REVIEW_SUCCESS:
            debugger;
            return { ...state, ...INITIAL_STATE, user: action.payload, loading: false };
        case HANDY_SERVICES_REQUEST_ADD_REVIEW_FAILED:
            debugger;
            return { ...state, ...INITIAL_STATE, user: action.payload, loading: false };

        case HANDY_SERVICES_REQUEST_MARK_COMPLETE:
            debugger;
            return { ...state, ...INITIAL_STATE, user: action.payload, loading: false };
        case HANDY_SERVICES_REQUEST_MARK_COMPLETE_SUCCESS:
            debugger;
            return { ...state, ...INITIAL_STATE, user: action.payload, loading: false };
        case HANDY_SERVICES_REQUEST_MARK_COMPLETE_FAILED:
            debugger;
            return { ...state, ...INITIAL_STATE, user: action.payload, loading: false };

        default:
            return state;
    }
}