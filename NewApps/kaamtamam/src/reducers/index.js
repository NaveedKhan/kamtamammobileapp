import { combineReducers }  from 'redux';
import AuthReducer from './AuthReducer';
import EmployeeReducer from './EmployeeReducres';
import LandingPageReducer from './LandingPageReducers';
import ProductListReducers from './ProductListReducers';
import ProductDetailReducers from './ProductDetailReducers';
import CartReducers from './CartReducers';
import CheckoutReducers from './CheckoutReducers';
import SplashScreensReducers from './SplashScreensReducers';
import SignUpReducers from './SignUpReducers';
import HandyServicesReducers from './HandyServicesReducers';
import AddressReducers from './AddressReducers';
import NotificationsReducer from './NotificationsReducer';


export default combineReducers({
  auth:  AuthReducer,
  employee: EmployeeReducer,
  landingPageReducer:LandingPageReducer,
  productList:ProductListReducers,
  productDetail:ProductDetailReducers,
  cart:CartReducers,
  checkout:CheckoutReducers,
  splashScreen:SplashScreensReducers,
  signUp:SignUpReducers,
  handyServices:HandyServicesReducers,
  userAddress:AddressReducers,
  notificationsReducer:NotificationsReducer
});
