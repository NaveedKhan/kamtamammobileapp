import { LANDING_CATEGORY_SELECTED, LANDING_DATAFECTHED, LANDING_DATAFECTHING } from '../actions/types';;
import { Actions } from 'react-native-router-flux';

const INITIAL_STATE = { user: '', isloading: false, selectedCategory: '', ProductsData: {} };

export default (state = INITIAL_STATE, action) => {
   // debugger;
    switch (action.type) {
        case LANDING_CATEGORY_SELECTED:
            return { ...state, selectedCategory: 'selected wala' }
        case LANDING_DATAFECTHED:
            return { ...state, user: 'naveed', isloading: false,ProductsData:action.payload }
        case LANDING_DATAFECTHING:
            return { ...state, isloading: true }
        default:
            return state;
    }
}