import {
    SIGNUP_EMAIL_CHANGED,
     SIGNUP_FAILED, SIGNUP_FULLNAME_CHANGED, 
     SIGNUP_PASSWORD_CHANGED, SIGNUP_PHONE_CHANGED, SIGNUP_START,
    SIGNUP_SUCCESSFUL, SIGNUP_USERNAME_CHANGED,SIGNUP_FORM_UPDATE
} from '../actions/types';
const INITIAL_STATE = {
    email: '', password: '', fullname: '', phone: '',
    error: '',username:'', loading: false,ProductsList:'',houseNo:'',
    streetNo:'',blockNo:'',fullAddress:''
};

export default (state = INITIAL_STATE, action) => {
    //debugger;
    switch (action.type) {
        case SIGNUP_FORM_UPDATE:
            return {...state , [action.payload.prop] : action.payload.value }
        case SIGNUP_EMAIL_CHANGED:
            return { ...state, email: action.payload };
        case SIGNUP_FULLNAME_CHANGED:
            return { ...state, fullname: action.payload };
        case SIGNUP_FAILED:
            return { ...state, error: action.payload,loading: false  };
        case SIGNUP_PASSWORD_CHANGED:
            return { ...state, password: action.payload };
        case SIGNUP_USERNAME_CHANGED:
            return { ...state, username: action.payload };
        case SIGNUP_SUCCESSFUL:
            return { INITIAL_STATE };
        case SIGNUP_PHONE_CHANGED:
            debugger;
            return { ...state, phone: action.payload };
        case SIGNUP_START:
            debugger;
            return { ...state, loading: true };    
        default:
            return state;
    }
}

//return {( ...state, ...INITIAL_STATE,)}
