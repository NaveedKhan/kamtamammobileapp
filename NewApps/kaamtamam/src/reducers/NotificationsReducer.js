import {NOTIFICATION_DETAIL_UPDATE } from '../actions/types';
import { Actions } from 'react-native-router-flux';

const INITIAL_STATE = { unReadNotificationCount: 0, notificationsList:[] ,totalpendingOrdercount:0};

export default (state = INITIAL_STATE, action) => {
    //debugger;
    switch (action.type) {
        case NOTIFICATION_DETAIL_UPDATE:
            return { ...state, [action.payload.prop]: action.payload.value }
        default:
            return state;
    }
};