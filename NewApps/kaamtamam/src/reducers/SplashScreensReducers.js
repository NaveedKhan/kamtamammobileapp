import { PRODUCT_FETCHING, PRODUCT_FETECHED_FAILED, PRODUCT_FETECHED_SUCCESS } from '../actions/types';

const INITIAL_STATE ={ Products: [], isloading:false,error:''};

export default ( state = INITIAL_STATE, action ) =>
{
    //debugger;
    switch(action.type)
    {
        case PRODUCT_FETCHING:
            return {...state ,isloading:true}
        case PRODUCT_FETECHED_FAILED:
                return {...state ,error:action.payload,isloading:false }
        case PRODUCT_FETECHED_SUCCESS:
                    return {...state ,isloading:false,error:'' }  
        default:
            return state;
    }
}