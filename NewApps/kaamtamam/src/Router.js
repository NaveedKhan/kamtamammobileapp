import React, { Component } from 'react';
import { Scene, Router, Stack, Actions, Drawer, ActionConst } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';
import CreateEmployee from './components/CreateEmployee';

import LandingPage from './components/Grocessry/Dashboard/LandingPage';
import Notification from './components/Grocessry/Notification/Notification';
import NotificationDetail from './components/Grocessry/Notification/NotificationDetail';

import ProductList from './components/Grocessry/Product/ProductList';
import ProductDetail from './components/Grocessry/Product/ProductDetail';
import Cart from './components/Grocessry/Product/Cart';
import Checkout from './components/Grocessry/Product/Checkout';
import SplashScreen from './components/Grocessry/SplashScreens/SplashScreen';
import SplashScreen2 from './components/Grocessry/SplashScreens/SplashScreen2';
import SignUp from './components/Grocessry/Account/SignUp';
import Login from './components/Grocessry/Account/Login';
import AddAddress from './components/Grocessry/Account/AddAddress';
import MyAccount from './components/Grocessry/Account/MyAccount';
import Carousel from './components/Grocessry/Carousel';
import Sidebar from './components/Grocessry/Common/sidebar';
import OrderHistory from './components/Grocessry/Order/OrderHistory';
import OrderDetail from './components/Grocessry/Order/OrderDetail';
import Status from './components/Handyman/ServicesRequest/Status';
import ServiecRequestStep1 from './components/Handyman/ServicesRequest/ServiecRequestStep1';
import ServiecRequestStep3 from './components/Handyman/ServicesRequest/ServiecRequestStep3';
import Summary from './components/Handyman/ServicesRequest/Summary';
import Map from './components/Handyman/ServicesRequest/Map';
import SelectJob from './components/SelectJob';
import SelectServices from './components/Handyman/SelectServices';
import OrderDelivery from './components/Handyman/ServicesRequest/OrderDelivery';
import RequestDetail from './components/Handyman/ServicesRequest/RequestDetail';
import ServiceDetails from './components/Handyman/ServicesRequest/ServiceDetails';
import ServiecRequestStep2 from './components/Handyman/ServicesRequest/ServiecRequestStep2';
import SearchArea from './components/SearchArea';
import OrderHistoryCard from './components/Grocessry/Order/OrderHistoryCard';
import TraceOrder from './components/Grocessry/Order/TraceOrder';
import SearchProducts from './components/Grocessry/Product/SearchProducts';
import MyWishList from './components/Grocessry/Product/MyWishList';
import ContactUs from './components/ContactUs';

// go to nodeModule -> react-native-snap-carousel -> src -> Carousel.js
// modify

// const AnimatedFlatList = FlatList ? Animated.createAnimatedComponent(FlatList) : null;
// const AnimatedScrollView = Animated.Animated.createAnimatedComponent(ScrollView);

// to
// const AnimatedFlatList = FlatList ? Animated.FlatList : null;
// const AnimatedScrollView = Animated.ScrollView;

// after find function _getWrappedRef
// modify like this
// _getWrappedRef () {
// // facebook/react-native#10635
// // https://stackoverflow.com/a/48786374/8412141
// return this._carouselRef
// }

const RouterComponent = () => {
  return (
    <Router hideNavBar sceneStyle={{ paddingTop: 0, backgroundColor: '#fff' }}>
      <Stack hideNavBar key="root">
      <Scene key="status" component={Status} title="Status" hideNavBar />
       <Scene key="searchArea" component={SearchArea} title="" hideNavBar  />
        <Scene key="Splash" component={SplashScreen} title="" hideNavBar initial />
        <Scene key="splash2" component={SplashScreen2} title="" hideNavBar hideNavBar swipeEnabled={false} panHandlers={null} type={ActionConst.RESET}/>
        <Scene key="login" component={Login} title="Login Here" hideNavBar swipeEnabled={false} panHandlers={null} type={ActionConst.RESET} />
        <Scene key="signUp" component={SignUp} title="" hideNavBar />
        <Scene key="Map2" component={Map} title="Map" hideNavBar />
        <Drawer
          key="userProfile"
          drawerPosition="right"
          contentComponent={Sidebar}
        
          >
            
          <Scene key="root2"   >

            <Scene key="SelectJob" component={SelectJob} title="SelectJob" hideNavBar type={ActionConst.RESET}/>
            <Scene key="addAddress" component={AddAddress} title="Dashboard" hideNavBar />
            <Scene key="Summary" component={Summary} title="Summary" hideNavBar />
            <Scene key="employeeList" component={EmployeeList} title="Employees" />
            {/* ServiceDetails */}
            <Scene key="selectServices" component={SelectServices} title="SelectServices"  hideNavBar />
            <Scene key="serviceDetails" component={ServiceDetails} title="SelectServices"  hideNavBar />
            <Scene key="contactUs" component={ContactUs} title="SelectServices"  hideNavBar />
            
            <Scene key="searchProducts" component={SearchProducts} title="Dashboard" hideNavBar />
            <Scene key="myWishList" component={MyWishList} title="Dashboard" hideNavBar />
            <Scene key="landingPage" component={LandingPage} title="Dashboard" hideNavBar />

            <Scene key="notification" component={Notification} title="Dashboard" hideNavBar />
            <Scene key="notificationDetail"   component={NotificationDetail}   title="Dashboard"   hideNavBar      />

            <Scene   key="orderHistory"  component={OrderHistory}   title="Dashboard"  hideNavBar  />
            <Scene   key="orderDetail"  component={OrderDetail}  title="Dashboard"   hideNavBar  />
            <Scene   key="traceOrder"  component={TraceOrder}  title="Dashboard"   hideNavBar  />
            
            <Scene
              key="MyAccount"
              component={MyAccount}
              title="Dashboard"
              hideNavBar
            />
            <Scene
              key="carousel"
              component={Carousel}
              title="Dashboard"
              hideNavBar
            />
            <Scene key="productList" component={ProductList} hideNavBar />
            <Scene
              key="productDetail"
              component={ProductDetail}
              title=" Product detail"
              hideNavBar
            />
            <Scene key="cart" component={Cart} title="Cart" hideNavBar />
            <Scene
              key="checkout"
              component={Checkout}
              title="Checkout"
              hideNavBar
            />

            <Scene
              key="createEmployee"
              component={CreateEmployee}
              title="Login Here"
            />
       

            <Scene
              key="PlumberRequestLandingPage"
              component={ServiecRequestStep1}
              title="Plumber Request"
              hideNavBar

            />

            <Scene
              key="PlumberRequestProceedPage"
              component={ServiecRequestStep3}
              title="Plumber Request"
              hideNavBar

            />
            <Scene
              key="requestDescription"
              component={ServiecRequestStep2}
              title="Plumber Request"
              hideNavBar

            />
            <Scene
              key="RequestDetail"
              component={RequestDetail}
              title="RequestDetail"
              hideNavBar

            />


            
            <Scene key="Map" component={Map} title="Map" hideNavBar />


            <Scene
              key="orderDelivery"
              component={OrderDelivery}
              title="OrderDelivery"
              hideNavBar

            />
          </Scene>
        </Drawer>
      </Stack>
    </Router>
  );
};

export default RouterComponent;
