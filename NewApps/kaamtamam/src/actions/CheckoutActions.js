import {
  PRODUCT_CHECKOUT_ADDRESS_SELECTED,
  PRODUCT_CHECKOUT_CONFIRM_CLICKED,
  PRODUCT_CART_CLEANED,
  PRODUCT_LIST_CART_CLEANED,
  PRODUCT_CHECKOUT_UPDATE_FORM,
  NOTIFICATION_DETAIL_UPDATE,
  PRODUCT_CHECKOUT_CONFIRM_FAIL,
  PRODUCT_CHECKOUT_CONFIRM_SUCCESSFUL,
} from './types';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import {Alert} from 'react-native';
import {config} from '../config';

export const UpdateCheckoutFormData = ({prop, value}) => {
  debugger;
  return {
    type: PRODUCT_CHECKOUT_UPDATE_FORM,
    payload: {prop, value},
  };
};

export const confirmButtonClicked = OrderdetailModel => {
  var me = this;
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };

  return dispatch => {
    dispatch({
      type: PRODUCT_CHECKOUT_CONFIRM_CLICKED,
    });

    const {REST_API} = config;
    axios
      .post(REST_API.Oders.AddOrder, OrderdetailModel, apiconfig)
      .then(response => {
        debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          Alert.alert(
            'Order Submitted',
            'Dear valued user, Your order for grocery is submitted. Our team will call you for confirmation within 5 minute.',
            [
              {
                text: 'OK',
                onPress: () => checkoutSucess(dispatch, baseModel.data),
              },
            ],
            {cancelable: false},
          );
        } else {
          if(baseModel.message.indexOf("You have already ordered for Grocerry") > -1)
          {
            Alert.alert(
              'Attention!',
              baseModel.message,
              [
                {
                  text: 'OK',
                  onPress: () =>  Actions.reset('SelectJob'),
                },
              ],
              {cancelable: false},
            );
          }
            checkoutFail(dispatch, baseModel.message);
          
        }
      })
      .catch(error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        checkoutFail(dispatch, err);
        console.log('error', error);
      });
  };
};

export const addressChanged = text => {
  return {
    type: PRODUCT_CHECKOUT_ADDRESS_SELECTED,
    payload: text,
  };
};

const checkoutSucess = (dispatch, data) => {
  debugger;
  dispatch({
    type: PRODUCT_CART_CLEANED,
  });
  dispatch({
    type: PRODUCT_CHECKOUT_CONFIRM_SUCCESSFUL,
  });

  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: {
      prop: 'unReadNotificationCount',
      value: data.unReadnotificationCount,
    },
  });
  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: {
      prop: 'totalpendingOrdercount',
      value: data.totalpendingOrdercount,
    },
  });
  dispatch({
    type: PRODUCT_LIST_CART_CLEANED,
  });

  debugger;
  Actions.reset('SelectJob');
};

const checkoutFail = (dispatch, error) => {
  debugger;
  dispatch({
    type: PRODUCT_CHECKOUT_CONFIRM_FAIL,
    payload: error,
  });
};
