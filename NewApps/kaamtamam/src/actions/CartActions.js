import { PRODUCT_CART_ADD_ITEM,PRODUCT_CART_CHECKOUT_CLICKED,PRODUCT_CART_REMOVE_ITEM } from './types';
import { Actions } from 'react-native-router-flux';

export const checkoutButton = () =>{

    return(dispatch)=>{
        console.log('clicked checkout button from cart detail');
        dispatch({type:PRODUCT_CART_CHECKOUT_CLICKED});
    }
}

export const addItem = (text) => {
  
    return {
      type: PRODUCT_CART_ADD_ITEM,
      payload: text
    };
  };

  export const rempveItem = (text) => {
 
    return {
      type: PRODUCT_CART_REMOVE_ITEM,
      payload: text
    };
  };