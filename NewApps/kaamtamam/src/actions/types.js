export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESSFUL = 'login_successful';
export const LOGIN_USER_FAIL = 'login_fail';
export const LOGIN_USER = 'login_user';
export const LOGOUT_USER = 'logout_user';
export const LOGOUT_USER_SUCCESSFUL = 'logout_user_success';
export const LOGOUT_USER_FAIL = 'logout_user_fail';
export const USER_DETAIL_UPDATE = 'user_detail_update';


export const SIGNUP_EMAIL_CHANGED = 'signup_email_changed';
export const SIGNUP_USERNAME_CHANGED = 'signup_username_changed';
export const SIGNUP_PASSWORD_CHANGED = 'signup_password_changed';
export const SIGNUP_PHONE_CHANGED = 'signup_phone_changed';
export const SIGNUP_FULLNAME_CHANGED = 'signup_fullname_changed';
export const SIGNUP_START = 'signup_start';
export const SIGNUP_SUCCESSFUL = 'signup_successful';
export const SIGNUP_FAILED = 'signup_failed';
export const SIGNUP_FORM_UPDATE = 'signup_form_update';

export const NOTIFICATION_DETAIL_UPDATE = 'notification_detail_update';


export const EMPLOYEE_UPDATE = 'employee_update';
export const EMPLOYEE_CREATE = 'employee_create';

export const LANDING_DATAFECTHED = 'landing_datfetched';
export const LANDING_DATAFECTHING = 'landing_datafetching';
export const LANDING_CATEGORY_SELECTED = 'landing_category_selected';

export const PRODUCT_LIST_ADD_ITEM = 'product_list_add_item';
export const PRODUCT_LIST_REMOVE_ITEM = 'product_list_remove_item';
export const PRODUCT_LIST_ITEM_SELECTED = 'product_list_item_slected';
export const PRODUCT_LIST_CART_CLICKED = 'product_list_cart_selected';
export const PRODUCT_LIST_CART_CLEANED = 'product_list_cart_cleaned';

export const PRODUCT_LIST_SEARCH_START = 'product_list_search_start';
export const PRODUCT_LIST_SEARCH_SUCCESS = 'product_list_search_success';
export const PRODUCT_LIST__SEARCH_FAILED = 'failed';




export const PRODUCT_DETAIL_ADD_ITEM = 'product_detail_add_item';
export const PRODUCT_DETAIL_REMOVE_ITEM = 'product_detail_remove_item';
export const PRODUCT_DETAIL_ADD_CLICKED = 'product_detail_add_clicked';


export const PRODUCT_CART_CHECKOUT_CLICKED = 'product_cart_checkout_clicked';
export const PRODUCT_CART_BACK_CLICKED = 'product_cart_back_clicked';
export const PRODUCT_CART_ADD_ITEM = 'product_cart_add_item';
export const PRODUCT_CART_REMOVE_ITEM = 'product_cart_remove_item';
export const PRODUCT_CART_CLEANED = 'product_cart_cleaned';


export const PRODUCT_CHECKOUT_CONFIRM_CLICKED = 'product_checkout_confirm_clicked';
export const PRODUCT_CHECKOUT_CONFIRM_SUCCESSFUL = 'product_checkout_confirm_successful';
export const PRODUCT_CHECKOUT_CONFIRM_FAIL = 'product_checkout_confirm_fail';
export const PRODUCT_CHECKOUT_ADD_ADDRESS_CLICKED = 'product_checkout_add_address_clicked';
export const PRODUCT_CHECKOUT_ADDRESS_SELECTED = 'product_checkout_address_selected'; 
export const PRODUCT_CHECKOUT_UPDATE_FORM = 'product_checkout_update_form'; 




export const UPDATE_USER_ADDRESS_DETAIL = 'update_user_address_detail'; 
export const UPDATE_NOTIFICATION_DETAIL = 'update_notifiaction_detail'; 





export const PRODUCT_FETCHING = 'product_fetching';
export const PRODUCT_FETECHED_SUCCESS = 'product_fetched_success';
export const PRODUCT_FETECHED_FAILED = 'product_fetched_failed';



export const HANDY_SERVICES_FETCHING = 'handy_services_fetching';
export const HANDY_SERVICES_FETECHED_SUCCESS = 'handy_services_fetched_success';
export const HANDY_SERVICES_FETECHED_FAILED = 'handy_services_fetched_failed';

export const HANDY_SERVICES_REQUEST = 'handy_services_request';
export const HANDY_SERVICES_REQUEST_SUCCESS = 'handy_services_request_success';
export const HANDY_SERVICES_REQUEST_FAILED = 'handy_services_request_failed';

export const HANDY_SERVICES_REQUEST_ADD_REVIEW = 'handy_services_request_add_review';
export const HANDY_SERVICES_REQUEST_ADD_REVIEW_SUCCESS = 'handy_services_request_add_review_success';
export const HANDY_SERVICES_REQUEST_ADD_REVIEW_FAILED = 'handy_services_request_add_review_failed';

export const HANDY_SERVICES_REQUEST_MARK_COMPLETE = 'handy_services_request_mark_complete';
export const HANDY_SERVICES_REQUEST_MARK_COMPLETE_SUCCESS = 'handy_services_request_mark_complete_success';
export const HANDY_SERVICES_REQUEST_MARK_COMPLETE_FAILED = 'handy_services_request_mark_complete_failed';

export const HANDY_SERVICES_REQUEST_FORMDATA = 'handy_services_request_formdata';
export const HANDY_SERVICES_REQUEST_STEP1 = 'handy_services_request_step1';
export const HANDY_SERVICES_REQUEST_STEP2 = 'handy_services_request_step2';
export const HANDY_SERVICES_REQUEST_STEP3 = 'handy_services_request_step3';
