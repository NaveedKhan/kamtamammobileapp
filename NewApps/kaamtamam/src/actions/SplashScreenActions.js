import {
  PRODUCT_FETCHING,
  PRODUCT_FETECHED_FAILED,
  PRODUCT_FETECHED_SUCCESS,
  LANDING_DATAFECTHED,
  HANDY_SERVICES_FETECHED_SUCCESS,
} from './types';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Request from '../Utility/Request';
import BaseApi from '../Utility/BaseApi';
import {Alert} from 'react-native';
import {config} from '../config';

export const fetchingProducts = () => {
  debugger;
  let configs = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
  return dispatch => {
    dispatch({type: PRODUCT_FETCHING});
    const {REST_API} = config;

    axios
      .get(REST_API.Test.GetProducts, configs)
      .then(response => {
        debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          productFetchingSucess(dispatch, response.data);
        } else {
          productFetchingFail(dispatch, baseModel.message);
        }
      })
      .catch(error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        productFetchingFail(dispatch, err);
        console.log('error', error);
      });
  };
};

const productFetchingSucess = (dispatch, productData) => {
  debugger;
  var productsDataModel = {
    products: productData.products,
    productsCat: productData.productsCat,
  };
  var serviceDataModel = {
    services: productData.services,
    servicestype: productData.servicestype,
  };

  dispatch({
    type: PRODUCT_FETECHED_SUCCESS,
  });
  dispatch({
    type: LANDING_DATAFECTHED,
    payload: productsDataModel,
  });

  dispatch({
    type: HANDY_SERVICES_FETECHED_SUCCESS,
    payload: serviceDataModel,
  });

  Actions.splash2();
};

const productFetchingFail = (dispatch, error) => {
  debugger;
  dispatch({
    type: PRODUCT_FETECHED_FAILED,
    payload: error ? error : 'some error occured',
  });

  Alert.alert('OOPs!', 'something went wrong ' + error);
};
