import { PRODUCT_LIST_ITEM_SELECTED,PRODUCT_LIST_ADD_ITEM,PRODUCT_LIST_CART_CLICKED,PRODUCT_LIST_REMOVE_ITEM ,
    PRODUCT_CART_ADD_ITEM,PRODUCT_CART_REMOVE_ITEM,PRODUCT_LIST_SEARCH_START,PRODUCT_LIST_SEARCH_SUCCESS,PRODUCT_LIST__SEARCH_FAILED
} from './types';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import {Alert} from 'react-native'; 
import { config } from '../config';



export const searchProductsByKeyword = (keyword) => {
    var me = this;
      const SearchVM = { SearchKeyword: keyword ,AppSelected:'Grocerry'};
      let apiconfig = {
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
      };
    
      return (dispatch) => {
        dispatch({
          type: PRODUCT_LIST_SEARCH_START
        });
    
        const { REST_API } = config;
        axios.post(REST_API.GrocProducts.SearchGrocProductsByKeyword, SearchVM, apiconfig)
          .then(response => {
            debugger;
            const baseModel = response.data;
            if (baseModel.success) {
                productSearchSuccess(dispatch,baseModel.data)
            } else {
                productSearchFail(dispatch, baseModel.message)
            }
          }).catch(error => {
            debugger;
            var err= String(error) == '' ? 'no error ' : String(error);
            productSearchFail(dispatch, err)
            console.log("error", error)
          });
    
      }
    }


    


export const AddItem = (text) =>{
    return (dispatch)=>{
        dispatch({type:PRODUCT_LIST_ADD_ITEM, payload:text});
        dispatch({type:PRODUCT_CART_ADD_ITEM, payload:text});
}};


export const RemoveItem = (text) =>{
    return (dispatch )=>
    {
     dispatch({ type:PRODUCT_LIST_REMOVE_ITEM, payload:text  });
     dispatch({type:PRODUCT_CART_REMOVE_ITEM, payload:text});
    
    }
};



export const OnItemClick = (productid,categoryname) =>{
 
        return (dispatch)=>{
                // console.log(name,phone,shift);
                 dispatch({type:PRODUCT_LIST_ITEM_SELECTED, payload:{catid:productid,catname:categoryname}})
                 console.log('data fetched');
                // Actions.employeeList({type:"reset"});             
    };
};

export const OnCartClick = () =>{
    
        return (dispatch)=>{
                // console.log(name,phone,shift);
                 dispatch({type:PRODUCT_LIST_CART_CLICKED})
                 console.log('data fetched');
                // Actions.employeeList({type:"reset"});             
    };
};

const productSearchSuccess = (dispatch, data) => {
    // debugger;
     dispatch({
       type: PRODUCT_LIST_SEARCH_SUCCESS,
       payload: data,
     });
   
   };
   
   const productSearchFail = (dispatch, error) => {
     debugger;
     dispatch({
       type: PRODUCT_LIST__SEARCH_FAILED,
       payload: error,
     });
     //Actions.landingPage();
   };