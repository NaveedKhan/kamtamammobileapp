import firebase from 'firebase';
import {
  SIGNUP_EMAIL_CHANGED, SIGNUP_FAILED, SIGNUP_FULLNAME_CHANGED, LOGIN_USER_SUCCESSFUL,
  SIGNUP_PASSWORD_CHANGED, SIGNUP_PHONE_CHANGED, SIGNUP_START, HANDY_SERVICES_FETECHED_SUCCESS,
  SIGNUP_SUCCESSFUL, SIGNUP_USERNAME_CHANGED, NOTIFICATION_DETAIL_UPDATE,USER_DETAIL_UPDATE,
  LANDING_DATAFECTHED, SIGNUP_FORM_UPDATE, PRODUCT_FETECHED_SUCCESS
} from './types';
import axios from 'axios';
import { config } from '../config';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { Alert } from 'react-native';




export const SignupUpdateFormData = ({ prop, value }) => {
  return {
    type: SIGNUP_FORM_UPDATE,
    payload: { prop, value }
  }
};
export const emailsignupChanged = (text) => {

  return {
    type: SIGNUP_EMAIL_CHANGED,
    payload: text
  };
};

export const passwordSignupChanged = (text) => {
  return {
    type: SIGNUP_PASSWORD_CHANGED,
    payload: text
  };
};

export const usernameChanged = (text) => {
  return {
    type: SIGNUP_USERNAME_CHANGED,
    payload: text
  };
};

export const fullnameChanged = (text) => {
  return {
    type: SIGNUP_FULLNAME_CHANGED,
    payload: text
  };
};

export const phoneChanged = (text) => {
  return {
    type: SIGNUP_PHONE_CHANGED,
    payload: text
  };
};

storeData = async (key, value) => {
  // debugger;
  try {
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    // saving error
  }
}

removeItemValue = async (key) => {
  try {
    await AsyncStorage.removeItem(key);
    return true;
  }
  catch (exception) {
    return false;
  }
}

export const signupUser = ({ email, password, fullname, phone, error, username }) => {
  debugger;
  const SignupModel = { UserName: username, PhoneNumber: phone, Email: email, Password: password, FullName: fullname };
  const SigninModel = { UserName: username, Password: password };
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8'
    }
  };

  return (dispatch) => {
    dispatch({
      type: SIGNUP_START
    });
    const { REST_API } = config;
    axios.post(REST_API.Account.SignUp, SignupModel, apiconfig)
      .then(response => {
        debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          signupUserSucess(dispatch, baseModel.data, SigninModel);
        } else {
          signupUserFail(dispatch, baseModel.message)
        }
      }).catch(error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        signupUserFail(dispatch, err)
        console.log("error", error)
      });
  }

};

const signupUserSucess = (dispatch, data, SigninModel) => {
  debugger;
  var productsDataModel = { products: data.products, productsCat: data.productsCat }
  var serviceDataModel = { services: data.services, servicestype: data.servicestype }
  dispatch({
    type: SIGNUP_SUCCESSFUL
  });
  dispatch({
    type: LOGIN_USER_SUCCESSFUL,
    payload: data.user,
  });
  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: { prop: 'unReadNotificationCount', value: data.unReadnotificationCount }
  });
  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: { prop: 'totalpendingOrdercount', value: data.totalpendingOrdercount }
  });

  dispatch({
    type: USER_DETAIL_UPDATE,
    payload: { prop: 'grocServiceChrges', value: data.grocservicecharges }
  });
  dispatch({
    type: USER_DETAIL_UPDATE,
    payload: { prop: 'handyServiceCharges', value: data.handyservicecharges }
  });
  dispatch({
    type: USER_DETAIL_UPDATE,
    payload: { prop: 'adverstisementsList', value: data.advertisementsList }
  });

  dispatch({
    type: LANDING_DATAFECTHED,
    payload: productsDataModel
  });

  dispatch({
    type: HANDY_SERVICES_FETECHED_SUCCESS,
    payload: serviceDataModel
  });




  this.storeData('userData', JSON.stringify(SigninModel));
  debugger;
  Actions.reset('userProfile');
};

const signupUserFail = (dispatch, error) => {
  debugger;
  dispatch({
    type: SIGNUP_FAILED,
    payload: error,
  });

  Alert.alert('OOps', 'Something went wrong! issue is ' + error);
  //Actions.employeeList();
};
