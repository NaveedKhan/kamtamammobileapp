import {
  HANDY_SERVICES_FETCHING,
  HANDY_SERVICES_FETECHED_FAILED,
  HANDY_SERVICES_FETECHED_SUCCESS,
  HANDY_SERVICES_REQUEST,
  HANDY_SERVICES_REQUEST_FAILED,
  HANDY_SERVICES_REQUEST_SUCCESS,
  HANDY_SERVICES_REQUEST_FORMDATA,
  HANDY_SERVICES_REQUEST_STEP1,
  HANDY_SERVICES_REQUEST_STEP2,
  HANDY_SERVICES_REQUEST_STEP3,
  NOTIFICATION_DETAIL_UPDATE,
} from './types';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import {Alert} from 'react-native';
import {config} from '../config';

export const step1Updated = data => {
  return {
    type: HANDY_SERVICES_REQUEST_STEP1,
    payload: data,
  };
};

export const step2Updated = data => {
  return {
    type: HANDY_SERVICES_REQUEST_STEP2,
    payload: data,
  };
};

export const step3Updated = data => {
  return {
    type: HANDY_SERVICES_REQUEST_STEP3,
    payload: data,
  };
};

export const ServiceRequestUpdateForm = ({prop, value}) => {
  return {
    type: HANDY_SERVICES_REQUEST_FORMDATA,
    payload: {prop, value},
  };
};

export const addServiceRequest = requestModel => {
  debugger;
  let configs = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
  return dispatch => {
    dispatch({type: HANDY_SERVICES_REQUEST});
    const {REST_API} = config;

    axios
      .post(REST_API.ServiceRequest.AddServiceRequest, requestModel, configs)
      .then(response => {
        debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          serviceAddSuccess(dispatch, baseModel.data);
        } else {
          serviceAddFail(dispatch, baseModel.message);
        }
      })
      .catch(error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        serviceAddFail(dispatch, err);
        console.log('error', error);
      });
  };
};

export const fetchingServices = () => {
  debugger;
  let configs = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
  return dispatch => {
    dispatch({type: HANDY_SERVICES_FETCHING});
    const {REST_API} = config;

    axios
      .get(REST_API.Test.GetProducts, configs)
      .then(response => {
        debugger;
        const baseModel = response.data;
        if (response.status == 200) {
          serviceFetchingSucess(dispatch, response.data);
        }
      })
      .catch(error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        serviceFetchingFail(dispatch, err);
        console.log('error', error);
      });
  };
};

const serviceFetchingSucess = (dispatch, productData) => {
  debugger;
  dispatch({
    type: HANDY_SERVICES_FETECHED_SUCCESS,
    payload: productData,
  });
  Actions.splash2();
};

const serviceFetchingFail = (dispatch, error) => {
  debugger;
  dispatch({
    type: HANDY_SERVICES_FETECHED_FAILED,
    payload: error,
  });

  Alert.alert(
    'Alert',
    error,
    [{text: 'OK', onPress: () => console.log('error', error)}],
    {cancelable: false},
  );
};

const serviceAddSuccess = (dispatch, data) => {
  debugger;
  dispatch({
    type: HANDY_SERVICES_REQUEST_SUCCESS,
  });
  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: {
      prop: 'unReadNotificationCount',
      value: data.unReadnotificationCount,
    },
  });
  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: {
      prop: 'totalpendingOrdercount',
      value: data.totalpendingOrdercount,
    },
  });
  Alert.alert(
    'Request Submitted',
    'Dear valued user, Your request is submitted. Our team will call you for confirmation within 5 minutes.',
    [
      {
        text: 'OK',
        onPress: () => Actions.reset('SelectJob'),
      },
    ],
    {cancelable: false},
  );
};

const serviceAddFail = (dispatch, error) => {
  debugger;
  dispatch({
    type: HANDY_SERVICES_REQUEST_FAILED,
    payload: error,
  });
  Alert.alert('Error', 'Something went wrong ' + error);
};
