import firebase from 'firebase';
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESSFUL,
  LOGIN_USER,
  LOGIN_USER_FAIL,
  LANDING_DATAFECTHED,
  LOGOUT_USER,
  LOGOUT_USER_FAIL,
  LOGOUT_USER_SUCCESSFUL,
  NOTIFICATION_DETAIL_UPDATE,
  PRODUCT_FETCHING,
  PRODUCT_FETECHED_FAILED,
  USER_DETAIL_UPDATE,
  PRODUCT_FETECHED_SUCCESS,
  HANDY_SERVICES_FETECHED_SUCCESS,
  UPDATE_USER_ADDRESS_DETAIL,
} from './types';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import {config} from '../config';
import {Toast} from 'native-base';

export const UpdateUserFormData = ({prop, value}) => {
  debugger;
  return {
    type: USER_DETAIL_UPDATE,
    payload: {prop, value},
  };
};

export const UpdateUserAddressDatail = ({prop, value}) => {
  debugger;
  return {
    type: UPDATE_USER_ADDRESS_DETAIL,
    payload: {prop, value},
  };
};

export const emailChanged = text => {
  return {
    type: EMAIL_CHANGED,
    payload: text,
  };
};

export const passwordChanged = text => {
  return {
    type: PASSWORD_CHANGED,
    payload: text,
  };
};

export const logOut = () => {
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };

  return dispatch => {
    dispatch({
      type: LOGOUT_USER,
    });
    const {REST_API} = config;
    axios
      .get(REST_API.Account.SignOut, apiconfig)
      .then(response => {
        debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          logOutUserSucess(dispatch);
        } else {
          Alert.alert(
            'error',
            baseModel.message ? baseModel.message : 'Unknown error',
          );
        }
      })
      .catch(error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        Alert.alert('error', err);
        console.log('error', error);
      });
  };
};


export const loginGuest = ({email, password, phoneNumbers,DeviceTocken,DeviceID}) => {
   debugger;
  let me = this;
  const SigninModel = {UserName: email, Password: password,PhoneNumbers:phoneNumbers,DeviceTocken:DeviceTocken,DeviceID:DeviceID};
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };

  return dispatch => {
    dispatch({
      type: LOGIN_USER,
    });

    const {REST_API} = config;
    axios
      .post(REST_API.Account.LoginGuest, SigninModel, apiconfig)
      .then(response => {
         debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          loginUserSucess(dispatch, baseModel.data, {UserName: email, Password: password});
        } else {
          loginUserFail(
            dispatch,
            baseModel.message ? baseModel.message : 'no error caught',
            false,
          );
        }
      })
      .catch(error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        loginUserFail(dispatch, err, false);
        console.log('error', error);
      });
  };
};



export const loginUser = ({email, password, fromSplashscreen,token,deviceId}) => {
  debugger;
  let me = this;
  const SigninModel = {UserName: email, Password: password,DeviceTocken:token,DeviceID:deviceId};
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };

  return dispatch => {
    dispatch({
      type: LOGIN_USER,
    });

    const {REST_API} = config;
    axios
      .post(REST_API.Account.Login, SigninModel, apiconfig)
      .then(response => {
        // debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          loginUserSucess(dispatch, baseModel.data, SigninModel);
        } else {
          loginUserFail(
            dispatch,
            baseModel.message ? baseModel.message : 'no error caught',
            fromSplashscreen,
          );
        }
      })
      .catch(error => {
        debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        loginUserFail(dispatch, err, fromSplashscreen);
        console.log('error', error);
      });
  };
};

storeData = async (key, value) => {
  // debugger;
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    // saving error
  }
};

removeItemValue = async key => {
  try {
    await AsyncStorage.removeItem(key);
    return true;
  } catch (exception) {
    return false;
  }
};

getdeviceId = async (dispatch) => {
  //Getting the Unique Id from here
  var id = DeviceInfo.getUniqueId();
  //this.setState({ deviceId: id, });

  dispatch({
    type: USER_DETAIL_UPDATE,
    payload: {prop: 'DeviceID', value: id},
  });
};




const logOutUserSucess = dispatch => {
  // debugger;
  dispatch({
    type: LOGOUT_USER_SUCCESSFUL,
  });
  this.removeItemValue('userData');
  Actions.reset('splash2');
};

const loginUserSucess = (dispatch, data, SigninModel) => {
  // debugger;

  var productsDataModel = {
    products: data.products,
    productsCat: data.productsCat,
  };
  var serviceDataModel = {
    services: data.services,
    servicestype: data.servicestype,
  };
  dispatch({
    type: LOGIN_USER_SUCCESSFUL,
    payload: data.user,
  });
  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: {
      prop: 'unReadNotificationCount',
      value: data.unReadnotificationCount,
    },
  });
  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: {
      prop: 'totalpendingOrdercount',
      value: data.totalpendingOrdercount,
    },
  });
  dispatch({
    type: USER_DETAIL_UPDATE,
    payload: {prop: 'grocServiceChrges', value: data.grocservicecharges},
  });
  dispatch({
    type: USER_DETAIL_UPDATE,
    payload: {prop: 'handyServiceCharges', value: data.handyservicecharges},
  });
  dispatch({
    type: USER_DETAIL_UPDATE,
    payload: {prop: 'adverstisementsList', value: data.advertisementsList},
  });

  dispatch({
    type: PRODUCT_FETECHED_SUCCESS,
  });
  dispatch({
    type: LANDING_DATAFECTHED,
    payload: productsDataModel,
  });

  dispatch({
    type: HANDY_SERVICES_FETECHED_SUCCESS,
    payload: serviceDataModel,
  });
  this.getdeviceId(dispatch);
  this.storeData('userData', JSON.stringify(SigninModel));
  Actions.reset('userProfile');
};

const loginUserFail = (dispatch, error, fromSplashscreen) => {
  debugger;

  dispatch({
    type: LOGIN_USER_FAIL,
    payload: error,
  });
  dispatch({
    type: PRODUCT_FETECHED_FAILED,
    payload: error ? error : 'some error occured',
  });
  this.removeItemValue('userData');
  Alert.alert('Alert!', error);
  if (fromSplashscreen) {
    Actions.splash2();
  }
};
