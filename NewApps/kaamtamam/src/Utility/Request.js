import axios from 'axios';
//import { storageService } from '.';
import { config } from '../config';
//import { WebStorageNames } from '../utils/Constants';
import { isEmpty } from 'lodash';

const getToken = () => {
  const token = null;//storageService.getItem(WebStorageNames.Token);
  return token ? `Bearer ${token}` : null;
};

const client = axios.create({
  responseType: 'json',
  baseURL: config.BASE_URL,
  transformResponse: response => response
});

client.defaults.headers.post['Content-Type'] = 'application/json';
client.defaults.headers.post.Accept = 'application/json';
client.defaults.headers.pragma = 'no-cache';
client.defaults.withCredentials = true;
client.defaults.timeout = 60000;

const Request = options => {
  var token = getToken();

  if (token) {
    client.defaults.headers.common.Authorization = token;
  } else {
    delete client.defaults.headers.common.Authorization;
  }

  const onSuccess = response => {
    if (typeof response.data === 'string') return JSON.parse(response.data);
    else return response.data;
  };

  const onFailure = error => {
    const { response, status, message, headers, data } = error;
    const UN_AUTHORIZED = 401;
    const FORBIDDEN = 403;
    if (
      (response &&
        (response.status === UN_AUTHORIZED || response.status === FORBIDDEN)) ||
      status === UN_AUTHORIZED ||
      status === FORBIDDEN
    ) {
      console.error('status:', status);
      console.error('data:', data);
      console.error('headers:', headers);
      // will call logout function here
     // window.location = '/login';
    } else {
      console.error('error message', message);
      if (isEmpty(response)) {
        var errorResponse = { message, success: false };
        return Promise.reject(errorResponse);
      }
    }

    return Promise.reject(response.data || message);
  };

  return client(options)
    .then(onSuccess)
    .catch(onFailure);
};

export const Axiosget = (url,) =>
{
    axios.get(REST_API.Test.GetProducts, configs)
    .then(response => {
        debugger;
        const baseModel = response.data;
        //if (baseModel.success) {
        if (response.status == 200) {
            productFetchingSucess(dispatch, response.data);
         }
    }).catch(error => {
        debugger;
        productFetchingFail(dispatch, error);
        console.log("error", error)
    });
}

export default Request;
