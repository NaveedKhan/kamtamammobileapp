import axios from 'axios';
import {config,ROOT_URL} from '../config';
//import store from '../utils/ConfigureStore';
//import ErrorWrapper from '../components/common/ErrorWrapper';
//import { ActionTypes, Messages } from '../utils';

const baseURL = ROOT_URL;

class BaseApi {
  static setDefaults(accessToken) {
    const d = axios.defaults;
    d.baseURL = baseURL;
    // if (accessToken) {
    //   const AUTH_TOKEN = 'Bearer '.concat(accessToken);
    //   d.headers.common.Authorization = AUTH_TOKEN;
    // } else {
    //   delete d.headers.common.Authorization;
    // }
    d.headers.post['Content-Type'] = 'application/json';
    d.headers.post.Accept = 'application/json';
    d.withCredentials = true;
    d.timeout = 60000; // minute in ms
  }

  static transformResponse() {
    return {
      transformResponse: [
        function (data) {
          return data ? JSON.parse(data) : {};
        },
      ],
    };
  }

 

  static handleResponse(callback, response) {
    //store.dispatch({ type: ActionTypes.SET_LOADING_STATE, payload: false });
    if (typeof callback === 'function') {
      callback(response);
    }
  }

  static loadingEnable() {
    const me = this;
    //store.dispatch({ type: ActionTypes.SET_ISREQUEST_STATE, payload: true });
    me.loadingTimer = setInterval(() => {
     // store.dispatch({ type: ActionTypes.SET_LOADING_STATE, payload: true });
     // store.dispatch({ type: ActionTypes.SET_ISREQUEST_STATE, payload: false });
      clearInterval(this.loadingTimer);
    }, 300);
  }

  static async get(api, params, callback) {
    const me = this;
    //store.dispatch({ type: ActionTypes.SET_LOADING_STATE, payload: true });
debugger;
    // code comment for hot fixes
    // this.loadingEnable();
    await axios
      .get(api, params, this.transformResponse())
      .then((response) => {
        debugger;
        //store.dispatch({ type: ActionTypes.SET_ISREQUEST_STATE, payload: false });
        // clearInterval(this.loadingTimer);
        const result = response.data;
        if (response.status !== 200) {
         // me.handleException(response);
        } else {
          me.handleResponse(callback, result);
        }
      })
      .catch((e) => {
        debugger;
       // store.dispatch({ type: ActionTypes.SET_ISREQUEST_STATE, payload: false });
        // clearInterval(this.loadingTimer);
      //  me.handleException(e);
      });
  }

  static async post(api, params, callback) {
    const me = this;
  //  store.dispatch({ type: ActionTypes.SET_LOADING_STATE, payload: true });
    await axios
      .post(api, params, this.transformResponse())
      .then((response) => {
        const result = response.data;
        if (response.status !== 200) {
        //  me.handleException(response);
        } else {
          me.handleResponse(callback, result);
        }
      })
      .catch((e) => {
     //   me.handleException(e);
      });
  }

  static async put(api, params, callback) {
    const me = this;
   // store.dispatch({ type: ActionTypes.SET_LOADING_STATE, payload: true });
    await axios
      .put(api, params, this.transformResponse())
      .then((response) => {
        const result = response.data;
        if (response.status !== 200) {
        //  me.handleException(response);
        } else {
          me.handleResponse(callback, result);
        }
      })
      .catch((e) => {
        //me.handleException(e);
      });
  }

  static async delete(api, params, callback) {
    const me = this;
   // store.dispatch({ type: ActionTypes.SET_LOADING_STATE, payload: true });
    await axios
      .delete(api, { data: params }, this.transformResponse())
      .then((response) => {
        const result = response.data;
        if (response.status !== 200) {
        //  me.handleException(response);
        } else {
          me.handleResponse(callback, result);
        }
      })
      .catch((e) => {
       // me.handleException(e);
      });
  }
}

export default BaseApi;
