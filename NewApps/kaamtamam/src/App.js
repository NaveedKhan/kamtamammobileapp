import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { Root } from "native-base";
import { createStore, applyMiddleware } from 'redux';
import Reducer from './reducers';
import ReduxThunk from 'redux-thunk';
import firebase from 'firebase';
import LoginForm from './components/LoginForm';
import Router from './Router';

class App extends Component {
  componentDidMount() {
    // Initialize Firebase
  configureFirebase();
  }
  render() {
    const storeInit = createStore(Reducer, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={storeInit}>
        <Root>
          <View style={{ flex: 1, backgroundColor: 'rgb(61,91,151)' }}>
            <Router />
          </View>
        </Root>
      </Provider>
    );
  }
}

export default App;




function configureFirebase() {

  var firebaseConfig = {
    apiKey: "AIzaSyBNDN-uUcHxm5SxqcfcISulGEWgdfOnsP0",
    authDomain: "kaamtamam-c8346.firebaseapp.com",
    databaseURL: "https://kaamtamam-c8346.firebaseio.com",
    projectId: "kaamtamam-c8346",
    storageBucket: "kaamtamam-c8346.appspot.com",
    messagingSenderId: "714740281376",
    appId: "1:714740281376:web:0558552972f7e431ba4c45",
    measurementId: "G-47MCWP8EJ1"
  };
  // Initialize Firebase
  !firebase.apps.length ? firebase.initializeApp(firebaseConfig): firebase.app();;
  ///firebase.analytics();

}
