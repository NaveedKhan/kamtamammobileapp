import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Spinner } from 'native-base';
import { CommonHeader, SubHeader, CustomHeader, CustomHeaderWithText } from '../Common/Header';
import axios from 'axios';
import { connect } from 'react-redux';
import { UpdateNotificationData } from '../../actions';
import { config } from '../../config';


class NotificationDetail extends Component {
  constructor(props) {
    super(props);
    //debugger;
    this.state = {
      notification: this.props.notification,
      isLoading: false
    }
  }

  componentDidMount() {
    //MarkNoificationRead
    debugger;
    const { notification } = this.state;
    var users = this.props.user;
    const notModel = { NotificationID: notification.notificationID };
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    };
    const { REST_API } = config;
    this.setState({ isLoading: true });
    var me = this;
    axios.post(REST_API.Notifications.MarkNoificationRead, notModel, configs)
      .then(response => {
        debugger;
        var baseModel = response.data;
        if (baseModel.success) {
          var notList = baseModel.unReadnotificationCount;
          me.props.UpdateNotificationData({ prop: 'unReadNotificationCount', value: notList });
          me.setState({ isLoading: false })

        } else {
          me.setState({ isLoading: true });
        }
        // console.log('api call' + response);
      }).catch(error => {
        debugger;
        me.setState({ isLoading: false });
        console.log("error", error)
      });
  }


  render() {
    const { detail, messageForDrvierTech,
      isRead,
      notificationID,
      notoficationToDriverId,
      notoficationToUserId,
      orderID, serviceID } = this.props.notification;

    let notificationheading = (orderID == 0 && serviceID == 0) ? `Notication` :
      (orderID != 0 && serviceID == 0) ? `Notification about Grocery Order No ${orderID}` : `Notification about Handoman Service No ${serviceID}`
    return (
      <Container>
        <CustomHeaderWithText leftIcon={true} text="Notification Detail" />
        <Content>
          {
            this.state.isLoading ? <Spinner /> :
              <Card>
                <CardItem header>
                  <Text>{notificationheading}</Text>
                </CardItem>
                <CardItem>
                  <Body>
                    <Text note>
                      {messageForDrvierTech? messageForDrvierTech:'no info available'}
                    </Text>
                  </Body>
                </CardItem>
                <CardItem footer>
                  {/* <Text>{isRead ? "Read" : "un read"}</Text> */}
                </CardItem>
              </Card>
          }
        </Content>
      </Container>
    );
  }
}


const mapStateToProps = ({ auth }) => {

  return { user: auth.user };
};
export default connect(mapStateToProps, { UpdateNotificationData })(NotificationDetail);