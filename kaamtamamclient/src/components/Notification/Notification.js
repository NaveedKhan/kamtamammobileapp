import React, { Component } from 'react';
import { Dimensions, ScrollView, View } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Spinner } from 'native-base';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import { connect } from 'react-redux';
import { config } from '../../config';
import { CommonHeader, SubHeader, CustomHeader, CustomHeaderWithText } from '../Common/Header';

class Notification extends Component {
    constructor(props) {
        super(props);
        //debugger;
        this.state = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            isLoading: false,
            notificationsArray: []
        }
    }

    componentDidMount() {
        //debugger;
        var users = this.props.user;
        let configs = {
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        };
        const { REST_API } = config;
        this.setState({ isLoading: true });
        var me = this;
        axios.get(REST_API.Notifications.GetNotByDriverID + `/${users.id}`, configs)
            .then(response => {
                //debugger;
                var baseModel = response.data;
                if (baseModel.success) {
                    var notList = baseModel.notifications;
                    me.setState({ isLoading: false, notificationsArray: notList })

                } else {
                    me.setState({ isLoading: true });
                }
                // console.log('api call' + response);
            }).catch(error => {
                //debugger;
                me.setState({ isLoading: true });
                console.log("error", error)
            });
    }

    onPressDetail(not,isGroc)
    {
       let {notificationsArray} = this.state;
      // var selectedNot = notificationsArray.find(aa=>aa.notificationID == not.notificationID);
      // if(selectedNot)
      // {
      //   selectedNot.isRead = true;
      // }
       not.isRead = true;
       this.setState({notificationsArray:notificationsArray});
     
      Actions.notificationDetail({notification: not,isGroc:isGroc})
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <CustomHeaderWithText leftIcon={true} text="Notifications" hideMenu={true} />
                <ScrollView scrollEnabled >
                    <View style={{ flex: 1,backgroundColor:'transparent' }}>
                        <List >
                            {this.state.isLoading ? <Spinner /> :
                                this.state.notificationsArray.length < 1 ? <Text style={{ textAlign: 'center', marginTop: 15 }}>There is no notification for you yet!</Text>
                                    :
                                    this.state.notificationsArray.map(not => {
                                       const  {orderID, serviceID } = not;
debugger;
                                        let notificationheading = (orderID == 0 && serviceID == 0) ? `Notication` :
                                          (orderID != 0 && serviceID == 0) ? ` Grocery Order No ${orderID}` : ` Handoman Service No ${serviceID}`
                                       return <ListItem key={not.notificationID}  style={{backgroundColor:'transparent',marginLeft:2}} onPress={() =>this.onPressDetail(not)}>

                                            <Body style={{backgroundColor:'transparent'}}>
                                                <Text style={{backgroundColor:'transparent',marginLeft:5}}> {notificationheading}</Text>
                                                <Text note style={{backgroundColor:'transparent',fontSize:12}} numberOfLines={2}>{not.messageForDrvierTech}</Text>
                                            </Body>
                                            <Right>
                                                <Button transparent onPress={() => this.onPressDetail(not)}>
                                                    <Text style={{color:'blue'}}>View</Text>
                                                </Button>
                                            </Right>
                                        </ListItem>
                                    })
                            }

                        </List>
                    </View>
                </ScrollView>
            </View>
        );
    }
}



const mapStateToProps = ({ auth }) => {
    //debugger;
    return { user: auth.user };
};
export default connect(mapStateToProps, null)(Notification);