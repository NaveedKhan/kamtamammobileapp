import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
  Linking,
  Modal,
  Alert,
  Platform,
  TextInput,
  StatusBar,
} from 'react-native';
import {
  Item,
  Input,
  Icon,
  Spinner,
  List,
  ListItem,
  Left,
  CheckBox,
  Right,
  Thumbnail,
  Body,
  Button,
  avatar,
} from 'native-base';
import {Rating, AirbnbRating} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import axios from 'axios';
import moment from 'moment';
import {config} from '../../config';
import {ServiceRequestUpdateForm, addServiceRequest} from '../../actions';
import {CustomHeaderWithText} from '../Common/Header';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class ServiceDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      swipedAllCards: false,
      swipeDirection: '',
      cardIndex: 0,
      modalVisible: false,
      estimationvalue: '',
      isLoading: false,
      selectedImageURL: '',
    };
  }

  renderRatingModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({modalVisible: false, modalButtonLoading: false});
        }}>
        <View style={styles.datecontainer}>
          {/* <View style={{ margin: 10 }} /> */}
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              borderRadius: 15,
              justifyContent: 'flex-end',
              alignItems: 'center',
              alignSelf: 'flex-end',
              margin: 10,
              borderColor: '#fff',
              borderWidth: 1,
            }}
            onPress={() => this.setState({modalVisible: false})}>
            <Icon name="close" style={{color: '#fff'}} />
          </TouchableOpacity>

          <View style={{
                  height: this.state.width/1.2,
                  width: this.state.width/1.2,
                  borderRadius: 10,
                  overflow: 'hidden',
                  //marginRight: 10,
                 // borderColor: 'black',
                 // borderWidth: 1,
                  alignSelf:'center',
                 
                }}>
            {this.state.selectedImageURL == '' ? (
              <View />
            ) : (
              <Image
                
                source={this.state.selectedImageURL}
                style={{
                  height: this.state.width/1.3,
                  width: this.state.width/1.3,
                  borderRadius: 10,
                  overflow: 'hidden',
                  margin: 10,
                  borderColor: 'black',
                  borderWidth: 1,
                }}
              />
            )}
          </View>
        </View>
      </Modal>
    );
  }

  swapButtonAndSpinner() {
    return this.props.loading ? (
      <Spinner />
    ) : (
      <TouchableOpacity style={styles.button} onPress={() => this.onConfirm()}>
        <Text style={styles.buttonText}>Confirm</Text>
      </TouchableOpacity>
    );
  }

  callTechnician(phone) {
    //let phone = '033323233232';
    console.log('callNumber ----> ', '033323233232');
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  }

  onConfirm() {
    //debugger;
    // const {
    //   serviceRequestModel, error, loading, RequestServiceType, RequestTime,
    //   RequestServicesList, RequestImages, RequestDescription, RequestAddress,
    //   maintainanceSelected,
    //   installationSelected, isImmediateRequest, user, selectedServies,
    //   serviceRequestScheduleDate
    // } = this.props;
    // const { userCurrentAddressDetail, userDeliveryAddressDetail, isCurrentLocatonSelected } = this.props;
    // const { userCurrentLat, userCurrentLong, userCurrentAddress } = userCurrentAddressDetail;
    // const { requsetLat, requetLong, requestSearchAddress } = userDeliveryAddressDetail;
    // var RequestModel = {
    //   ImagesData: RequestImages,
    //   ServiceRequestScheduleDate: !isImmediateRequest ? serviceRequestScheduleDate : new Date().toLocaleString(),
    //   IsServiceSchedule: !isImmediateRequest,
    //   IsMaintainance: maintainanceSelected,
    //   IsInstallation: installationSelected,
    //   ServiceRequestDetail: RequestDescription,
    //   ServiceList: selectedServies,
    //   RequesteeAddress: isCurrentLocatonSelected ? userCurrentLat : userCurrentAddress,
    //   ServiceTypeID: RequestServiceType,
    //   CurrentLocationLat: userCurrentLat,
    //   CurrentLocationLong: userCurrentLong,
    //   CurrentLocationAddress: userCurrentAddress,
    //   RequestAddressLat: isCurrentLocatonSelected ? userCurrentLat : requsetLat,
    //   RequestAddressLong: isCurrentLocatonSelected ? userCurrentLong : requetLong,
    //   RequestLocationAddress: isCurrentLocatonSelected ? userCurrentAddress : requestSearchAddress,
    //   RequestById: user.id,
    //   RequesteeName: user.fullName,
    //   RequesteeContactNo: user.phoneNumber
    // }
    // this.props.addServiceRequest(RequestModel);
  }

  estimateOrder() {
    //debugger;
    var users = this.props.user;
    const {selectedService} = this.props;
    const {estimationvalue} = this.state;
    var viewmodel = {
      ServiceReEstimatedTotalCharges: estimationvalue,
      ServiceRequestID: selectedService.serviceRequestID,
    };
    if (estimationvalue < 1 || estimationvalue == '') {
      Alert.alert('Alert', 'Invalid estimation value added');
      return;
    }
    let configs = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    };
    const {REST_API} = config;
    this.setState({isLoading: true});
    var me = this;
    axios
      .post(REST_API.ServiceRequest.ReEstimatedOrder, viewmodel, configs)
      .then(response => {
        //debugger;
        var baseModel = response.data;
        if (baseModel.success) {
          // var notList = baseModel.notifications;
          debugger;
          selectedService.serviceReEstimatedTotalCharges = estimationvalue;
          me.setState({isLoading: false});
        } else {
          me.setState({isLoading: false});
        }
        // console.log('api call' + response);
      })
      .catch(error => {
        //debugger;
        me.setState({isLoading: false});
        console.log('error', error);
      });
  }

  render() {
    // const {
    //   serviceRequestModel, error, loading, RequestServiceType, RequestTime,
    //   RequestServicesList, RequestImages, RequestDescription, RequestAddress,
    //   maintainanceSelected,
    //   installationSelected, isImmediateRequest, user, selectedServies,
    debugger;
    // } = this.props;

    // const { userCurrentAddressDetail, userDeliveryAddressDetail, isCurrentLocatonSelected } = this.props;
    // const { userCurrentLat, userCurrentLong, userCurrentAddress } = userCurrentAddressDetail;
    // const { requsetLat, requetLong, requestSearchAddress } = userDeliveryAddressDetail;

    const {selectedService} = this.props;
    const {
      requestAddressLat,
      serviceReEstimatedTotalCharges,
      requestAddressLong,
      requesteeName,
      requesteeContactNo,
      serviceTotalCharges,
    } = selectedService;
    //debugger;
    let orderItemsTotalChanregs = selectedService.serviceList.sum(
      'serviceTotalCharges',
    );
    let serviceCharges = serviceTotalCharges - orderItemsTotalChanregs;

    let assImage_Http_URL = {
      uri: 'http://115.186.182.33:8088/404.png'.replace(
        'http://localhost',
        'http://10.0.2.2',
      ),
    };
    var imagess = require('../Asset/Categoris/HomeCare.png');
    var jobType =
      selectedService.isMaintainance && selectedService.isInstallation
        ? 'Maintanance & Insatallation'
        : selectedService.isMaintainance && !selectedService.isInstallation
        ? 'Maintanance '
        : ' Insatallation';

    return (
      <View style={{flex: 1}}>
        <CustomHeaderWithText leftIcon={true} text="Service Request Detail" />
        <StatusBar hidden />
        <ScrollView>
          <View style={styles.thumbnailDiv}>
            <View style={styles.rowSelectType}>
              <Text style={styles.rowHeadingList}>Customer Detail </Text>
            </View>
            <Image
              source={assImage_Http_URL}
              style={{
                height: 90,
                width: 90,
                borderRadius: 45,
                marginTop: 10,
                overflow: 'hidden',
                borderColor: 'black',
                borderWidth: 1,
              }}
            />
            <Text style={{fontSize: 13, color: 'green', marginTop: 3}}>
              {requesteeName ? requesteeName : 'Unknown User'}
            </Text>
            <TouchableOpacity
              onPress={() =>
                this.callTechnician(
                  requesteeContactNo ? requesteeContactNo : '090078601',
                )
              }
              style={{
                borderColor: 'green',
                borderWidth: 1,
                width: 120,
                height: 30,
                margin: 5,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 15,
              }}>
              <Text style={{fontSize: 13, color: 'green'}}>
                {'Call Customer'}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.MainDetailwrapper}>
            <View style={styles.rowSelectType}>
              <Icon
                name="construct"
                color="#E9EBEE"
                style={{color: '#f48004'}}
              />
              <Text style={styles.rowHeadingList}>
                Request ID {selectedService.serviceRequestID}
              </Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Reqeust For</Text>
              <Text style={styles.jobDetailReight}>
                {selectedService.serviceTypeName}
              </Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Reqeust Type</Text>
              <Text style={styles.jobDetailReight}>{jobType}</Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Reqeust Discription</Text>
              <Text style={styles.jobDetailReight}>
                {selectedService.serviceRequestDetail == ''
                  ? 'No Description Provided'
                  : selectedService.serviceRequestDetail}
              </Text>
            </View>
          </View>

          <View style={styles.MainDetailwrapper}>
            <View style={styles.rowSelectType}>
              <Icon
                name="md-checkmark-circle-outline"
                color="#E9EBEE"
                style={{color: '#f48004'}}
              />
              <Text style={styles.rowHeadingList}>
                Reqeust Status {selectedService.serviceStatusName}{' '}
              </Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Request Urgency </Text>
              <Text style={styles.jobDetailReight}>
                {!selectedService.isServiceSchedule
                  ? 'Immediate Request'
                  : 'Schedule Request on' +
                  moment(selectedService.serviceRequestScheduleDate).format('MMMM Do YYYY, h:mm:ss a')}
              </Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Request Time</Text>
              <Text style={styles.jobDetailReight}>
                { moment(selectedService.serviceRequestDate).format('MMMM Do YYYY, h:mm:ss a')}
              </Text>
              <Text style={styles.jobDetailLeft}>Request Delivery Time</Text>
              <Text style={styles.jobDetailReight}>{'Not yet specified'}</Text>
            </View>
          </View>

          <TouchableOpacity
            style={styles.MainDetailwrapper}
            onPress={() =>
              Actions.traceOrder({
                Order: selectedService,
                requestAddressLat: parseFloat(requestAddressLat),
                requestAddressLong: parseFloat(requestAddressLong),
              })
            }>
            <View style={styles.rowSelectType}>
              <Icon name="pin" color="#E9EBEE" style={{color: '#f48004'}} />
              <Text style={styles.rowHeadingList}>
                Address Details (Click To trace order)
              </Text>
            </View>
            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>Request Delivery address</Text>
              <Text style={styles.jobDetailReight}>
                {selectedService.requestLocationAddress}
              </Text>
            </View>
          </TouchableOpacity>

          <View style={styles.MainDetailwrapper}>
            <View style={styles.rowSelectType}>
              <Icon
                name="md-images"
                color="#E9EBEE"
                style={{color: '#f48004'}}
              />
              <Text style={styles.rowHeadingList}>Request Problem Images</Text>
            </View>

            <View style={styles.rowList}>
              <View style={{flexDirection: 'row'}}>
                {selectedService.imagesSourceArray.map(el => {
                  //debugger;
                  let image = el.replace('http://localhost', 'http://10.0.2.2');
                  let Image_Http_URL = {uri: image};
                  return (
                    <TouchableOpacity  key={el}
                      onPress={() => this.setState({selectedImageURL:Image_Http_URL,modalVisible: true})}>
                      <Image
                       
                        source={Image_Http_URL}
                        style={{
                          height: 50,
                          width: 50,
                          borderRadius: 10,
                          overflow: 'hidden',
                          marginRight: 10,
                          borderColor: 'black',
                          borderWidth: 1,
                        }}
                      />
                    </TouchableOpacity>
                  );
                })}
                {selectedService.imagesSourceArray.length == 0 ? (
                  <Text>No Images Given</Text>
                ) : (
                  <View />
                )
                /* <Image source={imagess} style={{ height: 50, width: 50, borderRadius: 10, overflow: 'hidden', marginRight: 10, borderColor: 'black', borderWidth: 1 }} />
                <Image source={imagess} style={{ height: 50, width: 50, borderRadius: 10, overflow: 'hidden', marginRight: 10, borderColor: 'black', borderWidth: 1 }} />
                <Image source={imagess} style={{ height: 50, width: 50, borderRadius: 10, overflow: 'hidden', marginRight: 10, borderColor: 'black', borderWidth: 1 }} />
              */
                }
              </View>
            </View>
          </View>

          <View
            style={{
              flex: 1,
              backgroundColor: '#fff',
              margin: 20,
              borderColor: '#f48004',
              borderWidth: 3,
              borderRadius: 10,
              overflow: 'hidden',
            }}>
            <List>
              {selectedService.serviceList.map(item => {
                return (
                  <ListItem
                    key={item.serviceID}
                    style={{
                      backgroundColor: '#fff',
                      borderBottomWidth: 1,
                      borderBottomColor: '#f48004',
                      marginLeft: 0,
                    }}>
                    {/* <Left style={{ paddingLeft: 5, alignItems: 'center', justifyContent: 'center' }}>
                    <Thumbnail small source={imagess} />
                  </Left> */}
                    <Body
                      style={{
                        borderWidth: 0,
                        borderBottomWidth: 0,
                        marginLeft: 5,
                      }}>
                      <Text style={{fontWeight: 'bold', color: 'green'}}>
                        {item.handy_Services.serviceName}
                      </Text>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text
                          style={{fontSize: 12, margin: 5, marginLeft: 0}}
                          note
                          numberOfLines={1}>{` Rs ${
                          item.handy_Services.serviceCharges
                        } / ${item.handy_Services.groc_Units.unitName} `}</Text>
                        <Text
                          style={{fontSize: 12, margin: 5, marginRight: 20}}
                          note
                          numberOfLines={1}>{`Qty ${item.quantity}`}</Text>
                      </View>

                      {/* <Text>{item.handy_Services.serviceName}</Text>
                    <Text note numberOfLines={1}>{` $ ${item.handy_Services.serviceCharges}/item                Qty ${item.quantity}`}</Text> */}
                    </Body>
                    <Right style={{borderWidth: 0}}>
                      <Button transparent>
                        <Text>Rs. {item.serviceTotalCharges}</Text>
                      </Button>
                    </Right>
                  </ListItem>
                );
              })}
            </List>
            {/* <View style={{ backgroundColor: 'orange', height: 7, margin: 8, borderRadius: 8 }} /> */}

            <View
              style={{
                flexDirection: 'row',
                fontFamily: 'Helvica',
                marginTop: 10,
              }}>
              <View
                style={{
                  flex: 5,
                  backgroundColor: 'transparent',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}>
                {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 14,
                    fontFamily: 'Helvica',
                  }}>
                  Items Total Charges:{' '}
                </Text>
              </View>
              <View
                style={{
                  flex: 5,
                  backgroundColor: 'transparent',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text style={{fontSize: 12}}>
                  Rs. {orderItemsTotalChanregs}{' '}
                </Text>
              </View>
            </View>
            <Text style={{alignSelf: 'center', fontWeight: 'bold'}}>{`+`}</Text>
            <View
              style={{
                flexDirection: 'row',
                fontFamily: 'Helvica',
                marginBottom: 10,
              }}>
              <View
                style={{
                  flex: 5,
                  backgroundColor: 'transparent',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}>
                {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 14,
                    fontFamily: 'Helvica',
                  }}>
                  Service Charges:{' '}
                </Text>
              </View>
              <View
                style={{
                  flex: 5,
                  backgroundColor: 'transparent',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text style={{fontSize: 12}}>Rs. {serviceCharges} </Text>
              </View>
            </View>
          </View>

          <View style={styles.MainDetailwrapper}>
            <View style={styles.rowSelectType}>
              <Icon name="md-card" color="#E9EBEE" style={{color: '#f48004'}} />
              <Text style={styles.rowHeadingList}>Bill</Text>
            </View>

            <View style={styles.rowList}>
              <Text style={styles.jobDetailLeft}>
                Total bill (including service charges)
              </Text>
              <Text style={styles.jobDetailReight}>
                {selectedService.serviceTotalCharges} -/RS
              </Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#e2e2e2',
              margin: 10,
              borderRadius: 10,
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 3,
              alignSelf: 'center',
              borderColor: '#f48004',
            }}>
            {serviceReEstimatedTotalCharges == '0' ? (
              <View />
            ) : (
              <View
                style={{
                  height: this.state.height / 10,
                  backgroundColor: 'rgba(255,255,255,0.3)',
                  flexDirection: 'row',
                  fontFamily: 'Helvica',
                }}>
                <View
                  style={{
                    flex: 5,
                    backgroundColor: 'transparent',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                  }}>
                  {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 18,
                      fontFamily: 'Helvica',
                    }}>
                    Estimated Total :{' '}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 5,
                    backgroundColor: 'transparent',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{fontSize: 16}}>
                    Rs. {serviceReEstimatedTotalCharges}{' '}
                  </Text>
                </View>
              </View>
            )}

            <View
              style={{
                backgroundColor: '#e6e7e9',
                margin: 5,
                flexDirection: 'row',
                alignItems: 'center',
                borderColor: 'gray',
                borderBottomWidth: 0.5,
                borderRadius: 10,
              }}>
              {/* <Icon name='lock' style={{ color: '#f48004', fontSize: 22 ,marginLeft:5}} /> */}
              <TextInput
                onChangeText={text => this.setState({estimationvalue: text})}
                value={this.state.estimationvalue}
                style={{
                  width: width * 0.7,
                  borderWidth: 0.6,
                  borderRadius: 5,
                  justifyContent: 'flex-start',
                  alignSelf: 'stretch',
                  backgroundColor: 'transparent',
                }}
              />

              {this.state.isLoading ? (
                <Spinner />
              ) : (
                <Button
                  onPress={() => this.estimateOrder()}
                  style={{
                    borderRadius: 3,
                    margin: 5,
                    width: 70,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 12,
                      color: '#fff',
                      textAlign: 'center',
                      alignSelf: 'center',
                    }}>
                    Estimate
                  </Text>
                </Button>
              )}
            </View>
          </View>

          <View style={styles.buttonMain}>
            {/* {this.swapButtonAndSpinner()} */}
          </View>

          {/* {String(error) == '' ? <Text numberOfLines={4} style={{ margin: 10 }}>
          .
                                    </Text> :
            <View>
              <Text numberOfLines={4} style={{ margin: 10 }}>{String(error)}</Text>
              <Text numberOfLines={4} style={{ margin: 10 }}>{error.config.data}</Text>
            </View>
          } */}
        </ScrollView>
        {this.renderRatingModal()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 30,
  },
  datecontainer: {
    backgroundColor: 'green', //'#E9EBEE',
    borderRadius: 10,
    margin: 10,
    marginTop: 100,
  },
  jobDetailReight: {
    width: width * 0.7,
    color: 'green',
    marginTop: 5,
    marginLeft: 20,
  },
  thumbnailDiv: {
    //flex: 2.5,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    //marginTop: 10,
    margin: 10,
    borderWidth: 3,
    alignSelf: 'center',
    borderColor: '#f48004',
    backgroundColor: '#E9EBEE',
    width: width * 0.9,
    borderRadius: 10,
    //backgroundColor: 'green'
  },
  jobDetailLeft: {color: 'black', marginTop: 5, marginLeft: 20},
  MainDetailwrapper: {
    borderWidth: 3,
    borderColor: '#f48004',
    backgroundColor: '#E9EBEE',
    width: width * 0.9,
    margin: 10,
    borderRadius: 10,
    alignSelf: 'center',
    padding: 10,
  },
  rowSelectType: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
    marginTop: 10,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  StatusBorder: {
    borderWidth: 3,
    borderColor: 'orange',
    width: width * 0.9,
    height: height * 0.75,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#E9EBEE',
  },
  listItemText: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
    width: width * 0.5,
  },
  rowMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: width * 0.9,
    height: height * 0.1,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  rowIcon: {
    color: '#f48004',
    margin: 20,
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    marginRight: 5,
    marginLeft: 10,
    fontSize: 16,
    color: '#f48004',
    fontFamily: 'sens-serif',
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    backgroundColor: '#f48004',
    padding: 10,
    borderRadius: 20,
    width: width * 0.35,
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

const mapStateToProps = ({auth}) => {
  return {
    user: auth.user,
  };
};
export default connect(
  mapStateToProps,
  {ServiceRequestUpdateForm, addServiceRequest},
)(ServiceDetails);
//export default RequestDetail;

Array.prototype.sum = function(prop) {
  var total = 0;
  for (var i = 0, _len = this.length; i < _len; i++) {
    total += this[i][prop];
  }
  return total;
};
