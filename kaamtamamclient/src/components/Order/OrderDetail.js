import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions, TouchableHighlight,Alert, TextInput, Linking, Image, TouchableOpacity, PermissionsAndroid ,StatusBar} from 'react-native';
import { List, ListItem, Left, CheckBox, Right, Icon, Thumbnail, Body, Button, avatar ,Spinner} from 'native-base';
import axios from 'axios';
import { config } from '../../config';
import moment from 'moment';
//import CheckoutItemsList from './CheckoutItemsList'
import { connect } from 'react-redux';
//import { checkoutButton, assignOrder } from '../../actions';
import { confirmButtonClicked, addressChanged, assignOrder } from '../../actions';
import { CommonHeader, CustomHeaderWithText } from '../Common/Header';
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class OrderDetail extends Component {
    constructor(props) {
        super(props);
        //debugger;
        this.state = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            swipedAllCards: false,
            swipeDirection: '',
            cardIndex: 0,
            modalVisible: false,
            estimationvalue: '',
            isLoading:false
        }
    }
 
    onConfirmPressed() {
        //debugger;
        // const { user, orderInCartList } = this.props;
        // let id = user.user.id;
        // this.props.confirmButtonClicked(id, orderInCartList);


        const { Order, user, relatedOrders } = this.props;
        let id = user.id;
        this.props.assignOrder(Order.orderID, id, relatedOrders);

    }
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    estimateOrder() {
        //debugger;
        var users = this.props.user;
        const { Order } = this.props;
        const { estimationvalue } = this.state;

        if(estimationvalue < 1 || estimationvalue == '')
        {
           Alert.alert('Alert','Invalid estimation value added') ;  
           return;         
        }

        var viewmodel = { OrderReEstimatedTotalCharges: estimationvalue, OrderID: Order.orderID }

        let configs = {
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        };
        const { REST_API } = config;
        this.setState({ isLoading: true });
        var me = this;
        axios.post(REST_API.Oders.ReEstimatedOrder, viewmodel, configs)
            .then(response => {
                //debugger;
                var baseModel = response.data;
                if (baseModel.success) {
                    var notList = baseModel.notifications;
                    Order.orderReEstimatedTotalCharges = estimationvalue;
                    me.setState({ isLoading: false, notificationsArray: notList })

                } else {
                    me.setState({ isLoading: true });
                }
                // console.log('api call' + response);
            }).catch(error => {
                //debugger;
                me.setState({ isLoading: true });
                console.log("error", error)
            });

    }

    componentDidMount() {
        // let configs = {
        //     headers: {
        //         'Content-Type': 'application/json; charset=UTF-8'
        //     }
        // };
        // const { REST_API } = config;
        // const { UserCompany, orgid, UserData } = this.state;
        // axios.get(REST_API.Test.GetSimple, configs)
        //     .then(response => {
        //         //debugger;
        //         console.log('api call' + response);
        //     }).catch(error => {
        //         //debugger;
        //         console.log("error", error)
        //     });
      //  this.requestCameraPermission();
    }

    showmodal() {


    }

    callTechnician(phone) {
        //let phone = '033323233232';
        console.log('callNumber ----> ', '033323233232');
        let phoneNumber = phone;
        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${phone}`;
        }
        else {
            phoneNumber = `tel:${phone}`;
        }
        Linking.canOpenURL(phoneNumber)
            .then(supported => {
                if (!supported) {
                    Alert.alert('Phone number is not available');
                } else {
                    return Linking.openURL(phoneNumber);
                }
            })
            .catch(err => console.log(err));
    }






    _renderUserReviews(score) {
        return <View style={{
            // flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 20
        }}>
            <Rating
                // showRating
                ratingCount={5}
                readonly
                // reviews={["Terrible (1)", "Bad (2)", "Normal (3)", "GOOD (4)", "Outstanding (5)"]}
                startingValue={score}
                //onFinishRating={(rat) => this.ratingCompleted(rat)}
                size={10}
            />
        </View>
    }


    _renderbothReviews(userscore, driverscore) {
        return <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 10, marginBottom: 20,
            padding: 5
        }}>
            <Text>Your Review</Text>
            <Rating
                ratingCount={5}
                readonly
                startingValue={userscore}
                size={10}
            />
            <Text style={{ marginTop: 5 }}>Kaam Tamam Team Review</Text>
            <Rating
                ratingCount={5}
                readonly
                startingValue={driverscore}
                size={10}
            />

        </View>

    }

    _renderButtons(orderStatus, isexpire, el) {

        const { assigneeImageURL, assigneContactNo, assigneeName } = el;
        if (orderStatus == 'Delivered' || orderStatus.toLowerCase() == 'Canceled'.toLowerCase()) {
            let { reviews } = el;
            let { userReviewScore, driverReviewScore, isReviewedByUser, isReviewedByDriverTech } = reviews;
            var bothreivwesocre = parseInt(userReviewScore + driverReviewScore);
            let reviewscore = (isReviewedByUser && isReviewedByDriverTech);

            if (isReviewedByUser && !isReviewedByDriverTech) {
                return this._renderUserReviews(userReviewScore);
            }
            else if (!isReviewedByUser && isReviewedByDriverTech) {
                return this._renderUserReviews(driverReviewScore);
            }
            else if (isReviewedByUser && isReviewedByDriverTech) {
                return this._renderbothReviews(userReviewScore, driverReviewScore)

            }

        } else {
            if (orderStatus.toLowerCase() == 'initiated') {
                return <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10, marginBottom: 10 }}>

                    <TouchableOpacity onPress={() => this.ShowConfimrationMessage(el, 'Are You sure you want to take this order')} style={[styles.button, { marginLeft: 5, width: 150 }]}>
                        <Text style={styles.buttonText}> Take Order </Text>
                    </TouchableOpacity>

                </View>
            }
            else if (orderStatus.toLowerCase() == "Processing".toLowerCase()) {
                return <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, marginBottom: 10 }}>
                    <TouchableOpacity onPress={() => this.onChnageOrderStatusClicked(el, 3)} style={styles.button}>
                        <Text style={styles.buttonText}>Start Delivery Now</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.markCancel(el)} style={styles.button}>
                        <Text style={styles.buttonText}> Cancel Order</Text>
                    </TouchableOpacity>


                </View>
            }
            else if (orderStatus.toLowerCase() == "On The Way".toLowerCase()) {
                return <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, marginBottom: 10 }}>
                    <TouchableOpacity onPress={() => this.onChnageOrderStatusClicked(el, 7)} style={styles.button}>
                        <Text style={styles.buttonText}>Reached Destination</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.markCancel(el)} style={styles.button}>
                        <Text style={styles.buttonText}> Cancel Order</Text>
                    </TouchableOpacity>


                </View>
            }
            else {
                return <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, marginBottom: 10 }}>
                    <TouchableOpacity onPress={() => this.markComplete(el)} style={styles.button}>
                        <Text style={styles.buttonText}>Mark Complete</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.markCancel(el)} style={styles.button}>
                        <Text style={styles.buttonText}> Cancel Order</Text>
                    </TouchableOpacity>


                </View>
            }

        }
    }

    timeBetweenDates(toDate) {
        //debugger;
        var dateEntered = toDate;
        var now = new Date();
        var difference = now.getTime() - dateEntered.getTime();

        if (difference <= 0) {

            // Timer done
            //clearInterval(timer);

        } else {

            var seconds = Math.floor(difference / 1000);
            var minutes = Math.floor(seconds / 60);
            var hours = Math.floor(minutes / 60);
            var days = Math.floor(hours / 24);

            hours %= 24;
            minutes %= 60;
            seconds %= 60;

            if (hours > 0 || days > 0 || minutes > 5) {
                return true;
            } else {
                return false;
            }
            // this.setState({ countDownTime: days + 'D ' + hours + 'H ' + minutes + 'M ' + seconds + 'S' });
        }
    }

    ShowConfimrationMessage(el, message) {
        Alert.alert(
            'Confirmation!',
            message,
            [
                {
                    text: 'Yes Continue', onPress: () => {
                        //checkoutSucess(dispatch,baseModel.data)
                        //this.setState({ iscanceling: true, modalVisible: true, selectedOrderToComplete: el })
                        this.onTakeOrderPresss(el);
                    }
                },
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
            ],
            { cancelable: true }
        )
    }

    onTakeOrderPresss(el) {
        //debugger;

        const { user, relatedOrders } = this.props;
        let id = user.id;
        this.props.assignOrder(el.orderID, id, relatedOrders);

    }

    onChnageOrderStatusClicked(el, orderstatusid) {
        //debugger;
  
        const { Order, user, relatedOrders } = this.props;
        let id = user.id;
        this.props.changeOrderStatus(el.orderID, orderstatusid);

    }

    render() {
        debugger;
        const { Order } = this.props;
        const { requestAddressLat, requestAddressLong,orderScheduleDate,
             isScheduleOrder, orderReEstimatedTotalCharges, 
             requesteeName ,requesteeContactNo,orderTotalCharges,orderDetail} = Order;
        const { width, height } = this.state;
        let isexpire = this.timeBetweenDates(new Date(Order.orderDate));
        let assImage_Http_URL = { uri: 'http://115.186.182.33:8088/404.png'.replace('http://localhost', 'http://10.0.2.2') };
        let orderItemsTotalChanregs = Order.orderItems.sum('totalPrice');
        let serviceCharges =   orderTotalCharges - orderItemsTotalChanregs;
        // if (assigneeImageURL != null) {
        //     let imageass = assigneeImageURL.replace('http://localhost', 'http://10.0.2.2');
        //     assImage_Http_URL = { uri: imageass };
        // }


        return (
            <View style={{ backgroundColor: '#fff', flex: 1, marginTop: 0 }}>

                <View>
                    <CustomHeaderWithText leftIcon={true} text="Order Detail" />
                    <StatusBar hidden />
                </View>
                <ScrollView scrollEnabled >

                    <View style={styles.thumbnailDiv}>
                        <View style={styles.rowSelectType}>

                            <Text style={styles.rowHeadingList}>Customer Detail </Text>
                        </View>
                        <Image source={assImage_Http_URL} style={{ height: 90, width: 90, borderRadius: 45, marginTop: 10, overflow: 'hidden', borderColor: 'black', borderWidth: 1 }} />
                        <Text style={{ fontSize: 13, color: 'green', marginTop: 3 }}>{requesteeName}</Text>
                        <TouchableOpacity onPress={() => this.callTechnician(requesteeContactNo ? requesteeContactNo :'65554444')} style={{
                            borderColor: 'green', borderWidth: 1, width: 120, height: 30, margin: 5,
                            alignItems: 'center', justifyContent: 'center', borderRadius: 15
                        }}>
                            <Text style={{ fontSize: 13, color: 'green' }} >{requesteeContactNo ? requesteeContactNo :'No number'}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.container} >
                        <View style={{ flex: 2.8, backgroundColor: 'transparent', margin: 0, overflow: 'hidden', alignItems: 'center', flexDirection: 'row' }}>
                            <Icon name='check-circle' type="FontAwesome" style={{ color: 'orange', fontSize: 23, marginLeft: 20 }} />
                            <Text style={{ padding: 5, fontWeight: 'bold', fontSize: 19, marginLeft: 10 }}>Delivery Address</Text>
                        </View>
                        <View style={{ backgroundColor: 'transparent', flex: 6.7, flexDirection: 'row' }}>
                            <TouchableHighlight
                                onPress={() =>
                                    Actions.traceOrder({ Order: Order, requestAddressLat: parseFloat(requestAddressLat), requestAddressLong: parseFloat(requestAddressLong) })
                                }
                                style={{ flex: 1, backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' }} >
                                <View style={{ margin: 20 }}>
                                    <Text numberOfLines={5} style={{ fontFamily: 'sens-serif', fontSize: 15, backgroundColor: 'transparent' }}>

                                        {Order.requestLocationAddress}

                                    </Text>

                                </View>
                            </TouchableHighlight>
  
                        </View>
                        <View style={{ backgroundColor: 'orange', flex: 0.5, margin: 8, borderRadius: 8 }} />

                    </View>

                    <View style={{
                        backgroundColor: '#e6e7e9', margin: 20, marginTop: 10, borderRadius: 10, overflow: "hidden", borderWidth: 3,
                        borderColor: '#f48004'
                    }}>
                        <View style={{ flex: 1, backgroundColor: '#e6e7e9', margin: 0, overflow: 'hidden', alignItems: 'center', flexDirection: 'row' }}>
                            <Icon name='check-circle' type="FontAwesome" style={{ color: 'orange', fontSize: 23, marginLeft: 20 }} />
                            <Text style={{ padding: 5, fontWeight: 'bold', fontSize: 19, marginLeft: 10 }}>Order Urgency</Text>
                        </View>
                        <Text style={{ margin: 20 }}>{!isScheduleOrder ? "Urgent Order" : " Schedule Order " +  moment(orderScheduleDate).format('MMMM Do YYYY, h:mm:ss a') }</Text>
                    </View>

                    {orderDetail == '' || orderDetail == null ?
          <View/>:
         <View>
            < Text style={{margin:5,marginLeft:20,fontWeight:'bold',fontSize:15,color:'green', marginBottom:1}}>Extra Detail </Text>
       <View style={styles.DiscriptionBorder}>
            <TextInput
              placeholder="Enter Extra detail or items for your orders......."
              style={{borderColor: 'gray'}}
               editable={false}
              multiline={true}
              style={{color:'#000'}}
              value={orderDetail}
            />
          </View>
         </View>
          }


                    <View style={{
                        backgroundColor: '#e6e7e9', margin: 20, marginTop: 10, borderRadius: 10, overflow: "hidden", borderWidth: 3,
                        borderColor: '#f48004'
                    }}>


                   <View style={{  backgroundColor: '#e6e7e9', margin: 20, marginTop: 10, marginBottom: 0, borderRadius: 10, overflow: "hidden",
                      borderBottomLeftRadius:0,borderBottomRightRadius:0 }}>
                        <View style={{ flex: 1, backgroundColor: '#e6e7e9', margin: 0, overflow: 'hidden', alignItems: 'center', flexDirection: 'row' }}>
                            <Icon name='check-circle' type="FontAwesome" style={{ color: 'orange', fontSize: 23, marginLeft: 20 }} />
                            <Text style={{ padding: 5, fontWeight: 'bold', fontSize: 19, marginLeft: 10 }}>Order Summary</Text>
                        </View>
                    </View>


                    <View style={{ flex: 1, backgroundColor: '#e6e7e9' }}>
                        <List>

                            {Order.orderItems.map(item => {
                                let image = item.productImage.replace('http://localhost', 'http://10.0.2.2');
                                let Image_Http_URL = { uri: image };
                                return <ListItem key={item.productId} avatar style={{ backgroundColor: '#e6e7e9', borderBottomWidth: 1, borderBottomColor: "#f48004", marginLeft: 0 }}>
                                    <Left style={{ paddingLeft: 5, alignItems: 'center', justifyContent: 'center' }}>
                                        <Thumbnail small source={Image_Http_URL} />
                                    </Left>
                                    <Body style={{ borderWidth: 0, borderBottomWidth: 0 }} >
                                        <Text style={{ fontWeight: 'bold' }}>{item.productName}</Text>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={{ fontSize: 12, margin: 5, }} note numberOfLines={1}>{` Rs ${item.prodcutPriceEach} / ${item.unitName} `}</Text>
                                            <Text style={{ fontSize: 12, margin: 5, marginRight: 20 }} note numberOfLines={1}>{`Qty ${item.productCount}`}</Text>
                                        </View>
                                    </Body>
                                    <Right style={{ borderWidth: 0 }}>
                                        <Button transparent>
                                            <Text>Rs. {item.totalPrice}</Text>
                                        </Button>
                                    </Right>
                                </ListItem>

                            })}

                        </List>
                        <View style={{ backgroundColor: 'orange', height: 7, margin: 8, borderRadius: 8 }} />
                        
                        <View style={{ flexDirection: 'row', fontFamily: 'Helvica' }}>
                            <View style={{ flex: 5, backgroundColor: 'transparent', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}>
                                {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
                                <Text style={{ fontWeight: 'bold', fontSize: 14, fontFamily: 'Helvica' }}>Items Total Charges: </Text>

                            </View>
                            <View style={{ flex: 5, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }} >
                                <Text style={{ fontSize: 12 }}>Rs. {orderItemsTotalChanregs}   </Text>
                            </View>
                        </View>
                        <Text style={{alignSelf:'center',fontWeight:'bold'}}>{`+`}</Text>
                        <View style={{  flexDirection: 'row', fontFamily: 'Helvica',marginBottom:10 }}>
                            <View style={{ flex: 5, backgroundColor: 'transparent', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}>
                                {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
                                <Text style={{ fontWeight: 'bold', fontSize: 14, fontFamily: 'Helvica' }}>Service Charges: </Text>

                            </View>
                            <View style={{ flex: 5, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }} >
                                <Text style={{ fontSize: 12 }}>Rs. {serviceCharges}   </Text>
                            </View>
                        </View>
                        {/* <Text style={{alignSelf:'center',fontWeight:'bold'}}>{`Items Total Charges :       Rs.${orderItemsTotalChanregs}`}</Text>
                        <Text style={{alignSelf:'center',fontWeight:'bold'}}>{`+`}</Text>
                        <Text style={{alignSelf:'center',fontWeight:'bold'}}>{`Service Charges :     Rs. ${serviceCharges}`}</Text> */}
                    </View>


                    </View>

                    <View style={{
                        backgroundColor: '#e2e2e2', margin: 10, borderRadius: 10, marginLeft: 20, marginRight: 20, borderWidth: 3,
                        alignSelf: 'center',
                        borderColor: '#f48004'
                    }}>

                       <View style={{ height: this.state.height / 10, backgroundColor: 'rgba(255,255,255,0.3)', flexDirection: 'row', fontFamily: 'Helvica' }}>
                            <View style={{ flex: 5, backgroundColor: 'transparent', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}>
                                {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
                                <Text style={{ fontWeight: 'bold', fontSize: 18, fontFamily: 'Helvica' }}>Grand Total : </Text>

                            </View>
                            <View style={{ flex: 5, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }} >
                                <Text style={{ fontSize: 16 }}>Rs. {orderTotalCharges}   </Text>
                            </View>
                        </View>


                        {orderReEstimatedTotalCharges == "0" ? <View /> :

                            <View style={{ height: this.state.height / 10, backgroundColor: 'rgba(255,255,255,0.3)', flexDirection: 'row', fontFamily: 'Helvica' }}>
                                <View style={{ flex: 5, backgroundColor: 'transparent', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}>
                                    {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
                                    <Text style={{ fontWeight: 'bold', fontSize: 18, fontFamily: 'Helvica' }}>Estimated Total : </Text>

                                </View>
                                <View style={{ flex: 5, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }} >
                                    <Text style={{ fontSize: 16 }}>Rs. {orderReEstimatedTotalCharges}   </Text>
                                </View>
                            </View>
                        }



                        <View style={{ backgroundColor: '#e6e7e9', margin: 5, flexDirection: 'row', alignItems: 'center', borderColor: 'gray', borderBottomWidth: .5, borderRadius: 10 }}>
                            {/* <Icon name='lock' style={{ color: '#f48004', fontSize: 22 ,marginLeft:5}} /> */}
                            <TextInput
                                onChangeText={(text) => this.setState({ estimationvalue: text })}
                                value={this.state.estimationvalue}
                                style={{ width: width * 0.7, borderWidth: 0.6, borderRadius: 5, justifyContent: 'flex-start', alignSelf: 'stretch', backgroundColor: 'transparent' }} />
                           {this.state.isLoading ? <Spinner /> :
                            <Button
                                onPress={() => this.estimateOrder()}
                                style={{ borderRadius: 3, margin: 5, width: 70, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 12, color: '#fff', textAlign: 'center', alignSelf: 'center' }}>
                                    Estimate
                                </Text>
                            </Button>
                            }
                        </View>


                   
                    </View>


                    {/* {this._renderButtons(Order.orderStatus, isexpire, Order)} */}
                </ScrollView>



            </View>
        );
    }
}

const mapStateToProps = ({ checkout, cart, auth, orders }) => {
    const { relatedOrders } = orders;
    const { isloading, selectedCategory, selectedProduct, totalItems } = checkout;
    const { orderInCartList } = cart;
    //debugger;
    return { user: auth.user, isloading, selectedCategory, selectedProduct, totalItems, orderInCartList, relatedOrders };
};
export default connect(mapStateToProps, { confirmButtonClicked, addressChanged, assignOrder })(OrderDetail);

const styles = {
    container: {
        height: Dimensions.get('window').height / 3.3,
        backgroundColor: '#e6e7e9',
        margin: 20,
        marginBottom: 5,
        borderRadius: 9,
        overflow: 'hidden', borderWidth: 3,
        borderColor: '#f48004'
    },
    DiscriptionBorder: {
        borderWidth: 3,
        borderColor: '#f48004',
        backgroundColor: 'white',
        width: width * 0.9,
        height: height * 0.25,
        margin: 10,
        borderRadius: 10,
        alignSelf:'center'
      },
    button: {
        marginTop: 3, marginLeft: 5, marginBottom: 5, backgroundColor: 'transparent', flexDirection: 'row',
        padding: 7, borderRadius: 25, borderWidth: 1.5, borderColor: 'green', alignItems: 'center', justifyContent: 'center', alignSelf: 'center'
    },
    buttonText: {
        fontSize: 12,
        color: 'black',
        alignSelf: 'center',
        fontFamily: 'sens-serif',
    },
    thumbnailDiv: {
        //flex: 2.5,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        //marginTop: 10,
        margin: 10,
        borderWidth: 3,
        alignSelf: 'center',
        borderColor: '#f48004',
        backgroundColor: '#E9EBEE',
        width: width * 0.9,
        borderRadius: 10,
        //backgroundColor: 'green'
    },
    ItemListRowText: {
        fontSize: 22,
        color: '#f48004',
        fontWeight: 'bold',
        marginLeft: 5,
        marginRight: 5
    },
    card: {
        flex: 1,
        borderRadius: 4,
        borderWidth: 2,
        borderColor: '#E8E8E8',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    text: {
        textAlign: 'center',
        fontSize: 50,
        backgroundColor: 'transparent'
    },
    done: {
        textAlign: 'center',
        fontSize: 30,
        color: 'white',
        backgroundColor: 'transparent'
    }
};

Array.prototype.sum = function (prop) {
    var total = 0
    for (var i = 0, _len = this.length; i < _len; i++) {
        total += this[i][prop]
    }
    return total
}