import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
  Alert,StatusBar
} from 'react-native';
import {Item, Input, Icon, Spinner} from 'native-base';
import {CustomHeaderWithText} from '../Common/Header';
import MapView, {
  Marker,
  PROVIDER_GOOGLE,
  AnimatedRegion,
} from 'react-native-maps';
import { connect } from 'react-redux';
import Geolocation from 'react-native-geolocation-service';
import MapViewDirections from 'react-native-maps-directions';
import firebase from 'firebase';
//import { Spinner } from '../../common';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0722;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const greyColor = '#A0A1A5';

//H 9/4 H-9, Islamabad, Islamabad Capital Territory, Pakistan
//33.670743, 73.057848

//H 8/2 H-8, Islamabad, Islamabad Capital Territory, Pakistan
//33.682270, 73.049566
//34.0025363,71.5130443
//const origin = { latitude: 34.0025363, longitude: 71.5130443 };
//const destination = { latitude: 33.682270, longitude: 73.049566 };
const GOOGLE_MAPS_APIKEY = 'AIzaSyDrkNY7kg-7XWRo1Q-6s4zq94QemjGBwMk';

class TraceOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      latitude: 0,
      longitude: 0,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
      destination: {
        latitude: this.props.requestAddressLat,
        longitude: this.props.requestAddressLong,
      },
      simpleorigin: {latitude: 34.0025363, longitude: 71.5130443},
      marker: {latitude: 0, longitude: 0},
      duration: 0,
      distance: '',
      origin: new AnimatedRegion({
        latitude: 34.0025363,
        longitude: 71.5130443,
        latitudeDelta: 0,
        longitudeDelta: 0,
      }),
      waitfor: 0,
      firebaseupdateValue:0
      //requestAddressLat: parseInt(requestAddressLat), requestAddressLong: parseInt(requestAddressLong)
    };
    this.mapView = null;
    this.marker = null;
    debugger;
  }

  async getCurrentLocation() {
    //debugger;
    var me = this;
    //this.setState({isGettingLocation: true});

    await Geolocation.getCurrentPosition(
      position => {
        console.log(position);
        let lats = position.coords.latitude;
        let longs = position.coords.longitude;
      },
      error => {
        //debugger;
       // me.setState({isGettingLocation: false});
        console.log(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 15000},
    );
  }


  watchLocation() {
    var me = this;
    this.setState({isLoading: true});
    console.log('component did mount called ');
    this.watchID = Geolocation.watchPosition(
      position => {
        //debugger;
        const {latitude, longitude} = position.coords;
        const newCoordinate = {
          latitude,
          longitude,
        };
        console.log('postion got in wtch location ' + position.coords);
        me.setState({
          isLoading: false,
          simpleorigin: newCoordinate,
          waitfor: parseInt(me.state.waitfor + 1),
        });

        console.log(
          'wait for variable incrememnted and new value is',
          me.state.waitfor,
        );
        if (me.state.waitfor == 3) {
          //debugger;
          console.log(
            'wait for variable reached to limit 3 now adding coordinates to firebase ',
          );
          me.setState({waitfor: 0});
          me.addDataIntoFirebase(newCoordinate, me);
        }
      },
      error => {
        var err = String(error) == '' ? 'no error ' : String(error);
        Alert.alert('error on update location', err);
      },
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
        distanceFilter: 10,
      },
    );
  }


  componentDidMount() {
    //debugger;
    this.watchLocation();
  }

  addDataIntoFirebase(newCoordinate,me) {
    debugger;
    const {Order} = this.props;
    let firebaseupdateValue = me.state.firebaseupdateValue;
    const trackingid = Order.locationTrackingID;
    let lat = newCoordinate.latitude;
    let longs = newCoordinate.longitude;
    console.log('addDataIntoFirebase called and tracking id is'+ trackingid);
    //var me = this;
    var values = trackingid ? trackingid : '-M4uyXDmUGa4VFmJKuSJ';
    me.setState({isLoading: false});
    firebase
      .database()
      .ref('DriverLocation/' + values)
      .update({
        userLat: lat,
        userLong: longs
      })
      .then(() => {
        //debugger;
        me.setState({isLoading: false,firebaseupdateValue:parseInt(firebaseupdateValue+1)});
        console.log('firebaseupdateValue incremented and new values is  '+ me.state.firebaseupdateValue);
        me.setState({isLoading: false});
      })
      .catch(error => {
        //debugger;
        var err= String(error) == '' ? 'no error ' : String(error);
        Alert.alert(
          'problem',
          'There is an issue in location updation . Please refresh the page' +err,
        );
        me.setState({isLoading: false});
        //debugger;
        console.log('error while updating the firebase   ',error);
        //me.showToast("an error occured, try again");
      });
  }

  componentWillUnmount() {
    Geolocation.clearWatch(this.watchID);
  }

  render() {
    //debugger;
    var me = this;
    const {isLoading, destination, simpleorigin,waitfor} = this.state;
    const address = this.props.Order.requestLocationAddress;
    const {requestAddressLat, requestAddressLong} = this.props;

    return (
      <View style={{flex: 1}}>
        <CustomHeaderWithText leftIcon={true} text="Trace Order" />
        <StatusBar hidden />
        <ScrollView scrollEnabled>
          <View style={styles.StatusBorder}>
            {!isLoading ? (
              <MapView
                ref={c => (this.mapView = c)}
                mapType={'standard'}
                showsCompass={true}
                showsMyLocationButton={true}
                showsUserLocation={true}
                //showsTraffic={true}
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                region={{
                  latitude: requestAddressLat, //33.6641233,
                  longitude: requestAddressLong, //73.0505619,
                  latitudeDelta: LATITUDE_DELTA,
                  longitudeDelta: LONGITUDE_DELTA,
                }}>
                <Marker
                  coordinate={destination}
                  title={'Destination'}
                  description={address}
                />

                <MapViewDirections
                  origin={simpleorigin}
                  mode="DRIVING"
                  language="en"
                  destination={destination}
                  apikey={GOOGLE_MAPS_APIKEY}
                  strokeWidth={5}
                  strokeColor="hotpink"
                  resetOnChange={true}
                  optimizeWaypoints={true}
                  onStart={() => console.log('started')}
                  onReady={detail => {
                    debugger;
                    if (detail) {
                      me.setState({
                        duration: parseInt(detail.duration),
                        distance: detail.distance,
                      });
                      // if(firebaseupdateValue && (firebaseupdateValue % 2 == 0))
                      // {
                      //   // me.mapView.fitToCoordinates(detail.coordinates, {
                      //   //   edgePadding: {
                      //   //     right: width / 20,
                      //   //     bottom: height / 20,
                      //   //     left: width / 20,
                      //   //     top: height / 20,
                      //   //   },
                      //   // });
                      // }
                     
                    }
                    console.log('ready');
                  }}
                />
              </MapView>
            ) : (
              <Spinner />
            )}
          </View>
          <View style={styles.orderMain}>
            <Text style={styles.orderHeading}>
              {this.state.duration} Minutes Away
            </Text>
            <Text style={styles.orderValue}>
              Distance : {this.state.distance} KM
            </Text>
          </View>
          {/* <View>
            <Text>{`wait for values is ${waitfor}`}</Text>
            <Text>{`firebase updated value is ${firebaseupdateValue}`}</Text>
          </View> */}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  StatusBorder: {
    borderBottomWidth: 3,
    borderBottomColor: 'orange',
    width: width * 1,
    height: height * 0.75,
  },

  orderMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 15,
  },
  orderHeading: {
    color: 'black',
    alignSelf: 'center',
    marginTop: 5,
    fontWeight: 'bold',
    fontSize: 16,
    fontFamily: 'sens-serif',
  },
  orderValue: {
    color: 'grey',
    alignSelf: 'center',
    marginTop: 5,
    fontSize: 16,
    fontFamily: 'sens-serif',
    paddingLeft: 10,
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    borderColor: '#f48004',
    backgroundColor: '#f48004',
    borderWidth: 2,
    padding: 10,
    borderRadius: 20,
    width: width * 0.5,
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});



export default TraceOrder;
