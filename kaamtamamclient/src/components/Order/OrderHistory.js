import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView, TouchableHighlight ,StatusBar, Alert,Platform,PermissionsAndroid} from 'react-native';
import { Icon, Thumbnail, Button, Spinner, Tab, Tabs, ScrollableTab } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import axios from 'axios';


import { config } from '../../config';
import Geolocation from 'react-native-geolocation-service';
import Geocode from 'react-geocode';
import { checkoutButton, AddItem,UpdateUserAddressDatail } from '../../actions';
import { CustomHeaderWithText } from '../Common/Header';
import ServicesHistoryCard from './ServicesHistoryCard';
import OrderHistoryCard from './OrderHistoryCard';
Geocode.setApiKey('AIzaSyCZ_GeN6VIBOgZqe9mZ568ygvB8eUNuSbc');
Geocode.setLanguage('en');
Geocode.enableDebug();
class OrderHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            ordersList: this.props.Orders,
            servicesList: [],
            isGettingLocation:true
        }

    }

    async getCurrentLocation() {
        //debugger;
      //  userCurrentLat: 33.9976884, userCurrentLong: 71.4695614
        var me = this;
        this.setState({ isGettingLocation: true })
        // isGettingLocation:false
        await Geolocation.getCurrentPosition(
            position => {
                console.log(position);
                // let lats = String(position.coords.latitude).indexOf('37.') == 0 ? 33.9976884 :
                //     position.coords.latitude;
                // let longs =
                //     String(position.coords.longitude).indexOf('-122') == 0 ? 71.4695614:
                //         position.coords.longitude;

                        let lats= position.coords.latitude;
                        let longs =position.coords.longitude;
                        
                Geocode.fromLatLng(lats, longs).then(
                    response => {
                        //debugger;
                        const address = response.results[0].formatted_address;
                        //userCurrentLat: 33.9976884 
                        //userCurrentLong: 71.4695614
                        //Board Bazar Stop, University latsRoad, Peshawar, Pakistan
                        let userCurrentAddressDetail =
                            { userCurrentLat: lats, userCurrentLong: longs, userCurrentAddress: address };
                        me.props.UpdateUserAddressDatail({ prop: 'userCurrentAddressDetail', value: userCurrentAddressDetail });
                        me.props.UpdateUserAddressDatail({ prop: 'isCurrentLocatonSelected', value: true });
                        me.setState({ isGettingLocation: false })
                        // Actions.checkout()
                        console.log(address);
                    },
                    error => {
                        //debugger;
                        me.setState({ isGettingLocation: false })
                        console.error(error);
                    }
                ).catch(error => {
                    //debugger;
                    me.setState({ isGettingLocation: false })
                    //loginUserFail(dispatch, error);
                    console.log('error', error);
                });;
            },
            error => {
                //debugger;
                me.setState({ isGettingLocation: false })
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000 },
        );

    }

    componentDidMount() {
        debugger;

        if (Platform.OS !== 'android') 
        {
            this.getCurrentLocation();
        }else
        {
            this.requestLocationPermission();
        }
       
    }

    async  requestLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Cool Photo App Location Permission',
                    message:
                        'Cool Photo App needs access to your Location ' +
                        'so you can take awesome Location.',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the Location');
               await this.getCurrentLocation();
            } else {
                console.log('Camera permission denied');
                Alert.alert('Please allow your location permission to use to location service for order')
            }
        } catch (err) {
            console.warn(err);
        }
    }

    _renderOrdersGrid() {
        let { user, loading, ordersLoading } = this.props;
        var UserRole = user.roleName;
        debugger;
        if (UserRole == "GrocerryDriver") {
            return <ScrollView scrollEnabled>
                <View style={{ backgroundColor: '#fff', alignItems: 'center' }}>
                    {
                        this.state.isLoading ? <Spinner /> :
                            this.props.Orders.length < 1 ?
                                <Text style={{ textAlign: 'center', marginTop: 15 }}>You havnt made any order yey!</Text>
                                :
                                ordersLoading ? <Spinner /> :
                                    <OrderHistoryCard Order={this.props.Orders} assignedOrderCount={this.props.relatedOrders.Assigned} />
                        // )
                    }
                </View>
            </ScrollView>
        } else {
            return <ScrollView scrollEnabled>
                <View style={{ alignItems: 'center' }}>
                    {
                        this.state.isLoading ? <Spinner /> :
                            this.props.Orders.length < 1 ?
                                <Text style={{ textAlign: 'center', marginTop: 15 }}>You havnt made any order yey!</Text>
                                :
                                loading ? <Spinner /> :
                                    <ServicesHistoryCard services={this.props.Orders} assignedOrderCount={this.props.relatedOrders.Assigned} />
                        // )
                    }

                </View>
            </ScrollView>
        }
    }

    render() {
debugger;

        return (
            <View style={{ flex: 1, borderWidth: 1, borderColor: 'black' }}>
                <View style={{ borderWidth: 1, borderColor: 'black' }} >
                    <CustomHeaderWithText leftIcon={true} text="Orders History" />
                    <StatusBar hidden />
                </View>
                {this._renderOrdersGrid()}

            </View>
        );
    }
}


const mapStateToProps = ({ auth, handyServices, orders }) => {

    const { loading } = handyServices;
    const { isloading } = orders;
    return { user: auth.user, loading, ordersLoading: isloading };
};
export default connect(mapStateToProps, {UpdateUserAddressDatail})(OrderHistory);
//export default OrderHistory;

const styles = {
    ListStyle: {
        height: Dimensions.get('window').height / 5.5,
        //backgroundColor:'green',
        flexDirection: 'row'
    },
    ListStyleColum: {
        flex: 0.5,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Imagestyle:
    {
        width: 128,
        height: 128,
        // backgroundColor: 'red',
        // borderRadius:( Dimensions.get('window').width/1.8)/2,
        resizeMode: 'contain',
    },
}