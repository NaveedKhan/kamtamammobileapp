import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions, Image, Alert, ScrollView, Linking, Modal, Platform,TextInput } from 'react-native';
import { Item, Input, Icon } from 'native-base';
import { CustomHeaderWithText } from '../Common/Header';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';
import { Rating, AirbnbRating } from 'react-native-elements';
import { UpdateNotificationData, asigneHandyServiceRequest, changeHandyServiceStatus, markCompleteHandyService, markCancelHandyService } from '../../actions';
import { connect } from 'react-redux';
import axios from 'axios';
import { config } from '../../config';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class ServicesHistoryCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            reviewComment: '',
            reviewScore: 0,
            modalButtonLoading: false,
            selectedOrderToComplete: null
        }
        this.renderRow = this.renderRow.bind(this);
        //debugger;
    }

    timeBetweenDates(toDate) {
        // debugger;
        var dateEntered = toDate;
        var now = new Date();
        var difference = now.getTime() - dateEntered.getTime();

        if (difference <= 0) {

            // Timer done
            //clearInterval(timer);

        } else {

            var seconds = Math.floor(difference / 1000);
            var minutes = Math.floor(seconds / 60);
            var hours = Math.floor(minutes / 60);
            var days = Math.floor(hours / 24);

            hours %= 24;
            minutes %= 60;
            seconds %= 60;

            if (hours > 0 || days > 0 || minutes > 5) {
                return true;
            } else {
                return false;
            }
            // this.setState({ countDownTime: days + 'D ' + hours + 'H ' + minutes + 'M ' + seconds + 'S' });
        }
    }
    renderRow(iconName, headingTitle, headingValue, detail) {
        return (
            <View style={styles.rowMain}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                        //alignSelf: 'center',
                        //  backgroundColor:'green',
                        flex: 1
                    }}>
                    <Icon name={iconName} style={styles.rowIcon} />
                    <Text style={styles.rowHeadingList}>{headingTitle}:</Text>
                    <Text style={styles.listItemText}>{headingValue}</Text>
                </View>
                {detail && (
                    <TouchableOpacity
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                        }}>
                        <Text style={{ color: 'grey', paddingRight: 5 }}>Details</Text>
                        <Icon name="arrow-forward" style={{ fontSize: 16, color: 'grey' }} />
                    </TouchableOpacity>
                )}
            </View>
        );
    }

    ratingCompleted(rating) {
        this.setState({ reviewScore: rating })
        console.log("Rating is: " + rating)
    }

    submitReview() {

        this.setState({ modalVisible: false })
        if (this.state.iscanceling) {
            this.senpApiCallToMarkCancel()
        } else {
            this.senpApiCallToMarkComplete();
        }
        //this.senpApiCallToMarkComplete();
    }

    senpApiCallToMarkCancel() {
        debugger
        // const { Order } = this.props;
        const { reviewComment, reviewScore, selectedOrderToComplete } = this.state;
        var reviewModel = {
            ServiceRequestID: selectedOrderToComplete.serviceRequestID,
            IsMarkFinishedByDriverTech: true,
            Reviews: {
                DriverReview: reviewComment,
                DriverReviewScore: reviewScore,
                DriverId: selectedOrderToComplete.serviceTakenBy,
                ServiceRequestID: selectedOrderToComplete.serviceRequestID,
                IsReviewedByDriverTech: true,
            }
        }

        this.props.markCancelHandyService(reviewModel); 

    }


    senpApiCallToMarkComplete() {
        debugger
        // const { Order } = this.props;
        const { reviewComment, reviewScore, selectedOrderToComplete } = this.state;
        var reviewModel = {
            ServiceRequestID: selectedOrderToComplete.serviceRequestID,
            IsMarkFinishedByDriverTech: true,
            Reviews: {
                DriverReview: reviewComment,
                DriverReviewScore: reviewScore,
                DriverId: selectedOrderToComplete.serviceTakenBy,
                ServiceRequestID: selectedOrderToComplete.serviceRequestID,
                IsReviewedByDriverTech: true,
            }
        }

        this.props.markCompleteHandyService(reviewModel);

    }

    renderRatingModal() {
        debugger;
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    this.setState({ modalVisible: false, modalButtonLoading: false })
                }}>
                <View style={styles.datecontainer}>
                    <TouchableOpacity style={{
                        width: 30, height: 30, borderRadius: 15, justifyContent: 'flex-end', alignItems: "center",
                        alignSelf: "flex-end", margin: 10, borderColor: "#fff", borderWidth: 1
                    }}
                        onPress={() => this.setState({ modalVisible: false })}>
                        <Icon name="close" style={{ color: '#fff' }} />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 15, margin: 5, alignSelf: 'center', color: 'white' }}>Please Add Review</Text>
                    <View style={{ flexDirection: 'row', backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center', padding: 5 }}>
                        <AirbnbRating
                            count={5}
                            reviews={["Terrible (1)", "Bad (2)", "Normal (3)", "GOOD (4)", "Outstanding (5)"]}
                            defaultRating={3}
                            onFinishRating={(rat) => this.ratingCompleted(rat)}
                            size={40}
                        />
                    </View>

                    <View style={{ margin: 10, backgroundColor: '#fff', borderColor: 'gray', borderWidth: 1, padding: 5, borderRadius: 5 }}>
                        <TextInput
                            onChangeText={(text) => this.setState({ reviewComment: text })}
                            placeholder="Add any comment (optional but recomended)" placeholderTextColor="grey" maxLength={200} multiline={true} editable numberOfLines={5} style={{ height: 150, justifyContent: "flex-start" }} />
                    </View>
                    {this.state.modalButtonLoading ? <Spinner /> :
                        <TouchableOpacity
                            onPress={() => this.submitReview()}
                            style={{
                                width: 250,
                                height: 50,
                                borderRadius: 30,
                                margin: 10,
                                justifyContent: 'center',
                                alignItems: 'center',
                                alignSelf: 'center',
                                borderColor: '#f48004',
                                borderWidth: 1.7
                            }} >
                            <Text style={{ fontSize: 15, color: '#fff', fontWeight: 'bold' }} > SUBMIT NOW </Text>
                        </TouchableOpacity>
                    }

                </View>
            </Modal>
        );
    }

    showToast2() {
        return Toast.show({
            text: "Order already marked completed!",
            buttonText: "Okay",
            type: "danger",
            duration: 3000
        })
    }

    OnpressComplete(el, orderStatus) {
        debugger;
        if (orderStatus.toLowerCase() == "Delivered".toLowerCase() || orderStatus.toLowerCase() == 'Canceled'.toLowerCase()) {
            this.showToast2();
        }
        else {
            debugger;
            this.ShowConfimrationAlert(el, 'Are You sure your request is fulfilled and you want to close  it.');
        }

    }

    markCancel(el) {
        if (el.orderStatus == 'Delivered' || el.orderStatus.toLowerCase() == 'Canceled'.toLowerCase()) {
            this.showToast2();
        } else {
           // let isexpire = el.orderStatus.toLowerCase() == 'initiated'.toLowerCase() ? false : this.timeBetweenDates(new Date(el.orderAssignedToDriverTechDate));
            if (el.orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
                this.ShowConfimrationAlertForCancel(el, 'Are you sure you really want to cancel the order');

            } else {
           
                    this.ShowConfimrationAlertForCancel(el, 'Are you sure you really want to cancel the order');
                
            }
        }

    }

    markCancel(el, orderStatus) {
        if (orderStatus.toLowerCase() == 'Delivered'.toLowerCase() || orderStatus.toLowerCase() == 'Canceled'.toLowerCase()) {
            this.showToast2();
        } else {
            //let isexpire = orderStatus.toLowerCase() == 'initiated'.toLowerCase() ? false : this.timeBetweenDates(new Date(el.orderAssignedToDriverTechDate));
            if (orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
                this.ShowConfimrationAlertForCancel(el, 'Are you sure you really want to cancel the order');

            } else {
               
                    this.ShowConfimrationAlertForCancel(el, 'Are you sure you really want to cancel the order');
                
            }
            //this.ShowConfimrationAlertForCancel(el, 'Are you sure you really want to cancel the order');

        }
    }


    ShowConfimrationAlertForCancel(el, message) {
        Alert.alert(
            'Confirmation!',
            message,
            [
                {
                    text: 'Yes Continue', onPress: () => {
                        //checkoutSucess(dispatch,baseModel.data)
                        this.setState({ selectedOrderToComplete: el })
                        this.setState({ iscanceling: true, modalVisible: true })
                        //    this.senpApiCallToMarkCancel(el)
                    }
                },
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
            ],
            { cancelable: true }
        )
    }

    ShowConfimrationAlert(el, message) {
        debugger;
        Alert.alert(
            'Confirmation!',
            'Are You sure your request is fulfilled and you want to close the it.',
            [
                {
                    text: 'Yes,Continue', onPress: () => {
                        debugger;
                        //checkoutSucess(dispatch,baseModel.data)
                        this.setState({ selectedOrderToComplete: el })
                        this.setState({ iscanceling: false, modalVisible: true })
                        //this.setState({ modalVisible: true })
                    }
                },
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
            ],
            { cancelable: true }
        )
    }




    onTakeOrderPresss(el) {
        // debugger;

        const { user, relatedOrders, userCurrentAddressDetail } = this.props;
        const { userCurrentLat, userCurrentLong, userCurrentAddress } = userCurrentAddressDetail;
        let id = user.id;
        var trackingid =  el.locationTrackingID;
        var workerTrackingId = (trackingid ? trackingid : 'M3Bkno3vcxcbOTNhCrQ');
        this.props.asigneHandyServiceRequest(el.serviceRequestID, id, userCurrentLat, userCurrentLong,workerTrackingId);

    }

    ShowConfimrationMessage(el, message) {


        if (this.props.assignedOrderCount.length > 0) {
            Alert.alert('Alert!', 'You have already working on an order. Please finish your order first');
        } else {
            Alert.alert(
                'Confirmation!',
                message,
                [
                    {
                        text: 'Yes Continue', onPress: () => {
                            //checkoutSucess(dispatch,baseModel.data)
                            //this.setState({ iscanceling: true, modalVisible: true, selectedOrderToComplete: el })
                            this.onTakeOrderPresss(el);
                        }
                    },
                    {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                ],
                { cancelable: true }
            )
        }
    }

    onChnageOrderStatusClicked(el, orderstatusid) {
        // debugger;

        Alert.alert(
            'Confirmation!',
            'Are you sure you want to change the order status.',
            [
                {
                    text: 'Yes Continue', onPress: () => {
                        //checkoutSucess(dispatch,baseModel.data)
                        //this.setState({ iscanceling: true, modalVisible: true, selectedOrderToComplete: el })
                        this.props.changeHandyServiceStatus(el.serviceRequestID, orderstatusid);
                    }
                },
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
            ],
            { cancelable: true }
        )


    }

    _renderUserReviews(score) {
        return <View style={{
            // flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 20
        }}>
            <Rating
                // showRating
                ratingCount={5}
                readonly
                // reviews={["Terrible (1)", "Bad (2)", "Normal (3)", "GOOD (4)", "Outstanding (5)"]}
                startingValue={score}
                //onFinishRating={(rat) => this.ratingCompleted(rat)}
                size={10}
            />
        </View>
    }

    callUser(phonesss) {
        let phone = phonesss;//'033323233232';
        console.log('callNumber ----> ', '033323233232');
        let phoneNumber = phonesss;
        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${phone}`;
        }
        else {
            phoneNumber = `tel:${phone}`;
        }
        Linking.canOpenURL(phoneNumber)
            .then(supported => {
                if (!supported) {
                    Alert.alert('Phone number is not available');
                } else {
                    return Linking.openURL(phoneNumber);
                }
            })
            .catch(err => console.log(err));
    }

    _renderbothReviews(userscore, driverscore) {
        return <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 10, marginBottom: 20,
            padding: 5
        }}>
            <Text>Your Review</Text>
            <Rating
                ratingCount={5}
                readonly
                startingValue={userscore}
                size={10}
            />
            <Text style={{ marginTop: 5 }}>Kaam Tamam Team Review</Text>
            <Rating
                ratingCount={5}
                readonly
                startingValue={driverscore}
                size={10}
            />

        </View>

    }

    _renderButtons(orderStatus, el) {

        const { assigneeImageURL, assigneContactNo, assigneeName } = el;
        if (orderStatus == 'Delivered' || orderStatus.toLowerCase() == 'Canceled'.toLowerCase()) {
            let { reviews } = el;
            if (reviews != null) {
                let { userReviewScore, driverReviewScore, isReviewedByUser, isReviewedByDriverTech } = reviews;
                var bothreivwesocre = parseInt(userReviewScore + driverReviewScore);
                let reviewscore = (isReviewedByUser && isReviewedByDriverTech);

                if (isReviewedByUser && !isReviewedByDriverTech) {
                    return this._renderUserReviews(userReviewScore);
                }
                else if (!isReviewedByUser && isReviewedByDriverTech) {
                    return this._renderUserReviews(driverReviewScore);
                }
                else if (isReviewedByUser && isReviewedByDriverTech) {
                    return this._renderbothReviews(userReviewScore, driverReviewScore)

                }
            }


        } else {
            if (orderStatus.toLowerCase() == 'initiated') {
                return <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10, marginBottom: 10 }}>

                    <TouchableOpacity onPress={() => this.ShowConfimrationMessage(el, 'Are You sure you want to take this order')} style={[styles.button, { marginLeft: 5, width: 150 }]}>
                        <Text style={styles.buttonText}> Take Order </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.callUser(el.requesteeContactNo)} style={styles.button}>
                        <Text style={styles.buttonText}>Call Customer</Text>
                    </TouchableOpacity>

                </View>
            }
            else if (orderStatus.toLowerCase() == "Processing".toLowerCase()) {
                return <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, marginBottom: 10 }}>
                    <TouchableOpacity onPress={() => this.onChnageOrderStatusClicked(el, 4)} style={styles.button}>
                        <Text style={styles.buttonText}>Processed ,Lets GO</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.callUser(el.requesteeContactNo)} style={styles.button}>
                        <Text style={styles.buttonText}>Call Customer</Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={() => this.markCancel(el, orderStatus)} style={styles.button}>
                        <Text style={styles.buttonText}> Cancel Order</Text>
                    </TouchableOpacity>


                </View>
            }
            else if (orderStatus.toLowerCase() == "On The Way".toLowerCase()) {
                return <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, marginBottom: 10 }}>
                    <TouchableOpacity onPress={() => this.onChnageOrderStatusClicked(el, 3)} style={styles.button}>
                        <Text style={styles.buttonText}>Reached Now Start Working</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.markCancel(el, orderStatus)} style={styles.button}>
                        <Text style={styles.buttonText}> Cancel Order</Text>
                    </TouchableOpacity>


                </View>
            }
            else if (orderStatus.toLowerCase() == "Work In Progress".toLowerCase()) {
                return <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, marginBottom: 10 }}>
                    <TouchableOpacity onPress={() => this.OnpressComplete(el, orderStatus)} style={styles.button}>
                        <Text style={styles.buttonText}>Mark Complete</Text>
                    </TouchableOpacity>
                    {el.serviceTypeName == "Laundry" ?
                        <TouchableOpacity onPress={() => this.onChnageOrderStatusClicked(el, 8)} style={styles.button}>
                            <Text style={styles.buttonText}>Items Collected</Text>
                        </TouchableOpacity>
                        : <View />}
                    <TouchableOpacity onPress={() => this.markCancel(el, orderStatus)} style={styles.button}>
                        <Text style={styles.buttonText}> Cancel Order</Text>
                    </TouchableOpacity>


                </View>
            }

            else {
                return <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, marginBottom: 10 }}>
                    <TouchableOpacity onPress={() => this.OnpressComplete(el, orderStatus)} style={styles.button}>
                        <Text style={styles.buttonText}>Mark Complete</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.markCancel(el, orderStatus)} style={styles.button}>
                        <Text style={styles.buttonText}> Cancel Order</Text>
                    </TouchableOpacity>


                </View>
            }

        }
    }


    render() {
        const { services } = this.props;
        debugger;
        return (
            <View>
                {services.map(el => {
                    const urdgency = el.isServiceSchedule ?  moment(el.serviceRequestScheduleDate).format('MMMM Do YYYY, h:mm:ss a'): " Urgent";
                    const requestime = moment(el.serviceRequestDate).format('MMMM Do YYYY, h:mm:ss a');
                    const regionname = el.regionName;
                    return <TouchableOpacity key={el.serviceRequestID} style={styles.StatusBorder} onPress={() => Actions.serviceDetails({ selectedService: el })}>
                        {this.renderRow((iconName = 'paper'), (headingTitle = el.serviceTypeName + '  Request No'), (headingValue = el.serviceRequestID))}
                        {this.renderRow((iconName = 'paper'), (headingTitle = 'Request Region'), (headingValue = regionname))}
                        {this.renderRow((iconName = 'clock'), (headingTitle = 'Request Time'), (headingValue = requestime), (detail = false))}
                        {this.renderRow((iconName = 'md-checkmark-circle-outline'), (headingTitle = 'Status'), (headingValue = el.serviceStatusName))}
                        {this.renderRow((iconName = 'contact'), 
                        (headingTitle = el.isServiceSchedule? 'Schedule On':' Request Is'),
                         (headingValue = urdgency))}
                        {this.renderRow((iconName = 'construct'), (headingTitle = 'Total Chargea'), (headingValue = el.serviceTotalCharges))}


                        {this._renderButtons(el.serviceStatusName, el)}

                    </TouchableOpacity>
                })}

                {this.renderRatingModal()}
            </View>



        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        backgroundColor: 'white',
        marginTop: 30,
    },
    datecontainer: {
        backgroundColor: 'green',//'#E9EBEE',
        borderRadius: 10,
        margin: 10,
        marginTop: 100,
    },
    recentJobHeading: {
        alignSelf: 'center',
        fontSize: 20,
        color: '#f48004',
        fontWeight: 'bold',
        fontFamily: 'sens-serif',
    },
    StatusBorder: {
        borderWidth: 3,
        borderColor: 'orange',
        width: width * 0.9,
        //height: height * 0.68,
        margin: 10,
        borderRadius: 10,
    },
    listItemText: {
        fontSize: 12,
        color: 'green',
        fontFamily: 'sens-serif',
    },
    rowMain: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        width: width * 0.8,
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
        paddingTop: 10,
        justifyContent: 'space-between',
    },
    rowIcon: {
        color: '#f48004',
        margin: 10,
        marginRight: 20,
        marginBottom: 5,
    },
    rowHeadingList: {
        fontWeight: 'bold',
        margin: 20,
        marginRight: 10,
        marginLeft: 0,
        fontSize: 13,
        color: 'black',
        fontFamily: 'sens-serif',
    },
    buttonMain: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 20,
    },
    button: {
        marginTop: 3, marginLeft: 5, marginBottom: 5, backgroundColor: 'transparent', flexDirection: 'row',
        padding: 7, borderRadius: 25, borderWidth: 1.5, borderColor: 'green', alignItems: 'center', justifyContent: 'center', alignSelf: 'center'
    },
    buttonText: {
        fontSize: 12,
        color: 'black',
        alignSelf: 'center',
        fontFamily: 'sens-serif',
    },
});

//export default ServicesHistoryCard;
const mapStateToProps = ({ auth, userAddress }) => {
    const { userCurrentAddressDetail, userDeliveryAddressDetail, isCurrentLocatonSelected } = userAddress;
    return { user: auth.user, userCurrentAddressDetail };
};
export default connect(mapStateToProps, { asigneHandyServiceRequest, changeHandyServiceStatus, markCompleteHandyService, markCancelHandyService })(ServicesHistoryCard);