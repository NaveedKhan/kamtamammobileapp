import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView, TouchableHighlight } from 'react-native';
import { Icon, Thumbnail, Button, Spinner } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import axios from 'axios';
import { config } from '../../config';

import { checkoutButton, AddItem } from '../../actions';
import { CommonHeader, SubHeader, CustomHeader, CustomHeaderWithText } from '../Common/Header';
import { CustomFooterTab } from '../Common/Footer';
import OrderGridNormalRow from './OrderGridNormalRow';
import OrderGridHeader from './OrderGridHeader';
import OrderGridImageRow from './OrderGridImageRow';
import OrderGrid from './OrderGrid';

class OrderHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            ordersList: this.props.Orders,
            relatedOrders: this.props.relatedOrders
        }
       // debugger;
    }

   

    render() {
        const {relatedOrders} = this.state;
        return (
            <View style={{ flex: 1, borderWidth: 1, borderColor: 'black' }}>
                <View style={{ borderWidth: 1, borderColor: 'black' }} >
                    <CustomHeaderWithText leftIcon={true} text="Orders History"/>
                </View>
                <ScrollView scrollEnabled>
                    <View style={{ backgroundColor: '#fff', flex: 1 }}>
                        {
                            this.state.isLoading ? <Spinner /> :
                               this.state.ordersList.map(el=>
                                <OrderGrid key={el.orderID}  item={el} relatedOrders={relatedOrders}/>
                                ) 
                        }



                    </View>
                </ScrollView>
            </View>
        );
    }
}


const mapStateToProps = ({ auth }) => {

    debugger;
    return { user: auth.user };
};
export default connect(mapStateToProps, null)(OrderHistory);
//export default OrderHistory;

const styles = {
    ListStyle: {
        height: Dimensions.get('window').height / 5.5,
        //backgroundColor:'green',
        flexDirection: 'row'
    },
    ListStyleColum: {
        flex: 0.5,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Imagestyle:
    {
        width: 128,
        height: 128,
        // backgroundColor: 'red',
        // borderRadius:( Dimensions.get('window').width/1.8)/2,
        resizeMode: 'contain',
    },
}