import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  Modal,
  TextInput,
  Alert,
  Linking,
  Platform,
} from 'react-native';
import {Item, Input, Icon, Toast, Spinner} from 'native-base';
import {Rating, AirbnbRating} from 'react-native-elements';
//import { CustomHeaderWithText } from '../../Grocessry/Common/Header';
import axios from 'axios';
import moment from 'moment';
import {config} from '../../config';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {
  UpdateNotificationData,
  assignOrder,
  changeOrderStatus,
  markOrderCancel,
  markOrderComplete,
} from '../../actions';
//import {CustomFooterTab} from '../../Grocessry/Common/Footer';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';

class OrderHistoryCard extends Component {
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
    debugger;
    this.state = {
      modalVisible: false,
      reviewComment: '',
      reviewScore: 0,
      modalButtonLoading: false,
      selectedOrderToComplete: null,
    };
  }

  timeBetweenDates(toDate) {
    debugger;
    var dateEntered = toDate;
    var now = new Date();
    var difference = now.getTime() - dateEntered.getTime();

    if (difference <= 0) {
      // Timer done
      //clearInterval(timer);
    } else {
      var seconds = Math.floor(difference / 1000);
      var minutes = Math.floor(seconds / 60);
      var hours = Math.floor(minutes / 60);
      var days = Math.floor(hours / 24);

      hours %= 24;
      minutes %= 60;
      seconds %= 60;

      if (hours > 0 || days > 0 || minutes > 5) {
        return true;
      } else {
        return false;
      }
      // this.setState({ countDownTime: days + 'D ' + hours + 'H ' + minutes + 'M ' + seconds + 'S' });
    }
  }

  renderRow(iconName, headingTitle, headingValue, detail) {
    return (
      <View style={styles.rowMain}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            //alignSelf: 'center',
            //  backgroundColor:'green',
            flex: 1,
          }}>
          <Icon name={iconName} style={styles.rowIcon} />
          <Text style={styles.rowHeadingList}>{headingTitle}:</Text>
          <Text style={styles.listItemText}>{headingValue}</Text>
        </View>
        {detail && (
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <Text style={{color: 'grey', paddingRight: 5}}>Details</Text>
            <Icon name="arrow-forward" style={{fontSize: 16, color: 'grey'}} />
          </TouchableOpacity>
        )}
      </View>
    );
  }

  timeBetweenDates(toDate) {
    debugger;
    var dateEntered = toDate;
    var now = new Date();
    var difference = now.getTime() - dateEntered.getTime();

    if (difference <= 0) {
      // Timer done
      //clearInterval(timer);
    } else {
      var seconds = Math.floor(difference / 1000);
      var minutes = Math.floor(seconds / 60);
      var hours = Math.floor(minutes / 60);
      var days = Math.floor(hours / 24);

      hours %= 24;
      minutes %= 60;
      seconds %= 60;

      if (hours > 0 || days > 0 || minutes > 5) {
        return true;
      } else {
        return false;
      }
      // this.setState({ countDownTime: days + 'D ' + hours + 'H ' + minutes + 'M ' + seconds + 'S' });
    }
  }

  ratingCompleted(rating) {
    this.setState({reviewScore: rating});
    console.log('Rating is: ' + rating);
  }

  submitReview() {
    this.setState({modalVisible: false});
    if (this.state.iscanceling) {
      this.senpApiCallToMarkCancel();
    } else {
      this.senpApiCallToMarkComplete();
    }
  }

  senpApiCallToMarkComplete() {
    debugger;
    // const { Order } = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
      OrderID: selectedOrderToComplete.orderID,
      IsMarkFinishedByDriverTech: true,
      Reviews: {
        DriverReview: reviewComment,
        DriverReviewScore: reviewScore,
        DriverId: selectedOrderToComplete.orderTakenBy,
        OrderID: selectedOrderToComplete.orderID,
        IsReviewedByDriverTech: true,
      },
    };
    this.props.markOrderComplete(reviewModel);
  }

  ShowConfimrationAlert(el, message) {
    Alert.alert(
      'Confirmation!',
      message,
      [
        {
          text: 'Yes Continue',
          onPress: () =>
            //checkoutSucess(dispatch,baseModel.data)
            this.setState({
              iscanceling: false,
              modalVisible: true,
              selectedOrderToComplete: el,
            }),
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  ShowConfimrationAlertForCancel(el, message) {
    Alert.alert(
      'Confirmation!',
      message,
      [
        {
          text: 'Yes Continue',
          onPress: () => {
            //checkoutSucess(dispatch,baseModel.data)
            this.setState({
              iscanceling: true,
              modalVisible: true,
              selectedOrderToComplete: el,
            });
            //    this.senpApiCallToMarkCancel(el)
          },
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  renderRatingModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({modalVisible: false, modalButtonLoading: false});
        }}>
        <View style={styles.datecontainer}>
          {/* <View style={{ margin: 10 }} /> */}
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              borderRadius: 15,
              justifyContent: 'flex-end',
              alignItems: 'center',
              alignSelf: 'flex-end',
              margin: 10,
              borderColor: '#fff',
              borderWidth: 1,
            }}
            onPress={() => this.setState({modalVisible: false})}>
            <Icon name="close" style={{color: '#fff'}} />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 15,
              margin: 5,
              alignSelf: 'center',
              color: 'white',
            }}>
            Please Add Review
          </Text>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 5,
            }}>
            <AirbnbRating
              count={5}
              reviews={[
                'Terrible (1)',
                'Bad (2)',
                'Normal (3)',
                'GOOD (4)',
                'Outstanding (5)',
              ]}
              defaultRating={3}
              onFinishRating={rat => this.ratingCompleted(rat)}
              size={40}
            />
          </View>

          <View
            style={{
              margin: 10,
              backgroundColor: '#fff',
              borderColor: 'gray',
              borderWidth: 1,
              padding: 5,
              borderRadius: 5,
            }}>
            <TextInput
              onChangeText={text => this.setState({reviewComment: text})}
              placeholder="Add any comment (optional but recomended)"
              placeholderTextColor="grey"
              maxLength={200}
              multiline={true}
              editable
              numberOfLines={5}
              style={{height: 150, justifyContent: 'flex-start'}}
            />
          </View>
          {this.state.modalButtonLoading ? (
            <Spinner />
          ) : (
            <TouchableOpacity
              onPress={() => this.submitReview()}
              style={{
                width: 250,
                height: 50,
                borderRadius: 30,
                margin: 10,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                borderColor: '#fff',
                borderWidth: 1.7,
              }}>
              <Text style={{fontSize: 15, color: '#fff', fontWeight: 'bold'}}>
                {' '}
                SUBMIT NOW{' '}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </Modal>
    );
  }

  showToast2() {
    return Toast.show({
      text: 'Order already marked completed!',
      buttonText: 'Okay',
      type: 'danger',
      duration: 3000,
    });
  }

  markCancel(el) {
    if (
      el.orderStatus == 'Delivered' ||
      el.orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      this.showToast2();
    } else {
      // let isexpire =  el.orderStatus.toLowerCase() == 'initiated'.toLowerCase() ? false : this.timeBetweenDates(new Date(el.orderAssignedToDriverTechDate));
      if (el.orderStatus.toLowerCase() == 'initiated'.toLowerCase()) {
        this.ShowConfimrationAlertForCancel(
          el,
          'Are you sure you really want to cancel the order',
        );
      } else {
        this.ShowConfimrationAlertForCancel(
          el,
          'Are you sure you really want to cancel the order',
        );
      }
    }
  }

  senpApiCallToMarkCancel() {
    debugger;
    // const { Order } = this.props;
    const {reviewComment, reviewScore, selectedOrderToComplete} = this.state;
    var reviewModel = {
      OrderID: selectedOrderToComplete.orderID,
      IsMarkFinishedByDriverTech: true,
      Reviews: {
        DriverReview: reviewComment,
        DriverReviewScore: reviewScore,
        DriverId: selectedOrderToComplete.orderTakenBy,
        OrderID: selectedOrderToComplete.orderID,
        IsReviewedByDriverTech: true,
      },
    };
    this.props.markOrderCancel(reviewModel);
  }

  markComplete(el) {
    if (
      el.orderStatus.toLowerCase() == 'Delivered'.toLowerCase() ||
      el.orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      this.showToast2();
    } else {
      this.ShowConfimrationAlert(
        el,
        'Are You sure your request is fulfilled and you want to close  it.',
      );
    }
  }

  ShowConfimrationMessage(el, message) {
    if (this.props.assignedOrderCount.length > 0) {
      Alert.alert(
        'Alert!',
        'You have already working on an order. Please finish your order first',
      );
    } else {
      Alert.alert(
        'Confirmation!',
        message,
        [
          {
            text: 'Yes Continue',
            onPress: () => {
              //checkoutSucess(dispatch,baseModel.data)
              //this.setState({ iscanceling: true, modalVisible: true, selectedOrderToComplete: el })
              this.onTakeOrderPresss(el);
            },
          },
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ],
        {cancelable: true},
      );
    }
  }

  onTakeOrderPresss(el) {
    debugger;

    const {user, relatedOrders, userCurrentAddressDetail} = this.props;
    const {
      userCurrentLat,
      userCurrentLong,
      userCurrentAddress,
    } = userCurrentAddressDetail;
    let id = user.id;
    var trackingid = el.locationTrackingID;
    var workerTrackingId = trackingid ? trackingid : 'M3Bkno3vcxcbOTNhCrQ';
    this.props.assignOrder(
      el.orderID,
      id,
      relatedOrders,
      userCurrentLat,
      userCurrentLong,
      workerTrackingId,
    );
  }

  onChnageOrderStatusClicked(el, orderstatusid) {
    debugger;

    const {Order, user, relatedOrders} = this.props;
    let id = user.id;

    Alert.alert(
      'Confirmation!',
      'Are You sure you want to change the order status!',
      [
        {
          text: 'Yes Continue',
          onPress: () => {
            this.props.changeOrderStatus(el.orderID, orderstatusid);
          },
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  onReachedDestinationPresses(el) {
    debugger;
    el;
    // const { user, orderInCartList } = this.props;
    // let id = user.user.id;
    // this.props.confirmButtonClicked(id, orderInCartList);

    const {Order, user, relatedOrders} = this.props;
    let id = user.id;
    this.props.assignOrder(el.orderID, id, relatedOrders);
  }

  _renderUserReviews(score) {
    return (
      <View
        style={{
          // flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}>
        <Rating
          // showRating
          ratingCount={5}
          readonly
          // reviews={["Terrible (1)", "Bad (2)", "Normal (3)", "GOOD (4)", "Outstanding (5)"]}
          startingValue={score}
          //onFinishRating={(rat) => this.ratingCompleted(rat)}
          size={10}
        />
      </View>
    );
  }

  _renderbothReviews(userscore, driverscore) {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 10,
          marginBottom: 20,
          padding: 5,
        }}>
        <Text>Your Review</Text>
        <Rating ratingCount={5} readonly startingValue={userscore} size={10} />
        <Text style={{marginTop: 5}}>Kaam Tamam Team Review</Text>
        <Rating
          ratingCount={5}
          readonly
          startingValue={driverscore}
          size={10}
        />
      </View>
    );
  }

  _renderButtons(orderStatus, el) {
    const {assigneeImageURL, assigneContactNo, assigneeName} = el;
    if (
      orderStatus == 'Delivered' ||
      orderStatus.toLowerCase() == 'Canceled'.toLowerCase()
    ) {
      let {reviews} = el;
      if (reviews != null) {
        let {
          userReviewScore,
          driverReviewScore,
          isReviewedByUser,
          isReviewedByDriverTech,
        } = reviews;
        var bothreivwesocre = parseInt(userReviewScore + driverReviewScore);
        let reviewscore = isReviewedByUser && isReviewedByDriverTech;

        if (isReviewedByUser && !isReviewedByDriverTech) {
          return this._renderUserReviews(userReviewScore);
        } else if (!isReviewedByUser && isReviewedByDriverTech) {
          return this._renderUserReviews(driverReviewScore);
        } else if (isReviewedByUser && isReviewedByDriverTech) {
          return this._renderbothReviews(userReviewScore, driverReviewScore);
        }
      }
    } else {
      if (orderStatus.toLowerCase() == 'initiated') {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() =>
                this.ShowConfimrationMessage(
                  el,
                  'Are You sure you want to take this order',
                )
              }
              style={[styles.button, {marginLeft: 5, width: 150}]}>
              <Text style={styles.buttonText}> Take Order </Text>
            </TouchableOpacity>
          </View>
        );
      } else if (orderStatus.toLowerCase() == 'Processing'.toLowerCase()) {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() => this.onChnageOrderStatusClicked(el, 3)}
              style={styles.button}>
              <Text style={styles.buttonText}>Start Delivery Now</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.markCancel(el)}
              style={styles.button}>
              <Text style={styles.buttonText}> Cancel Order</Text>
            </TouchableOpacity>
          </View>
        );
      } else if (orderStatus.toLowerCase() == 'On The Way'.toLowerCase()) {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() => this.onChnageOrderStatusClicked(el, 7)}
              style={styles.button}>
              <Text style={styles.buttonText}>Reached Destination</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.markCancel(el)}
              style={styles.button}>
              <Text style={styles.buttonText}> Cancel Order</Text>
            </TouchableOpacity>
          </View>
        );
      } else {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() => this.markComplete(el)}
              style={styles.button}>
              <Text style={styles.buttonText}>Mark Complete</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.markCancel(el)}
              style={styles.button}>
              <Text style={styles.buttonText}> Cancel Order</Text>
            </TouchableOpacity>
          </View>
        );
      }
    }
  }

  callUser(phoness) {
    let phone = '033323233232';
    console.log('callNumber ----> ', '033323233232');
    let phoneNumber = phoness;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  }

  render() {
    /// const { services } = this.props;
    const {Order} = this.props;

    debugger;
    return (
      <View>
        {Order.map(el => {
          //let isexpire = this.timeBetweenDates(new Date(el.orderDate));
          const urdgency = el.isScheduleOrder
            ? 'Scheduled On '
            : 'Urgent Order';

          const date = el.isScheduleOrder
            ? moment(el.orderScheduleDate).format('MMMM Do YYYY, h:mm:ss a')
            : moment(el.orderDate).format('MMMM Do YYYY, h:mm:ss a');
          const regionname = el.regionName;
          return (
            <TouchableOpacity
              key={el.orderID}
              style={styles.StatusBorder}
              onPress={() => Actions.orderDetail({Order: el})}>
              {this.renderRow(
                (iconName = 'paper'),
                (headingTitle = 'Order No'),
                (headingValue = el.orderID),
              )}
              {this.renderRow(
                (iconName = 'paper'),
                (headingTitle = 'Order Region'),
                (headingValue = regionname),
              )}
              {this.renderRow(
                (iconName = 'clock'),
                (headingTitle = urdgency),
                (headingValue = date),
                (detail = false),
              )}
              {this.renderRow(
                (iconName = 'md-checkmark-circle-outline'),
                (headingTitle = 'Status'),
                (headingValue = el.orderStatus),
              )}
              {this.renderRow(
                (iconName = 'contact'),
                (headingTitle = 'Total Items'),
                (headingValue = el.orderTotalItems),
              )}
              {this.renderRow(
                (iconName = 'construct'),
                (headingTitle = 'Total Cost '),
                (headingValue = `Rs.  ${el.orderTotalCharges}`),
              )}

              {this._renderButtons(el.orderStatus, el)}

              {/* <Text style={{ color: 'grey', alignSelf: 'center', marginTop: 5 }}>
                            You can Cancel the Request within 5 min.
                     </Text> */}
            </TouchableOpacity>
          );
        })}

        {this.renderRatingModal()}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 30,
  },
  datecontainer: {
    backgroundColor: 'green', //'#E9EBEE',
    borderRadius: 10,
    margin: 10,
    marginTop: 100,
  },
  recentJobHeading: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#f48004',
    fontWeight: 'bold',
    fontFamily: 'sens-serif',
  },
  StatusBorder: {
    borderWidth: 3,
    borderColor: 'orange',
    width: width * 0.9,
    //  height: height * 0.68,
    margin: 10,
    borderRadius: 10,
  },
  listItemText: {
    fontSize: 12,
    color: 'green',
    fontFamily: 'sens-serif',
  },
  rowMain: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    width: width * 0.8,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  rowIcon: {
    color: '#f48004',
    margin: 10,
    marginRight: 20,
    marginBottom: 5,
    //  backgroundColor:'red'
  },
  rowHeadingList: {
    fontWeight: 'bold',
    margin: 20,
    marginRight: 10,
    marginLeft: 0,
    fontSize: 13,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  buttonMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    marginTop: 3,
    marginLeft: 5,
    marginBottom: 5,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    padding: 7,
    borderRadius: 25,
    borderWidth: 1.5,
    borderColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 12,
    color: 'black',
    alignSelf: 'center',
    fontFamily: 'sens-serif',
  },
});

const mapStateToProps = ({auth, userAddress}) => {
  const {
    userCurrentAddressDetail,
    userDeliveryAddressDetail,
    isCurrentLocatonSelected,
  } = userAddress;
  return {user: auth.user, userCurrentAddressDetail};
};
export default connect(
  mapStateToProps,
  {
    UpdateNotificationData,
    assignOrder,
    changeOrderStatus,
    markOrderCancel,
    markOrderComplete,
  },
)(OrderHistoryCard);
