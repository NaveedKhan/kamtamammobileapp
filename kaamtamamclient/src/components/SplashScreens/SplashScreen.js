import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StatusBar, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase, { RNFirebase } from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';
import { fetchingProducts, loginUser ,UpdateUserFormData} from '../../actions';
import { connect } from 'react-redux';
import { Spinner } from 'native-base';


// This is the default FCM Channel Name (required by Android)
const fcmChannelID = 'fcm_default_channel';
 
// This is the Firebase Server Key (for test purposes only - DO NOT SHARE IT!)
// cfr: https://www.ryadel.com/en/react-native-push-notifications-setup-firebase-4/
const firebase_server_key = "AAAApmnXMCA:APA91bEEgvogk6SVOeCciLoh9a2Fq1Aw8kJ5tUxDYYz3y8lMQq3MhhzsjAFhccIsHrtBJ269_djyt7cFgGA-9ULyMDRfc38T_Nxk9Tr9Gn7HAHZNZ1qsi6vZ42j87YEl7nXGMYpuSvkw";


class SplashScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isloading: false,
      firebase_messaging_token: '',
      firebase_messaging_message: '',
      firebase_notification: '',
      firebase_send: ''
    }
    //debugger;
  }

  getdeviceIdAndLoggedIn = (UserName,Password,ptoken) => {
    //Getting the Unique Id from here
    var id = DeviceInfo.getUniqueId();
    //this.setState({ deviceId: id, });
    this.props.loginUser({
      email: UserName,
      password: Password,
      fromSplashscreen: true,
      token: ptoken,
      deviceId:id
    });
  };

  getdeviceId = () => {
    //Getting the Unique Id from here
    var id = DeviceInfo.getUniqueId();
    //this.setState({ deviceId: id, });
    this.props.UpdateUserFormData({
      prop: 'DeviceID',
      value: id,
    });
    Actions.login();
  };
// Firebase PN Start

componentWillUnmount() {
  //this.removeNotificationListeners();
}


createNotificationChannel = () => {
    console.log("createNotificationChannel");
    // Build a android notification channel
    const channel = new firebase.notifications.Android.Channel(
      fcmChannelID, // channelId
      "FCM Default Channel", // channel name
      firebase.notifications.Android.Importance.High // channel importance
    ).setDescription("Test Channel"); // channel description
    // Create the android notification channel
    firebase.notifications().android.createChannel(channel);
  };

  checkNotificationPermissions() {
    console.log("checkNotificationPermissions");
    // show token
    firebase.messaging().hasPermission()
      .then(enabled => {
        if (enabled) {
          console.log('user has notification permission')
          this.setToken();
        } else {
          console.log('user does not have notification permission')
          firebase.messaging().requestPermission()
            .then((result) => {
              if (result) {
                this.setToken();
              }
              else {
              }
            });
        }
      });
  }
 
  setToken(token) {
    console.log("setToken");
    firebase.messaging().getToken().then((token) => {
      this.setState({ firebase_messaging_token: token });
      this.props.UpdateUserFormData({prop: 'DeviceTocken', value: token});
      console.log(token);
      this.isAlreadyLoggedIn(token);
    });
  }


  addNotificationListeners() {
    console.log("receiveNotifications");
    this.messageListener = firebase.messaging().onMessage((message) => {
      // "Headless" Notification
      console.log("onMessage");
    });
 
    this.notificationInitialListener = firebase.notifications().getInitialNotification().then((notification) => {
          if (notification) {
            // App was opened by a notification
            // Get the action triggered by the notification being opened
            // Get information about the notification that was opened
            console.log("onInitialNotification");
          }
      });
 
    this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
      console.log("onNotificationDisplayed");
    });
 
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      console.log("onNotification");
      notification.android.setChannelId(fcmChannelID);
      firebase.notifications().displayNotification(notification).catch((err) => {
        console.log(err);
      });
 
      // Process your notification as required
 
      // #1: draw in View
      var updatedText = this.state.firebase_notification + "\n" +
        "[" + new Date().toLocaleString() + "]" + "\n" + 
        notification.title + ":" + notification.body + "\n";
 
      this.setState({ firebase_notification: updatedText });
    });
 
    this.tokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
      // Process your token as required
      console.log("onTokenRefresh");
    });  

    
  }
 
  removeNotificationListeners() {
    this.messageListener();
    this.notificationInitialListener();
    this.notificationDisplayedListener();
    this.notificationListener();
    this.tokenRefreshListener();    
  }


//Firebase PN End

async componentDidMount() {
  debugger;

 this.createNotificationChannel();
 this.checkNotificationPermissions();
 this.addNotificationListeners();
 ////{
 //}
 
}


  isAlreadyLoggedIn(ptoken) {
    var me = this;
    console.log('isAlreadyLoggedIn');
    //me.setState({ isLoading: true });
    try {
      const token = AsyncStorage.getItem('userData').then((token) => {
        //debugger;
        if (token != null) {
          let userObj = JSON.parse(token);
          const { UserName, Password } = userObj;
          if (UserName.length > 5 && Password.length > 5)
            // this.props.loginUser({ email: UserName, password: Password ,token:ptoken });
            this.getdeviceIdAndLoggedIn(UserName,Password,ptoken);
          //me.setState({ isLoading: false });
          //return token;
          //me.sendApicall(token,me);
        } else {
          this.getdeviceId();
        
         /// me.setState({ isLoading: false });
        }

      }).done();

    } catch (e) {
     // me.setState({ isLoading: false });
     Alert.alert('somthing went wrong','server error');
      return null;

    }
  }

  // componentDidMount() {
  //   //debugger;
  //   this.isAlreadyLoggedIn();
  // }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#f78320' }}>
        <StatusBar hidden />
        <View style={{ flex: 7, justifyContent: 'center', alignItems: 'center', margin: 10 }} >
          <Image source={require('../Asset/Kaam_Tamam_Logo.png')} style={styles.Imagestyle} />
        </View>

        <Text style={{ alignSelf: 'center', color: '#fff', fontSize: 30, fontWeight: 'bold' }}>WORKERS</Text>
        <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
          {this.props.loading ? <Spinner /> : <View />}
          <Text numberOfLines={2} style={{ justifyContent: 'center', alignItems: 'center', width: 250, textAlign: 'center', color: '#fff', fontSize: 20 }}>
            One Stop Solution to all
                     your household needs...! </Text>
        </View>

      </View>
    );
  }
}
const mapStateToProps = ({ splashScreen ,auth}) => {
  const { Products, isloading, ProductsCategory, error } = splashScreen;
  //
  const {loading} = auth;
  return { Products, isloading, ProductsCategory, error,loading };
};
export default connect(mapStateToProps, { fetchingProducts, loginUser,UpdateUserFormData })(SplashScreen);

function chnagepage() {

  //Actions.splash2();

}

const styles = {
  errorTextStyel: {
    fontSize: 17,
    alignSelf: 'center',
    color: 'red'
  },
  Imagestyle:
  {
    width: Dimensions.get('window').width / 1.6,
    height: Dimensions.get('window').width / 1.6,
    // borderRadius:( Dimensions.get('window').width/1.8)/2,
    resizeMode: 'contain',

    // opacity: 0.9
  }
};