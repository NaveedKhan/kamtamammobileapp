import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button } from 'native-base';
class SplashScreen2 extends Component {
    constructor(props) {
        super(props)
        // ...
        this.state = {
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
        }
       // debugger;
    }
   
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#f78320' }}>
                <View style={{ flex: 4, justifyContent: 'center', alignItems: 'center', margin: 10 }} >
                    <Image source={require('../Asset/Kaam_Tamam_Logo.png')} style={styles.Imagestyle} />
                </View>
                <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                    <Text numberOfLines={2} style={{ justifyContent: 'center', alignItems: 'center', width: 250, textAlign: 'center', color: '#fff', fontSize: 18 }}>
                        One Stop Solution to all
                     your household needs...! </Text>
                </View>
                <View style={styles.buttonsArea} >
                    <TouchableHighlight style={styles.buttonStyle} onPress={() => Actions.login()}>
                        <Text style={{ fontSize: 15, color: '#ffff' }} >LOGIN</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.buttonStyle} onPress={() => Actions.signUp()}>
                        <Text style={{ fontSize: 15, color: '#ffff' }}>SIGNUP</Text>
                    </TouchableHighlight>

                    <Button rounded transparent style={{ width: 250, justifyContent: 'center' }}>
                        <Text style={{ fontSize: 15, color: '#ffff', textDecorationLine: 'underline' }}>CONTINUE AS GUEST</Text>
                    </Button>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'bltransparentue' }} />
            </View>
        );
    }
}

export default SplashScreen2;


const styles = {
    errorTextStyel: {
        fontSize: 17,
        alignSelf: 'center',
        color: 'red'
    },
    Imagestyle:
    {
        width: Dimensions.get('window').width / 1.6,
        height: Dimensions.get('window').width / 1.6,
        // borderRadius:( Dimensions.get('window').width/1.8)/2,
        resizeMode: 'contain',

        // opacity: 0.9
    },
    buttonStyle: {
        width: 250,
        height: 50,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#fff',
        borderWidth: 1.7
    },
    buttonsArea: {
        flex: 3,
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'transparent'
    }
};