import React, { Component } from 'react';
import { View, Text, Dimensions, Image, ScrollView, TouchableHighlight } from 'react-native';
import {
    Content,
    Button,
    List,
    ListItem,
    Icon,
    Container,
    Left,
    Right,
    Badge
} from "native-base";
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { AddItem, RemoveItem, OnItemClick } from '../../actions';
import { CommonHeader, SubHeader } from '../Common/Header';

class ProductListGridColum extends Component {
    constructor(props) {
        super(props);
        let numbersOfItems = 0;
        const cart = this.props.orderInCartList;
        var resultpr = cart.find(aa => aa.ProductId == this.props.item.itemID);
        if (resultpr != undefined) {
            numbersOfItems = resultpr.ProductCount;
        }

        this.state = {
            numbersOfItems
        }

    }
    
    _renderUpperIcon() {
        return (
            <View style={{ backgroundColor: '#fcfcfc', flex: .2, marginLeft: 5 }} >
                <Icon name="grid" style={{ color: "#777", fontSize: 26, width: 20 }} />
            </View>
        )
    }

    _renderProductImage() {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent' }} >
                <Image source={require('../Asset/Categoris/Vegetables.png')} style={styles.Imagestyle} />
            </View>
        )
    }

    addProduct(itemselected) {
        var selectedProdcutMiodel = {
            ProductName: itemselected.itemName,
            ProductId: itemselected.itemID,
            ProductCount: 1,
            ProdcutPriceEach: itemselected.itemPrice,
            TotalPrice: itemselected.itemPrice
        }
        //debugger;
        var orderincart = this.props.orderInCartList;
        var result = orderincart.find(aa => aa.ProductId == itemselected.itemID);
        if (result != undefined) {
            result.ProductCount = (result.ProductCount + 1);
            result.TotalPrice = result.ProdcutPriceEach * result.ProductCount;
        } else {
            orderincart.push(selectedProdcutMiodel);
        }

        this.setState({ numbersOfItems: this.state.numbersOfItems + 1 })
        this.props.AddItem(orderincart)

    }

    deleteProduct(itemselected) {

        var orderincart = this.props.orderInCartList;
        var result = orderincart.find(aa => aa.ProductId == itemselected.itemID);
        if (result != undefined) {
            if (result.ProductCount <= 1) {
                var index = orderincart.indexOf(itemselected);
                orderincart.splice(index,1);
                this.setState({ numbersOfItems: this.state.numbersOfItems - 1 })
            }
            else {
                result.ProductCount = (result.ProductCount - 1);
                result.TotalPrice = result.ProdcutPriceEach * result.ProductCount;
            }
            this.props.RemoveItem(orderincart);
        }
        

        if (this.state.numbersOfItems > 0) {
            this.setState({ numbersOfItems: this.state.numbersOfItems - 1 })

        }
    }

    render() {
        const { itemName, itemPrice } = this.props.item;
        return (
            <View style={styles.ProductGridColumn}>

                {/* product detail grid column inner first row  */}
                <View style={{ backgroundColor: '#fcfcfc', flex: .1, flexDirection: 'row' }}>
                    {this._renderUpperIcon()}
                </View>


                {/* product detail grid column inner second row for item image */}
                <View style={{ backgroundColor: '#fcfcfc', flex: .55 }} >
                    <TouchableHighlight onPress={() => Actions.productDetail()} style={{ backgroundColor: '#fcfcfc', flex: .55 }}>

                        {
                            this._renderProductImage()
                        }
                    </TouchableHighlight>
                </View>


                {/* product detail grid column inner third row for item name price and add button */}
                <View style={{ backgroundColor: '#f78320', flex: .35, flexDirection: 'column' }} >

                    {/* product detail grid column inner third row title and price */}
                    <View style={{ backgroundColor: '#f78320', flex: 45, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'stretch' }} >
                        <View style={{ flex: 7, backgroundColor: 'transparent' }}>
                            <Text numberOfLines={2}
                                style={{
                                    fontSize: 14, padding: 5,
                                    fontWeight: '900', fontFamily: 'Arial', alignItems: 'center', color: '#000'
                                }}>
                                {itemName}
                            </Text>
                        </View>

                        <View  >
                            <Text style={{ color: '#000', padding: 5, textAlign: 'right', marginRight: 3, fontSize: 13, fontFamily: 'Arial', backgroundColor: 'transparent', alignSelf: 'stretch', justifyContent: 'space-around' }}>
                                Rs. {itemPrice}
                            </Text>
                        </View>

                    </View>

                    {/* product detail grid column inner third row add buttons */}
                    <View style={{ backgroundColor: '#f78320', flex: 55, alignItems: 'center', justifyContent: 'space-evenly', flexDirection: 'row' }} >
                        <TouchableHighlight  onPress={() => this.addProduct(this.props.item)}
                            style={{
                                height: 30, marginTop: 5, marginBottom: 5, backgroundColor: 'transparent',
                                width: 60, borderRadius: 15, borderWidth: 1.5, borderColor: '#fff', alignItems: 'center', justifyContent: 'center'
                            }} >
                            <Text style={{ fontSize: 15, color: '#fff' }}>+</Text>
                        </TouchableHighlight>
                        <Text style={{ textAlign: 'center' }}> {this.state.numbersOfItems} </Text>
                        <TouchableHighlight  onPress={() => this.deleteProduct(this.props.item)}
                            style={{
                                height: 30, marginTop: 5, marginBottom: 5, backgroundColor: 'transparent',
                                width: 60, borderRadius: 15, borderWidth: 1.5, borderColor: '#fff', alignItems: 'center', justifyContent: 'center'
                            }} >
                            <Text style={{ fontSize: 16, color: '#fff' }}>-</Text>
                        </TouchableHighlight>
                    </View>

                </View>

            </View>

        );
    }
}

const mapStateToProps = ({ productList }) => {
    const { user, isloading, selectedCategory, selectedProduct, totalItems, orderInCartList } = productList;
    //debugger;
    return { user, isloading, selectedCategory, selectedProduct, totalItems, orderInCartList };
};


export default connect(mapStateToProps, { AddItem, RemoveItem })(ProductListGridColum);

const styles = {


    Imagestyle:
    {
        width: Dimensions.get('window').width / 2.2,
        height: Dimensions.get('window').width / 2.2,
        // borderRadius:( Dimensions.get('window').width/1.8)/2,
        resizeMode: 'contain',
        //backgroundColor:'red',
        opacity: 0.9
    },
    ProductGridColumn: {
        backgroundColor: '#fcfcfc',
        flex: 5,
        flexDirection: 'column',
        margin: 5,
        borderWidth: 1,
        overflow: 'hidden',
        borderRadius: 6//, shadowOffset: { width: 1, height: 1 }
    },


}