import React, { Component } from 'react';
import {View, Text,Button } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect }  from 'react-redux';
import { addClicked }  from '../../actions';

class ProductDetail extends Component {
    constructor(props) {
        super(props);
        //debugger;
    }
    componentDidMount()
    {
       this.props.addClicked();

    }
render(){
    return (
    <View>
        <Text  style={{fontSize:20,textAlign:'center'}}>ProductDetail Page </Text>
        <Button title="Product List" onPress={()=> Actions.productList()}>
            <Text>
                GO to Cart
            </Text>
        </Button>
    </View>
        );
    }
}
const mapStateToProps = ({ productDetail }) => {
    const { user, isloading, selectedCategory,selectedProduct,totalItems } = productDetail;
    //debugger;
    return {  user, isloading, selectedCategory,selectedProduct,totalItems};
  };
export default connect(mapStateToProps,{addClicked})(ProductDetail);