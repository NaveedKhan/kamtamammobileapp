import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions, TouchableHighlight } from 'react-native';
import { List, ListItem, Left, CheckBox, Right } from 'native-base';
import axios from 'axios';

import CheckoutItemsList from './CheckoutItemsList'
import { connect } from 'react-redux';
import { confirmButtonClicked, addressChanged } from '../../actions';
import { CommonHeader,CustomHeaderWithText } from '../Common/Header';


class Checkout extends Component {
    constructor(props) {
        super(props);
        ////debugger;
        this.state = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            swipedAllCards: false,
            swipeDirection: '',
            cardIndex: 0
        }
    }

    onConfirmPressed() {
        ////debugger;
        const { user, orderInCartList } = this.props;
        let id = user.user.id;
        this.props.confirmButtonClicked(id, orderInCartList);

    }

    componentDidMount() {
        // let configs = {
        //     headers: {
        //         'Content-Type': 'application/json; charset=UTF-8'
        //     }
        // };
        // const { REST_API } = config;
        // const { UserCompany, orgid, UserData } = this.state;
        // axios.get(REST_API.Test.GetSimple, configs)
        //     .then(response => {
        //         //debugger;
        //         console.log('api call' + response);
        //     }).catch(error => {
        //         //debugger;
        //         console.log("error", error)
        //     });
    }

    render() {
        const { orderInCartList } = this.props;
        return (
            <View style={{ backgroundColor: '#fff', flex: 1, marginTop: 0}}>
                <View>
                    <CustomHeaderWithText text="Checkout" />
                </View>
                <View style={styles.container} >
                    <View style={{ height: 30, backgroundColor: '#f2eded', margin: 10 }}>
                        <Text style={{ padding: 5, fontWeight: 'bold', fontSize: 15 }}> Adress Selected</Text>
                    </View>
                    <ScrollView scrollEnabled>
                        <View style={{ marginLeft: 10, marginRight: 10, backgroundColor: '#fff' }}>


                            <View style={{ height: 60, backgroundColor: 'transparent', flexDirection: 'row', borderBottomColor: '#f2eded', borderBottomWidth: 1 }}>

                                <View style={{ width: Dimensions.get('window').width / 1.3, backgroundColor: 'transparent', justifyContent: 'center' }}>
                                    <Text numberOfLines={2} style={{ marginRight: 10, marginLeft: 10 }}>COMEDY khan g dagh se habara do che ckld ldjfklj</Text>
                                </View>

                                <View style={{ width: Dimensions.get('window').width / 8.7, backgroundColor: 'transparent', justifyContent: 'center' }}>
                                    <CheckBox checked={true} style={{ margin: 10, marginRight: 10 }} />
                                </View>

                            </View>

                            <View style={{ height: 60, backgroundColor: 'transparent', flexDirection: 'row', borderBottomColor: '#f2eded', borderBottomWidth: 1 }}>

                                <View style={{ width: Dimensions.get('window').width / 1.3, backgroundColor: 'transparent', justifyContent: 'center' }}>
                                    <Text numberOfLines={2} style={{ marginRight: 10, marginLeft: 10 }}>COMEDY khan g dagh seahjf lkkld ldjfklj</Text>
                                </View>

                                <View style={{ width: Dimensions.get('window').width / 8.7, backgroundColor: 'transparent', justifyContent: 'center' }}>
                                    <CheckBox checked={true} style={{ margin: 10, marginRight: 10 }} />
                                </View>

                            </View>

                            <View style={{ height: 60, backgroundColor: 'transparent', flexDirection: 'row', borderBottomColor: '#f2eded', borderBottomWidth: 1 }}>

                                <View style={{ width: Dimensions.get('window').width / 1.3, backgroundColor: 'transparent', justifyContent: 'center' }}>
                                    <Text numberOfLines={2} style={{ marginRight: 10, marginLeft: 10 }}>COMEDY khan g dagh se hab dahjf lkkld ldjfklj</Text>
                                </View>

                                <View style={{ width: Dimensions.get('window').width / 8.7, backgroundColor: 'transparent', justifyContent: 'center' }}>
                                    <CheckBox checked={true} style={{ margin: 10, marginRight: 10 }} />
                                </View>

                            </View>


                        </View>
                    </ScrollView>

                </View>

                <View style={{ height: 30, backgroundColor: '#f2eded', margin: 10 }}>
                    <Text style={{ padding: 5, fontWeight: 'bold', fontSize: 15 }}> Items Selected</Text>
                </View>

                <ScrollView scrollEnabled >
                    <View style={{ flex: 1, backgroundColor: '#f2eded', marginLeft: 10, marginRight: 10 }}>
                        <List>

                            {orderInCartList.map(order =>
                                <CheckoutItemsList key={order.ProductId} item={order} />
                            )}

                        </List>
                    </View>
                </ScrollView>

                <View style={{ height: this.state.height / 10, backgroundColor: 'rgba(255,255,255,0.3)', flexDirection: 'row', fontFamily: 'Helvica' }}>
                    <View style={{ flex: 5, backgroundColor: 'transparent', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}>
                        {/* <Icon type="FontAwesome"  name="arrow-circle-left" style={{fontSize:30, color: '#fff'}} /> */}
                        <Text style={{ fontWeight: 'bold', fontSize: 18, fontFamily: 'Helvica' }}>Total : </Text>
                        <Text style={{ fontSize: 16 }}>$ {orderInCartList.sum('TotalPrice')}   </Text>
                    </View>
                    <View style={{ flex: 5, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }} >
                        <TouchableHighlight onPress={() => this.onConfirmPressed()}
                            style={{
                                borderRadius: 30, backgroundColor: '#e8a035', height: this.state.height / 16,
                                width: this.state.width / 3.3, justifyContent: 'center', alignItems: 'center'
                            }} color="#e8a035" title="Checkouts">
                            <Text style={{ textAlign: 'center', color: '#fff' }}>Confirm Order</Text>
                        </TouchableHighlight>
                    </View>
                </View>

            </View>
        );
    }
}

const mapStateToProps = ({ checkout, cart, auth }) => {
    const { user, isloading, selectedCategory, selectedProduct, totalItems } = checkout;
    const { orderInCartList } = cart;
    //debugger;
    return { user:auth.user, isloading, selectedCategory, selectedProduct, totalItems, orderInCartList };
};
export default connect(mapStateToProps, { confirmButtonClicked, addressChanged })(Checkout);

const styles = {
    container: {
        height: Dimensions.get('window').height / 2.9,
        backgroundColor: '#fff',
        marginTop: 1
    },
    card: {
        flex: 1,
        borderRadius: 4,
        borderWidth: 2,
        borderColor: '#E8E8E8',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    text: {
        textAlign: 'center',
        fontSize: 50,
        backgroundColor: 'transparent'
    },
    done: {
        textAlign: 'center',
        fontSize: 30,
        color: 'white',
        backgroundColor: 'transparent'
    }
};