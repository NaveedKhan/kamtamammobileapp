import React, { Component } from 'react';
import { View, Text, Button, Dimensions, Image, ScrollView, TouchableHighlight ,StatusBar} from 'react-native';
import {
    Content,

    List,
    ListItem,
    Icon,
    Container,
    Left,
    Right,
    Badge
} from "native-base";
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { AddItem, RemoveItem, OnItemClick } from '../../actions';
import { CommonHeader, SubHeader, CustomHeader } from '../Common/Header';
import { CustomFooterTab } from '../Common/Footer';
import { products, productsCat } from '../Common/Cities.json';
import ProductListGrid from './ProductListGrid';


class ProductList extends Component {
    constructor(props) {
        super(props);
        //debugger;
        this.state = {
            productsList: products,
            itemId: this.props.selectedCategory,
            categoryname: this.props.selectedCategoryName
        }
    }
    componentDidMount() {
        //debugger;

        //debugger;
        // this.props.AddItem("hello");
        //this.props.RemoveItem("hello");
    }



    render() {
        //debugger;
        const { productsList, itemId } = this.state;
        let filterData = productsList.filter(aa => aa.itemCategoryID == itemId);

        //debugger
        // const categories = this.state.cate;
        // const categoriesDB = this.state.productCategories;
        let count = Math.ceil(filterData.length);
        let numberofrows = Math.ceil(count / 2);
        var numberofrowsArray = []
        for (i = 0; i < numberofrows; i++) {
            numberofrowsArray.push(i)
        }

        return (
            <View style={{ fontSize: 20, textAlign: 'center', flex: 1, marginTop: 0}}>
                  <StatusBar hidden />
                {/* <CommonHeader text="Product List" /> */}
                <CustomHeader />
                <View>
                    <SubHeader mainText={this.props.selectedCategoryName} />
                </View>

                <ScrollView scrollEnabled>
                    {/* product detatil grid  */}
                    <View style={{ flex: 1, margin: 5 }}>

                        {
                            numberofrowsArray.map(el => {
                                var firstIndex = ((el * 2) + 0);
                                var secondIndex = ((el * 2) + 1);
                                return (

                                    <ProductListGrid key={el} item1={filterData[firstIndex]} item2={filterData[secondIndex]} />
                                )

                            })
                        }


                    </View>
                </ScrollView>

                {/* product detatil grid row */}
                <CustomFooterTab cartLength = {this.props.orderInCartList.length} notifications={2}/>

            </View>
        );
    }
}
const mapStateToProps = ({ productList }) => {
    const { user, isloading, selectedCategory,selectedCategoryName, selectedProduct, totalItems, orderInCartList } = productList;
    ////debugger;
    return { user, isloading, selectedCategory,selectedCategoryName, selectedProduct, totalItems, orderInCartList };
};
export default connect(mapStateToProps, { AddItem, RemoveItem, OnItemClick })(ProductList);

const styles = {
    ListStyle: {
        height: Dimensions.get('window').height / 5.5,
        //backgroundColor:'green',
        flexDirection: 'row'
    },
    ListStyleColum: {
        flex: 0.5,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Imagestyle:
    {
        width: Dimensions.get('window').width / 2.2,
        height: Dimensions.get('window').width / 2.4,
        // borderRadius:( Dimensions.get('window').width/1.8)/2,
        resizeMode: 'contain',

        opacity: 0.9
    },
}