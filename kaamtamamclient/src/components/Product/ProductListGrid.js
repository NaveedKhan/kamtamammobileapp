import React, { Component } from 'react';
import { View, Text, Button, Dimensions, Image, ScrollView, TouchableHighlight } from 'react-native';
import {
    Content,

    List,
    ListItem,
    Icon,
    Container,
    Left,
    Right,
    Badge
} from "native-base";
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { AddItem, RemoveItem, OnItemClick } from '../../actions';
import { CommonHeader, SubHeader } from '../Common/Header';
import ProductGridColumn from './ProductListGridColum';

class ProductListGrid extends Component {
   // debugger;
    render() {
        return (
            <View style={styles.ProductGridRowStyle}>

                <View style={styles.ProductGridInnerRow}>

                    {/* <TouchableHighlight onPress={() => Actions.productDetail()} style={styles.ProductGridColumn}> */}

                        {/* product detail grid left side column */}
                     {this.props.item1 == undefined ? <View/> :<ProductGridColumn item={this.props.item1} />}   
                    {/* </TouchableHighlight> */}

                    {/* <TouchableHighlight onPress={() => Actions.productDetail()} style={styles.ProductGridColumn}> */}
                        {/* product detail grid right  side column */}
                        {this.props.item2 == undefined ? <View/> :<ProductGridColumn  item={this.props.item2}/>}  
                    {/* </TouchableHighlight> */}

                </View>

            </View>


        );
    }
}

export default ProductListGrid;

const styles = {


    ProductGridRowStyle: {
        height: Dimensions.get('window').height / 2.45,
        backgroundColor: 'yellow'
    },
    ProductGridInnerRow: {
        backgroundColor: '#fcfcfc',//'#fcfcfc',
        flexDirection: 'row',
        //flex: 1,
        height: Dimensions.get('window').height / 2.45,
        alignItems: 'center'
    },
    ProductGridColumn: {
        backgroundColor: '#fcfcfc',
        flex: 5,
        flexDirection: 'column',
        margin: 5,
        // borderWidth: 1,
        overflow: 'hidden',
        borderRadius: 6//, shadowOffset: { width: 1, height: 1 }
    },
}