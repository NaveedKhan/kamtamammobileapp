import React, { Component } from 'react';
import { View, Text, Platform, ScrollView, Dimensions, TouchableHighlight } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Left, Body, Right, Button,avatar } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { confirmButtonClicked, addressChanged } from '../../actions';

class CheckoutItemsList extends Component {
    constructor(props) {
        super(props);
        //this.state = { itemvalue: 0 }
        //debugger;
        let numbersOfItems = 0;
        let cart = this.props.item;

        numbersOfItems = cart.ProductCount;
    }
    render() {
        const {item} = this.props;
        return (
            
                
                    <ListItem avatar style={{backgroundColor:'#fff',borderBottomWidth:1,borderBottomColor:"#f48004",marginLeft:0}}>
                        <Left>
                            <Thumbnail  source={require('../Asset/Apple.jpg')} />
                        </Left>
                        <Body >
                            <Text>{item.ProductName}</Text>
                            <Text note numberOfLines={1}>{` $ ${item.ProdcutPriceEach}/kg                Qty ${item.ProductCount}`}</Text>
                        </Body>
                        <Right>
                            <Button transparent>
                                <Text>$ {item.TotalPrice}</Text>
                            </Button>
                        </Right>
                    </ListItem>
              
           
        );
    }
}

export default CheckoutItemsList;