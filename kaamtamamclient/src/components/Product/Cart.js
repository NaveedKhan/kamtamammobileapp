import React, { Component } from 'react';
import { View, Text, Button, Image, Dimensions, ScrollView, TouchableHighlight } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { checkoutButton, AddItem } from '../../actions';
import { CommonHeader, SubHeader,CustomHeader ,CustomHeaderWithText} from '../Common/Header';
import { CustomFooterTab } from '../Common/Footer';

import CartList from './CartList';
class Cart extends Component {
    constructor(props) {
        super(props);
        ////debugger;
        this.state = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height

            // var selectedProdcutMiodel = {
            //     ProductName: itemselected.itemName,
            //     ProductId: itemselected.itemID,
            //     ProductCount: 1,
            //     ProdcutPriceEach: itemselected.itemPrice,
            //     TotalPrice: itemselected.itemPrice
            // }
        }
    }


    render() {
        const { orderInCartList } = this.props;

        return (
            <View style={{ flex: 1,marginTop:0 }}>
                {/* <CommonHeader text="Cart" /> */}
                <CustomHeaderWithText text="CART"/>
                <View>
                    <SubHeader mainText="Cart" />
                </View>
                <ScrollView scrollEnabled>
                    <View style={{ flex: 1, margin: 5 }}>
                        {orderInCartList.map(order =>
                            <CartList key={order.ProductId} item={order} />
                        )}
                
                    </View>
                </ScrollView>

                <View style={{ height: this.state.height / 10, backgroundColor: 'rgba(255,255,255,0.3)',flexDirection:'row' }}>
                    <View style={{ flex: 5, backgroundColor: 'transparent', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}>
                   
                        <Text style={{ fontWeight: 'bold', fontSize: 18, fontFamily: 'serif' }}>Total : </Text>
                        <Text style={{ fontSize: 16 }}>$ {orderInCartList.sum('TotalPrice')}  </Text>
                    </View>
                    <View style={{ flex: 5, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }} >
                        <TouchableHighlight onPress={()=>Actions.checkout()}
                        style={{ borderRadius: 40, backgroundColor: '#e8a035', height: this.state.height / 16, width: this.state.width / 3.9, 
                        justifyContent: 'center', alignItems: 'center' }} color="#e8a035" title="Checkouts">
                            <Text style={{ textAlign: 'center', color: '#fff' }}>Checkout</Text>
                        </TouchableHighlight>
                    </View>
                   
                </View>
                
            </View>
        );
    }
}
const mapStateToProps = ({ cart }) => {
    const { user, isloading, selectedCategory, selectedProduct, totalItems, orderInCartList } = cart;
    ////debugger;
    return { user, isloading, selectedCategory, selectedProduct, totalItems, orderInCartList };
};
export default connect(mapStateToProps, { checkoutButton, AddItem })(Cart);

const styles = {
    ListStyle: {
        height: Dimensions.get('window').height / 5.5,
        //backgroundColor:'green',
        flexDirection: 'row'
    },
    ListStyleColum: {
        flex: 0.5,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Imagestyle:
    {
        width: 128,
        height: 128,
        // backgroundColor: 'red',
        // borderRadius:( Dimensions.get('window').width/1.8)/2,
        resizeMode: 'contain',
    },
}

Array.prototype.sum = function (prop) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
        total += this[i][prop]
    }
    return total
}