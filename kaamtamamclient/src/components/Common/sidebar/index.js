import React, { Component } from "react";
import { Image, View, Dimensions, ScrollView, TouchableHighlight } from "react-native";
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge,
  Thumbnail
} from "native-base";
import styles from "./style";
import { logOut } from "../../../actions";


const drawerCover = require("../../Asset/Kaam_Tamam_Logo.png");
const drawerImage = require("../../Asset/logo-kitchen-sink.png");
const datas = [
  {
    name: "Anatomy",
    route: "Anatomy",
    icon: "phone-portrait",
    bg: "#C5F442"
  },
  {
    name: "Header",
    route: "Header",
    icon: "arrow-up",
    bg: "#477EEA",
    types: "11"
  },
  {
    name: "Footer",
    route: "Footer",
    icon: "arrow-down",
    bg: "#DA4437",
    types: "4"
  },
  {
    name: "Accordion",
    route: "NHAccordion",
    icon: "repeat",
    bg: "#C5F442",
    types: "5"
  },
  {
    name: "Actionsheet",
    route: "Actionsheet",
    icon: "easel",
    bg: "#C5F442"
  },
  {
    name: "Badge",
    route: "NHBadge",
    icon: "notifications",
    bg: "#4DCAE0"
  },
  {
    name: "Button",
    route: "NHButton",
    icon: "radio-button-off",
    bg: "#1EBC7C",
    types: "9"
  },
  {
    name: "Card",
    route: "NHCard",
    icon: "keypad",
    bg: "#B89EF5",
    types: "8"
  },
  {
    name: "Check Box",
    route: "NHCheckbox",
    icon: "checkmark-circle",
    bg: "#EB6B23"
  },
  {
    name: "Date Picker",
    route: "NHDatePicker",
    icon: "calendar",
    bg: "#EB6B23"
  },
  {
    name: "Deck Swiper",
    route: "NHDeckSwiper",
    icon: "swap",
    bg: "#3591FA",
    types: "2"
  },
  {
    name: "Fab",
    route: "NHFab",
    icon: "help-buoy",
    bg: "#EF6092",
    types: "2"
  },
  {
    name: "Form & Inputs",
    route: "NHForm",
    icon: "call",
    bg: "#EFB406",
    types: "12"
  },
  {
    name: "Icon",
    route: "NHIcon",
    icon: "information-circle",
    bg: "#bfe9ea",
    types: "4"
  },
  {
    name: "Layout",
    route: "NHLayout",
    icon: "grid",
    bg: "#9F897C",
    types: "5"
  },
  {
    name: "List",
    route: "NHList",
    icon: "lock",
    bg: "#5DCEE2",
    types: "8"
  },
  {
    name: "ListSwipe",
    route: "ListSwipe",
    icon: "code-working",
    bg: "#C5F442",
    types: "3"
  },
  {
    name: "Picker",
    route: "NHPicker",
    icon: "arrow-dropdown",
    bg: "#F50C75"
  },
  {
    name: "Radio",
    route: "NHRadio",
    icon: "radio-button-on",
    bg: "#6FEA90"
  },
  {
    name: "SearchBar",
    route: "NHSearchbar",
    icon: "search",
    bg: "#29783B"
  },
  {
    name: "Segment",
    route: "Segment",
    icon: "menu",
    bg: "#0A2C6B",
    types: "3"
  },
  {
    name: "Spinner",
    route: "NHSpinner",
    icon: "navigate",
    bg: "#BE6F50"
  },
  {
    name: "Tabs",
    route: "NHTab",
    icon: "home",
    bg: "#AB6AED",
    types: "3"
  },
  {
    name: "Thumbnail",
    route: "NHThumbnail",
    icon: "image",
    bg: "#cc0000",
    types: "2"
  },
  {
    name: "Toast",
    route: "NHToast",
    icon: "albums",
    bg: "#C5F442",
    types: "6"
  },
  {
    name: "Typography",
    route: "NHTypography",
    icon: "paper",
    bg: "#48525D"
  }
];

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4
    };
    // debugger;
  }
  logoutUser() {
    this.props.logOut();
  }
  render() {
    const { user,relatedOrders } = this.props;
    const cancelled = relatedOrders.Canceled;
    const completed = relatedOrders.Completed;
    const all = [...cancelled,...completed]
    let assImage_Http_URL = { uri: 'http://localhost:8089/404.png'.replace('http://localhost', 'http://10.0.2.2') };
    if (user.imageURL != null) {
        let imageass = user.imageURL.replace('http://localhost', 'http://10.0.2.2');
        assImage_Http_URL = { uri: imageass };
    }
    return (

      <View style={{ flex: 1, backgroundColor: "#fff" }}>
        {/* <Image source={drawerCover} style={styles.drawerCover} /> */}

        <View style={{
          height: Dimensions.get('window').height / 3, borderBottomColor: '#f48004',
          borderBottomWidth: 0, alignItems: 'flex-start', justifyContent: 'flex-start', backgroundColor: 'orange'
        }} >


          <View style={{ flex: 5 }} />
          <View style={{ flex: 5, justifyContent: 'flex-end' }}>
            <View style={{
              alignContent: 'flex-start', alignSelf: 'stretch',
              flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'stretch', borderColor: '#f48004', borderWidth: 0, margin: 13
            }}>

              <Thumbnail small source={assImage_Http_URL} style={{ margin: 6,marginLeft:0, height: 90, width: 90, borderRadius: 45, overflow: 'hidden', resizeMode: 'contain' }} />
              <View style={{ alignItems: 'flex-start', justifyContent: 'center' }}>
                {/* <Icon name='key' style={{ marginLeft: 10, color: '#000', fontSize: 32 }} /> */}
                <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: 5 }}>{(user && user.fullName) ? user.fullName.toUpperCase() : ''}</Text>
                <Text style={{ fontWeight: 'normal', fontSize: 13, marginLeft: 5 }}>{(user && user.phoneNumber) ? user.phoneNumber : ''}</Text>
              </View>

            </View>
          </View>


        </View>
        <ScrollView scrollEnabled >

          <View style={{ height: Dimensions.get('window').height / 1.6, }} >

            <View style={{
              flex: 2, alignItems: 'center', justifyContent: 'flex-start', borderBottomColor: '#f48004',
              borderBottomWidth: 1, flexDirection: 'row'
            }}>
              <Icon name='heart' type="AntDesign" style={{ marginLeft: 15, color: '#f78320', fontSize: 30 }} />
              {/* <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 8 }}>Gul Khan</Text> */}
              <Text style={{ fontWeight: 'bold', fontSize: 20, marginLeft: 15 }}>My WishList</Text>
            </View>

            <TouchableHighlight onPress={() => Actions.orderHistory({ Orders: all, relatedOrders })}
              style={{ flex: 2, borderBottomColor: '#f48004', borderBottomWidth: 1, justifyContent: 'center' }}>
              <View style={{ alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row' }}>
                <Icon name='back-in-time' type="Entypo" style={{ marginLeft: 15, color: '#f78320', fontSize: 30 }} />
                {/* <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 8 }}>Gul Khan</Text> */}
                <Text style={{ fontWeight: 'bold', fontSize: 20, marginLeft: 15 }}>Order History</Text>
              </View>
            </TouchableHighlight>


            <TouchableHighlight onPress={() => Actions.MyAccount()}
              style={{ flex: 2, borderBottomColor: '#f48004', borderBottomWidth: 1, justifyContent: 'center' }}>
              <View style={{ alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row' }}>
                <Icon name='contacts' type="AntDesign" style={{ marginLeft: 15, color: '#f78320', fontSize: 30 }} />
                {/* <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 8 }}>Gul Khan</Text> */}
                <Text style={{ fontWeight: 'bold', fontSize: 20, marginLeft: 15 }}>My Account</Text>
              </View>

            </TouchableHighlight>


            <View style={{
              flex: 2, alignItems: 'center', justifyContent: 'flex-start', borderBottomColor: '#f48004',
              borderBottomWidth: 1, flexDirection: 'row'
            }}>
              <Icon name='old-mobile' type="Entypo" style={{ marginLeft: 15, color: '#f78320', fontSize: 30 }} />
              {/* <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 8 }}>Gul Khan</Text> */}
              <Text style={{ fontWeight: 'bold', fontSize: 20, marginLeft: 15 }}>Contact us</Text>
            </View>

            <TouchableHighlight onPress={() => this.logoutUser()} style={{
              flex: 2, alignItems: 'center', justifyContent: 'flex-start', borderBottomColor: '#f48004',
              borderBottomWidth: 1, flexDirection: 'row'
            }}>
              {
                this.props.loading ? <Spinner style={{ alignSelf: 'center', justifyContent: 'center' }} /> :
                  <View style={{ alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row' }}>
                    <Icon name='log-out' type="Entypo" style={{ marginLeft: 15, color: '#f78320', fontSize: 28 }} />
                    <Text style={{ fontWeight: 'bold', fontSize: 20, marginLeft: 15 }}>Log Out</Text>
                  </View>
              }
            </TouchableHighlight>

          </View>


        </ScrollView>

      </View>

    );
  }
}

const mapStateToProps = ({ auth ,orders}) => {
 // debugger;
  //sidebar
  const { loading } = auth;
  const { totalOrdersCountData, relatedOrders } = orders;
  return { user: auth.user, loading ,relatedOrders};
};
export default connect(mapStateToProps, { logOut })(SideBar);
