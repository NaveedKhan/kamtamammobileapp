import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Tab, Tabs, Icon, Input, ScrollableTab, Button } from 'native-base';
import { IconInput } from '.';


class CustomHeader extends Component {

  clickc() {
   // debugger;
    Actions.drawerOpen()
  }

  render() {
    const { leftIcon, searchBar, rightIcon } = this.props;
    return (

      <View style={styles.headerMainDiv}>
        
          <TouchableHighlight style={styles.leftIconDiv} onPress={() => Actions.pop()}>
            <Icon name='arrow-back' style={{ color: '#fff', fontSize: 27 }} />
          </TouchableHighlight>
      
        <View style={styles.centerDiv} >
          <IconInput placeholder="Search" />
        </View>
        {/* <View style={styles.righIconDiv}> */}
          <TouchableHighlight style={styles.righIconDiv} onPress={() => this.clickc()}>
            <Icon name='menu' style={{ color: '#fff', fontSize: 27 }} />
          </TouchableHighlight>
        {/* </View> */}
      </View>

    );
  }
}

export { CustomHeader };

const styles = {
  headerMainDiv: {
    height: Dimensions.get('window').height / 7.7,
    backgroundColor: '#e2e2e2',
    flexDirection: 'row'
  },
  leftIconDiv: {
    backgroundColor: 'transparent',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  centerDiv: { backgroundColor: 'transparent', flex: 8, justifyContent: 'center', alignItems: 'center' },
  centerTextFieldDiv: {
    flexDirection: 'row', alignItems: 'center', borderColor: 'gray',
    backgroundColor: '#fff', borderWidth: 1, marginLeft: 10, marginRight: 10, borderRadius: 26
  },
  righIconDiv: { backgroundColor: 'transparent', flex: 1, justifyContent: 'center', alignItems: 'center' }
};