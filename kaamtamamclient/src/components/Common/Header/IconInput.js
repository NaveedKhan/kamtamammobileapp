import React from 'react';
import { View, Text, TextInput } from 'react-native';
import { Icon, Input } from 'native-base';

const IconInput = ({ label, value, onChangeText, placeholder, secureTextEntry }) => (

    <View style={styles.centerTextFieldDiv}>
        <Icon name='search' style={{ marginLeft: 10, color: '#000', fontSize: 25 }} />
        <Input
            secureTextEntry={secureTextEntry}
            autoCorrect={false}
            placeholder={placeholder}
            placeholderColor="gray"
            value={value}
            onChangeText={onChangeText}
            style={{ marginLeft: 20, fontSize: 15 }} />
    </View>


);

export { IconInput };

const styles = {
    centerTextFieldDiv: {
        flexDirection: 'row', alignItems: 'center', borderColor: 'gray',
        backgroundColor: '#fff', borderWidth: 1, marginLeft: 10, marginRight: 10, borderRadius: 26
    },
}