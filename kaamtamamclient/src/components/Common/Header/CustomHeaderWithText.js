import React, { Component } from 'react';
import { View, Text, Dimensions,TouchableHighlight ,BackHandler} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Tab, Tabs, Icon, Input, ScrollableTab, Button } from 'native-base';
import { IconInput } from '../Header';


class CustomHeaderWithText extends Component {

  clickc() {
   // debugger;
    Actions.drawerOpen()
  }

  backpress()
  {
    debugger;
    if (Actions.currentScene == 'dashboard') {
      BackHandler.exitApp();
      return true;
    } else {
      Actions.pop()
    }
  
  }

  render() {
    const { leftIcon, searchBar, rightIcon, refreshButton,
      refreshButtonClicked } = this.props;
    return (

      <View style={styles.headerMainDiv}>
        {/* <View style={styles.leftIconDiv}> */}
          {leftIcon ? <TouchableHighlight style={styles.leftIconDiv}  onPress={()=>this.backpress()}>
          <Icon name='arrow-back' style={{ color: '#f48004', fontSize: 27 }} />
          </TouchableHighlight> : <View style={styles.leftIconDiv}/>}
          
        {/* </View> */}
        <View style={styles.centerDiv} >
         <Text style={{fontSize:22,color:'#f48004',alignSelf:'center',fontWeight:'bold'}}>
           {this.props.text}
         </Text>
        </View>
        {/* <View style={styles.righIconDiv}> */}
          <TouchableHighlight style={styles.righIconDiv}  onPress={() => this.clickc()}>
            <Icon name='menu' style={{ color: '#f48004', fontSize: 27 }} />
          </TouchableHighlight>
        {/* </View> */}
      </View>

    );
  }
}

export { CustomHeaderWithText };

const styles = {
  headerMainDiv: {
    height: Dimensions.get('window').height / 8,
    backgroundColor: '#e2e2e2',
    flexDirection: 'row'
  },
  leftIconDiv: {
    backgroundColor: 'transparent',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  centerDiv: { backgroundColor: 'transparent', flex: 8, justifyContent: 'center', alignItems: 'center' },
  centerTextFieldDiv: {
    flexDirection: 'row', alignItems: 'center', borderColor: 'gray',
    backgroundColor: '#fff', borderWidth: 1, marginLeft: 10, marginRight: 10, borderRadius: 26
  },
  righIconDiv: { backgroundColor: 'transparent', flex: 1, justifyContent: 'center', alignItems: 'center' }
};