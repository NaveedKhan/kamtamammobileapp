import React, { Component } from 'react';
import { View, Text, Button, Dimensions, Image, StatusBar, ScrollView } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Icon, Input, ScrollableTab } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { callDataFetch, Datafecthed } from '../../actions';
import { connect } from 'react-redux';
import { CommonHeader, CustomHeader } from '../Common/Header';
import LandingPageCategories from './LandingPageCategories';
import { CustomFooterTab } from '../Common/Footer';

class LandingPage extends Component {
  constructor(props) {
    super(props);
//debugger;
    this.state = {
      productsList:this.props.ProductsData.products,
      productCategories: this.props.ProductsData.productsCat,  
    };
  }



  componentDidMount() {
   // this.props.Datafecthed();

  }

 

  render() {
    return (

      <View style={{ flex: 1 }}>
    <StatusBar hidden />
        <CustomHeader leftIcon={true} searchBar={true} rightIcon={true} />
  


        <Tabs style={{ flex: 1 }} tabBarUnderlineStyle={{ backgroundColor: '#f48004' }} renderTabBar={() => <ScrollableTab />}>

          <Tab heading="CATEGORIES" tabStyle={{ backgroundColor: '#fff' }}
            textStyle={{ color: '#666363' }} activeTextStyle={{ color: '#f48004', fontWeight: 'bold' }} activeTabStyle={{ backgroundColor: '#fff' }}>
            <ScrollView scrollEnabled={true} style={{ flex: 1 }} >
              <LandingPageCategories productCategories={this.state.productCategories}/>
            </ScrollView>
          </Tab>

          <Tab heading="POPULAR" tabStyle={{ backgroundColor: '#fff' }}
            textStyle={{ color: '#666363' }} activeTextStyle={{ color: '#f48004', fontWeight: 'bold' }} activeTabStyle={{ backgroundColor: '#fff' }}>
            <ScrollView scrollEnabled={true} style={{ flex: 1 }} >
              <Text style={{ marginTop: 10 }}>TAB 2</Text>
            </ScrollView>

          </Tab>

          <Tab heading="WHATS NEW" tabStyle={{ backgroundColor: '#fff' }}
            textStyle={{ color: '#666363' }} activeTextStyle={{ color: '#f48004', fontWeight: 'bold' }} activeTabStyle={{ backgroundColor: '#fff' }}>
            <ScrollView scrollEnabled={true} style={{ flex: 1 }} >
              <LandingPageCategories productCategories={this.state.productCategories} />
            </ScrollView>
          </Tab>

          <Tab heading="Offers" tabStyle={{ backgroundColor: '#fff' }}
            textStyle={{ color: '#666363' }} activeTextStyle={{ color: '#f48004', fontWeight: 'bold' }} activeTabStyle={{ backgroundColor: '#fff' }}>
            <ScrollView scrollEnabled={true} style={{ flex: 1 }} >
              <Text style={{ marginTop: 10 }}>TAB 4</Text>
            </ScrollView>

          </Tab>

        </Tabs>



               <CustomFooterTab cartLength = {this.props.orderInCartList.length} notifications={2} />

      </View>
    );
  }
}
const mapStateToProps = ({ landingPageReducer, productList }) => {
  const { user, isloading, selectedCategory,ProductsData } = landingPageReducer;
  const {orderInCartList} = productList;
  //debugger;
  return { user, isloading, selectedCategory,ProductsData,orderInCartList };
};
export default connect(mapStateToProps, { Datafecthed, callDataFetch })(LandingPage);

const styles = {
  ListStyle: {
    height: Dimensions.get('window').height / 5.5,
    //backgroundColor:'green',
    flexDirection: 'row'
  },
  yellowback: {
    backgroundColor: 'transparent'
  },
  graywback: {
    backgroundColor: 'transparent'
  },
  brownback: {
    backgroundColor: 'transparent'
  },
  ListStyleColum: {
    flex: 0.5,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  }
  ,
  ListColumImageStyle: {
    height: 90,
    width: Dimensions.get('window').width / 2.4,
    borderRadius: 10,
    borderWidth: 2,
    overflow: 'hidden',
    borderColor: "black"
  },
  ListColumnTextStyle: {
    color: '#fff',
    fontSize: 15
  }
};