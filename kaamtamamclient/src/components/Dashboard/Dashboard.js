import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableHighlight,
  ScrollView,
  PermissionsAndroid,
  StatusBar,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {
  UpdateUserAddressDatail,
  getAllNewRelatedOrdersToWorker,
  getAllNewRelatedHandyServicesToWorker,
} from '../../actions';
import Geolocation from 'react-native-geolocation-service';
import Geocode from 'react-geocode';
import {Button, Input, Icon, Thumbnail} from 'native-base';
import {
  CommonHeader,
  CustomHeader,
  CustomHeaderWithText,
} from '../Common/Header';
import {CustomFooterTab} from '../Common/Footer';
import CustomFooter from '../Common/Footer/CustomFooter';
import BackgroundTimer from 'react-native-background-timer';
Geocode.setApiKey('AIzaSyCZ_GeN6VIBOgZqe9mZ568ygvB8eUNuSbc');
Geocode.setLanguage('en');
Geocode.enableDebug();

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: Dimensions.get('window').height,
      width: Dimensions.get('window').width,
      routName: this.props.navigation.state.routeName,
    };
    this.interval = null;
    //debugger;
    //login page
  }
  _renderItem({item, index}) {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
      </View>
    );
  }

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Cool Photo App Camera Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the camera');
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  updateOrdersList() {
    debugger;
    if (Actions.currentScene == 'dashboard') {
      const {user} = this.props;
      let userid = user.id;
      let rolename = user.roleName;
      if (rolename != null) {
        if (rolename.indexOf('Grocerry') == 0) {
         // Alert.alert('Alert G', 'Grocerry udpated');
          this.props.getAllNewRelatedOrdersToWorker(userid);
        } else {
         // Alert.alert('Alert H', 'handy udpated');
          this.props.getAllNewRelatedHandyServicesToWorker(userid);
        }
      } else {
        Alert.alert('Error', 'rolename is null');
      }
    }
  }

  componentDidMount() {
    this.interval = setInterval(() => this.updateOrdersList(), 12000);
    //  BackgroundTimer.runBackgroundTimer(() => {
    //   //code that will be called every 3 seconds
    //   this.updateOrdersList()
    //   },
    //   5000);
  }

  componentWillUnmount() {
    //BackgroundTimer.stopBackgroundTimer()
    debugger;
    clearInterval(this.interval);
  }


  render() {
    //0088A6
    //debugger;
    const routName = this.props.navigation.state.routeName;
    const images = require('../Asset/Categoris/Fruits.jpg');
    const {user, totalOrdersCountData, relatedOrders} = this.props;
    let assImage_Http_URL = {
      uri: 'http://localhost:8089/404.png'.replace(
        'http://localhost',
        'http://10.0.2.2',
      ),
    };
    if (user.imageURL != null) {
      let imageass = user.imageURL.replace(
        'http://localhost',
        'http://10.0.2.2',
      );
      assImage_Http_URL = {uri: imageass};
    }
    return (
      <View style={styles.mainContainerDiv}>
        <CustomHeaderWithText leftIcon={true} text="Welcome Kaam Tamaam" />
        <StatusBar hidden />
        <View style={styles.contentDiv}>
          <ScrollView style={{flex: 1, margin: 5}} scrollEnabled>
            <View style={styles.thumbnailDiv}>
              <Thumbnail
                large
                source={assImage_Http_URL}
                style={{
                  height: 100,
                  width: 100,
                  borderRadius: 50,
                  overflow: 'hidden',
                  resizeMode: 'stretch',
                }}
              />
            </View>

            <View style={styles.detailMainContainer}>
              <View style={styles.detailMainContent}>
                <View style={styles.detailBasicInfo}>
                  <Text style={{fontWeight: 'bold', fontSize: 18}}>
                    {user.fullName.toUpperCase()}
                  </Text>
                  <Text style={{fontWeight: 'normal', fontSize: 16}}>
                    {user.phoneNumber}
                  </Text>
                   <Text style={{ fontWeight: 'normal', fontSize: 13,color:'green' }}> {user.roleName}</Text> 
                </View>

                <View style={styles.ordersSummaryArea}>
                  <View style={styles.ordersSummaryColumnwithBorder}>
                    <Text style={styles.ordersummaryText}>
                      {relatedOrders.Total}
                    </Text>
                    <Text>Orders Recieved</Text>
                  </View>
                  <View style={styles.ordersSummaryColumnwithBorder}>
                    <Text style={styles.ordersummaryText}>
                      {relatedOrders.Completed.length}
                    </Text>
                    <Text>Orders Delivered</Text>
                  </View>
                  <View style={styles.ordersSummaryColumnWithoutBorder}>
                    <Text style={styles.ordersummaryText}>
                      {relatedOrders.Canceled.length}
                    </Text>
                    <Text>Orders Canceled</Text>
                  </View>
                </View>

                <View style={styles.buttonsArea}>
                  <TouchableHighlight
                    style={styles.buttonStyle}
                    onPress={() =>
                      Actions.orderHistory({
                        Orders: relatedOrders.Assigned,
                        relatedOrders,
                      })
                    }>
                    <Text
                      style={{fontSize: 20, color: '#000', fontWeight: 'bold'}}>
                      {relatedOrders.Assigned.length == 0
                        ? 'MY ORDERS'
                        : `MY ORDERS (${relatedOrders.Assigned.length})`}
                    </Text>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={styles.buttonStyle}
                    onPress={() =>
                      Actions.orderHistory({
                        Orders: relatedOrders.InQuee,
                        relatedOrders,
                      })
                    }>
                    <Text
                      style={{fontSize: 20, color: '#000', fontWeight: 'bold'}}>
                      {relatedOrders.InQuee.length == 0
                        ? 'WAITING ORDERS'
                        : `WAITING ORDERS (${relatedOrders.InQuee.length})`}
                    </Text>
                  </TouchableHighlight>

                  {/* <TouchableHighlight style={styles.buttonStyle} onPress={() => Actions.orderHistory({ Orders: relatedOrders.Completed, relatedOrders })}>
                                        <Text style={{ fontSize: 20, color: '#000', fontWeight: 'bold' }}>Completed Orders</Text>
                                    </TouchableHighlight> */}
                </View>
              </View>
            </View>
          </ScrollView>
          <CustomFooter landingTo="H" isHomePage={routName == 'dashboard'} />
          {/* <CustomFooterTab cartLength={5} notifications={2} /> */}
        </View>
      </View>
    );
  }
}
const mapStateToProps = ({auth, orders, handyServices}) => {
  const {loading} = handyServices;
  const {totalOrdersCountData, relatedOrders} = orders;
  //debugger;
  return {user: auth.user, loading, totalOrdersCountData, relatedOrders};
};
export default connect(
  mapStateToProps,
  {
    UpdateUserAddressDatail,
    getAllNewRelatedOrdersToWorker,
    getAllNewRelatedHandyServicesToWorker,
  },
)(Dashboard);
//export default MyAccount;

const styles = {
  mainContainerDiv: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentDiv: {
    flex: 1,
    // backgroundColor: 'blue'
  },
  thumbnailDiv: {
    flex: 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 25,
    overflow: 'hidden',
    marginTop: 10,
    backgroundColor: 'green',
  },
  editButtonStyle: {
    width: 70,
    height: 25,
    justifyContent: 'center',
    borderColor: '#f48004',
    borderWidth: 1,
    backgroundColor: 'transparent',
  },
  editButtonTextStyle: {
    fontSize: 13,
    color: '#000',
    fontWeight: 'bold',
  },

  errorTextStyel: {
    fontSize: 17,
    alignSelf: 'center',
    color: 'red',
  },
  Imagestyle: {
    width: Dimensions.get('window').width / 1.5,
    height: Dimensions.get('window').width / 1.5,
    // borderRadius:( Dimensions.get('window').width/1.8)/2,
    resizeMode: 'contain',

    // opacity: 0.9
  },
  buttonStyle: {
    width: Dimensions.get('window').width * 0.8,
    height: 80,
    borderRadius: 80 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#f48004',
    borderWidth: 2,
    margin: 8,
  },
  buttonsArea: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginTop: 15,
  },
  detailMainContainer: {
    flex: 7.5,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: 'transparent',
  },
  detailMainContent: {
    flex: 1,
    backgroundColor: 'transparent',
    marginTop: 7,
  },
  detailBasicInfo: {
    flex: 1.4,
    alignItems: 'center',
  },
  ordersSummaryArea: {
    flex: 1.2,
    marginTop: 15,
    flexDirection: 'row',
    backgroundColor: 'transparent',
  },

  ordersSummaryColumnwithBorder: {
    flex: 3.3,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    borderRightWidth: 1.5,
    borderRightColor: '#f48004',
  },

  ordersSummaryColumnWithoutBorder: {
    flex: 3.3,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ordersummaryText: {fontSize: 18, fontWeight: 'bold'},
};
