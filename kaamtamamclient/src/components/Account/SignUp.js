import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, ScrollView, Dimensions } from 'react-native';
import { Container, Header, Content, Item, Input, Icon, Button, Spinner } from 'native-base';
import { emailsignupChanged, passwordSignupChanged, usernameChanged, phoneChanged, fullnameChanged, signupUser } from '../../actions';

import { CommonHeader } from '../Common/Header';
class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
        }
    }

    fullnamechangeSignup(txt) {
        this.props.fullnameChanged(txt);
    }

    emailhangeSignup(txt) {
        this.props.emailsignupChanged(txt);
    }

    passwordchangeSignup(txt) {
        this.props.passwordSignupChanged(txt);
    }

    phonechangeSignup(txt) {
        this.props.phoneChanged(txt);
    }

    usernamechangeSignup(txt) {
        this.props.usernameChanged(txt);
    }

    signupClicked() {
      //  debugger;
        const { email, password, fullname, phone, error, username, loading } = this.props;
        this.props.signupUser({ email, password, fullname, phone, error, username });
    }

    swapButtonAndSpinner() {
        return this.props.loading ?
            <Spinner />
            :
            <Button rounded block warnings style={{ backgroundColor: '#f48004' }}
                onPress={() => this.signupClicked()}
            >
                <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 19 }}>SING UP</Text>
            </Button>
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                <CommonHeader text="SIGNUP" styles={styles.backgroundOrange} rightButton={false} />

                <ScrollView >
                    <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignContent: 'center', margin: 20 }}>

                        <View style={{ height: this.state.height / 9.5, alignContent: 'center', justifyContent: 'center', margin: 10, alignItems: 'center' }}>
                            <Text style={{ fontSize: 22, fontWeight: 'bold' }}> Personal Details</Text>
                        </View>


                        <View style={{ marginTop: 15, flexDirection: 'row', alignItems: 'center', borderColor: 'gray', borderBottomWidth: .5 }}>
                            <Icon name='person' style={{ color: '#f48004', fontSize: 22 }} />
                            <Input placeholder='Full Name' style={{ marginLeft: 20, fontSize: 15 }}
                                onChangeText={this.fullnamechangeSignup.bind(this)}
                                value={this.props.fullname}
                            />
                        </View>

                        <View style={{ marginTop: 15, flexDirection: 'row', alignItems: 'center', borderColor: 'gray', borderBottomWidth: .5 }}>
                            <Icon name='person' style={{ color: '#f48004', fontSize: 22 }} />
                            <Input placeholder='Phone' style={{ marginLeft: 20, fontSize: 15 }}
                                onChangeText={this.phonechangeSignup.bind(this)}
                                value={this.props.phone}
                            />
                        </View>

                        <View style={{ marginTop: 15, flexDirection: 'row', alignItems: 'center', borderColor: 'gray', borderBottomWidth: .5 }}>
                            <Icon name='calendar' style={{ color: '#f48004', fontSize: 22 }} />
                            <Input placeholder='username' style={{ marginLeft: 20, fontSize: 15 }}
                                onChangeText={this.usernamechangeSignup.bind(this)}
                                value={this.props.username}
                            />
                        </View>

                        <View style={{ marginTop: 15, flexDirection: 'row', alignItems: 'center', borderColor: 'gray', borderBottomWidth: .5 }}>
                            <Icon name='mail' style={{ color: '#f48004', fontSize: 22 }} />
                            <Input placeholder='Email' style={{ marginLeft: 20, fontSize: 15 }}
                                onChangeText={this.emailhangeSignup.bind(this)}
                                value={this.props.email}
                            />
                        </View>

                        <View style={{ marginTop: 15, flexDirection: 'row', alignItems: 'center', borderColor: 'gray', borderBottomWidth: .5 }}>
                            <Icon name='lock' style={{ color: '#f48004', fontSize: 22 }} />
                            <Input placeholder='Password' style={{ marginLeft: 20, fontSize: 15 }} secureTextEntry={true}
                                onChangeText={this.passwordchangeSignup.bind(this)}
                                value={this.props.password}
                            />
                        </View>


                        <View style={{ flex: .2, marginTop: 30, alignContent: 'stretch', justifyContent: 'space-around', alignItems: 'center', marginLeft: 30, marginRight: 30 }}>
                            {/* <Button rounded block warnings style={{ backgroundColor: '#f48004' }}
                                onPress={() => this.signupClicked()}
                            >
                                <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 19 }}>SING UP</Text>
                            </Button> */
                            this.swapButtonAndSpinner()
                            }
                        </View>
                        <View style={{ height: this.state.height / 10, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                            <Text numberOfLines={2} style={{ justifyContent: 'center', alignItems: 'center', alignContent: 'center', textAlign: 'center', width: 230 }}>
                                By continuing you agree to read and confirm to our terms and conditions
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = ({ signUp }) => {
    const { email, password, fullname, phone, error, username, loading } = signUp;
    //debugger;
    return { email, password, fullname, phone, error, username, loading };
};

export default connect(mapStateToProps, { emailsignupChanged, passwordSignupChanged, usernameChanged, phoneChanged, fullnameChanged, signupUser })(SignUp);

const styles = {
    container: {
        height: 200,
        backgroundColor: '#F5FCFF'
    },
    backgroundOrange: {
        backgroundColor: 'orange'
    },
    card: {
        flex: 1,
        borderRadius: 4,
        borderWidth: 2,
        borderColor: '#E8E8E8',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    text: {
        textAlign: 'center',
        fontSize: 50,
        backgroundColor: 'transparent'
    },
    done: {
        textAlign: 'center',
        fontSize: 30,
        color: 'white',
        backgroundColor: 'transparent'
    }
};