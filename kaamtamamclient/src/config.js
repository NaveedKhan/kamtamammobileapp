
//const ROOT_URL = "http://hanif268t.nayatel.net:8091/api/";
//const ROOT_URL = "http://10.0.2.2:8080/api/";

export const ROOT_URL = "http://115.186.182.33:8080/api/";
//export const ROOT_URL = "http://10.0.2.2:59430/api/";
//https://stackoverflow.com/questions/45229579/could-not-run-adb-reverse-react-native
export const config = {
  REST_API: {
    Account: {
      SignOut: `${ROOT_URL}Auth/SignOut`,
      Login: `${ROOT_URL}Auth/LoginGrocerryDriver`,
      SignUp: `${ROOT_URL}Auth/SignUp`
    },
    Oders:{
      AddOrder:`${ROOT_URL}Orders/AddOrder`,
      GetOrderByUserID:`${ROOT_URL}Orders/GetOrderByUserID/`,
      ChnageOrderStatus:`${ROOT_URL}Orders/ChnageOrderStatus/`,
      AssignOrderToDriver:`${ROOT_URL}Orders/AssignOrderToDriver`,
      ReEstimatedOrder:`${ROOT_URL}Orders/ReEstimatedOrder`,
      MarkOrderCompleted: `${ROOT_URL}Orders/MarkOrderCompleteByWorker/`,
      MarkOrderCancelByWorker:`${ROOT_URL}Orders/MarkOrderCancelByWorker/`,  
      GetAllNewOrdersRelatedToWorker:`${ROOT_URL}Orders/GetAllNewOrdersRelatedToWorker/`  
    },
    Test: {
      GetSimple: `${ROOT_URL}values`,
    },
    Notifications:{
      GetNotByDriverID:`${ROOT_URL}GrocNotification/GetNotByDriverID`,
      GetNotByUserID:`${ROOT_URL}GrocNotification/GetNotByUserID`,
      MarkNoificationRead: `${ROOT_URL}GrocNotification/MarkNoificationReadByWorker`,
    },
    ServiceRequest: {
      AddServiceRequest: `${ROOT_URL}Handy_ServiceRequest/`,
      GetServiceRequests: `${ROOT_URL}Handy_ServiceRequest/GetNotByUserID`,
      AssignHandyServiceToTechny: `${ROOT_URL}Handy_ServiceRequest/AssignHandyServiceToTechny`,
      ChangeHandyRequestStatus: `${ROOT_URL}Handy_ServiceRequest/ChangeHandyRequestStatus`,
      MarkServiceRequestCompleteByUser: `${ROOT_URL}Handy_ServiceRequest/MarkServiceRequestCompleteByWorker`,
      CancelServiceRequestByUser: `${ROOT_URL}Handy_ServiceRequest/CancelServiceRequestByWorker`,
      ReEstimatedOrder:`${ROOT_URL}Handy_ServiceRequest/ReEstimatedOrder`,
      GetAllRelatedRequestsOfWorker:`${ROOT_URL}Handy_ServiceRequest/GetAllRelatedRequestsOfWorker/`
    }
  }
};