import { PRODUCT_LIST_ITEM_SELECTED,PRODUCT_LIST_ADD_ITEM,PRODUCT_LIST_CART_CLICKED,PRODUCT_LIST_REMOVE_ITEM ,
    PRODUCT_CART_ADD_ITEM,PRODUCT_CART_REMOVE_ITEM
} from './types';
import { Actions } from 'react-native-router-flux';

export const AddItem = (text) =>{
    return (dispatch)=>{
        dispatch({type:PRODUCT_LIST_ADD_ITEM, payload:text});
        dispatch({type:PRODUCT_CART_ADD_ITEM, payload:text});
}};


export const RemoveItem = (text) =>{
    return (dispatch )=>
    {
     dispatch({ type:PRODUCT_LIST_REMOVE_ITEM, payload:text  });
     dispatch({type:PRODUCT_CART_REMOVE_ITEM, payload:text});
    
    }
};



export const OnItemClick = (productid,categoryname) =>{
    //debugger;
        return (dispatch)=>{
                // console.log(name,phone,shift);
                 dispatch({type:PRODUCT_LIST_ITEM_SELECTED, payload:{catid:productid,catname:categoryname}})
                 console.log('data fetched');
                // Actions.employeeList({type:"reset"});             
    };
};

export const OnCartClick = () =>{
    //debugger;
        return (dispatch)=>{
                // console.log(name,phone,shift);
                 dispatch({type:PRODUCT_LIST_CART_CLICKED})
                 console.log('data fetched');
                // Actions.employeeList({type:"reset"});             
    };
};