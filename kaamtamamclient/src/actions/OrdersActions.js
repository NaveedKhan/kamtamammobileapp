import {
  ASSIGN_ORDER,
  CANCEL_ORDER,
  COMPLETE_ORDER,
  UPDATE_ORDER_COUNT,
  UPDATE_PAYMENT,
  ASSIGNED_ORDER,
  ASSIGNING_ORDER,
  ASSIGNING_ORDER_FAILED,
  NOTIFICATION_DETAIL_UPDATE,
  RELATED_ORDERS,
  CHANGE_ORDER_STATUS,
  CHANGE_ORDER_STATUS_FALIED,
  CHANGE_ORDER_STATUS_SUCCESS,
} from './types';
import axios from 'axios';
import {config} from '../config';
import {Actions} from 'react-native-router-flux';
import firebase from 'firebase';
import {Alert} from 'react-native';

export const markOrderCancel = reviewModel => {
  debugger;
  let configs = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
  var me = this;

  const {REST_API} = config;
  return dispatch => {
    dispatch({
      type: CHANGE_ORDER_STATUS,
    });
    try {
      const response = axios
        .post(REST_API.Oders.MarkOrderCancelByWorker, reviewModel, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          if (response.status == 200) {
            chnageOrderStatusSucess(dispatch, baseModel.data);
          } else {
            chnageOrderStatusFail(dispatch, baseModel.message);
          }
        })
        .catch(error => {
          debugger;
          var err = String(error) == '' ? 'no error ' : String(error);
          chnageOrderStatusFail(dispatch, err);
        });
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      chnageOrderStatusFail(dispatch, err);
    }
  };
};

export const markOrderComplete = reviewModel => {
  debugger;

  let configs = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
  var me = this;
  const {REST_API} = config;
  return dispatch => {
    dispatch({
      type: CHANGE_ORDER_STATUS,
    });
    try {
      const response = axios
        .post(REST_API.Oders.MarkOrderCompleted, reviewModel, configs)
        .then(response => {
          debugger;
          const baseModel = response.data;
          if (response.status == 200) {
            chnageOrderStatusSucess(dispatch, baseModel.data);
          } else {
            chnageOrderStatusFail(dispatch, baseModel.message);
          }
        })
        .catch(error => {
          var err = String(error) == '' ? 'no error ' : String(error);
          chnageOrderStatusFail(dispatch, err);
        });
    } catch (error) {
      debugger;
      console.log('error', error);
      var err = String(error) == '' ? 'no error ' : String(error);
      chnageOrderStatusFail(dispatch, err);
    }
  };
};




export const getAllNewRelatedOrdersToWorker = (workerId) => {

  //const orderModel = {OrderID: orderId, OrderStatusID: orderstatusid};
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };

  return dispatch => {
    dispatch({
      type: CHANGE_ORDER_STATUS,
    });
    const {REST_API} = config;
    axios
      .get(`${REST_API.Oders.GetAllNewOrdersRelatedToWorker}${workerId}`, apiconfig)
      .then(response => {
       // debugger;
        const baseModel = response.data;
        if (response.status == 200) {
          getAllNewOrdersSucess(dispatch, baseModel.data);
        } else {
          getAllNewOrdersFail(dispatch, baseModel.message? baseModel.message : 'No error');
        }
      })
      .catch(error => {
     // debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        getAllNewOrdersFail(dispatch, err);
        console.log('error', error);
      });
  };
};



export const changeOrderStatus = (orderId, orderstatusid) => {
  //debugger;
  //return {  type: ASSIGN_ORDER,   payload: text };
  const orderModel = {OrderID: orderId, OrderStatusID: orderstatusid};
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };

  return dispatch => {
    dispatch({
      type: CHANGE_ORDER_STATUS,
    });
    const {REST_API} = config;
    axios
      .post(REST_API.Oders.ChnageOrderStatus, orderModel, apiconfig)
      .then(response => {
        //debugger;
        const baseModel = response.data;
        if (response.status == 200) {
          chnageOrderStatusSucess(dispatch, baseModel.data);
        } else {
          chnageOrderStatusFail(dispatch, baseModel.message);
        }
      })
      .catch(error => {
        //debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        chnageOrderStatusFail(dispatch, err);
        console.log('error', error);
      });
  };
};

export const assignOrder = (
  orderId,
  userDriverId,
  relatedOrders,
  userCurrentLat,
  userCurrentLong,
  workerTrackingId,
) => {
  //debugger;
  var coored = {longitude: userCurrentLat, latitude: userCurrentLong};
  const orderModel = {OrderID: orderId, OrderTakenByID: userDriverId};
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };

  return dispatch => {
    dispatch({
      type: ASSIGNING_ORDER,
    });
    const {REST_API} = config;
    axios
      .post(REST_API.Oders.AssignOrderToDriver, orderModel, apiconfig)
      .then(response => {
        //debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          //debugger;

          orderAssignedSucess(
            dispatch,
            baseModel.data,
            orderId,
            coored,
            workerTrackingId,
          );
        } else {
          orderAssignedFail(
            dispatch,
            baseModel.message ? baseModel.message : 'error occured',
          );
        }
      })
      .catch(error => {
        //debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        orderAssignedFail(dispatch, err);
        console.log('error', error);
      });
  };
};

export const completeOrder = text => {
  //debugger;
  return {
    type: COMPLETE_ORDER,
    payload: text,
  };
};

addDataIntoFirebase = (dispatch, newCoordinate, workerTrackingId) => {
  let lat = newCoordinate.latitude;
  let longs = newCoordinate.longitude;

  var me = this;

  firebase
    .database()
    .ref('DriverLocation/' + workerTrackingId)
    .update({
      userLat: lat,
      userLong: longs,
      OrderId: '',
    })
    .then(() => {})
    .catch(error => {
      var err = String(error) == '' ? 'no error ' : String(error);
      orderAssignedFail(dispatch, err);
      //me.showToast("an error occured, try again");
    });

};

const chnageOrderStatusFail = (dispatch, error) => {
  //debugger;
  dispatch({
    type: CHANGE_ORDER_STATUS_FALIED,
  });

  Alert.alert('alert!', 'something went wrong! ' + error);
};

const chnageOrderStatusSucess = (dispatch, data) => {
  //debugger;
  dispatch({
    type: CHANGE_ORDER_STATUS_SUCCESS,
  });

  // var UserRole = data.user.roleName;
  // if (UserRole == "GrocerryDriver") {
  var relatedOrders = data.relatedOrders;
  var assigned = relatedOrders.filter(
    aa =>
      aa.orderStatus == 'Processing' ||
      aa.orderStatus == 'On The Way' ||
      aa.orderStatus == 'Reached Destination',
  ); //.length;
  var completed = relatedOrders.filter(aa => aa.orderStatus == 'Delivered'); //.length;
  var inQuee = relatedOrders.filter(aa => aa.orderStatus == 'Initiated'); //.length;
  var canceled = relatedOrders.filter(aa => aa.orderStatus == 'Canceled'); //.length;

  var countmodel = {
    Assigned: assigned,
    Completed: completed,
    InQuee: inQuee,
    Total: relatedOrders.length,
    Canceled: canceled,
  };

  dispatch({
    type: RELATED_ORDERS,
    payload: countmodel,
  });

  dispatch({
    type: UPDATE_ORDER_COUNT,
    payload: countmodel,
  });

  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: {
      prop: 'unReadNotificationCount',
      value: data.unReadnotificationCount,
    },
  });
  Alert.alert(
    'Notification!',
    'You have successfully changed the Order status ',
    [
      {
        text: 'OK',
        onPress: () => {
          Actions.refresh({Orders: assigned, relatedOrders: countmodel});
        },
      },
    ],
    {cancelable: false},
  );
};

const orderAssignedSucess = (
  dispatch,
  data,
  orderId,
  coored,
  workerTrackingId,
) => {
  // var relatedOrders =  relatedOrderss;

  this.addDataIntoFirebase(dispatch, coored, workerTrackingId);

  //var relatedOrders =  data.relatedOrderss.relatedOrders;
  var assigned = data.relatedOrders.filter(
    aa =>
      aa.orderStatus == 'Processed' ||
      aa.orderStatus == 'Processing' ||
      aa.orderStatus == 'On The Way' ||
      aa.orderStatus == 'Reached Destination',
  ); //.length;
  var completed = data.relatedOrders.filter(
    aa => aa.orderStatus == 'Delivered',
  ); //.length;
  var inQuee = data.relatedOrders.filter(aa => aa.orderStatus == 'Initiated'); //.length;
  var canceled = data.relatedOrders.filter(aa => aa.orderStatus == 'Canceled'); //.length;

  var countmodel = {
    Assigned: assigned,
    Completed: completed,
    InQuee: inQuee,
    Total: data.relatedOrders.length,
    Canceled: canceled,
  };

  dispatch({
    type: RELATED_ORDERS,
    payload: countmodel,
  });

  dispatch({
    type: UPDATE_ORDER_COUNT,
    payload: countmodel,
  });

  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: {
      prop: 'unReadNotificationCount',
      value: data.unReadnotificationCount,
    },
  });

  //debugger;
  Alert.alert(
    'Notification!',
    'This service is assigned to you now. Please start working on that ',
    [
      {
        text: 'OK',
        onPress: () =>
          Actions.refresh({Orders: assigned, relatedOrders: countmodel}),
      },
    ],
    {cancelable: false},
  );
};

const orderAssignedFail = (dispatch, error) => {
  //debugger;
  dispatch({
    type: ASSIGNING_ORDER_FAILED,
    payload: error,
  });
  Alert.alert('soneting went wrong', error);
};


const getAllNewOrdersFail = (dispatch, error) => {
  //debugger;
  dispatch({
    type: ASSIGNING_ORDER_FAILED,
    payload: error,
  });
  Alert.alert('sonething went wrong.o', error);
};

const getAllNewOrdersSucess = (
  dispatch,
  data,
) => {


  var assigned = data.relatedOrders.filter(
    aa =>
      aa.orderStatus == 'Processed' ||
      aa.orderStatus == 'Processing' ||
      aa.orderStatus == 'On The Way' ||
      aa.orderStatus == 'Reached Destination',
  ); //.length;
  var completed = data.relatedOrders.filter(
    aa => aa.orderStatus == 'Delivered',
  ); //.length;
  var inQuee = data.relatedOrders.filter(aa => aa.orderStatus == 'Initiated'); //.length;
  var canceled = data.relatedOrders.filter(aa => aa.orderStatus == 'Canceled'); //.length;

  var countmodel = {
    Assigned: assigned,
    Completed: completed,
    InQuee: inQuee,
    Total: data.relatedOrders.length,
    Canceled: canceled,
  };
  
  dispatch({
    type: CHANGE_ORDER_STATUS_SUCCESS,
  });
  dispatch({
    type: RELATED_ORDERS,
    payload: countmodel,
  });

  dispatch({
    type: UPDATE_ORDER_COUNT,
    payload: countmodel,
  });

};
