
import {
  SIGNUP_EMAIL_CHANGED, SIGNUP_FAILED, SIGNUP_FULLNAME_CHANGED,
  SIGNUP_PASSWORD_CHANGED, SIGNUP_PHONE_CHANGED, SIGNUP_START,
  SIGNUP_SUCCESSFUL, SIGNUP_USERNAME_CHANGED, LANDING_DATAFECTHED
} from './types';
import axios from 'axios';
import { config } from '../config';
import { Actions } from 'react-native-router-flux';

export const emailsignupChanged = (text) => {
  //debugger;
  return {
    type: SIGNUP_EMAIL_CHANGED,
    payload: text
  };
};

export const passwordSignupChanged = (text) => {
  return {
    type: SIGNUP_PASSWORD_CHANGED,
    payload: text
  };
};

export const usernameChanged = (text) => {
  return {
    type: SIGNUP_USERNAME_CHANGED,
    payload: text
  };
};

export const fullnameChanged = (text) => {
  return {
    type: SIGNUP_FULLNAME_CHANGED,
    payload: text
  };
};

export const phoneChanged = (text) => {
  return {
    type: SIGNUP_PHONE_CHANGED,
    payload: text
  };
};


export const signupUser = ({ email, password, fullname, phone, error, username }) => {
  //debugger;
  const SignupModel = { UserName: username, PhoneNumber: phone, Email: email, Password: password, FullName: fullname };
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8'
    }
  };

  return (dispatch) => {
    dispatch({
      type: SIGNUP_START
    });
    const { REST_API } = config;
    axios.post(REST_API.Account.SignUp, SignupModel, apiconfig)
      .then(response => {
        //debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          signupUserSucess(dispatch, baseModel.data);
        } else {
          signupUserFail(dispatch, baseModel.data, error)
        }

        //Actions.tendersResult({TendersList:baseModel.data});
      }).catch(error => {
        //debugger;
        signupUserFail(dispatch, baseModel.data, error)
        console.log("error", error)
      });
    //Actions.landingPage();
  }

};

const signupUserSucess = (dispatch, productsdata) => {
  //debugger;
  dispatch({
    type: SIGNUP_SUCCESSFUL,
    payload: productsdata
  });
  dispatch({
    type: LANDING_DATAFECTHED,
    payload: productsdata
  });

  //debugger;
  Actions.landingPage();
};

const signupUserFail = (dispatch, error) => {
  //debugger;
  dispatch({
    type: SIGNUP_FAILED,
    payload: error,
  });
  //Actions.employeeList();
};
