import {
  HANDY_SERVICES_ASSIGN,
  HANDY_SERVICES_ASSIGN_FALIED,
  HANDY_SERVICES_ASSIGN_SUCCESS,
  HANDY_SERVICES_STATUS_CHNAGES,
  HANDY_SERVICES_STATUS_CHNAGES_FAILED,
  HANDY_SERVICES_STATUS_CHNAGES_SUCCESS,
  HANDY_SERVICES_UPDATE_ANY_DATA,
  NOTIFICATION_DETAIL_UPDATE,
  RELATED_ORDERS,
  UPDATE_ORDER_COUNT,
} from './types';
import {Actions} from 'react-native-router-flux';
import {Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import firebase from 'firebase';
import {config} from '../config';

export const handyServiceUpdateData = ({prop, value}) => {
  return {
    type: HANDY_SERVICES_UPDATE_ANY_DATA,
    payload: {prop, value},
  };
};

export const markCompleteHandyService = reviewModel => {
  debugger;
  // const { Order } = this.props;
  //const { reviewComment, reviewScore, selectedOrderToComplete } = this.state;
  let configs = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
  var me = this;
  // this.setState({ modalButtonLoading: true });
  const {REST_API} = config;
  return dispatch => {
    dispatch({type: HANDY_SERVICES_STATUS_CHNAGES});
    try {
      const response = axios
        .post(
          REST_API.ServiceRequest.MarkServiceRequestCompleteByUser,
          reviewModel,
          configs,
        )
        .then(response => {
          debugger;
          const baseModel = response.data;
          if (baseModel.success) {
            // if (response.status == 200) {
            serviceAssignSuccess(dispatch, baseModel.data, true);
          } else {
            serviceAssignFail(dispatch, baseModel.message);
          }
        })
        .catch(error => {
          debugger;
          console.log('error', error);
          var err = String(error) == '' ? 'no error ' : String(error);
          // Alert.alert('error',err);
          serviceAssignFail(dispatch, err);
        });

      //productFetchingSucess(dispatch, response.data);
    } catch (error) {
      debugger;
      var err = String(error) == '' ? 'no error ' : String(error);
      console.log('error', error);
      serviceAssignFail(dispatch, err);
      //productFetchingFail(dispatch);
    }
  };
};

export const markCancelHandyService = reviewModel => {
  debugger;

  let configs = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
  var me = this;
  //this.setState({ modalButtonLoading: true });
  const {REST_API} = config;
  return dispatch => {
    dispatch({type: HANDY_SERVICES_STATUS_CHNAGES});
    try {
      const response = axios
        .post(
          REST_API.ServiceRequest.CancelServiceRequestByUser,
          reviewModel,
          configs,
        )
        .then(response => {
          debugger;
          //const baseModel = response.data;
          const baseModel = response.data;
          if (baseModel.success) {
            // if (response.status == 200) {
            serviceAssignSuccess(dispatch, baseModel.data, true);
          } else {
            serviceAssignFail(dispatch, baseModel.message);
          }
        })
        .catch(error => {
          debugger;
          console.log('error', error);
          var err = String(error) == '' ? 'no error ' : String(error);
          serviceAssignFail(dispatch, err);
        });
    } catch (error) {
      debugger;
      console.log('error', error);
      var err = String(error) == '' ? 'no error ' : String(error);
      serviceAssignFail(dispatch, err);
    }
  };
};


export const getAllNewRelatedHandyServicesToWorker = (workerId) => {

  //const orderModel = {OrderID: orderId, OrderStatusID: orderstatusid};
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };

  return dispatch => {
    dispatch({
      type: HANDY_SERVICES_STATUS_CHNAGES,
    });
    const {REST_API} = config;
    axios
      .get(`${REST_API.ServiceRequest.GetAllRelatedRequestsOfWorker}${workerId}`, apiconfig)
      .then(response => {
       // debugger;
        const baseModel = response.data;
        if (response.status == 200) {
          getAllNewRequestsSucess(dispatch, baseModel.data);
        } else {
          getAllNewRequestsFail(dispatch, baseModel.message);
        }
      })
      .catch(error => {
     // debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        getAllNewRequestsFail(dispatch, err);
        console.log('error', error);
      });
  };
};



addDataIntoFirebase = (dispatch, newCoordinate,workerTrackingId) => {
  let lat = newCoordinate.latitude;
  debugger;
  let longs = newCoordinate.longitude;

  var me = this;

  firebase
    .database()
    .ref('DriverLocation/' + workerTrackingId)
    .update({
      userLat: lat,
      userLong: longs,
      OrderId: '',
    })
    .then(() => {})
    .catch(error => {
      var err = String(error) == '' ? 'no error ' : String(error);
      orderAssignedFail(dispatch, err);
      //me.showToast("an error occured, try again");
    });
};

export const asigneHandyServiceRequest = (
  orderid,
  driverid,
  lat,
  long,
  workerTrackingId,
) => {
  debugger;
  const orderModel = {ServiceRequestID: orderid, ServiceTakenBy: driverid};
  let configs = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
  return dispatch => {
    dispatch({type: HANDY_SERVICES_ASSIGN});
    const {REST_API} = config;

    axios
      .post(
        REST_API.ServiceRequest.AssignHandyServiceToTechny,
        orderModel,
        configs,
      )
      .then(response => {
        // debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          // if (response.status == 200) {
          serviceAssignSuccess(
            dispatch,
            baseModel.data,
            false,
            lat,
            long,
            workerTrackingId,
          );
        } else {
          serviceAssignFail(dispatch, baseModel.message);
        }
      })
      .catch(error => {
        ////debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        serviceAssignFail(dispatch, err);
        console.log('error', error);
      });
  };
};

export const changeHandyServiceStatus = (orderid, statusID) => {
  //debugger;
  //const orderModel = { OrderID: orderId, OrderStatusID: orderstatusid };
  const orderModel = {ServiceRequestID: orderid, ServiceStatusID: statusID};
  let configs = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
  return dispatch => {
    dispatch({type: HANDY_SERVICES_STATUS_CHNAGES});
    const {REST_API} = config;

    axios
      .post(
        REST_API.ServiceRequest.ChangeHandyRequestStatus,
        orderModel,
        configs,
      )
      .then(response => {
        //debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          // if (response.status == 200) {
          serviceAssignSuccess(dispatch, baseModel.data, true);
        } else {
          serviceAssignFail(dispatch, baseModel.message);
        }
      })
      .catch(error => {
        //debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        serviceAssignFail(dispatch, err);
        console.log('error', error);
      });
  };
};

export const fetchingServices = () => {
  //debugger;
  let configs = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
  return dispatch => {
    dispatch({type: HANDY_SERVICES_FETCHING});
    const {REST_API} = config;

    axios
      .get(REST_API.Test.GetProducts, configs)
      .then(response => {
        //debugger;
        const baseModel = response.data;
        //if (baseModel.success) {
        if (response.status == 200) {
          serviceFetchingSucess(dispatch, response.data);
        } else {
          serviceFetchingFail(
            dispatch,
            baseModel.message ? baseModel.message : 'no error',
          );
        }
      })
      .catch(error => {
        //debugger;
        var err = String(error) == '' ? 'no error ' : String(error);
        // serviceAssignFail(dispatch, err);
        serviceFetchingFail(dispatch, err);
        console.log('error', error);
      });
  };
};

const serviceFetchingSucess = (dispatch, productData) => {
  //debugger;
  dispatch({
    type: HANDY_SERVICES_FETECHED_SUCCESS,
    payload: productData,
  });
  // dispatch({
  //     type: LANDING_DATAFECTHED,
  //     payload: productData
  // });
  Actions.splash2();
};

const serviceFetchingFail = (dispatch, error) => {
  //debugger;
  dispatch({
    type: HANDY_SERVICES_FETECHED_FAILED,
    payload: error,
  });
  // Actions.employeeList();

  Alert.alert('Alert', error);
};

const serviceAssignSuccess = (
  dispatch,
  data,
  isStatusChanged,
  lat,
  long,
  workerTrackingId,
) => {
  //debugger;
  dispatch({
    type: HANDY_SERVICES_ASSIGN_SUCCESS,
  });
  if (!isStatusChanged) {
    var coored = {longitude: long, latitude: lat};
    this.addDataIntoFirebase(dispatch, coored, workerTrackingId);
  }

  var relatedOrders = data.relatedOrders;
  var assigned = relatedOrders.filter(
    aa =>
      aa.serviceStatusName == 'Processing' ||
      aa.serviceStatusName == 'Work In Progress' ||
      aa.serviceStatusName == 'On The Way' ||
      aa.serviceStatusName == 'Items Collected' ||
      aa.serviceStatusName == 'Estimating',
  ); //.length;
  var completed = relatedOrders.filter(
    aa => aa.serviceStatusName == 'Delivered',
  ); //.length;
  var inQuee = relatedOrders.filter(aa => aa.serviceStatusName == 'Initiated'); //.length;
  var canceled = relatedOrders.filter(aa => aa.serviceStatusName == 'Canceled'); //.length;

  var countmodel = {
    Assigned: assigned,
    Completed: completed,
    InQuee: inQuee,
    Total: relatedOrders.length,
    Canceled: canceled,
  };

  dispatch({
    type: RELATED_ORDERS,
    payload: countmodel,
  });

  dispatch({
    type: UPDATE_ORDER_COUNT,
    payload: countmodel,
  });

  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: {
      prop: 'unReadNotificationCount',
      value: data.unReadnotificationCount,
    },
  });

  Alert.alert(
    'Notification!',
    isStatusChanged
      ? 'You have successfully changed the status '
      : 'This service is assigned to you now. Please start working.',
    [
      {
        text: 'OK',
        onPress: () => {
          Actions.refresh({Orders: assigned, relatedOrders: countmodel});
        },
      },
    ],
    {cancelable: false},
  );
};

const serviceAssignFail = (dispatch, error) => {
  //debugger;
  dispatch({
    type: HANDY_SERVICES_ASSIGN_FALIED,
    payload: error,
  });
  // Actions.employeeList();

  Alert.alert('something went wrong', error);
};




const getAllNewRequestsFail = (dispatch, error) => {
  //debugger;
  dispatch({
    type: HANDY_SERVICES_ASSIGN_FALIED,
    payload: error,
  });
  Alert.alert('something went wrong.', error);
};

const getAllNewRequestsSucess = (
  dispatch,
  data,
) => {

  var relatedOrders = data.relatedOrders;
  var assigned = relatedOrders.filter(aa => aa.serviceStatusName == "Processing" || aa.serviceStatusName == "Work In Progress"  || aa.serviceStatusName == "On The Way" 
  || aa.serviceStatusName == "Items Collected"  || aa.serviceStatusName == "Estimating"  );//.length;
  var completed = relatedOrders.filter(aa => aa.serviceStatusName == "Delivered");//.length;
  var inQuee = relatedOrders.filter(aa => aa.serviceStatusName == "Initiated");//.length;
  var canceled = relatedOrders.filter(aa => aa.serviceStatusName == "Canceled");//.length;

  var countmodel = {
    Assigned: assigned,
    Completed: completed,
    InQuee: inQuee,
    Total: relatedOrders.length,
    Canceled: canceled
  };

  dispatch({
    type: HANDY_SERVICES_ASSIGN_SUCCESS,
  });
  dispatch({
    type: RELATED_ORDERS,
    payload: countmodel
  });

  dispatch({
    type: UPDATE_ORDER_COUNT,
    payload: countmodel
  });

};