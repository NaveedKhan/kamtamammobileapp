//import firebase from 'firebase';
import {
    NOTIFICATION_DETAIL_UPDATE
} from './types';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { config } from '../config';

export const UpdateNotificationData = ({ prop, value }) => {
    debugger;
    return {
      type: NOTIFICATION_DETAIL_UPDATE,
      payload: { prop, value }
    }
  };