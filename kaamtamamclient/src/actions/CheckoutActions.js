import {
  PRODUCT_CHECKOUT_ADDRESS_SELECTED, PRODUCT_CHECKOUT_CONFIRM_CLICKED,
  PRODUCT_CHECKOUT_ADD_ADDRESS_CLICKED
} from './types';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import {Alert} from 'react-native'; 
import { config } from '../config';


const showAlert = () => {
  return Alert.alert(
    'Done Done',
    'your order is submitted and you will recieve notification',
    [
      { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
      { text: 'OK', onPress: () => Actions.reset('landingPage') },
    ],
    { cancelable: false }
  )
}

export const confirmButtonClicked = (id, ordersList) => {
var me = this;
  const OrderdetailModel = { UserId: id, OrderVM: ordersList };
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8'
    }
  };

  return (dispatch) => {
    dispatch({
      type: PRODUCT_CHECKOUT_CONFIRM_CLICKED
    });

    const { REST_API } = config;
    axios.post(REST_API.Oders.AddOrder, OrderdetailModel, apiconfig)
      .then(response => {
      //  debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          Alert.alert(
            'Done Done',
            'your order is submitted and you will recieve notification',
            [
              { text: 'OK', onPress: () => Actions.reset('landingPage') },
            ],
            { cancelable: false }
          )
        } else {
          //loginUserFail(dispatch, error)
        }

        //Actions.tendersResult({TendersList:baseModel.data});
      }).catch(error => {
       // debugger;
        // loginUserFail(dispatch, error)
        console.log("error", error)
      });

  }
}

export const addressChanged = (text) => {
  //debugger;
  return {
    type: PRODUCT_CHECKOUT_ADDRESS_SELECTED,
    payload: text
  };
};