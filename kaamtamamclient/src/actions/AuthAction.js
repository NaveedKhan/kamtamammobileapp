
import {
  EMAIL_CHANGED, PASSWORD_CHANGED, LOGIN_USER_SUCCESSFUL, LOGIN_USER, NOTIFICATION_DETAIL_UPDATE,
  RELATED_ORDERS, UPDATE_ORDER_COUNT, LOGIN_USER_FAIL, LANDING_DATAFECTHED, USER_DETAIL_UPDATE, LOGOUT_USER,
  LOGOUT_USER_FAIL,UPDATE_USER_ADDRESS_DETAIL,
  LOGOUT_USER_SUCCESSFUL,
} from './types';
import {Alert} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info';
import axios from 'axios';
import { config } from '../config';
import { Form } from 'native-base';

export const UpdateUserFormData = ({ prop, value }) => {
 // debugger;
  return {
    type: USER_DETAIL_UPDATE,
    payload: { prop, value }
  }
};


export const UpdateUserAddressDatail = ({ prop, value }) => {
 // debugger;
  return {
    type: UPDATE_USER_ADDRESS_DETAIL,
    payload: { prop, value }
  }
};

getdeviceId = async (dispatch) => {
  //Getting the Unique Id from here
  var id = DeviceInfo.getUniqueId();
  //this.setState({ deviceId: id, });

  dispatch({
    type: USER_DETAIL_UPDATE,
    payload: {prop: 'DeviceID', value: id},
  });
};


export const logOut = () => {
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };

  return dispatch => {
    dispatch({
      type: LOGOUT_USER,
    });
    const { REST_API } = config;
    axios
      .get(REST_API.Account.SignOut, apiconfig)
      .then(response => {
       // debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          logOutUserSucess(dispatch);
        } else {
          Alert.alert('error',baseModel.message);
        }
      })
      .catch(error => {
       // debugger;
       var err= String(error) == '' ? 'no error ' : String(error);
        Alert.alert('error',err);
        console.log('error', error);
      });
  };
};

export const loginUser = ({ email, password,token,deviceId }) => {

  const SigninModel = { UserName: email, Password: password ,DeviceTocken:token,DeviceID:deviceId};
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8'
    }
  };

  return (dispatch) => {
    dispatch({
      type: LOGIN_USER
    });
    const { REST_API } = config;
    axios.post(REST_API.Account.Login, SigninModel, apiconfig)
      .then(response => {
        //debugger;
        const baseModel = response.data;
        if (baseModel.success) {
          loginUserSucess(dispatch, baseModel.data, SigninModel);
        } else {
          loginUserFail(dispatch, baseModel.message)
        }
      }).catch(error => {
       // debugger;
       var err= String(error) == '' ? 'no error ' : String(error);
        loginUserFail(dispatch, err)
        console.log("error", error)
      });
  }
};

storeData = async (key, value) => {
  // debugger;
  try {
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    // saving error
  }
}

removeItemValue = async (key) => {
  try {
    await AsyncStorage.removeItem(key);
    return true;
  }
  catch (exception) {
    return false;
  }
}

const loginUserSucess = (dispatch, data, SigninModel) => {
 // debugger;
  dispatch({
    type: LOGIN_USER_SUCCESSFUL,
    payload: data.user
  });

  var UserRole = data.user.roleName;
  if(UserRole != null)
  {
    if (UserRole.indexOf('Grocerry') == 0)
    {
      var relatedOrders = data.relatedOrders;
      var assigned = relatedOrders.filter(aa => aa.orderStatus == "Processing" || aa.orderStatus == "On The Way" 
      || aa.orderStatus == "Reached Destination" );//.length;
      var completed = relatedOrders.filter(aa => aa.orderStatus == "Delivered");//.length;
      var inQuee = relatedOrders.filter(aa => aa.orderStatus == "Initiated");//.length;
      var canceled = relatedOrders.filter(aa => aa.orderStatus == "Canceled");//.length;
  
      var countmodel = {
        Assigned: assigned,
        Completed: completed,
        InQuee: inQuee,
        Total: relatedOrders.length,
        Canceled: canceled
      };
  
      dispatch({
        type: RELATED_ORDERS,
        payload: countmodel
      });
  
      dispatch({
        type: UPDATE_ORDER_COUNT,
        payload: countmodel
      });
    }else
    {
      var relatedOrders = data.relatedOrders;
      var assigned = relatedOrders.filter(aa => aa.serviceStatusName == "Processing" || aa.serviceStatusName == "Work In Progress"  || aa.serviceStatusName == "On The Way" 
      || aa.serviceStatusName == "Items Collected"  || aa.serviceStatusName == "Estimating"  );//.length;
      var completed = relatedOrders.filter(aa => aa.serviceStatusName == "Delivered");//.length;
      var inQuee = relatedOrders.filter(aa => aa.serviceStatusName == "Initiated");//.length;
      var canceled = relatedOrders.filter(aa => aa.serviceStatusName == "Canceled");//.length;
  
      var countmodel = {
        Assigned: assigned,
        Completed: completed,
        InQuee: inQuee,
        Total: relatedOrders.length,
        Canceled: canceled
      };
  
      dispatch({
        type: RELATED_ORDERS,
        payload: countmodel
      });
  
      dispatch({
        type: UPDATE_ORDER_COUNT,
        payload: countmodel
      });
    }
  }
  // if (UserRole == "GrocerryDriver") {
 
  // }
  // else {
  

  // }


  dispatch({
    type: NOTIFICATION_DETAIL_UPDATE,
    payload: { prop: 'unReadNotificationCount', value: data.unReadnotificationCount }
  });
  this.getdeviceId(dispatch);
  this.storeData('userData', JSON.stringify(SigninModel));
  Actions.reset('userProfile');
};




const logOutUserSucess = dispatch => {
  // debugger;
  dispatch({
    type: LOGOUT_USER_SUCCESSFUL,
  });
  // dispatch({
  //   type: LANDING_DATAFECTHED,
  //   payload: user
  // });
  this.removeItemValue('userData')
  Actions.reset('login');
};

const loginUserFail = (dispatch, error) => {
  //debugger;
  dispatch({
    type: LOGIN_USER_FAIL,
    payload: error
  });
  this.removeItemValue('userData')
  Alert.alert('Alert!', error);
  //Actions.landingPage();
};



