export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESSFUL = 'login_successful';
export const LOGIN_USER_FAIL = 'login_fail';
export const LOGIN_USER = 'login_user';
export const LOGOUT_USER = 'logout_user';
export const LOGOUT_USER_SUCCESSFUL = 'logout_user_success';
export const LOGOUT_USER_FAIL = 'logout_user_fail';
export const USER_DETAIL_UPDATE = 'user_detail_update';

export const SIGNUP_EMAIL_CHANGED = 'signup_email_changed';
export const SIGNUP_USERNAME_CHANGED = 'signup_username_changed';
export const SIGNUP_PASSWORD_CHANGED = 'signup_password_changed';
export const SIGNUP_PHONE_CHANGED = 'signup_phone_changed';
export const SIGNUP_FULLNAME_CHANGED = 'signup_fullname_changed';
export const SIGNUP_START = 'signup_start';
export const SIGNUP_SUCCESSFUL = 'signup_successful';
export const SIGNUP_FAILED = 'signup_failed';

export const UPDATE_USER_ADDRESS_DETAIL = 'update_user_address_detail'; 
export const NOTIFICATION_DETAIL_UPDATE = 'notification_detail_update';

export const EMPLOYEE_UPDATE = 'employee_update';
export const EMPLOYEE_CREATE = 'employee_create';

export const LANDING_DATAFECTHED = 'landing_datfetched';
export const LANDING_DATAFECTHING = 'landing_datafetching';
export const LANDING_CATEGORY_SELECTED = 'landing_category_selected';

export const PRODUCT_LIST_ADD_ITEM = 'product_list_add_item';
export const PRODUCT_LIST_REMOVE_ITEM = 'product_list_remove_item';
export const PRODUCT_LIST_ITEM_SELECTED = 'product_list_item_slected';
export const PRODUCT_LIST_CART_CLICKED = 'product_list_cart_selected';

export const PRODUCT_DETAIL_ADD_ITEM = 'product_detail_add_item';
export const PRODUCT_DETAIL_REMOVE_ITEM = 'product_detail_remove_item';
export const PRODUCT_DETAIL_ADD_CLICKED = 'product_detail_add_clicked';


export const PRODUCT_CART_CHECKOUT_CLICKED = 'product_cart_checkout_clicked';
export const PRODUCT_CART_BACK_CLICKED = 'product_cart_back_clicked';
export const PRODUCT_CART_ADD_ITEM = 'product_cart_add_item';
export const PRODUCT_CART_REMOVE_ITEM = 'product_cart_remove_item';


export const PRODUCT_CHECKOUT_CONFIRM_CLICKED = 'product_checkout_confirm_clicked';
export const PRODUCT_CHECKOUT_CONFIRM_SUCCESSFUL = 'product_checkout_confirm_successful';
export const PRODUCT_CHECKOUT_ADD_ADDRESS_CLICKED = 'product_checkout_add_address_clicked';
export const PRODUCT_CHECKOUT_ADDRESS_SELECTED = 'product_checkout_address_selected'; 


export const PRODUCT_FETCHING = 'product_fetching';
export const PRODUCT_FETECHED_SUCCESS = 'product_fetched_success';
export const PRODUCT_FETECHED_FAILED = 'product_fetched_failed';


export const ASSIGN_ORDER = 'assign_order';
export const ASSIGNING_ORDER = 'assigning_order';
export const ASSIGNED_ORDER = 'assigned_order';
export const ASSIGNING_ORDER_FAILED = 'assigning_order_failed';
export const CANCEL_ORDER = 'cancel_order';
export const COMPLETE_ORDER = 'complete_order';
export const UPDATE_ORDER_COUNT = 'update_order_count';
export const UPDATE_PAYMENT = 'update_payment';
export const CONFIRM_PAYMENT = 'confirm_payment';
export const RELATED_ORDERS = 'related_orders';
export const CHANGE_ORDER_STATUS = 'change_order_Status';
export const CHANGE_ORDER_STATUS_FALIED = 'change_order_Status_failed';
export const CHANGE_ORDER_STATUS_SUCCESS = 'change_order_Status_success';


export const HANDY_SERVICES_ASSIGN = 'handy_services_assign';
export const HANDY_SERVICES_ASSIGN_SUCCESS = 'handy_services_assign_success';
export const HANDY_SERVICES_ASSIGN_FALIED = 'handy_services_assign_failed';

export const HANDY_SERVICES_STATUS_CHNAGES = 'handy_services_status_changes';
export const HANDY_SERVICES_STATUS_CHNAGES_FAILED = 'handy_services_status_changes_failed';
export const HANDY_SERVICES_STATUS_CHNAGES_SUCCESS = 'handy_services_status_changes_success';
export const HANDY_SERVICES_UPDATE_ANY_DATA = 'handy_services_status_update_any_date';

