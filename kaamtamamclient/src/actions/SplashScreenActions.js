import { PRODUCT_FETCHING, PRODUCT_FETECHED_FAILED, PRODUCT_FETECHED_SUCCESS, LANDING_DATAFECTHED } from './types';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import { Alert } from 'react-native';
import { config } from '../config';

export const fetchingProducts = () => {
    //debugger;
    //  return (dispatch) => {
    //  Actions.landingPage();
    //  //dispatch({ type: LOGIN_USER });
    // }
    let configs = {
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        }
    };
    return (dispatch) => {
        dispatch({ type: PRODUCT_FETCHING });
        const { REST_API } = config;

        axios.get(REST_API.Test.GetSimple, configs)
            .then(response => {
               // debugger;
                const baseModel = response.data;
                //if (baseModel.success) {
                if (response.status == 200) {
                    productFetchingSucess(dispatch, response.data);
                 }// else {
                //     productFetchingFail(dispatch, 'something went wrong')
                // }

                //Actions.tendersResult({TendersList:baseModel.data});
            }).catch(error => {
                //debugger;
                productFetchingFail(dispatch, error);

                console.log("error", error)
            });



    };
};

const productFetchingSucess = (dispatch, productData) => {
    //debugger;
    dispatch({
        type: PRODUCT_FETECHED_SUCCESS,
        payload: productData
    });
    dispatch({
        type: LANDING_DATAFECTHED,
        payload: productData
    });
    //Actions.splash2();
    Actions.login();
};

const productFetchingFail = (dispatch, error) => {
   // debugger;
    dispatch({
        type: PRODUCT_FETECHED_FAILED
    });
    // Actions.employeeList();

    Alert.alert(
        'Alert',
        error,
        [
            { text: 'OK', onPress: () => console.log("error", error) },
        ],
        { cancelable: false }
    )
};