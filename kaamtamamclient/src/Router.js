import React, { Component } from 'react';
import { Scene, Router, Stack, Actions, Drawer } from 'react-native-router-flux';


import LandingPage from './components/Dashboard/LandingPage';
import Dashboard from './components/Dashboard/Dashboard';

import ProductList from './components/Product/ProductList';
import ProductDetail from './components/Product/ProductDetail';
import Cart from './components/Product/Cart';
import Checkout from './components/Product/Checkout';
import SplashScreen from './components/SplashScreens/SplashScreen';
import SplashScreen2 from './components/SplashScreens/SplashScreen2';
import SignUp from './components/Account/SignUp'
import Login from './components/Account/Login'
import AddAddress from './components/Account/AddAddress'
import MyAccount from './components/Account/MyAccount'
import Sidebar from './components/Common/sidebar'
import OrderHistory from './components/Order/OrderHistory'
import OrderDetail from './components/Order/OrderDetail'
import PendingOrder from './components/Order/PendingOrder'
import ServiceDetails from './components/Order/ServiceDetails'

import Notification from './components/Notification/Notification';
import NotificationDetail from './components/Notification/NotificationDetail';
import TraceOrder from './components/Order/TraceOrder';
//import TraceOrder from './components/Order/TraceOrder';



const RouterComponent = () => {
  return (
    <Router sceneStyle={{ paddingTop: 0, backgroundColor: '#fff' }}>
      <Stack key="root">

        <Scene key="login" component={Login} title="Login Here" hideNavBar />
        <Scene key="Splash" component={SplashScreen} initial title="" hideNavBar />
        <Scene key="splash2" component={SplashScreen2} title="" hideNavBar />
        <Drawer
          key='userProfile'
          drawerPosition='right'
          contentComponent={Sidebar}
          hideNavBar
        >
          <Scene key="auth">
            <Scene key="dashboard" component={Dashboard} title="Dashboard" hideNavBar  swipeEnabled={false} panHandlers={null}/>
            <Scene key="addAddress" component={AddAddress} title="Dashboard" hideNavBar />
            <Scene key="notification" component={Notification} title="Dashboard" hideNavBar />
            <Scene key="notificationDetail" component={NotificationDetail} title="Dashboard" hideNavBar />
            <Scene key="serviceDetails" component={ServiceDetails} title="SelectServices"  hideNavBar />   

            <Scene key="traceOrder" component={TraceOrder} title="Dashboard" hideNavBar />
            <Scene key="orderHistory" component={OrderHistory} title="Dashboard" hideNavBar />
            <Scene key="orderDetail" component={OrderDetail} title="Dashboard" hideNavBar />
            <Scene key="pendingOrder" component={PendingOrder} title="Dashboard" hideNavBar />
            <Scene key="MyAccount" component={MyAccount} title="Dashboard" hideNavBar />  
          </Scene>





        </Drawer>


      </Stack>
    </Router>
  );
};

export default RouterComponent;
