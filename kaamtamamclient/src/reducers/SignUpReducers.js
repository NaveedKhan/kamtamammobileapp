import {
    SIGNUP_EMAIL_CHANGED,
     SIGNUP_FAILED, SIGNUP_FULLNAME_CHANGED, 
     SIGNUP_PASSWORD_CHANGED, SIGNUP_PHONE_CHANGED, SIGNUP_START,
    SIGNUP_SUCCESSFUL, SIGNUP_USERNAME_CHANGED
} from '../actions/types';
const INITIAL_STATE = {
    email: '', password: '', fullname: '', phone: '',
    error: '',username:'', loading: false,ProductsList:''
};

export default (state = INITIAL_STATE, action) => {
    //debugger;
    switch (action.type) {
        case SIGNUP_EMAIL_CHANGED:
            return { ...state, email: action.payload };
        case SIGNUP_FULLNAME_CHANGED:
            return { ...state, fullname: action.payload };
        case SIGNUP_FAILED:
            return { ...state, error: action.payload,loading: false  };
        case SIGNUP_PASSWORD_CHANGED:
            return { ...state, password: action.payload };
        case SIGNUP_USERNAME_CHANGED:
            return { ...state, username: action.payload };
        case SIGNUP_SUCCESSFUL:
            return { ...state, ProductsList: action.payload,loading: false };
        case SIGNUP_PHONE_CHANGED:
            //debugger;
            return { ...state, phone: action.payload };
        case SIGNUP_START:
            //debugger;
            return { ...state, loading: true };    
        default:
            return state;
    }
}

//return {( ...state, ...INITIAL_STATE,)}
