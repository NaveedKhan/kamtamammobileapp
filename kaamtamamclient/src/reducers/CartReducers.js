import { PRODUCT_CART_ADD_ITEM, PRODUCT_CART_CHECKOUT_CLICKED, PRODUCT_CART_REMOVE_ITEM } from '../actions/types';
import { Actions } from 'react-native-router-flux';

const INITIAL_STATE = { user: '', isloading: false, selectedCategory: '', selectedProduct: '', totalItems: 0, orderInCartList: [] };

export default (state = INITIAL_STATE, action) => {
    //debugger;
    switch (action.type) {
        case PRODUCT_CART_ADD_ITEM:
            return { ...state, totalItems: state.totalItems + 1, orderInCartList: action.payload }
        case PRODUCT_CART_REMOVE_ITEM:
            return { ...state, isloading: false, totalItems: state.totalItems - 1, orderInCartList: action.payload }
        case PRODUCT_CART_CHECKOUT_CLICKED:
            return INITIAL_STATE;
        default:
            return state;
    }
};