import {
    ASSIGN_ORDER, CANCEL_ORDER, COMPLETE_ORDER, UPDATE_ORDER_COUNT,
    RELATED_ORDERS, UPDATE_PAYMENT, CONFIRM_PAYMENT, ASSIGNED_ORDER, ASSIGNING_ORDER, ASSIGNING_ORDER_FAILED, CHANGE_ORDER_STATUS, CHANGE_ORDER_STATUS_FALIED, CHANGE_ORDER_STATUS_SUCCESS
} from '../actions/types';
import { Actions } from 'react-native-router-flux';

const INITIAL_STATE = { user: '', isloading: false, pendingOrderLatest: {}, relatedOrders: [], totalOrdersCountData: {}, error: '' };

export default (state = INITIAL_STATE, action) => {
    //debugger;
    switch (action.type) {
        case ASSIGNED_ORDER:
            return { ...state, isloading: false, pendingOrderLatest: action.payload }
        case ASSIGNING_ORDER:
        case CHANGE_ORDER_STATUS:
            return { ...state, isloading: true }
        case ASSIGNING_ORDER_FAILED:
            return { ...state, isloading: false, error: action.payload }
        case CANCEL_ORDER:
            return { ...state, isloading: false, pendingOrderLatest: {} }
        case COMPLETE_ORDER:
            return { ...state, isloading: false, pendingOrderLatest: {} }
        case UPDATE_ORDER_COUNT:
            return { ...state, totalOrdersCountData: action.payload }
        case UPDATE_PAYMENT:
            return { ...state, totalItems: state.totalItems + 1, orderInCartList: action.payload }
        case CONFIRM_PAYMENT:
            return { ...state, totalItems: state.totalItems + 1, orderInCartList: action.payload }
        case RELATED_ORDERS:
            return { ...state, isloading: false, relatedOrders: action.payload }
        case CHANGE_ORDER_STATUS_SUCCESS:
        case CHANGE_ORDER_STATUS_FALIED:
            return { ...state, isloading: false }
        default:
            return state;
    }
};