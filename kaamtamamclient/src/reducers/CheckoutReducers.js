import { PRODUCT_CHECKOUT_ADDRESS_SELECTED,PRODUCT_CHECKOUT_CONFIRM_CLICKED,
    PRODUCT_CHECKOUT_ADD_ADDRESS_CLICKED,PRODUCT_CHECKOUT_CONFIRM_SUCCESSFUL } from '../actions/types';
import { Actions } from 'react-native-router-flux';

const INITIAL_STATE ={user: '', isloading:false, selectedCategory:'',selectedProduct:'',totalItems:0,orderInCartList:[],orderDetail:{}};

export default (state = INITIAL_STATE, action) => {
//debugger;
    switch(action.type)
    {
        case PRODUCT_CHECKOUT_ADDRESS_SELECTED:
            return {...state , [action.payload.prop] : action.payload.value }
         case PRODUCT_CHECKOUT_CONFIRM_CLICKED:
            return {...state , isloading : true }
            case PRODUCT_CHECKOUT_CONFIRM_SUCCESSFUL:
            return {...state , isloading : false,orderDetail:action.payload }
        case PRODUCT_CHECKOUT_ADD_ADDRESS_CLICKED:
            return INITIAL_STATE;
            default:
            return state;
    }
};