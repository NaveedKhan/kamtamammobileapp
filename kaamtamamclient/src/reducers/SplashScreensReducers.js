import { PRODUCT_FETCHING, PRODUCT_FETECHED_FAILED, PRODUCT_FETECHED_SUCCESS } from '../actions/types';

const INITIAL_STATE ={ Products: [], isloading:false, ProductsCategory:'',error:''};

export default ( state = INITIAL_STATE, action ) =>
{
   // debugger;
    switch(action.type)
    {
        case PRODUCT_FETCHING:
            return {...state ,isloading:true}
        case PRODUCT_FETECHED_FAILED:
                return {...state ,error:'Data fetching failed',isloading:false }
        case PRODUCT_FETECHED_SUCCESS:
                    return {...state ,Products:action.payload,isloading:false,error:'' }  
        default:
            return state;
    }
}