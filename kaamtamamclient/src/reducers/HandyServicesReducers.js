import {
    HANDY_SERVICES_ASSIGN, HANDY_SERVICES_ASSIGN_FALIED, HANDY_SERVICES_ASSIGN_SUCCESS, HANDY_SERVICES_STATUS_CHNAGES, HANDY_SERVICES_STATUS_CHNAGES_FAILED,
    HANDY_SERVICES_STATUS_CHNAGES_SUCCESS, HANDY_SERVICES_UPDATE_ANY_DATA
} from '../actions/types';

const INITIAL_STATE = { serviceRequestModel: {}, error: '', loading: false };


export default (state = INITIAL_STATE, action) => {
    //debugger;
    switch (action.type) {
        case HANDY_SERVICES_UPDATE_ANY_DATA:
            return { ...state, [action.payload.prop]: action.payload.value }

        case HANDY_SERVICES_ASSIGN:
        case HANDY_SERVICES_STATUS_CHNAGES:
            return { ...state, loading: true };

        case HANDY_SERVICES_ASSIGN_SUCCESS:
        case HANDY_SERVICES_STATUS_CHNAGES_SUCCESS:
            return { ...state, loading: false };

        case HANDY_SERVICES_ASSIGN_FALIED:
        case HANDY_SERVICES_STATUS_CHNAGES_FAILED:
            return { ...state, loading: false };

        default:
            return state;
    }
}