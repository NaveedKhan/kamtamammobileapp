import { combineReducers }  from 'redux';
import AuthReducer from './AuthReducer';
import EmployeeReducer from './EmployeeReducres';
import LandingPageReducer from './LandingPageReducers';
import ProductListReducers from './ProductListReducers';
import ProductDetailReducers from './ProductDetailReducers';
import CartReducers from './CartReducers';
import CheckoutReducers from './CheckoutReducers';
import SplashScreensReducers from './SplashScreensReducers';
import SignUpReducers from './SignUpReducers';
import OrderReducers from './OrderReducers';
import NotificationsReducer from './NotificationsReducer';
import AddressReducers from './AddressReducers';
import HandyServicesReducers from './HandyServicesReducers';
export default combineReducers({
  auth:  AuthReducer,
  employee: EmployeeReducer,
  landingPageReducer:LandingPageReducer,
  productList:ProductListReducers,
  productDetail:ProductDetailReducers,
  cart:CartReducers,
  checkout:CheckoutReducers,
  splashScreen:SplashScreensReducers,
  signUp:SignUpReducers,
  orders:OrderReducers,
  notificationsReducer:NotificationsReducer,
  userAddress:AddressReducers,
  handyServices:HandyServicesReducers
});
