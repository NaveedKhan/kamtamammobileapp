import { PRODUCT_LIST_ITEM_SELECTED,PRODUCT_LIST_ADD_ITEM,PRODUCT_LIST_CART_CLICKED,
    PRODUCT_LIST_REMOVE_ITEM } from '../actions/types';
import { Actions } from 'react-native-router-flux';

const INITIAL_STATE ={ user: '', isloading:false,selectedCategoryName:'', selectedCategory:'',selectedProduct:'',totalItems:0 , orderInCartList:[] };

export default ( state = INITIAL_STATE, action ) =>
{
   // debugger;
    switch(action.type)
    {
        case PRODUCT_LIST_ITEM_SELECTED:
            return {...state ,selectedCategory : action.payload.catid ,selectedCategoryName:action.payload.catname}
        case PRODUCT_LIST_ADD_ITEM:
                return {...state ,selectedCategory : 'selected wala',totalItems:state.totalItems+1,orderInCartList:action.payload }
        case PRODUCT_LIST_CART_CLICKED:
                    return {...state ,selectedCategory : 'selected wala' }
        case PRODUCT_LIST_REMOVE_ITEM:
            return {...state ,user : 'naveed',isloading:false,totalItems:state.totalItems-1,orderInCartList:action.payload}    
        default:
            return state;
    }
}

