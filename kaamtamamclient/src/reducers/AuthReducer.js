import { EMAIL_CHANGED, PASSWORD_CHANGED, LOGIN_USER_SUCCESSFUL, LOGIN_USER, LOGIN_USER_FAIL,USER_DETAIL_UPDATE } from '../actions/types';
const INITIAL_STATE ={ email: '', password: '', user: null, error: '', loading: false, UserRole:'',
 IsGuestUser:false,DeviceTocken:'',DeviceID:'' };

export default (state = INITIAL_STATE, action) => {
  //debugger;
switch (action.type) {
  case USER_DETAIL_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value }
  case EMAIL_CHANGED:
  return { ...state, email: action.payload };
  case PASSWORD_CHANGED:
  return { ...state, password: action.payload };
  case LOGIN_USER_SUCCESSFUL:
  //debugger;
  return { ...state, user: action.payload,loading: false,error: '' };
  case LOGIN_USER_FAIL:
  //debugger;
  return { ...state, error: action.payload, password: '', loading: false };
  case LOGIN_USER:
  return { ...state, loading: true, error: '' };
  default:
  return state;
    }
}

//return {( ...state, ...INITIAL_STATE,)}
