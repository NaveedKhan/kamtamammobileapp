import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import Reducer from './src/reducers';
import ReduxThunk from 'redux-thunk';
import Router from './src/Router';
import firebase from 'firebase';

class App extends Component {
  componentDidMount() {
    // Initialize Firebase
    debugger;
    configureFirebase();
  }
  render() {
  const storeInit= createStore(Reducer,{},applyMiddleware(ReduxThunk));
    return (
      <Provider store={storeInit}>
        <View style={{ flex: 1, backgroundColor: 'rgb(61,91,151)' }}>
            <Router />
       </View>
      </Provider>
    );
  }
}

export default App;

function configureFirebase() {

  var firebaseConfig = {
    apiKey: "AIzaSyBNDN-uUcHxm5SxqcfcISulGEWgdfOnsP0",
    authDomain: "kaamtamam-c8346.firebaseapp.com",
    databaseURL: "https://kaamtamam-c8346.firebaseio.com",
    projectId: "kaamtamam-c8346",
    storageBucket: "kaamtamam-c8346.appspot.com",
    messagingSenderId: "714740281376",
    appId: "1:714740281376:web:0558552972f7e431ba4c45",
    measurementId: "G-47MCWP8EJ1"
  };
  // Initialize Firebase
  !firebase.apps.length ? firebase.initializeApp(firebaseConfig): firebase.app();;


  // var config = {
  //   apiKey: "AIzaSyDPC8r4cpW5nzM83WVXzhFX54LggI60hZs",
  //   authDomain: "homeservices-a9c03.firebaseapp.com",
  //   databaseURL: "https://homeservices-a9c03.firebaseio.com",
  //   projectId: "homeservices-a9c03",
  //   storageBucket: "homeservices-a9c03.appspot.com",
  //   messagingSenderId: "850301661410"
  // };

  // !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();
  //firebase.initializeApp(config);
}



